﻿using System.Collections.Generic;
using PushSharp.Core;
using TravelRecon.Api.Model;
using TravelRecon.Api.ServiceIntarfaces;


namespace TravelRecon.Api.Push.Service
{
    public abstract class BasePushService<T>: IBasePushService where T : INotification
    {
        protected TravelRecon.Logger.ILogger Logger;


        private bool _disposed = false;
        protected ServiceBroker<T> Broker;

        public BasePushService(TravelRecon.Logger.ILogger logger)
        {
            Logger = logger;
            
            Broker = GetBroker();

            Broker.OnNotificationFailed += (notification, exception) =>
            {
                Logger.Error(exception);
            };

            Broker.OnNotificationSucceeded += (notification) =>
            {
                //log it
            };

            Broker.Start();
        }

        public void Dispose()
        {
            Broker?.Stop();
        }

        protected abstract ServiceBroker<T> GetBroker(); 
        protected abstract T GetNotification(PushNotification notification);

        public void Push(IEnumerable<PushNotification> notifications)
        {
            foreach (var n in notifications)
                Push(n);
        }

        public void Push(PushNotification notification)
        {
            var notif = GetNotification(notification);
            Broker.QueueNotification(notif);
        }
    }
}
