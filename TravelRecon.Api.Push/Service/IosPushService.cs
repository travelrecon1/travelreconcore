﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using PushSharp.Apple;
using PushSharp.Core;
using TravelRecon.Api.Model;
using TravelRecon.Api.ServiceIntarfaces;

namespace TravelRecon.Api.Push.Service
{
    public class IosPushService : BasePushService<ApnsNotification>, IIosPushService
    {
        public IosPushService(TravelRecon.Logger.ILogger logger) : base(logger)
        {
        }

        protected override ServiceBroker<ApnsNotification> GetBroker()
        {
            ApnsConfiguration config;

            if (ConfigSettings.iOSUseProductionCertificate)
            {
                config = new ApnsConfiguration(ApnsConfiguration.ApnsServerEnvironment.Production, "Resources\\PushProductionCertificate.p12", ConfigSettings.iOSProdCertificatePassword);
            }
            else
            {
                config = new ApnsConfiguration(ApnsConfiguration.ApnsServerEnvironment.Sandbox, "Resources\\PushDevelopmentCertificate.p12", ConfigSettings.iOSDevCertificatePassword);
            }

            var broker = new ApnsServiceBroker(config);
            return broker;
        }

        protected override ApnsNotification GetNotification(PushNotification notification)
        {
            return new ApnsNotification
            {
                DeviceToken = notification.DeviceId,
                Payload = JObject.FromObject(new 
                {
                    Aps = notification.Data
                }, new JsonSerializer
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                })
            };
        }
    }
}
