﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using PushSharp.Core;
using PushSharp.Google;
using TravelRecon.Api.Model;
using TravelRecon.Api.ServiceIntarfaces;

namespace TravelRecon.Api.Push.Service
{
    public class GooglePushService : BasePushService<GcmNotification>, IAndroidPushService
    {
        public GooglePushService(TravelRecon.Logger.ILogger logger) : base(logger)
        {
        }

        protected override ServiceBroker<GcmNotification> GetBroker()
        {
            var config = new GcmConfiguration(ConfigSettings.GcmApiKey); 
            var broker = new GcmServiceBroker(config);
            return broker;
        }

        protected override GcmNotification GetNotification(PushNotification notification)
        {
            var jsonSerializer = new JsonSerializer { ContractResolver = new CamelCasePropertyNamesContractResolver() };

            return new GcmNotification
            {
                To = notification.DeviceId,
                ContentAvailable = true,
                Data = JObject.FromObject(notification.Data, jsonSerializer),
                Notification = JObject.FromObject(new
                {
                    Title = notification.Data.Title
                }, jsonSerializer),
            };
        }
    }
}
