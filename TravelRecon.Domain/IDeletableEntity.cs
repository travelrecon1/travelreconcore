﻿namespace TravelRecon.Domain
{
    public interface IDeletableEntity: IEntity
    {
        bool IsDeleted { get; set; }
    }
}
