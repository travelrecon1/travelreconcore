﻿using System.Data.Entity.Spatial;

namespace TravelRecon.Domain.Map
{
    public interface IEntityWithLocation
    {
        int Id { get; set; }
        DbGeography Location { get; set; }
    }
}
