﻿namespace TravelRecon.Domain.Map
{
    public class Location
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string AddressFormatted { get; set; }
    }
}
