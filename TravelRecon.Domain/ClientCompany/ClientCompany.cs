﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TravelRecon.Domain.Identity;
using TravelRecon.Domain.Locale;

namespace TravelRecon.Domain.ClientCompany
{
    public class ClientCompany: IEntity
    {
        public int Id { get; set; }
        [Required]             
        [StringLength(255)]
        public string Name { get; set; }
        [Required]
        [StringLength(255)]
        public string LegalName { get; set; }
        [StringLength(255)]
        [Required]
        public string Abbreviation { get; set; }
        [ForeignKey("User")]
        public int UserId { get; set; }
        [Required]
        [StringLength(255)]
        public string ClientFirstName { get; set; }
        [Required]
        [StringLength(255)]
        public string ClientLastName { get; set; }
        [Required]
        [StringLength(255)]
        public string ClientTitle { get; set; }
        [Required]
        [StringLength(255)]
        public string ClientDepartment { get; set; }

        [StringLength(255)]
        public string ClientAddress { get; set; }

        [StringLength(255)]
        public string ClientCity { get; set; }

        [StringLength(255)]
        public string ClientState { get; set; }

        [StringLength(255)]
        public string ClientCounry { get; set; }




        public int? ClientPhysicalAddressId { get; set; }

        [ForeignKey("ClientPhysicalAddressId")]
        public Address ClientPhysicalAddress { get; set; }


        [StringLength(255)]
        public string PhoneNumber { get; set; }
        [StringLength(255)]
        public string MobileNumber { get; set; }
        [Required]
        [StringLength(255)]
        public string Email { get; set; }
        [StringLength(255)]
        public string EmergencyInformation { get; set; }
        [StringLength(255)]
        public string Notes { get; set; }
        public User User { get; set; }

        [ForeignKey("AffiliatedClient")]
        public int? AffiliatedClientId { get; set; }
        public ClientCompany AffiliatedClient { get; set; }
    }
}
