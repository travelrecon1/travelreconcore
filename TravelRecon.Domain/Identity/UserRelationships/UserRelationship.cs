using System;
using System.ComponentModel.DataAnnotations.Schema;
using TravelRecon.Domain.Localization;

namespace TravelRecon.Domain.Identity.UserRelationships
{
    public class UserRelationship : IEntity
    {
        public int Id { get; set; }
        public int UserRequesterFk { get; set; }
        public int UserInvitedFk { get; set; }
        public int UserRelationshipStatusFk { get; set; }
        public bool IsIgnored { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? AcceptedDate { get; set; }
        public DateTime? RejectedDate { get; set; }
        public DateTime? IgnoredDate { get; set; }

        [ForeignKey("UserInvitedFk")]
        public User UserInvited { get; set; }

        [ForeignKey("UserRequesterFk")]
        public User UserRequester { get; set; }

        public void AcceptInvitation()
        {
            this.UserRelationshipStatusFk = (int)UserRelationshipStatusEnum.Accepted;
            this.AcceptedDate = DateTime.Now;
            this.IsIgnored = false;
        }

        public void Reject()
        {
            this.UserRelationshipStatusFk = (int) UserRelationshipStatusEnum.Rejected;
            this.RejectedDate = DateTime.Now;
        }

        

        public void Ignore()
        {
            this.IsIgnored = true;
            this.IgnoredDate = DateTime.Now;
        }

        
    }
}