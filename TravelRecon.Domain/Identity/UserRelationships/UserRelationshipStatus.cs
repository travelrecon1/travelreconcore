﻿using System.Collections.Generic;
using TravelRecon.Domain.Localization;

namespace TravelRecon.Domain.Identity.UserRelationships
{
    public class UserRelationshipStatus : IEntity, IHasTranslations<UserRelationshipStatusTranslation>
    {
        public UserRelationshipStatus()
        {
            Translations = new List<UserRelationshipStatusTranslation>();
        }

        public int Id { get; set; }
        public virtual IList<UserRelationshipStatusTranslation> Translations { get; protected set; }
    }
    
}