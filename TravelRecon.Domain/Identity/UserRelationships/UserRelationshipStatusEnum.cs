namespace TravelRecon.Domain.Identity.UserRelationships
{
    public enum UserRelationshipStatusEnum
    {
        None = 0,
        Pending = 1,
        Accepted = 2,
        Rejected = 3
    }
}