﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;
using TravelRecon.Domain.Alerts;
using TravelRecon.Domain.IntelReports;

namespace TravelRecon.Domain.Identity
{
    public class UserLocationHistory : IEntity
    {
        public int Id { get; set; }

        [ForeignKey("User")]
        public int UserId { get; set; }
        public DbGeography Location { get; set; }
        [ForeignKey("Destination")]
        public int? DestinationId { get; set; }
        [ForeignKey("Alert")]
        public int? AlertId { get; set; }
        public DateTime CreatedOn { get; set; }
        [StringLength(255)]
        [Required]
        public string Address { get; set; }
        [StringLength(255)]
        [Required]
        public string City { get; set; }
        [StringLength(255)]
        [Required]
        public string Country { get; set; }

        public Alert Alert { get; set; }
        public User User { get; set; }
        public Destination Destination { get; set; }
    }
}