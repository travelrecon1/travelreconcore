﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TravelRecon.Domain.Identity
{
    /// <summary>
    /// Maps to SubscriptionLevelTypes table
    /// </summary>

    public class UserRoleType :IEntity
    {
        public int Id { get; set; }
        [StringLength(255)]
        [Required]
        public string Name { get; set; }
    }


    public enum UserRoleTypeEnum
    {
        Guest = 0,
        GoRecon = 1,
        TravelRecon = 2,
        TravelReconPro = 3,
        Administrator = 4,
        PrimaryAdmin = 5,
        IntelDirector = 6,
        EditorLead = 7,
        Analyst = 8,
        TravelReconManager = 9,
        TravelReconProManager = 10,
        GISEditor = 11,
        TRManager = 12,
        TRPClient = 13,
        TravelReconProAdministrator = 14
    }
}
