﻿using System.ComponentModel.DataAnnotations.Schema;

namespace TravelRecon.Domain.Identity
{
    public class UserRoles :IEntity
    {
        public int Id { get; set; }

        public int RoleId { get; set; }
        public int UserId { get; set; }

        [ForeignKey("RoleId")]
        public virtual UserRoleType UserRole { get; set; }
    }
}
