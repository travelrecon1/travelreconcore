﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TravelRecon.Domain.IntelReports;

namespace TravelRecon.Domain.Identity
{
    public class AnalystsDestination:IEntity
    {
        public int Id { get; set; }
        [ForeignKey("Analyst")]
        public int AnalystId { get; set; }
        [ForeignKey("Destination")]
        public int DestinationId { get; set; }
        public User Analyst { get; set; }
        public Destination Destination { get; set; }
    }
}
