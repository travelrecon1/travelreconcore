﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TravelRecon.Domain.Identity
{
    public class UserDetails : IEntity
    {
        public int Id { get; set; }
        [ForeignKey("User")]
        public int UserId { get; set; }
        public User User { get; set; }
        [StringLength(255)]
        public string Dependent { get; set; }
        [StringLength(255)]
        public string Employee { get; set; }
        [StringLength(255)]
        public string EmployeeNumber { get; set; }
        [StringLength(255)]
        public string PhoneNumber { get; set; }
        [StringLength(255)]
        public string EmergencyContactInfo { get; set; }
        [StringLength(255)]
        public string Passport { get; set; }
        public string GovtPassport  { get; set; }
        public string PersonalPassport { get; set; }
        public string CountryOfPassport { get; set; }
        public DateTime? PassportExpiration  { get; set; }
        public string PassportNumber  { get; set; }
        public BloodTypeEnum Blood { get; set; }
        [StringLength(255)]
        public string Allergies { get; set; }
        [StringLength(255)]
        public string Disabilities { get; set; }
    }

    public enum BloodTypeEnum
    {
        Unknown = 0,
        APlus = 1,
        AB = 2,
        BMinus = 3,
        O = 4
    }
}