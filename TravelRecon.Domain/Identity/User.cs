﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;
using System.Linq;
using Microsoft.AspNet.Identity;
using TravelRecon.Domain.Alerts;
using TravelRecon.Domain.Localization;
using TravelRecon.Domain.Groups;
using TravelRecon.Domain.IntelReports;
using TravelRecon.Domain.License;
using TravelRecon.Domain.Map;
using TravelRecon.Domain.Points;
using TravelRecon.Domain.Statuses;

namespace TravelRecon.Domain.Identity
{
    public class User : IDeletableEntity, IUser<int>, IUser<string>, IEntityWithLocation
    {
        
        public int Id { get; set; }

        /// <summary>
        /// Simply pass the Id back as a string to satisfy the IUser<string> interface
        /// </summary>
        string IUser<string>.Id => Id.ToString();

        [StringLength(255)]
        [Required]
        public string UserName { get; set; }

        [StringLength(255)]
        public string PasswordHash { get; set; }

        //TODO remove nullable when Drupal users deleted
        public DateTime? CreatedDate { get; set; }
        
        public bool EmailConfirmed { get; set; }

        // Pass through to CurrentLocation to implement IEntityWithLocation
        [NotMapped]
        public DbGeography Location {
            get { return CurrentLocation; }
            set { CurrentLocation = value; }
        }

        public bool Active { get; set; }
        
        public ICollection<UserRoles> UserRoles { get; set; }
		public double? RadiusInMeters { get; set; }
        public LanguageType? Language { get; set; }
        public DbGeography CurrentLocation { get; set; }
        [StringLength(800)]
        public string CurrentAddressFormatted { get; set; }
        public DateTime? CurrentLocationDateTime { get; set; }
        public DbGeography CurrentMapLocation { get; set; }
        [StringLength(800)]
        public string CurrentMapAddressFormatted { get; set; }
        [Required]
        [StringLength(255)]
        public string FirstName { get; set; }
        [Required]
        [StringLength(255)]
        public string LastName { get; set; }
        [StringLength(255)]
        [Required]
        public string Email { get; set; }
        [StringLength(255)]
        public string Initials { get; set; }
        [StringLength(250)]
        public string ElectronicSignature { get; set; }
        public DateTime? AgreementDate { get; set; }
        public int LocationUpdateFrequecySeconds { get; set; }
        public bool ObfuscateLocation { get; set; }
        public bool ShowAlertPopups { get; set; }
        public int AlertPopupsShowIntervalMinutes { get; set; }
        public int MaxAlertPopups { get; set; }
        [StringLength(50)]
        public string IpAddr { get; set; }
        [StringLength(254)]
        public string EmergencyEmail { get; set; }
        [StringLength(20)]
        public string EmergencyPhone { get; set; }

        public int? CurrentMapLocationDrupalDestinationId { get; set; }
        public int MapZoom { get; set; }

        public string Devices { get; set; }

	    public bool PushNotificationsModalViewed { get; set; }

        public LanguageType GetLanguageSetting() {
            return Language ?? LanguageType.English;
        }

        public User()
        {
            UserRoles = new List<UserRoles>();
        }


        // constructor creates user with default values for registration
        public User(string username)
        {
            UserName = username;
            Email = string.Empty;
            CreatedDate = DateTime.UtcNow;
            FirstName = string.Empty;
            LastName = string.Empty;
            LocationUpdateFrequecySeconds = 30;
            ObfuscateLocation = false;
            ShowAlertPopups = true;
            AlertPopupsShowIntervalMinutes = 5;
            MaxAlertPopups = 10;
            MapZoom = 15;
            UserRoles = new List<UserRoles>();
        }

        /// <returns>Defaults to 1 KM if no setting found in the DB</returns>
        public double GetRadiusInMetersSetting() {
            return RadiusInMeters.HasValue ? (RadiusInMeters.Value > 0 ? RadiusInMeters.Value : 1000) : 1000;
        }

        /// <summary>
        /// Used to update an existing user during the authentication process
        /// </summary>
        /// <param name="user"></param>
        /// <param name="existingRecord"></param>
        public static void MapFrom(User user, User existingRecord)
        {
            existingRecord.UserName = user.UserName;
            existingRecord.FirstName = user.FirstName;
            existingRecord.LastName = user.LastName;
            existingRecord.Email = user.Email;

        }

        public bool IsAdmin()
        {
            return HasRole(UserRoleTypeEnum.Administrator) || HasRole(UserRoleTypeEnum.PrimaryAdmin);
        }


        public bool IsOnline()
        {
            if (CurrentLocation == null)
            {
                return false;
            }

            var hasLocation = CurrentLocation.Latitude.HasValue && CurrentLocation.Longitude.HasValue;
            var isRecentlyOnline = CurrentLocationDateTime.HasValue && ((DateTime.UtcNow - CurrentLocationDateTime.Value).TotalSeconds <= LocationUpdateFrequecySeconds);
            return isRecentlyOnline && hasLocation;
        }

        public bool HasRole(UserRoleTypeEnum role)
        {
            return UserRoles.Any(x => x.UserRole.Id == (int)role);
        }

        public bool HasMaxReconRole(UserRoleTypeEnum role)
        {
            if (role != UserRoleTypeEnum.GoRecon && role != UserRoleTypeEnum.TravelRecon && role != UserRoleTypeEnum.TravelReconPro)
                throw new ArgumentException("This method is used only to check for TravelRecon user roles");

            switch (role)
            {
                case UserRoleTypeEnum.TravelReconPro:
                    return UserRoles.Any(x => x.UserRole.Id == (int)role);
                case UserRoleTypeEnum.TravelRecon:
                    return UserRoles.Any(x => x.UserRole.Id == (int)role) 
                        && UserRoles.All(x => x.UserRole.Id != (int)UserRoleTypeEnum.TravelReconPro);
                case UserRoleTypeEnum.GoRecon:
                    return UserRoles.Any(x => x.UserRole.Id == (int)role) &&
                           UserRoles.All(x => x.UserRole.Id != (int)UserRoleTypeEnum.TravelReconPro 
                                && x.UserRole.Id != (int)UserRoleTypeEnum.TravelRecon);
            }

            return false;
        }

        public bool HasMinReconRole(UserRoleTypeEnum role)
        {
            if (role != UserRoleTypeEnum.GoRecon && role != UserRoleTypeEnum.TravelRecon && role != UserRoleTypeEnum.TravelReconPro)
                throw new ArgumentException("This method is used only to check for TravelRecon user roles");

            switch (role)
            {
                case UserRoleTypeEnum.TravelReconPro:
                    return UserRoles.Any(x => x.UserRole.Id == (int)role);
                case UserRoleTypeEnum.TravelRecon:
                    return UserRoles.Any(x => x.UserRole.Id == (int)role 
                        || x.UserRole.Id == (int)UserRoleTypeEnum.TravelReconPro);
                case UserRoleTypeEnum.GoRecon:
                    return UserRoles.Any(x => x.UserRole.Id == (int)role 
                            || x.UserRole.Id == (int)UserRoleTypeEnum.TravelRecon 
                            || x.UserRole.Id == (int)UserRoleTypeEnum.TravelReconPro);
            }

            return false;
        }

        public UserRoleTypeEnum GetMaxReconRole()
        {
            if (UserRoles.Any(x => x.UserRole.Id == (int)UserRoleTypeEnum.TravelReconPro))
                return UserRoleTypeEnum.TravelReconPro;

            if (UserRoles.Any(x => x.UserRole.Id == (int)UserRoleTypeEnum.TravelRecon))
                return UserRoleTypeEnum.TravelRecon;
            
            return UserRoleTypeEnum.GoRecon;
        }

        public ICollection<PushedAlert> PushedAlerts { get; set; }
        public ICollection<EmailAlert> EmailAlerts { get; set; }
        public ICollection<Status> Statuses { get; set; } 
        public ICollection<UserGroupRelation> UserGroupRelations { get; set; }
        public ICollection<Point> Points { get; set; }

        public double? RiskFactor { get; set; }
        public double? ComputedRiskFactor { get; set; }
        public DateTime? ComputedRiskFactorDate { get; set; }

        public ICollection<AnalystsUser> AssignedUsers { get; set; }
        public ICollection<AnalystsCountry> AssignedCountries { get; set; }
        public ICollection<AnalystsDestination> AssignedDestinations { get; set; }
        public ManagerDestinationSet OrganizationalDestinationSet { get; set; }
        //public UserLicense License { get; set; }
        public bool IsDeleted { get; set; }
    }

    public enum EmergencyStatusEnum
    {
        Unknown = 0,
        Safe = 1,
        InDanger = 2,
        Critical = 3
    }
}