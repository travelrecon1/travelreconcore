﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TravelRecon.Domain.Identity
{
    public class AnalystsUser:IEntity
    {       
        public int Id { get; set; }

        [ForeignKey("Analyst"), Column(Order = 0)]
        public int? AnalystId { get; set; }
     
        public int? UserId { get; set; }

        public User Analyst { get; set; }
        [ForeignKey("UserId"), Column(Order = 1)]
        [InverseProperty("AssignedUsers")]
        public User User { get; set; }
    }
}
