﻿using System.ComponentModel.DataAnnotations;

namespace TravelRecon.Domain.Missions
{
    public class Mission : IEntity
    {
        public int Id { get; set; }
          
        [StringLength(255)]
        [Required]
        public string MissionDescription { get; set; }
        public int Score { get; set; }

       
        public int LabelsFk { get; set; }
    }
}
