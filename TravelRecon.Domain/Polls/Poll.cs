﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TravelRecon.Domain.Identity;

namespace TravelRecon.Domain.Polls
{
    public class Poll: IEntity
    {
        public int Id { get; set; }
        [Required]
        public string Question { get; set; }
        [ForeignKey("CreatedBy")]
        public int CreatedById { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ExpirationDate { get; set; }

        public User CreatedBy { get; set; }
        public ICollection<PollAnswer> PollAnswers { get; set; }
    }
}
