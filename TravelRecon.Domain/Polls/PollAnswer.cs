﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using TravelRecon.Domain.Identity;

namespace TravelRecon.Domain.Polls
{
    public class PollAnswer : IEntity
    {
        public int Id { get; set; }
        public string Answer { get; set; }

        [ForeignKey("User")]
        public int UserId { get; set; }
        [ForeignKey("Poll")]
        public int PollId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public PollAnswerStatusEnum Status { get; set; }

        public User User { get; set; }
        public Poll Poll { get; set; }
    }

    public enum PollAnswerStatusEnum
    {
        None = 0,
        Pending = 1,
        Answered = 2
    }
}
