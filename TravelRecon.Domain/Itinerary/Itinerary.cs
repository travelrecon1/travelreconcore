﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;
using TravelRecon.Domain.Identity;

namespace TravelRecon.Domain.Itinerary
{
    public class Itinerary: IEntity
    {
        public int Id { get; set; }
        public int UserId { get; set; }

        [ForeignKey("UserId")]
        public virtual User User { get; set; }
        public ItineraryType ItineraryType { get; set; }
        [StringLength(255)]
        [Required]
        public string ReservationNumber { get; set; }

        [StringLength(255)]
        [Required]
        public string DepartureCountry { get; set; }
        [StringLength(255)]
        [Required]
        public string DepartureCity { get; set; }
        [StringLength(255)]
        [Required]
        public string DepartureAirport { get; set; }
        public DateTime DepartureDateTime { get; set; }
        public DbGeography DepartureLocation { get; set; }

        [StringLength(255)]
        [Required]
        public string ArrivalCountry { get; set; }
        [StringLength(255)]
        [Required]
        public string ArrivalCity { get; set; }
        [StringLength(255)]
        [Required]
        public string ArrivalAirport { get; set; }
        public DateTime ArrivalDateTime { get; set; }

        [StringLength(255)]
        public string HotelReservationNumber { get; set; }
        [StringLength(255)]
        public string HotelCountry { get; set; }
        [StringLength(255)]
        public string HotelCity { get; set; }
        [StringLength(255)]
        public string HotelName { get; set; }
        [StringLength(255)]
        public string HotelAddress { get; set; }
        public DateTime? HotelCheckinDate { get; set; }
        public DateTime? HotelCheckoutDate { get; set; }
        public DbGeography ArrivalLocation { get; set; }


        public DateTime CreatedOn { get; set; }
        public DateTime ModifedOn { get; set; }
        public int CreatedById { get; set; }

        [ForeignKey("CreatedById")]
        public virtual User CreatedBy { get; set; }

        public int ModifiedById { get; set; }

        [ForeignKey("ModifiedById")]
        public User ModifiedBy { get; set; }
    }

    public enum ItineraryType
    {
        Other = 0,
        Flight = 1,
        Bus = 2,
        Train = 3,
        Taxi = 4,
        Ride = 5,
        Walk = 6,
        Bike = 7,
        Motorcycle = 8,
        Metro = 9
    }
}
