﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TravelRecon.Domain.Infrastructure
{
    public class InfrastructureFieldValue : IEntity
    {
        public int Id { get; set; }

        
        public int InfrastructureId { get; set; }

        [ForeignKey("InfrastructureId")]
        public Infrastructure Infrastructure { get; set; }       
        public int InfrastructureTypeFieldId { get; set; }

        [ForeignKey("InfrastructureTypeFieldId")]
        public InfrastructureTypeField InfrastractureTypeField { get; set; } 
        
        [ForeignKey("InfrastructureType")]
        public int TypeId { get; set; }      
        
        public InfrastructureType InfrastructureType { get; set; } 
        
        [StringLength(255)]
        [Required]
        public string Value { get; set; }
    }
}
