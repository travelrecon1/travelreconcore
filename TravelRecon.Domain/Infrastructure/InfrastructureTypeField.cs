﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TravelRecon.Domain.Infrastructure
{
    public class InfrastructureTypeField : IEntity
    {
        public int Id { get; set; }

        [ForeignKey("InfrastructureType")]
        public int TypeId { get; set; }
          
        public InfrastructureType InfrastructureType { get; set; }
         
        [StringLength(255)]
        [Required]
        public string Name { get; set; }    
    }
}
