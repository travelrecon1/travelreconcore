﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TravelRecon.Domain.Infrastructure
{
    public class Infrastructure :IEntity
    {
        public int Id { get; set; }

        [ForeignKey("Type")]
        public int TypeId { get; set; }

        [StringLength(255)]
        public string Name { get; set; }

        [StringLength(255)]
        [Required]
        public string Country { get; set; }

        [StringLength(255)]
        [Required]
        public string City { get; set; }

        public double Lat { get; set; }
    
        public double Long { get; set; }

        public int UserId { get; set; }

        public double RiskRating { get; set; }

        [StringLength(255)]
        public string Address { get; set; }

        [StringLength(255)]
        public string Description { get; set; }

        public InfrastructureType Type { get; set; }

        public DateTimeOffset Created { get; set; }

        public ICollection<InfrastructureFieldValue> ValuesCollection { get; set; }

    }
}
