﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TravelRecon.Domain.Infrastructure
{
    public class InfrastructureType: IEntity
    {
        public int Id { get; set; }

        [StringLength(255)]
        [Required]
        public string Name { get; set; }

        [StringLength(255)]
        public string Icon { get; set; }

        public ICollection<Infrastructure> Infrastructure { get; set; }

        public ICollection<InfrastructureTypeField> FieldsCollection { get; set; }
    }
}
