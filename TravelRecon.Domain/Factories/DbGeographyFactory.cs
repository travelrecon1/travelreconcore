using System;
using System.Data.Entity.Spatial;
using System.Globalization;
using TravelRecon.Domain.Map;

namespace TravelRecon.Domain.Factories
{
    public class DbGeographyFactory
    {
        public static DbGeography GenerateDbGeographyValue(Location mapCenterPoint)
        {
            return FromText(mapCenterPoint.Latitude, mapCenterPoint.Longitude);
        }

        public static DbGeography GenerateDbGeographyValue(double latitude, double longitude)
        {
            return FromText(latitude, longitude);
        }

        private static DbGeography FromText(double latitude, double longitude)
        {
			string lon = longitude.ToString(CultureInfo.InvariantCulture);
			string lat = latitude.ToString(CultureInfo.InvariantCulture);
			return DbGeography.FromText($"POINT ({lon} {lat} )");
        }
    }
}