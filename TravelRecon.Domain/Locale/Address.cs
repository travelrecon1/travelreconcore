﻿using System.ComponentModel.DataAnnotations;

namespace TravelRecon.Domain.Locale
{
    public class Address
    {
        public int Id { get; set; }
        
        [Required]
        [StringLength(255)]
        public string Address1 { get; set; }

        [Required]
        [StringLength(255)]
        public string City { get; set; }

        [Required]
        [StringLength(255)]
        public string State { get; set; }

        [Required]
        [StringLength(255)]
        public string Country { get; set; }
    }
}