﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TravelRecon.Domain.Alerts
{
    public class AnalystAlertType : IEntity
    {
        public int Id { get; set; }
        public virtual AnalystAlertCategory AnalystCategory { get; set; }

        [ForeignKey("AnalystCategory")]
        public int AnalystCategoryFk { get; set; }

        [StringLength(255)]
        [Required]
        public string Name { get; set; }
    }
}
