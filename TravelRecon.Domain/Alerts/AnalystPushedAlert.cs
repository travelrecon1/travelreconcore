﻿using System.ComponentModel.DataAnnotations.Schema;

namespace TravelRecon.Domain.Alerts
{
    public class AnalystPushedAlert : IEntity
    {
        public int Id { get; set; }
        [ForeignKey("Alert")]
        public int AlertId { get; set; }
        public int ReceiverId { get; set; }
        public AnalystPushedAlertReceiverType ReceiverType { get; set; }
        [ForeignKey("AnalystPushedGroup")]
        public int AnalystPushedGroupId { get; set; }

        public AnalystPushedGroup AnalystPushedGroup { get; set; }
        public Alert Alert { get; set; }
    }

    public enum AnalystPushedAlertReceiverType
    {
        None = 0,
        User = 1,
        Group = 2
    }
}
