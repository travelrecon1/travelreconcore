﻿using System.ComponentModel.DataAnnotations;

namespace TravelRecon.Domain.Alerts
{
	public class AnalystAlertPriority : IEntity
	{
		public int Id { get; set; }
        [StringLength(255)]
		public string Name { get; set; }
	}

    public enum AnalystAlertPriorityType
    {
        Routine = 1,
        Priority = 2,
        Immediate = 3,
        Critical = 4
    }
}
