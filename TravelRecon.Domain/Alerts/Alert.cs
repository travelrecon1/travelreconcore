﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;
using TravelRecon.Domain.Identity;
using TravelRecon.Domain.IntelReports;
using TravelRecon.Domain.Map;

namespace TravelRecon.Domain.Alerts
{
    public class Alert : IEntity, IEntityWithLocation
    {
        public Alert() {
            ReportedOn = DateTime.UtcNow;
            ShouldShowOnMap = true;
            Source = AlertSourceType.Scout;
        }

        public int Id { get; set; }
        public string Description { get; set; }
        [StringLength(255)]
        public string Title { get; set; }
        public DbGeography Location { get; set; }
        [StringLength(800)]
        public string AddressFormatted { get; set; }
        [StringLength(255)]
        public string Country { get; set; }
        [StringLength(255)]
        public string City { get; set; }
        public bool ShouldShowOnMap { get; set; }
        public bool ShouldPushToUsers { get; set; }
        public bool IsRejected { get; set; }
        public bool IsEmergency { get; set; }
        public DateTime ReportedOn { get; set; }
        public DateTime? ExpiresOn { get; set; }
        public int? DestinationId { get; set; }

        public int? Accuracy { get; set; }
        public int? ConfidenceRating { get; set; }
        public int? AnalystAlertPriorityId { get; set; }
        [StringLength(255)]
        public string Tags { get; set; }
        public double? ImpactRating { get; set; }

        [ForeignKey("AnalystAlertPriorityId")]
        public AnalystAlertPriority AnalystAlertPriority { get; set; }

        [ForeignKey("DestinationId")]
        public Destination Destination { get; set; }

        public virtual AlertType AlertType { get; set; }
        [ForeignKey("AlertType")]
        public int AlertTypeFk { get; set; }

        public virtual AnalystAlertType AnalystAlertType { get; set; }
        [ForeignKey("AnalystAlertType")]
        public int? AnalystAlertTypeFk { get; set; }

        public virtual User ReportedBy { get; set; }
        [ForeignKey("ReportedBy")]
        public int ReportedByFk { get; set; }

        public virtual User RejectedBy { get; set; }
        [ForeignKey("RejectedBy")]
        public int? RejectedByFk { get; set; }

        public DateTime? RejectedOn { get; set; }

        public AlertSourceType? Source { get; set; }
        public AlertWorkflow? Workflow { get; set; }

        public int? InitialImpactPercentage { get; set; }
        public DateTime? PeakImpactTime { get; set; }

        public IList<AlertSource> AlertSources { get; set; }
        public IList<AnalystPushedAlert> AnalystPushedAlerts { get; set; }
        public IList<AlertsValidation> AlertsValidations { get; set; }
        public IList<AlertsThank> AlertsThanks { get; set; }
    }
}
