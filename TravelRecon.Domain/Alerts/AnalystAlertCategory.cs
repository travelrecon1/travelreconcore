﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TravelRecon.Domain.Alerts
{
    public class AnalystAlertCategory : IEntity
    {
        public int Id { get; set; }

        public virtual IList<AnalystAlertType> AlertTypes { get; protected set; }

        [StringLength(255)]
        [Required]
        public string Name { get; set; }

        public AnalystAlertCategory()
        {
            AlertTypes = new List<AnalystAlertType>();
        }
    }
}
