﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using TravelRecon.Domain.Identity;

namespace TravelRecon.Domain.Alerts
{
    /// <summary>
    /// Captures when an alert was pushed to a user via their phone
    /// </summary>
    public class PushedAlert : IEntity
    {
        public PushedAlert() {
            WhenPushed = DateTime.UtcNow;
        }

        public int Id { get; set; }

        public virtual User User { get; set; }
        [ForeignKey("User")]
        public int UserFk { get; set; }

        public virtual Alert Alert { get; set; }
        [ForeignKey("Alert")]
        public int AlertFk { get; set; }

        public DateTime WhenPushed { get; set; }

        public bool IsRead { get; set; }
    }
}
