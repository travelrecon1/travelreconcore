﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using TravelRecon.Domain.Identity;

namespace TravelRecon.Domain.Alerts
{
    public class AlertSource : IEntity
    {
        public int Id { get; set; }

        public virtual Source Source { get; set; }
        [ForeignKey("Source")]
        public int SourceId { get; set; }

        public DateTime CreatedOn { get; set; }

        public virtual User Creator { get; set; }
        [ForeignKey("Creator")]
        public int CreatedBy { get; set; }

        public virtual Alert Alert { get; set; }
        [ForeignKey("Alert")]
        public int AlertId { get; set; }

        public int Realibility { get; set; }
    }
}
