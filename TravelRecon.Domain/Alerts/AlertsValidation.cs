﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using TravelRecon.Domain.Identity;

namespace TravelRecon.Domain.Alerts
{
    public class AlertsValidation : IEntity
    {
        public int Id { get; set; }

        public int AlertId { get; set; }

        [ForeignKey("AlertId")]
        public virtual Alert Alert { get; set; }

        public int UserId { get; set; }

        [ForeignKey("UserId")]
        public virtual User User { get; set; }

        public DateTime Date { get; set; }
    }
}
