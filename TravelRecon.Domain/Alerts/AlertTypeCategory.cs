﻿using System.Collections.Generic;
using TravelRecon.Domain.Localization;

namespace TravelRecon.Domain.Alerts
{
    public class AlertTypeCategory : IEntity, IHasTranslations<AlertTypeCategoryTranslation>
    {
        public AlertTypeCategory() {
            AlertTypes = new List<AlertType>();
            Translations = new List<AlertTypeCategoryTranslation>();
        }

        public int Id { get; set; }
        public virtual IList<AlertType> AlertTypes { get; protected set; }
        public virtual IList<AlertTypeCategoryTranslation> Translations { get; protected set; }
    }
}
