﻿namespace TravelRecon.Domain.Alerts
{
    public enum AlertSourceType
    {
        None = 0,
        Scout = 1,
        Analyst = 2
    }
}
