﻿using System.Collections.Generic;
using System.Linq;
using TravelRecon.Domain.Localization;

namespace TravelRecon.Domain.Alerts.QueryObjects
{
    public static partial class AlertTypeTranslationsExtensions
    {
        public static AlertTypeTranslation FindAlertTypeLocalized(this IEnumerable<AlertTypeTranslation> alertTypeTranslations, 
            LanguageType targetLanguage) {

            return alertTypeTranslations
                .First(at => at.Language == targetLanguage);
        }
    }
}
