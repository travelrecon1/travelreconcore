﻿using System.Collections.Generic;
using System.Linq;
using TravelRecon.Domain.Localization;

namespace TravelRecon.Domain.Alerts.QueryObjects
{
    public static partial class AlertTypeCategoryTranslationsExtensions
    {
        public static AlertTypeCategoryTranslation FindAlertTypeCategoryLocalized(
            this IEnumerable<AlertTypeCategoryTranslation> alertTypeCategoryTranslations, LanguageType targetLanguage) {

            return alertTypeCategoryTranslations
                .Single(at => at.Language == targetLanguage);
        }
    }
}
