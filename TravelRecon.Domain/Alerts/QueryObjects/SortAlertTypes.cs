﻿namespace TravelRecon.Domain.Alerts.QueryObjects
{
    public enum SortAlertTypes
    {
        None = 0,
        ByMostRecentFirst = 1,
        ByProximity = 2
    }
}
