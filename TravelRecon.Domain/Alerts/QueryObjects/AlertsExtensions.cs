﻿using System;
using System.Data.Entity;
using System.Data.Entity.Spatial;
using System.Linq;
using TravelRecon.Domain.Identity;

namespace TravelRecon.Domain.Alerts.QueryObjects
{
    public static partial class AlertsExtensions
    {
        public static IQueryable<Alert> FindActiveAlertsNearby(
            this IQueryable<Alert> alertTypeCategoryTranslations, DbGeography fromLocation, double radiusInMeters, User currentUser)
        {

            if (currentUser.HasMaxReconRole(UserRoleTypeEnum.GoRecon))
                return alertTypeCategoryTranslations
                    .FindActiveAlerts()
                    .Where(a => a.Location.Distance(fromLocation) <= radiusInMeters &&
                                (a.DestinationId == null || !a.Destination.IsLocked));
            else
                return alertTypeCategoryTranslations
                    .FindActiveAlerts()
                    .Where(a => a.Location.Distance(fromLocation) <= radiusInMeters);
        }

        public static IQueryable<Alert> FindActiveAlerts(
            this IQueryable<Alert> alertTypeCategoryTranslations)
        {

            var now = DateTime.UtcNow;

            return alertTypeCategoryTranslations
                .Where(a =>
                    a.ShouldShowOnMap &&
                    !a.IsRejected &&
                    !a.Workflow.HasValue &&
                    ((a.ExpiresOn.HasValue && a.ExpiresOn.Value > now)
                        || (!a.AlertType.ExpirationTime.HasValue || DbFunctions.AddHours(a.ReportedOn, a.AlertType.ExpirationTime) > now))
                );
        }
    }
}
