﻿namespace TravelRecon.Domain.Alerts.QueryObjects
{
    public enum FilterDateTypes
    {
        None = 0,
        SevenDays = 1,
        ThirtyDays = 2,
        SixMonths = 3
    }
}
