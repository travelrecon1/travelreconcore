﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Spatial;
using System.Linq;
using LinqKit;
using TravelRecon.Domain.Identity;

namespace TravelRecon.Domain.Alerts.QueryObjects
{
    public static partial class AlertsExtensions
    {
        public static IQueryable<Alert> Filter(this IQueryable<Alert> query, 
            int? alertTypeId, 
            bool shouldIncludeScoutReports, 
            bool shouldIncludeAnalystAlerts, 
            bool shouldIncludePushAlerts,
            FilterDateTypes? filterDateType,
            AnalystAlertPriorityType? analystAlertPriorityType,
            ICollection<UserRoles> userRoles, 
            int?[] purchasedDestinationsId)
        {
            DateTime? dateFrom = ConvertFilterDateTypesEnumToDate(filterDateType);

            if (!userRoles.Any(x => x.UserRole.Id == (int)UserRoleTypeEnum.TravelRecon || x.UserRole.Id == (int)UserRoleTypeEnum.TravelReconPro))
            {
                shouldIncludeAnalystAlerts = false;
                shouldIncludePushAlerts = false;
            }

            //filter by category and type
            if (alertTypeId.HasValue)
            {
                query = query.Where(m => m.AlertTypeFk == alertTypeId.Value);
            }

            //query = query.Where(sr => (alertTypeId == null || sr.AlertTypeFk == alertTypeId) && (alertTypeCategoryId == null || sr.AlertType.AlertTypeCategoryFk == alertTypeCategoryId));

            //filters by alert source   
            var sourcePredicate = PredicateBuilder.False<Alert>();
            if (shouldIncludeAnalystAlerts)
            {
                sourcePredicate = sourcePredicate.Or(sr => sr.Source == AlertSourceType.Analyst
                                                           && (!sr.DestinationId.HasValue || purchasedDestinationsId.Contains(sr.DestinationId))
                                                           && (!sr.ShouldPushToUsers
                                                               ||
                                                               (sr.ExpiresOn.HasValue && sr.ExpiresOn <= DateTime.UtcNow)
                                                               || sr.AlertType.ExpirationTime == null
                                                               ||
                                                               DbFunctions.AddHours(sr.ReportedOn,
                                                                   sr.AlertType.ExpirationTime) <= DateTime.UtcNow));
            }

            if (shouldIncludeScoutReports)
            {
                sourcePredicate = sourcePredicate.Or(sr => sr.Source == AlertSourceType.Scout);
            }

            if (shouldIncludePushAlerts)
            {
                sourcePredicate = sourcePredicate.Or(sr => sr.Source == AlertSourceType.Analyst 
                && (!sr.DestinationId.HasValue || purchasedDestinationsId.Contains(sr.DestinationId))
                && sr.ShouldPushToUsers 
                && ((sr.ExpiresOn.HasValue && sr.ExpiresOn > DateTime.UtcNow) 
                        ||  (sr.AlertType.ExpirationTime.HasValue 
                                && DbFunctions.AddHours(sr.ReportedOn, sr.AlertType.ExpirationTime) > DateTime.UtcNow)));
            }

            if (analystAlertPriorityType.HasValue)
            {
                query = query.Where(x => x.AnalystAlertPriorityId == (int)analystAlertPriorityType.Value);
            }

            query = query.AsExpandable().Where(sourcePredicate);

            //filter by date
            return query.Where(sr => dateFrom == null || sr.ReportedOn > dateFrom);
        }

        private static DateTime? ConvertFilterDateTypesEnumToDate(FilterDateTypes? filterDateType)
        {
            switch (filterDateType)
            {
                case FilterDateTypes.SevenDays:
                    return DateTime.UtcNow.AddDays(-7);
                case FilterDateTypes.ThirtyDays:
                    return DateTime.UtcNow.AddDays(-30);

                case FilterDateTypes.SixMonths:
                    return DateTime.UtcNow.AddMonths(-6);
            }

            return null;
        }

        public static IQueryable<Alert> Sort(this IQueryable<Alert> query, DbGeography currentMapLocation, SortAlertTypes? sortAlertType)
        {
            var sat = sortAlertType ?? SortAlertTypes.ByMostRecentFirst;

            switch (sat)
            {
                case SortAlertTypes.ByMostRecentFirst:
                    query = query.OrderByDescending(sr => sr.ReportedOn);
                    break;
                case SortAlertTypes.ByProximity:
                    query = query.OrderBy(sr => sr.Location.Distance(currentMapLocation));
                    break;
            }

            return query;
        }
    }
}
