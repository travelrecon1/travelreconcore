﻿using System.Data.Entity.Spatial;
using System.Linq;
using TravelRecon.Domain.IntelReports;

namespace TravelRecon.Domain.Alerts.QueryObjects
{
    public static class DestinationsExtensions
    {
        public static IQueryable<Destination> FindByLocation(
            this IQueryable<Destination> destinations, DbGeography fromLocation, double destinationRadius, double? radiusInMeters = null)
        {
            return destinations.Where(d => d.Location.Distance(fromLocation) <= destinationRadius + (radiusInMeters ?? 0));
        }
    }
}
