﻿using System.ComponentModel.DataAnnotations;

namespace TravelRecon.Domain.Alerts
{
	public class AlertPriority : IEntity
	{
		public int Id { get; set; }

        [Required]
        [StringLength(255)]
		public string Name { get; set; }
	}
    public enum AlertPriorityType
    {
        Low = 1,
        Medium = 2,
        High = 3
    }
}
