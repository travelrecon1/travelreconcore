﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TravelRecon.Domain.Localization;

namespace TravelRecon.Domain.Alerts
{
    public class AlertTypeTranslation : ITranslation
    {
        public int Id { get; set; }
        public LanguageType Language { get; set; }

        [Required]
        [StringLength(800)]
        public string Name { get; set; }

        public virtual AlertType AlertType { get; set; }
        [ForeignKey("AlertType")]
        public int AlertTypeFk { get; set; }
    }
}
