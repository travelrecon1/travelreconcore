﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using TravelRecon.Domain.Localization;

namespace TravelRecon.Domain.Alerts
{
    public class AlertType : IEntity, IHasTranslations<AlertTypeTranslation>
    {
        public AlertType() {
            Translations = new List<AlertTypeTranslation>();
        }

        public int Id { get; set; }

        //expiration time in hours
        public int? ExpirationTime { get; set; }

        public virtual AlertTypeCategory Category { get; set; }
        [ForeignKey("Category")]
        public int AlertTypeCategoryFk { get; set; }

		public virtual AlertPriority Priority { get; set; }

		[ForeignKey("Priority")]
		public int AlertPriorityFk { get; set; }

        public double? Severity { get; set; }
        public double? AffectProximity { get; set; }

        public virtual IList<AlertTypeTranslation> Translations { get; protected set; }
    }
}
