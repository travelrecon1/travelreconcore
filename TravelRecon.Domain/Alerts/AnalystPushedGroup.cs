﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TravelRecon.Domain.Identity;
using TravelRecon.Domain.IntelReports;

namespace TravelRecon.Domain.Alerts
{
    public class AnalystPushedGroup : IEntity
    {
        public int Id { get; set; }
        [StringLength(255)]
        [Required]
        public string Title { get; set; }
        public DateTime CreatedDate { get; set; }
        public int ConfidenceRating { get; set; }
        public int Accurancy { get; set; }
        [ForeignKey("Destination")]
        public int DestinationId { get; set; }
        [ForeignKey("CreatedBy")]
        public int CreatedById { get; set; }
        public bool ShouldPush { get; set; }

        public Destination Destination { get; set; }
        public User CreatedBy { get; set; }

        public IList<AnalystPushedAlert> AnalystPushedAlerts { get; set; }
    }
}
