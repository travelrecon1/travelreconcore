﻿
namespace TravelRecon.Domain.Alerts
{
	public enum AlertWorkflow
    {
        None = 0,
        Draft = 1,
        Review = 2
	}
}
