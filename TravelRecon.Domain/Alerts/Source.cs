﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TravelRecon.Domain.Alerts
{
    public class Source : IEntity
    {
        public int Id { get; set; }
        [StringLength(255)]
        [Required]
        public string Name { get; set; }
        public int? Rating { get; set; }

        public IList<AlertSource> AlertSources { get; set; }
    }
}
