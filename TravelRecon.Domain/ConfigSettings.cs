﻿using System.Configuration;

namespace TravelRecon.Domain
{
    public class ConfigSettings
    {
        public static bool ShouldEnforceSsl => bool.Parse(GetAppSetting("ShouldEnforceSsl"));
        public static string GoogleTranslateApiKey => GetAppSetting("GoogleTranslateApiKey");
        public static string GoogleTranslateApplicationName => GetAppSetting("GoogleTranslateApplicationName");
        public static string RamenOrganizationId => GetAppSetting("RamenOrganizationId");
        public static string RamenOrganizationApiKey => GetAppSetting("RamenOrganizationApiKey");
        public static string StripeApiKeyTest => GetAppSetting("StripeApiKeyTest");
        public static string StripeApiKeyLive => GetAppSetting("StripeApiKeyLive");
        public static bool StripeIsLiveMode => bool.Parse(GetAppSetting("StripeIsLiveMode"));


        public static string GetAppSetting(string settingName) {
            var settingValue = ConfigurationManager.AppSettings[settingName];

            if (string.IsNullOrEmpty(settingValue))
                throw new ConfigurationErrorsException("AppSetting could not be found for " + settingName);

            return settingValue;
        }
    }
}
