﻿namespace TravelRecon.Domain.Groups
{
    public enum UserGroupRole
    {
        None = 0,
        Administrator = 1,
        User = 2
    }
}
