﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using TravelRecon.Domain.Identity;

namespace TravelRecon.Domain.Groups
{
    public class UserGroupRelation : IEntity
    {
        public int Id { get; set; }
        public int GroupId { get; set; }
        public int UserId { get; set; }
        public UserGroupRelationType UserGroupRelationType { get; set; }
        public UserGroupRole UserGroupRole { get; set; }
        public int UpdatedById { get; set; }

        [ForeignKey("GroupId")]
        public virtual Group Group { get; set; }

        [ForeignKey("UserId")]
        public virtual User User { get; set; }

        public virtual User UpdatedBy { get; set; }
    }
}
