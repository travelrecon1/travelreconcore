﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TravelRecon.Domain.Groups
{
    public class Group: IEntity
    {
        public static readonly string DefaultColor = "#ffffff";

        public string mapColor { get; set; }
        public int Id { get; set; }
        [StringLength(256)]
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
        [StringLength(1024)]
        [Required]
        public string Icon { get; set; }
        public DateTime CreatedDate { get; set; }

        public ICollection<UserGroupRelation> UserGroupRelations { get; set; } 
    }
}
