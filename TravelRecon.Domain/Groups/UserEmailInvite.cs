﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TravelRecon.Domain.Identity;

namespace TravelRecon.Domain.Groups
{
    public class UserEmailInvite : IEntity
    {
        public int Id { get; set; }

        [ForeignKey("UserFk")]
        public User User { get; set; }

        public int UserFk { get; set; }

        [StringLength(255)]
        [Required]
        public string Email { get; set; }

        public Group Group { get; set; }

        [ForeignKey("Group")]
        public int? InvitedUserGroup { get; set; }
    }
}
