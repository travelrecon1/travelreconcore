﻿namespace TravelRecon.Domain.Groups
{
    public enum UserGroupRelationType
    {
        None = 0,
        Accepted = 1,
        RequestedByUser = 2,
        InvitedByAdministrator = 3,
        RejectedByUser = 4,
        RejectedByAdministrator = 5
    }
}
