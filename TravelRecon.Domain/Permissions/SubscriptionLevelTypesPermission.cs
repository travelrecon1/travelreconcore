using System;

namespace TravelRecon.Domain.Permissions
{
    public class SubscriptionLevelTypesPermission : IEntity
    {
        public int Id { get; set; }
        public int SubscriptionlevelTypeId { get; set; }
        //public int PermissionsIdFk { get; set; }
        public string Permission { get; set; }
        public string Module { get; set; }
        public DateTime CreatedDate { get; set; }
        //public DateTime ModifiedDate { get; set; }
    }
}