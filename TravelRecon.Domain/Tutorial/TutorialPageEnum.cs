﻿namespace TravelRecon.Domain.Tutorial
{
    public enum PageTypeEnum
    {
        BasePage = 0,
        Map = 1,
        Alerts = 2,
        Communications = 3,
        Contacts = 4
    }
}
