﻿namespace TravelRecon.Domain.Tutorial
{
    public class ViewedPage: IEntity
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public PageTypeEnum PageType { get; set; }
    }
}
