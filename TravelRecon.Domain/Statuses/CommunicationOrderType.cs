﻿namespace TravelRecon.Domain.Statuses
{
    public enum CommunicationOrderType
    {
        FirstName = 1,
        LastName = 2,
        Recent = 3
    }
}
