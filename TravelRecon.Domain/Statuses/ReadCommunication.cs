﻿using System.ComponentModel.DataAnnotations.Schema;
using TravelRecon.Domain.Identity;

namespace TravelRecon.Domain.Statuses
{
    public class ReadCommunication : IEntity
    {
        public int Id { get; set; }
        public StatusType StatusType { get; set; }
        public int StatusId { get; set; }
        public int UserId { get; set; }

        [ForeignKey("UserId")]
        public virtual User User { get; set; }
    }
}
