﻿namespace TravelRecon.Domain.Statuses
{
    public enum StatusType
    {
        CheckIn = 1,
        Emergency = 2,
        PollAnswer = 3,
        NotRespondedPollAnswer = 4
    }
}
