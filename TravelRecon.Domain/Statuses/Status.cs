﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;
using TravelRecon.Domain.Identity;
using TravelRecon.Domain.Map;

namespace TravelRecon.Domain.Statuses
{
    public class Status : IEntity, IEntityWithLocation
    {
        public int Id { get; set; }
        public DbGeography Location { get; set; }
        public string AddressFormatted { get; set; }
        public StatusType Type { get; set; }
        public DateTime ReportedOn { get; set; }
        public int ReportedBy { get; set; }
        [StringLength(255)]
        public string Description { get; set; }

        [ForeignKey("ReportedBy")]
        public User Reporter { get; set; }
    }
}
