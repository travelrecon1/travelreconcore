﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TravelRecon.Domain.Identity;
using TravelRecon.Domain.IntelReports;

namespace TravelRecon.Domain.Risk
{
    public class RiskRatingHistory : IEntity
    {
        public int Id { get; set; }

        public double CurrentValue { get; set; }
        public double Shift { get; set; }

        [ForeignKey("UserId")]
        public User User { get; set; }

        public int UserId { get; set; }

        [ForeignKey("CountryId")]
        public Country Country { get; set; }
        public int? CountryId { get; set; }

        [ForeignKey("DestinationId")]
        public Destination Destination { get; set; }
        public int? DestinationId { get; set; }
        public DateTimeOffset Created { get; set; }
    }
}
