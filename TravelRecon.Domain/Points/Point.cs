﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using TravelRecon.Domain.Identity;

namespace TravelRecon.Domain.Points
{
    public class Point: IEntity
    {
        public Point()
        {
            Created = DateTime.Now;
        }

        public int Id { get; set; }
        public DateTime Created { get; set; }
        public short Value { get; set; }
        public PointType Type { get; set; }
        public User User { get; set; }

        [ForeignKey("User")]
        public int OwnerId { get; set; }
    }
}
