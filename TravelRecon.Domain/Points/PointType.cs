namespace TravelRecon.Domain.Points
{
    public enum PointType: short
    {
        None = 0,
        Referral,
        InfoScoutReport,
        CriminalScoutReport,
        Newcomer
    }
}