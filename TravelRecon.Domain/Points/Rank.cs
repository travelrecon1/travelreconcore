﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TravelRecon.Domain.Localization;

namespace TravelRecon.Domain.Points
{
	public class Rank : IEntity
	{
		public int Id { get; set; }
        [StringLength(255)]
        [Required]
		public string Name { get; set; }

		[ForeignKey("LabelFk")]
		public Label Label { get; set; }
		public int LabelFk { get; set; }
		public int MinPoints { get; set; }
	}
}
