﻿using System.Xml.Serialization;

namespace TravelRecon.Domain
{
    /// <summary>
    /// Provides a generic typed mechanism for returning success/failure feedback along with a value
    /// </summary>
    [XmlType(TypeName = "ActionConfirmation")]
    public class ActionConfirmation<T>
    {
        protected ActionConfirmation() { }

        private ActionConfirmation(bool wasSuccessful) {
            WasSuccessful = wasSuccessful;
        }

        private ActionConfirmation(string message, bool wasSuccessful) {
            Message = message;
            WasSuccessful = wasSuccessful;
        }

        public static ActionConfirmation<T> CreateSuccessConfirmation() {
            return new ActionConfirmation<T>(true);
        }

        public static ActionConfirmation<T> CreateFailureConfirmation() {
            return new ActionConfirmation<T>(false);
        }

        public static ActionConfirmation<T> CreateSuccessConfirmation(string message) {
            return new ActionConfirmation<T>(message, true);
        }

        public static ActionConfirmation<T> CreateSuccessConfirmation(T value) {
            return new ActionConfirmation<T>(null, true) {
                Value = value
            };
        }

        public static ActionConfirmation<T> CreateFailureConfirmation(string message) {
            return new ActionConfirmation<T>(message, false);
        }

        public static ActionConfirmation<T> CreateSuccessConfirmation(string message, T value) {
            return new ActionConfirmation<T>(message, true) {
                Value = value
            };
        }

        public static ActionConfirmation<T> CreateFailureConfirmation(string message, T value) {
            return new ActionConfirmation<T>(message, false) {
                Value = value
            };
        }

        public bool WasSuccessful { get; set; }
        public string Message { get; set; }
        public T Value { get; set; }
    }
}
