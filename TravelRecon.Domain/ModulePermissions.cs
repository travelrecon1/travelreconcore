using System.Collections.Generic;
using System.Linq;

namespace TravelRecon.Domain
{
    public class ModulePermissions : ModulePermissionsBase
    {
        public class PermissionSelector
        {
            public PermissionSelector(int id, string name)
            {
                ID = id;
                Name = name;
            }

            public int ID { get; set; }
            public string Name { get; set; }
        }
    }
}