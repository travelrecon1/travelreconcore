﻿namespace TravelRecon.Domain.Localization
{
    public interface ITranslation : IEntity
    {
        LanguageType Language { get; set; }
    }
}
