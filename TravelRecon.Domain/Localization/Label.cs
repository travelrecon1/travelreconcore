﻿using System.Collections.Generic;

namespace TravelRecon.Domain.Localization
{
    public class Label : IEntity, IHasTranslations<LabelTranslation>
    {
        public Label()
        {
            Translations = new List<LabelTranslation>();
        }

        public int Id { get; set; }
        public virtual IList<LabelTranslation> Translations { get; protected set; }
    }
}