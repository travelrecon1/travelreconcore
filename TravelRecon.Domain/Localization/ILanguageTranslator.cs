﻿namespace TravelRecon.Domain.Localization
{
    public interface ILanguageTranslator
    {
        /// <summary>
        /// Infers the language source from the content
        /// </summary>
        string Translate(string content, LanguageType targetLanguage);

        string Translate(string content, LanguageType sourceLanguage, LanguageType targetLanguage);
    }
}
