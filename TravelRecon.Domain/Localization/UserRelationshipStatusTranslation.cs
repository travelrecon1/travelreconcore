﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TravelRecon.Domain.Identity.UserRelationships;

namespace TravelRecon.Domain.Localization
{
    public class UserRelationshipStatusTranslation : ITranslation
    {
        public int Id { get; set; }
        [StringLength(255)]
        [Required]
        public string Description { get; set; }
        [StringLength(255)]
        [Required]
        public string Label { get; set; }
        public LanguageType Language { get; set; }

        public virtual UserRelationshipStatus UserRelationshipStatus { get; set; }
        [ForeignKey("UserRelationshipStatus")]
        public int UserRelationshipStatusFk { get; set; }
        
        
    }
}