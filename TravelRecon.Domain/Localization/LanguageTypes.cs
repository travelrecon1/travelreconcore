﻿namespace TravelRecon.Domain.Localization
{
    /// <summary>
    /// Maps to LanguageTypes; so don't change the numbers without updating the table
    /// </summary>
    public enum LanguageType
    {
        None = 0,
        English = 1,
        Spanish = 2,
        Portuguese = 3,
        French = 4
    }
}
