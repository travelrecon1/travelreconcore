﻿using System.Collections.Generic;

namespace TravelRecon.Domain.Localization
{
    public interface IHasTranslations<T> where T : ITranslation
    {
        IList<T> Translations { get; }
    }
}
