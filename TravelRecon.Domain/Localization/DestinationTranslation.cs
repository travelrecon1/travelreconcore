﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TravelRecon.Domain.IntelReports;

namespace TravelRecon.Domain.Localization
{
    public class DestinationTranslation : ITranslation
    {
        public int Id { get; set; }
        public LanguageType Language { get; set; }
        [StringLength(255)]
        [Required]
        public string City { get; set; }
        public string Description { get; set; }

        public virtual Destination Destination { get; set; }
        [ForeignKey("Destination")]
        public int DestinationId { get; set; }
    }
}