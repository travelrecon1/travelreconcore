﻿using System;

namespace TravelRecon.Domain.Localization
{
    public class LocalizationException : Exception
    {

        public LocalizationException(string message) : base(message)
        {

        }
    }
}