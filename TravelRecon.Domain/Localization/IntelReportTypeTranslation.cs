﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TravelRecon.Domain.IntelReports;

namespace TravelRecon.Domain.Localization
{
    public class IntelReportTypeTranslation : ITranslation
    {
        public int Id { get; set; }
        public LanguageType Language { get; set; }

        [StringLength(255)]
        [Required]
        public string Name { get; set; }

        [Required]
        public string Description { get; set; }

        public virtual IntelReportType IntelReportType { get; set; }

        [ForeignKey("IntelReportType")]
        public int IntelReportTypeFk { get; set; }

    }
}