﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TravelRecon.Domain.Localization
{
    public class LabelTranslation : ITranslation
    {
        public int Id { get; set; }
        public LanguageType Language { get; set; }
        [Required]
        public string Content { get; set; }

        public virtual Label Label { get; set; }
        [ForeignKey("Label")]
        public int LabelFk { get; set; }
    }
}
