using TravelRecon.Domain.Identity.UserRelationships;

namespace TravelRecon.Domain.Localization.QueryObjects
{
    public class StatusLocalizedDto
    {
        public UserRelationshipStatusEnum UserRelationshipStatus { get; set; }
        public string Label { get; set; }
        public string Description { get; set; }
        public int UserRelationshipStatusId { get; set; }
    }
}