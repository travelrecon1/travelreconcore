﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using TravelRecon.Domain.Identity.UserRelationships;

namespace TravelRecon.Domain.Localization.QueryObjects
{
    public static class UserRelationshipStatusLocalized
    {
        public static IEnumerable<StatusLocalizedDto> FindLocalStatus(
            this IQueryable<UserRelationshipStatus> userRelationshipStatus, LanguageType targetLanguage)
        {
            var localizedStatus = userRelationshipStatus
                .Include(ur => ur.Translations)
                .Select(local => new StatusLocalizedDto
                {
                    UserRelationshipStatus = (UserRelationshipStatusEnum) local.Id,
                    UserRelationshipStatusId = local.Id,
                    // I'd rather use Single here, but .NET won't allow it
                    Label = local.Translations.FirstOrDefault(t => t.Language == targetLanguage).Label,
                    Description = local.Translations.FirstOrDefault(t => t.Language == targetLanguage).Description,
                });

            return localizedStatus;
        }
    }
}