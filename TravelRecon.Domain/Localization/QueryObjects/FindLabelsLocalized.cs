﻿using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;

namespace TravelRecon.Domain.Localization.QueryObjects
{
    public static partial class LabelsExtensions
    {
        public static IEnumerable<LabelLocalizedDto> FindLabelsLocalized(this IQueryable<Label> labels, 
            LabelType[] labelsToRetrieve, LanguageType targetLanguage) {

            if (!labelsToRetrieve.Any())
                return new List<LabelLocalizedDto>();

            var labelsToRetrieveAsInts = Array.ConvertAll(labelsToRetrieve, type => (int) type);

            var labelsLocalized = labels
                .Where(l => labelsToRetrieveAsInts.Contains(l.Id))
                .Include(l => l.Translations)
                .Select(l => new LabelLocalizedDto {
                    LabelType = (LabelType) l.Id,
                    // I'd rather use Single here, but .NET won't allow it
                    Content = l.Translations.FirstOrDefault(t => t.Language == targetLanguage).Content
                });

            return labelsLocalized;
        }

        public static Dictionary<int, string> FindLabelsLocalizedDictionary(this IQueryable<Label> labels,
            LabelType[] labelsToRetrieve, LanguageType targetLanguage)
        {

            if (!labelsToRetrieve.Any())
                return new Dictionary<int, string>();

            var labelsToRetrieveAsInts = Array.ConvertAll(labelsToRetrieve, type => (int)type);

            var labelsLocalized = labels
                .Where(l => labelsToRetrieveAsInts.Contains(l.Id))
                .Include(l => l.Translations)
                .Select(l => new LabelLocalizedDto
                {
                    LabelType = (LabelType)l.Id,
                    // I'd rather use Single here, but .NET won't allow it
                    Content = l.Translations.FirstOrDefault(t => t.Language == targetLanguage).Content
                }).ToDictionary(x => (int)x.LabelType, x => x.Content);

            return labelsLocalized;
        }
    }
}
