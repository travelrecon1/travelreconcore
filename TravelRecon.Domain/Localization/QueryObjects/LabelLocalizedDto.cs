﻿namespace TravelRecon.Domain.Localization.QueryObjects
{
    public class LabelLocalizedDto
    {
        public LabelType LabelType { get; set; }
        public string Content { get; set; }
    }
}
