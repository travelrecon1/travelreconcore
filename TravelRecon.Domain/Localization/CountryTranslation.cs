﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TravelRecon.Domain.IntelReports;

namespace TravelRecon.Domain.Localization
{
    public class CountryTranslation : ITranslation
    {
        public int Id { get; set; }
        public LanguageType Language { get; set; }
        [StringLength(255)]
        [Required]
        public string Name { get; set; }

        public virtual Country Country { get; set; }
        [ForeignKey("Country")]
        public int CountryId { get; set; }

    }
}