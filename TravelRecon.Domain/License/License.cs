﻿using System.ComponentModel.DataAnnotations;

namespace TravelRecon.Domain.License
{
    public class License: IEntity
    {
        public int Id { get; set; }
        [StringLength(255)]
        [Required]
        public string Name { get; set; }
        public LicenseType Type { get; set; }
    }

    public enum LicenseType
    {
        None = 0,
        TrpManagersLicense = 1,
        TrpUsersLicense = 2
    }
}
