﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TravelRecon.Domain.Identity;

namespace TravelRecon.Domain.License
{
    public class UserLicense: IEntity
    {
        public int Id { get; set; }

        [ForeignKey("Owner")]
        public int? OwnerId { get; set; }
        [ForeignKey("AssignedBy")]
        public int AssignedById { get; set; }
        [ForeignKey("Manager")]
        public int? ManagerId { get; set; }

        [ForeignKey("License")]
        public int LicenseId { get; set; }
        [ForeignKey("ParentLicense")]
        public int? ParentLicenseId { get; set; }
        public DateTime? AssignedDate { get; set; }
        public DateTime ExpirationDate { get; set; }
        public DateTime? ActivatedDate { get; set; }

        public User Owner { get; set; }
        public User AssignedBy { get; set; }
        public User Manager { get; set; }

        [StringLength(255)]
        [Index("UserLicense_LicenseKey", 1, IsUnique = true)]
        [Required]
        public string LicenseKey { get; set; }

        [ForeignKey("ClientCompany")]
        public int ClientCompanyId { get; set; }
       
        public ClientCompany.ClientCompany ClientCompany { get; set; }
        public License License { get; set; }
        public UserLicense ParentLicense { get; set; }
    }
}
