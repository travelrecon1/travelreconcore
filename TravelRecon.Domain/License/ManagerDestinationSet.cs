﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TravelRecon.Domain.IntelReports;

namespace TravelRecon.Domain.License
{
    public class ManagerDestinationSet
    {
        public ManagerDestinationSet()
        {
            LicensedDestinations = new HashSet<Destination>();
        }
        public int Id { get; set; }
        [Required]

        public int DestinationCountAllotted { get; set; }

        [ForeignKey("ClientCompany")]
        public int ClientCompanyId { get; set; }

        public ClientCompany.ClientCompany ClientCompany { get; set; }

        public virtual ICollection<Destination> LicensedDestinations { get; set; }
    }
}