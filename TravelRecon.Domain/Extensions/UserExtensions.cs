﻿using System.CodeDom;
using TravelRecon.Domain.Identity;

namespace TravelRecon.Domain.Extensions
{
    public static class UserExtensions
    {
        public static string Fullname(this User user)
        {
            return user.FirstName + " " + user.LastName;
        }
    }
}