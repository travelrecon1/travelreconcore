﻿using System;
using System.Linq;

namespace TravelRecon.Domain.Extensions
{
    public static class EnumExtensions
    {
        public static string ConvertToString(this Enum enummy) {
            return Enum.GetName(enummy.GetType(), enummy);
        }

        public static TEnumType ConverToEnum<TEnumType>(this string enumValue) {
            return (TEnumType) Enum.Parse(typeof (TEnumType), enumValue.RemoveWhitespace(), true);
        }

        public static string RemoveWhitespace(this string input) {
            return new string(input.ToCharArray()
                .Where(c => !char.IsWhiteSpace(c))
                .ToArray());
        }
    }
}