﻿namespace TravelRecon.Domain.Ecommerce
{
    public enum PaymentTypes
    {
        Other = 0,
        CreditCard = 1,
        Bitcoin = 2
    }
}
