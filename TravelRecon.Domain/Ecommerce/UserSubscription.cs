﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using TravelRecon.Domain.Identity;
using TravelRecon.Domain.SubscriptionOffers;

namespace TravelRecon.Domain.Ecommerce
{
    public class UserSubscription : IEntity
    {
        public int Id { get; set; }

        public virtual User User { get; set; }

        [ForeignKey("User")]
        public int UserFk { get; set; }

        public SubscriptionTypes SubscriptionType { get; set; }

        public DateTime PurchaseDate { get; set; }

        public DateTime? ExpirationDate { get; set; }

        public bool IsActivated { get; set; }

        public virtual StripePayment Payment { get; set; }

        [ForeignKey("Payment")]
        public int PaymentFk { get; set; }
    }
}
