﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TravelRecon.Domain.Identity;

namespace TravelRecon.Domain.Ecommerce
{
    public class StripePayment : IEntity
    {
        public int Id { get; set; }

        [StringLength(255)]
        [Required]
        public string PaymentId { get; set; }

        public virtual User User { get; set; }

        [ForeignKey("User")]
        public int UserFk { get; set; }

        public decimal AmountPaid { get; set; }

        [StringLength(255)]
        [Required]
        public string ReceiptEmail { get; set; }

        public bool IsTestPayment { get; set; }

        public PaymentTypes PaymentType { get; set; }

        public DateTime Date { get; set; }

        public bool IsPaid { get; set; }

        [StringLength(255)]
        [Required]
        public string Status { get; set; }
    }
}
