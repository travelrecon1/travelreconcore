﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TravelRecon.Domain.RepositoryInterfaces
{
    /// <summary>
    /// Used for all non-tenant entities which may be accessed by all tenants
    /// </summary>
    public interface IRepository<TEntity> where TEntity : class, IEntity
    {
        TEntity Get(int id);
        IQueryable<TEntity> GetAll();
        TEntity Add(TEntity t);
        void AddRange(IList<TEntity> t);
        void Delete(TEntity t);
        void DeleteRange(IList<TEntity> t);
        void SaveChanges();
        Task SaveChangesAsync();
        void Reload(TEntity t);
        void Modified(TEntity t);
        void AddRange(IEnumerable<TEntity> t);

        IDisposable BeginTransaction();
        void CommitTransaction();
    }
}
