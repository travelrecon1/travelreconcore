﻿namespace TravelRecon.Domain.SubscriptionOffers
{
    public enum SubscriptionTypes
    {
        None = 0,
        Monthly = 1,
        Annual = 2
    }
}
