﻿using System.ComponentModel.DataAnnotations;
using TravelRecon.Domain.Localization;

namespace TravelRecon.Domain.SubscriptionOffers
{
	public class SubscriptionOffer : IEntity
	{
		public int Id { get; set; }

		public LanguageType LanguageTypeFk { get; set; }

        [StringLength(255)]
        [Required]
		public string Title { get; set; }

        [StringLength(255)]
		public string PrimaryText { get; set; }

        [StringLength(255)]
		public string SecondaryText { get; set; }

        [StringLength(255)]
		public string OfferPrice { get; set; }

		public bool HasSpecialOffer { get; set; }

        [StringLength(255)]
		public string SpecialOfferPrice { get; set; }

        [StringLength(255)]
        [Required]
		public string ButtonText { get; set; }

        [StringLength(255)]
		public string ButtonLink { get; set; }

        public SubscriptionTypes? SubscriptionType { get; set; }

        public bool ShowInApp { get; set; }

        public decimal? Price { get; set; }
	}
}
