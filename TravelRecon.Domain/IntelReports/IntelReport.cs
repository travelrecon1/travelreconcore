using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TravelRecon.Domain.IntelReports
{
    public class IntelReport : IEntity
    {
        public int Id { get; set; }
        [StringLength(255)]
        public string ReportLocationUri { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsBusinessReport { get; set; }

        public DateTime ModifiedDate { get; set; }
        public DateTime CreatedDate { get; set; }
        
        public virtual IntelReportType IntelReportType { get; set; }
        [ForeignKey("IntelReportType")]
        public int IntelReportTypeFk { get; set; }

        public Destination Destination { get; set; }

        [ForeignKey("Destination")]
        public int DestinationId { get; set; }
    }
}