﻿namespace TravelRecon.Domain.IntelReports
{
    public enum IntelReportTypeEnum
    {
        SituationReport = 1,
        HotelAssessment = 2,
        EmergencyContactReport = 3,
        HealthAndMedical= 4,
        HotelThreatAssessment = 5,
        HotelThreatVulnerabilityAssessment = 6,
        IntelligenceSummary = 7,
        MustKnowReport =8,
        Telecommunications =9,
        ThreatAreaReport = 10,
        ThreatOverview = 11,
        TransportationAssessment =12, 
        TransportationAssessmentBusiness =13, 
        DoingBusinessIn = 14
    }
}