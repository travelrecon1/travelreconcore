﻿using System.Collections.Generic;
using TravelRecon.Domain.Localization;

namespace TravelRecon.Domain.IntelReports
{
    public class Country: IDeletableEntity
    {
        public int Id { get; set; }

        public double RiskFactor { get; set; }

        public bool IsDeleted { get; set; }

        public IList<CountryTranslation> Translations { get; set; }
    }
}
