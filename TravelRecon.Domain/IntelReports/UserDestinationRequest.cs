﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using TravelRecon.Domain.Identity;

namespace TravelRecon.Domain.IntelReports
{
	public class UserDestinationRequest : IEntity
	{
		public int Id { get; set; }
        public int UserId { get; set; }
        public int DestinationId { get; set; }
        public DateTime RequestDate { get; set; }

        [ForeignKey("UserId")]
        public User User { get; set; }

        [ForeignKey("DestinationId")]
        public Destination Destination { get; set; }
	}
}
