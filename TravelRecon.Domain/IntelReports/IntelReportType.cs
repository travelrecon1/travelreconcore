using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using TravelRecon.Domain.Localization;

namespace TravelRecon.Domain.IntelReports
{
    public class  IntelReportType : IEntity, IHasTranslations<IntelReportTypeTranslation>
    {
        public IntelReportType()
        {
            Translations = new List<IntelReportTypeTranslation>();
        }

        public int Id { get; set; }
        [StringLength(255)]
        [Required]
        public string ReportTypeAcronym { get; set; }
        [StringLength(255)]
        [Required]
        public string Thumbnail { get; set; }


        public IList<IntelReportTypeTranslation> Translations { get; protected set; }
        public bool IsEnabled { get; set; }
        public bool IsBusinessReport { get; set; }
    }
}