using System.Collections.Generic;
using System.Data.Entity.Spatial;

namespace TravelRecon.Domain.IntelReports
{
    public class LocalizedDestination
    {
        public int Id { get; set; }
        public int CountryId { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public int ZoomLevel { get; set; }
        public DbGeography Location { get; set; }
        public bool IsLocked { get; set; }
        public bool IsComingSoon { get; set; }

        public string Country { get; set; }
        public string City { get; set; }
        public string Description { get; set; }

        public string DestinationName
        {
            get
            {
                var result = string.Empty;
                if (City != null)
                {
                    result += City;

                    if (Country != null)
                        result += ", ";
                }
                result += Country;
                return result;
            }
        }

        public double? RiskFactor { get; set; }
    }
}