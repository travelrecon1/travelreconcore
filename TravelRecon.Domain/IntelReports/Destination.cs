using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;
using TravelRecon.Domain.License;
using TravelRecon.Domain.Localization;

namespace TravelRecon.Domain.IntelReports
{
    public class Destination : IDeletableEntity
    {
        public int Id { get; set; }
        [ForeignKey("Country")]
        public int CountryId { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public int ZoomLevel { get; set; }
        public DbGeography Location { get; set; }
        public bool IsLocked { get; set; }
	    public bool IsComingSoon { get; set; }
        public decimal Cost { get; set; }
        public bool IsGoreconAccess { get; set; }

        public virtual Country Country { get; set; }
        public IList<DestinationTranslation> Translations { get; set; }
        public IList<UserDestinations> UserDestinations { get; set; }
        public virtual ICollection<ManagerDestinationSet> ManagerDestinationSets { get; set; }

        public double? RiskFactor { get; set; }
        public bool IsDeleted { get; set; }
    }
}