﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;
using System.Linq;
using TravelRecon.Domain.Ecommerce;
using TravelRecon.Domain.Identity;
using TravelRecon.Domain.Localization;

namespace TravelRecon.Domain.IntelReports
{
    public class UserDestinations : IEntity
    {
        public int Id { get; set; }
        [StringLength(255)]
        public string Country { get; set; }
        [StringLength(255)]
        public string City { get; set; }
        [StringLength(255)]
        public string Description { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public DbGeography Location { get; set; }

        [ForeignKey("Destination")]
        public int? DestinationId { get; set; }
        public DateTime ModifiedDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public bool IsAvailable { get; set; }
        public bool ReceiveNotifications { get; set; }

        public virtual User User { get; set; }
        [ForeignKey("User")]
        public int UserFk { get; set; }

        public virtual UserSubscription Subscription { get; set; }
        public Destination Destination { get; set; }

        [ForeignKey("Subscription")]
        public int? SubscriptionFk { get; set; }

        public static UserDestinations ConvertFrom(LocalizedDestination destination)
        {
            return new UserDestinations
            {
                Country = destination.Country,
                City = destination.City,
                Description = destination.Description,
                Latitude = destination.Latitude,
                Longitude = destination.Longitude,
                Location = destination.Location,
                DestinationId = destination.Id
            };
        }
    }
}
