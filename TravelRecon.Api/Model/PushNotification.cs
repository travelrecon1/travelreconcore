﻿namespace TravelRecon.Api.Model
{
    public class PushNotification
    {
        public string DeviceId { get; set; }
        public NotificationBaseDto Data { get; set; }
    }
}
