﻿using TravelRecon.Domain.Localization;

namespace TravelRecon.Api.Model
{
    public class Language
    {
        public string Name { get; set; }
        public string Lang { get; set; }
        public LanguageType LanguageType { get; set; }
    }
}
