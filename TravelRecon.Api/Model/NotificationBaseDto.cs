﻿namespace TravelRecon.Api.Model
{
    public class NotificationBaseDto
    {
        public string Alert { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
        public string Badge => "increment";
        public bool Sound => true;
        public virtual NotificationType Type => NotificationType.Base;
    }
}
