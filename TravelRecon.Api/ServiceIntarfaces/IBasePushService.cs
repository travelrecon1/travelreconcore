﻿using System;
using System.Collections.Generic;
using TravelRecon.Api.Model;

namespace TravelRecon.Api.ServiceIntarfaces
{
    public interface IBasePushService: IDisposable
    {
        void Push(IEnumerable<PushNotification> notification);
        void Push(PushNotification notification);
    }
}
