﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TravelRecon.Api.Model;
using TravelRecon.Domain.Localization;

namespace TravelRecon.Api.ServiceIntarfaces
{
    public interface ITranslateService : ILanguageTranslator
    {
        Task<string> TranslateAsync(string text, LanguageType source, LanguageType target);
        Task<string> TranslateAsync(string text, LanguageType target);
        IEnumerable<string> TranslateList(string[] text, LanguageType source, LanguageType target);
        IEnumerable<string> TranslateList(string[] text, LanguageType target);
        Task<IEnumerable<string>> TranslateListAsync(string[] text, LanguageType source, LanguageType target);
        Task<IEnumerable<string>> TranslateListAsync(string[] text, LanguageType target);
        IEnumerable<Language> GetLanguages();
        Task<IEnumerable<Language>> GetLanguagesAsync();
    }
}
