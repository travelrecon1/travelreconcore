﻿using System.Linq;
using System.Threading.Tasks;
using TravelRecon.Common.Models;

namespace TravelRecon.Common.Extensions
{
    public static class QueryExtensions
    {
        public static Task<RangeResult<T>> ToRangeResultAsync<T>(this IQueryable<T> query, int skip, int take)
        {
            var count = query.Count();

            var result = new RangeResult<T>
            {
                TotalCount = count,
                Items = query.Skip(skip).Take(take).ToList()
            };

            return Task.FromResult(result);
        }
    }
}
