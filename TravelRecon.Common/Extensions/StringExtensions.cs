﻿namespace TravelRecon.Common.Extensions
{
    public static class StringExtensions
    {
        public static string GetValueOrEmpty(this string str)
        {
            return str ?? string.Empty;
        }
    }
}
