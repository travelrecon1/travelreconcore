﻿namespace TravelRecon.Common.Enums
{
    public enum RoleEnum
    {
        None = 0,
        User = 1,
        Administrator = 2
    }
}
