﻿namespace TravelRecon.Common.Enums
{
    public enum HistoryTypeEnum
    {
        Default,
        Destination,
        Country,
        User
    }
}
