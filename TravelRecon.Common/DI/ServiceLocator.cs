﻿using System;
using System.Collections.Generic;

namespace TravelRecon.Common.DI
{
    public static class ServiceLocator
    {
        public static T Resolve<T>()
        {
            return Resolver.Resolve<T>();
        }

        public static IEnumerable<T> ResolveAll<T>()
        {
            return Resolver.ResolveAll<T>();
        }

        public static object Resolve(Type type)
        {
            return Resolver.Resolve(type);
        }

        public static void Release(object instance)
        {
            Resolver.Release(instance);
        }

        public static void SetResolver(IComponentResolver resolver)
        {
            Resolver = resolver;
        }

        private static IComponentResolver Resolver { get; set; }
    }
}
