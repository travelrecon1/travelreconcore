﻿using System;
using System.Collections.Generic;

namespace TravelRecon.Common.DI
{
    public interface IComponentResolver
    {
        T Resolve<T>();

        object Resolve(Type type);

        void Release(object instance);

        IEnumerable<T> ResolveAll<T>();

        IEnumerable<object> ResolveAll(Type t);
    }
}