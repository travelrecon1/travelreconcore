﻿using System;
using System.Collections.Generic;
using System.Linq;
using Castle.Windsor;

namespace TravelRecon.Common.DI
{
    public class WindsorComponentResolver : IComponentResolver
    {
        protected internal IWindsorContainer Container { get; private set; }

        public WindsorComponentResolver(IWindsorContainer container)
        {
            Container = container;
        }

        public T Resolve<T>()
        {
            return Container.Resolve<T>();
        }

        public object Resolve(Type type)
        {
            return Container.Resolve(type);
        }

        public void Release(object instance)
        {
            Container.Release(instance);
        }

        public IEnumerable<T> ResolveAll<T>()
        {
            return Container.ResolveAll<T>();
        }

        public IEnumerable<object> ResolveAll(Type t)
        {
            return Container.ResolveAll(t).Cast<object>();
        }
    }
}
