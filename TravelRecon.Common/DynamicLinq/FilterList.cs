﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace TravelRecon.Common.DynamicLinq
{
    /// <summary>
    /// Represents a filterList expression of DataSource.
    /// </summary>
    [DataContract]
    public class FilterList
    {
       
        [DataMember(Name = "field")]
        public string Field { get; set; }

        [DataMember(Name = "listType")]
        public String ListType { get; set; }

        [DataMember(Name = "idList")]
        public List<int> IdList { get; set; }

        [DataMember(Name = "stringList")]
        public List<int> StringList { get; set; }
    }

    
}
