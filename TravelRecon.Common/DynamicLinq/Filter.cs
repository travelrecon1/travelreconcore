using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using TravelRecon.Common.Exceptions;

namespace TravelRecon.Common.DynamicLinq
{
    public static class FilterTypes
    {
        public static readonly string String = "string";
        public static readonly string Int = "int";
        public static readonly string Double = "double";
        public static readonly string Decimal = "decimal";
        public static readonly string Guid = "guid";
        public static readonly string DateTime = "datetime";
        public static readonly string Boolean = "boolean";
        public static readonly string StringList = "stringList";
        public static readonly string NumberList = "numberList";
    }

    /// <summary>
    /// Represents a filter expression of DataSource.
    /// </summary>
    [DataContract]
    public class Filter
    {
        /// <summary>
        /// Gets or sets the name of the sorted field (property). Set to <c>null</c> if the <c>Filters</c> property is set.
        /// </summary>
        [DataMember(Name = "field")]
        public string Field { get; set; }

        /// <summary>
        /// Gets or sets the filtering operator. Set to <c>null</c> if the <c>Filters</c> property is set.
        /// </summary>
        [DataMember(Name = "operator")]
        public string Operator { get; set; }

        /// <summary>
        /// Gets or sets the filtering value. Set to <c>null</c> if the <c>Filters</c> property is set.
        /// </summary>
        [DataMember(Name = "value")]
        public object Value { get; set; }

        /// <summary>
        /// Gets or sets the filtering logic. Can be set to "or" or "and". Set to <c>null</c> unless <c>Filters</c> is set.
        /// </summary>
        [DataMember(Name = "logic")]
        public string Logic { get; set; }

        [DataMember(Name = "type")]
        public string Type { get; set; }

        /// <summary>
        /// Gets or sets the child filter expressions. Set to <c>null</c> if there are no child expressions.
        /// </summary>
        [DataMember(Name = "filters")]
        public IEnumerable<Filter> Filters { get; set; }

        /// <summary>
        /// Mapping of DataSource filtering operators to Dynamic Linq
        /// </summary>
        protected virtual IDictionary<string, string> Operators => new Dictionary<string, string>
        {
            {"eq", "="},
            {"neq", "!="},
            {"lt", "<"},
            {"lte", "<="},
            {"gt", ">"},
            {"gte", ">="},
            {"startswith", "StartsWith"},
            {"endswith", "EndsWith"},
            {"contains", "Contains"},
            {"doesnotcontain", "Contains"},
            {"in","in" }
        };

        public virtual object GetValue()
        {
            if (string.IsNullOrWhiteSpace(Type) && string.IsNullOrWhiteSpace(Logic))
                throw new BusinessLogicException("Type required");

            if (Value == null)
                return null;

            if (string.Equals(Type, FilterTypes.NumberList, StringComparison.InvariantCultureIgnoreCase))
            {
                return String.Join(",", Value);
            }
            if (string.Equals(Type, FilterTypes.StringList, StringComparison.InvariantCultureIgnoreCase))
            {
                return "'" + String.Join("','", Value) + "'";
            }

            var str = Value.ToString();

            if (string.Equals(Type, FilterTypes.Int, StringComparison.InvariantCultureIgnoreCase))
                return int.Parse(str);

            if (string.Equals(Type, FilterTypes.Double, StringComparison.InvariantCultureIgnoreCase))
                return double.Parse(str);

            if (string.Equals(Type, FilterTypes.Decimal, StringComparison.InvariantCultureIgnoreCase))
                return decimal.Parse(str);

            if (string.Equals(Type, FilterTypes.Guid, StringComparison.InvariantCultureIgnoreCase))
                return Guid.Parse(str);

            if (string.Equals(Type, FilterTypes.DateTime, StringComparison.InvariantCultureIgnoreCase))
                return DateTime.Parse(str);

            if (string.Equals(Type, FilterTypes.Boolean, StringComparison.InvariantCultureIgnoreCase))
                return bool.Parse(str);

            if (string.Equals(Type, FilterTypes.String, StringComparison.InvariantCultureIgnoreCase))
                return str;

            throw new BusinessLogicException($"{Type} not supported");
        }

        /// <summary>
        /// Get a flattened list of all child filter expressions.
        /// </summary>
        public IList<Filter> All()
        {
            var filters = new List<Filter>();

            Collect(filters);

            return filters;
        }

        private void Collect(IList<Filter> filters)
        {
            if (Filters != null && Filters.Any())
            {
                foreach (Filter filter in Filters)
                {
                    filters.Add(filter);

                    filter.Collect(filters);
                }
            }
            else
            {
                filters.Add(this);
            }
        }

        /// <summary>
        /// Converts the filter expression to a predicate suitable for Dynamic Linq e.g. "Field1 = @1 and Field2.Contains(@2)"
        /// </summary>
        /// <param name="filters">A list of flattened filters.</param>
        public virtual string ToExpression(IList<Filter> filters)
        {
            if (Filters != null && Filters.Any())
            {
                return "(" + String.Join(" " + Logic + " ", Filters.Select(filter => filter.ToExpression(filters)).ToArray()) + ")";
            }

            int index = filters.IndexOf(this);

            string comparison = Operators[Operator];
            if (Operator == "in")
            {
                return $"{Field} {comparison}(@{index})";
            }
            
            if (Operator == "doesnotcontain")
            {
                return $"!{Field}.{comparison}(@{index})";
            }

            if (comparison == "StartsWith" || comparison == "EndsWith" || comparison == "Contains")
            {
                return $"{Field}.{comparison}(@{index})";
            }

            return $"{Field} {comparison} @{index}";
        }
    }
}
