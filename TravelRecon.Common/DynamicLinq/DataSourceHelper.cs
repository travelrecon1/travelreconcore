﻿using System;
using System.Collections.Generic;
using System.Linq;
using Castle.Core.Internal;

namespace TravelRecon.Common.DynamicLinq
{
    public static class DataSourceHelper
    {
        public static List<Filter> GetFiltersByField(this DataSourceRequest request, string field)
        {
            return GetFiltersByField(request.Filter, field);
        }

        public static void RemoveFiltersByField(this DataSourceRequest request, string field)
        {
            RemoveFiltersByField(request.Filter, field);

            if (request.Filter != null && !request.Filter.Filters.Any())
                request.Filter = null;
        }

        private static void RemoveFiltersByField(Filter filter, string field)
        {
            if (filter == null)
                return;

            filter.Filters = filter.Filters.Where(x => !string.Equals(x.Field, field, StringComparison.InvariantCultureIgnoreCase));
            filter.Filters.ForEach(f => RemoveFiltersByField(f, field));
        }

        private static List<Filter> GetFiltersByField(Filter filter, string field)
        {
            if (filter == null)
                return new List<Filter>();

            var findedFilters =
                filter.Filters.Where(x => string.Equals(x.Field, field, StringComparison.InvariantCultureIgnoreCase));

            var childFilters = filter.Filters.Select(f => GetFiltersByField(f, field)).SelectMany(fl => fl);

            return findedFilters.Union(childFilters)
                .ToList();
        }
    }
}
