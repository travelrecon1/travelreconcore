﻿using System;

namespace TravelRecon.Common.Exceptions
{
    public class ValidationException : BusinessLogicException
    {
        public ValidationException()
        {
        }

        public ValidationException(string msg): base(msg)
        {
        }

        public ValidationException(string msg, Exception innerException): base(msg, innerException)
        {
        }
    }
}
