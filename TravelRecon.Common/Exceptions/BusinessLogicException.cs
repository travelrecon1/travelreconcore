﻿using System;

namespace TravelRecon.Common.Exceptions
{
    public class BusinessLogicException: Exception
    {
        public BusinessLogicException()
        {
        }

        public BusinessLogicException(string msg): base(msg)
        {
        }

        public BusinessLogicException(string msg, Exception innerException): base(msg, innerException)
        {
        }
    }
}
