﻿using System.Text;
using TravelRecon.Common.Helpers.Json;

namespace TravelRecon.Common.Helpers.Objects
{
    public static class ObjectHelper
    {
        public static byte[] GetBytes(this object obj)
        {
            if (obj == null)
                return null;

            return Encoding.UTF8.GetBytes(JsonHelper.Serialize(obj));
        }
    }
}
