﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace TravelRecon.Common.Helpers.Json
{
    public static class JsonHelper
    {
        public static string Serialize<T>(T obj)
        {
            return JsonConvert.SerializeObject(obj, new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            });
        }
    }
}
