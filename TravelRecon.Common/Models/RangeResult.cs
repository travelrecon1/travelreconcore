﻿using System.Collections.Generic;

namespace TravelRecon.Common.Models
{
    public class RangeResult<T>
    {
        public int TotalCount { get; set; }
        public IList<T> Items { get; set; }
    }
}
