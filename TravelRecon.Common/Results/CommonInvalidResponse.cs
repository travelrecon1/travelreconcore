﻿using System;
using System.Collections.Generic;

namespace TravelRecon.Common.Results
{
    public class CommonValidationItem
    {
        public string Message { get; set; }
        public string Exception { get; set; }
    }

    public class CommonInvalidResponse
    {
        public Exception Exception { get; set; }
        public Dictionary<string, CommonValidationItem[]> ModelState { get; set; }

        public string Message { get; set; }

        public CommonInvalidResponse(string msg)
        {
            this.Message = msg;
        }

        public CommonInvalidResponse(string msg, Exception exception) : this(msg)
        {
            this.Exception = exception;
        }

        public CommonInvalidResponse(string msg, Dictionary<string, CommonValidationItem[]> modelSate) : this(msg)
        {
            this.ModelState = modelSate;
        }
    }
}
