﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TravelRecon.Logger
{
    public interface ILogger
    {
        void Fatal(string fatal);
        void Error(Exception e);
        void Error(string error);
        void Warn(string warn);
        void Info(string info);
        void Trace(string trace);
        void Debug(string debug);
    }
}
