using System;

namespace TravelRecon.Logger.Exceptions
{
    internal class FatalException : Exception
    {
        public FatalException(string ex) : base(ex)
        {
        }
    }
}