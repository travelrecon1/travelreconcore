﻿using System;

namespace TravelRecon.Logger.Exceptions
{
    internal class WarnException : Exception
    {
        public WarnException(string ex) : base(ex)
        {
        }
    }
}