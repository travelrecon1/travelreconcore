﻿using System;

namespace TravelRecon.Logger.Exceptions
{
    internal class TraceException : Exception
    {
        public TraceException(string ex) : base(ex)
        {

        }
    }
}