﻿using System;

namespace TravelRecon.Logger.Exceptions
{
    internal class DebugException : Exception
    {
        public DebugException(string ex) : base(ex)
        {
        }
    }
}