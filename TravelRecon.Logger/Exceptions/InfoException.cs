using System;

namespace TravelRecon.Logger.Exceptions
{
    internal class InfoException : Exception
    {
        public InfoException(string ex) : base(ex)
        {

        }
    }
}