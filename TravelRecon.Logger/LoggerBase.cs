﻿using System;
using TravelRecon.Logger.Exceptions;

namespace TravelRecon.Logger
{
    public abstract class LoggerBase : ILogger
    {
        // JEN protected string ApplicationName => ConfigurationManager.AppSettings["applicationName"];

        protected abstract void Raise(Exception ex);

        public void Fatal(string fatal)
        {
            Raise(new FatalException(fatal));
        }

        public void Error(Exception e)
        {
            Raise(e);
        }

        public void Error(string error)
        {
            Raise(new Exception(error));
        }

        public void Warn(string warn)
        {
            Raise(new WarnException(warn));
        }

        public void Info(string info)
        {
            Raise(new InfoException(info));
        }

        public void Trace(string trace)
        {
            Raise(new TraceException(trace));
        }

        public void Debug(string debug)
        {
            Raise(new DebugException(debug));
        }
    }
}
