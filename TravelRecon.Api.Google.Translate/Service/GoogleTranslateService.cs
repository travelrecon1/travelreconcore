﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Google;
using Google.Apis.Services;
using Google.Apis.Translate.v2;
using Google.Apis.Translate.v2.Data;
using TravelRecon.Api.Model;
using TravelRecon.Api.ServiceIntarfaces;
using TravelRecon.Domain;
using TravelRecon.Domain.Localization;

namespace TravelRecon.Api.Google.Translate.Service
{
    public class GoogleTranslateService: ITranslateService
    {
        private readonly TranslateService _translateService;

        public GoogleTranslateService() 
            : this(ConfigSettings.GoogleTranslateApiKey, ConfigSettings.GoogleTranslateApplicationName) {
        }

        public GoogleTranslateService(string apiKey, string applicationName) {
            _stringToLangTypeMap = _langTypeToStringMap.ToDictionary(x => x.Value, x => x.Key);

            _translateService = new TranslateService(new BaseClientService.Initializer {
                ApiKey = apiKey,
                ApplicationName = applicationName
            });
        }

        private readonly Dictionary<LanguageType, string> _langTypeToStringMap = new Dictionary<LanguageType, string>
        {
            { LanguageType.English, "EN" },
            { LanguageType.Spanish, "ES" },
            { LanguageType.Portuguese, "PT" },
            { LanguageType.French, "FR" },
        };

        private readonly Dictionary<string, LanguageType> _stringToLangTypeMap;

        private string GetLang(LanguageType languageType)
        {
            if (!_langTypeToStringMap.ContainsKey(languageType))
                throw new NotSupportedException();

            return _langTypeToStringMap[languageType];
        }

        private LanguageType GetLangType(string lang)
        {
            return _stringToLangTypeMap.ContainsKey(lang) ? _stringToLangTypeMap[lang] : LanguageType.None;
        }

        private IEnumerable<string> GetTranslatedText(TranslationsListResponse listResponse)
        {
            return listResponse.Translations.Select(x => x.TranslatedText);
        }

        private async Task<string> TranslateAsync(string text, string source, string target)
        {
            try
            {
                var result = await TranslateListAsync(new[] {text}, source, target);
                return result.FirstOrDefault();
            }
            catch (GoogleApiException e)
            {
                //log it
                return text;
            }
        }

        private string Translate(string text, string source, string target)
        {
            try
            {
                var result = TranslateList(new[] { text }, source, target);
                return result.FirstOrDefault();
            }
            catch (GoogleApiException e)
            {
                //log it
                return text;
            }
        }

        private IEnumerable<string> TranslateList(string[] text, string source, string target)
        {
            try
            {
                var request = _translateService.Translations.List(text, target);
                request.Source = source;
                var translatedList = request.Execute();
                return GetTranslatedText(translatedList);
            }
            catch (GoogleApiException e)
            {
                //log it
                return text;
            }
        }

        private async Task<IEnumerable<string>> TranslateListAsync(string[] text, string source, string target)
        {
            try
            {
                var request = _translateService.Translations.List(text, target);
                request.Source = source;
                var translatedList = await request.ExecuteAsync();
                return GetTranslatedText(translatedList);
            }
            catch (GoogleApiException e)
            {
                //log it
                return text;
            }
        }

        public Task<string> TranslateAsync(string text, LanguageType source, LanguageType target)
        {
            return TranslateAsync(text, GetLang(source), GetLang(target));
        }

        public Task<string> TranslateAsync(string text, LanguageType target)
        {
            return TranslateAsync(text, null, GetLang(target));
        }

        public string Translate(string text, LanguageType source, LanguageType target)
        {
            return Translate(text, GetLang(source), GetLang(target));
        }

        public string Translate(string text, LanguageType target)
        {
            return Translate(text, null, GetLang(target));
        }

        public IEnumerable<string> TranslateList(string[] text, LanguageType source, LanguageType target)
        {
            return TranslateList(text, GetLang(source), GetLang(target));
        }

        public IEnumerable<string> TranslateList(string[] text, LanguageType target)
        {
            return TranslateList(text, null, GetLang(target));
        }

        public Task<IEnumerable<string>> TranslateListAsync(string[] text, LanguageType source, LanguageType target)
        {
            return TranslateListAsync(text, GetLang(source), GetLang(target));
        }

        public Task<IEnumerable<string>> TranslateListAsync(string[] text, LanguageType target)
        {
            return TranslateListAsync(text, null, GetLang(target));
        }

        public IEnumerable<Language> GetLanguages()
        {
            var languagesList = _translateService.Languages.List().Execute();
            return languagesList.Languages.Select(x => new Language { Lang = x.Language, Name = x.Name, LanguageType = GetLangType(x.Language) });
        }

        public async Task<IEnumerable<Language>> GetLanguagesAsync()
        {
            var languagesList = await _translateService.Languages.List().ExecuteAsync();
            return languagesList.Languages.Select(x => new Language { Lang = x.Language, Name = x.Name, LanguageType = GetLangType(x.Language) });
        }
    }
}
