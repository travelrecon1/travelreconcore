﻿using System.Collections.Generic;
using TravelRecon.Domain.Localization;

namespace TravelRecon.Localization.Job.Classes
{
    public class LocalizationObject
    {
        public int Id { get; set; }
        public int FkId { get; set; }

        public string InsertScriptTemplate { get; set; }

        private string SetParam(string str, string paramName, object value)
        {
            return str.Replace($"#{paramName}#", value.ToString());
        }

        public string GetInserScript(LanguageType languageType)
        {
            var script = InsertScriptTemplate;
            script = SetParam(script, "FK_ID", FkId);
            script = SetParam(script, "LANGUAGE_TYPE", (int)languageType);

            foreach (var item in Dictionary[languageType])
                script = SetParam(script, item.Key, item.Value);

            return script;
        }

        public Dictionary<LanguageType, Dictionary<string, string>> Dictionary { get; set; }
    }
}
