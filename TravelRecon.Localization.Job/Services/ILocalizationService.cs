﻿using System.Collections.Generic;
using TravelRecon.Domain.Localization;
using TravelRecon.Localization.Job.Classes;

namespace TravelRecon.Localization.Job.Services
{
    public interface ILocalizationService
    {
        Dictionary<string, List<LocalizationObject>> GetLocalizationObjects(LanguageType language);
        LocalizationObject Translate(LocalizationObject localizationObj, LanguageType from, LanguageType to);
        List<LocalizationObject> Translate(List<LocalizationObject> localizationObjs, LanguageType from, LanguageType to);
    }
}
