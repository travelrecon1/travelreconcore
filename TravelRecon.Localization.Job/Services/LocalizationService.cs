﻿using System.Collections.Generic;
using System.Linq;
using TravelRecon.Api.ServiceIntarfaces;
using TravelRecon.Data;
using TravelRecon.Domain.Localization;
using TravelRecon.Localization.Job.Classes;

namespace TravelRecon.Localization.Job.Services
{
    public class LocalizationService : ILocalizationService
    {
        private readonly ITranslateService _translateService;
        private readonly TravelReconDbContext _context;

        private string labelsInsertScriptTemplate = "INSERT INTO [TravelRecon].[dbo].[LabelTranslations] (LabelFk, LanguageTypeFk, Content) VALUES (#FK_ID#, #LANGUAGE_TYPE#, '#CONTENT#')";
        private string alertTypesInserScriptTemplate = "INSERT INTO [TravelRecon].[dbo].[AlertTypeTranslations] (AlertTypeFk, LanguageTypeFk, Name) VALUES (#FK_ID#, #LANGUAGE_TYPE#, '#NAME#')";
        private string alertTypeCategoryInsertScriptTemplate = "INSERT INTO [TravelRecon].[dbo].[AlertTypeCategoryTranslations] (AlertTypeCategoryFk, LanguageTypeFk, Name) VALUES (#FK_ID#, #LANGUAGE_TYPE#, '#NAME#')";
        private string destinationsInsertScriptTemplate = "INSERT INTO [TravelRecon].[dbo].[DestinationTranslations] (DestinationId, Language, City, Description) VALUES (#FK_ID#, #LANGUAGE_TYPE#, '#CITY#', '#DESCRIPTION#')";
        private string countriesInsertScriptTemplate = "INSERT INTO [TravelRecon].[dbo].[CountryTranslations] (CountryId, Language, Name) VALUES (#FK_ID#, #LANGUAGE_TYPE#, '#NAME#')";
        private string intelReportTypesInsertScriptTemplate = "INSERT INTO [TravelRecon].[dbo].[IntelReportTypeTranslations] (IntelReportTypeFk, LanguageTypeFk, Name, Description) VALUES (#FK_ID#, #LANGUAGE_TYPE#, '#NAME#', '#DESCRIPTION#')";
        private string userRelationshipStatusInsertScriptTemplate = "INSERT INTO [TravelRecon].[dbo].[UserRelationshipStatusTranslations] (UserRelationshipStatusFk, LanguageTypeFk, Label, Description) VALUES (#FK_ID#, #LANGUAGE_TYPE#, '#LABEL#', '#DESCRIPTION#')";

        public LocalizationService(ITranslateService translateService, TravelReconDbContext context)
        {
            _translateService = translateService;
            _context = context;
        }

        public Dictionary<string, List<LocalizationObject>> GetLocalizationObjects(LanguageType language)
        {
            var result = new Dictionary<string, List<LocalizationObject>>();

            var labels = _context.Labels
                .SelectMany(x => x.Translations)
                .Where(x => x.Language == language && x.LabelFk != (int)LabelType.Eula)
                .ToList();

            result.Add(nameof(labels), labels.Select(x => new LocalizationObject
            {
                Id = x.Id,
                FkId = x.LabelFk,
                InsertScriptTemplate = labelsInsertScriptTemplate,
                Dictionary = new Dictionary<LanguageType, Dictionary<string, string>> {
                    {
                        language,
                        new Dictionary<string, string>
                        {
                            { "CONTENT", x.Content }
                        }
                    }
                }
            }).ToList());

            var alertTypes = _context.AlertTypeTranslations
                .Where(x => x.Language == language)
                .ToList();

            result.Add(nameof(alertTypes), alertTypes.Select(x => new LocalizationObject
            {
                Id = x.Id,
                FkId = x.AlertTypeFk,
                InsertScriptTemplate = alertTypesInserScriptTemplate,
                Dictionary = new Dictionary<LanguageType, Dictionary<string, string>> {
                    {
                        language,
                        new Dictionary<string, string>
                        {
                            { "NAME", x.Name }
                        }
                    }
                }
            }).ToList());

            var alertTypeCategories = _context.AlertCategoryTypes
                .SelectMany(x => x.Translations)
                .Where(x => x.Language == language)
                .ToList();

            result.Add(nameof(alertTypeCategories), alertTypeCategories.Select(x => new LocalizationObject
            {
                Id = x.Id,
                FkId = x.AlertTypeCategoryFk,
                InsertScriptTemplate = alertTypeCategoryInsertScriptTemplate,
                Dictionary = new Dictionary<LanguageType, Dictionary<string, string>> {
                    {
                        language,
                        new Dictionary<string, string>
                        {
                            { "NAME", x.Name }
                        }
                    }
                }
            }).ToList());

            var destinations = _context.DestinationTranslations
                .Where(x => x.Language == language)
                .ToList();

            result.Add(nameof(destinations), destinations.Select(x => new LocalizationObject
            {
                Id = x.Id,
                FkId = x.DestinationId,
                InsertScriptTemplate = destinationsInsertScriptTemplate,
                Dictionary = new Dictionary<LanguageType, Dictionary<string, string>> {
                    {
                        language,
                        new Dictionary<string, string>
                        {
                            { "CITY", x.City },
                            { "DESCRIPTION", x.Description }
                        }
                    }
                }
            }).ToList());

            var countries = _context.CountryTranslations
                .Where(x => x.Language == language)
                .ToList();

            result.Add(nameof(countries), countries.Select(x => new LocalizationObject
            {
                Id = x.Id,
                FkId = x.CountryId,
                InsertScriptTemplate = countriesInsertScriptTemplate,
                Dictionary = new Dictionary<LanguageType, Dictionary<string, string>> {
                    {
                        language,
                        new Dictionary<string, string>
                        {
                            { "NAME", x.Name }
                        }
                    }
                }
            }).ToList());

            var intelReportTypes = _context.IntelReportTypeTranslations
                .Where(x => x.Language == language)
                .ToList();

            result.Add(nameof(intelReportTypes), intelReportTypes.Select(x => new LocalizationObject
            {
                Id = x.Id,
                FkId = x.IntelReportTypeFk,
                InsertScriptTemplate = intelReportTypesInsertScriptTemplate,
                Dictionary = new Dictionary<LanguageType, Dictionary<string, string>> {
                    {
                        language,
                        new Dictionary<string, string>
                        {
                            { "NAME", x.Name },
                            { "DESCRIPTION", x.Description },
                        }
                    }
                }
            }).ToList());

            var userRelationshipStatus = _context.UserRelationshipStatus
                .SelectMany(x => x.Translations)
                .Where(x => x.Language == language)
                .ToList();

            result.Add(nameof(userRelationshipStatus), userRelationshipStatus.Select(x => new LocalizationObject
            {
                Id = x.Id,
                FkId = x.UserRelationshipStatusFk,
                InsertScriptTemplate = userRelationshipStatusInsertScriptTemplate,
                Dictionary = new Dictionary<LanguageType, Dictionary<string, string>> {
                    {
                        language,
                        new Dictionary<string, string>
                        {
                            { "LABEL", x.Label },
                            { "DESCRIPTION", x.Description },
                        }
                    }
                }
            }).ToList());

            return result;
        }

        public List<LocalizationObject> Translate(List<LocalizationObject> localizationObjs, LanguageType from, LanguageType to)
        {
            var allString = localizationObjs.SelectMany(x => x.Dictionary[from].Values)
                .Where(x => !string.IsNullOrWhiteSpace(x))
                .Distinct()
                .ToArray();
            var translations = _translateService.TranslateList(allString, from, to).ToArray();

            var translationsDictionary = new Dictionary<string, string>();
            for (int i = 0; i < allString.Length; i++)
                translationsDictionary.Add(allString[i], translations[i]);

            foreach (var obj in localizationObjs)
            {
                if (!obj.Dictionary.ContainsKey(to))
                    obj.Dictionary.Add(to, new Dictionary<string, string>());

                foreach (var item in obj.Dictionary[from])
                {
                    if (!obj.Dictionary[to].ContainsKey(item.Key))
                        obj.Dictionary[to].Add(item.Key, "");

                    if (string.IsNullOrWhiteSpace(obj.Dictionary[from][item.Key]))
                        continue;

                    obj.Dictionary[to][item.Key] = translationsDictionary[obj.Dictionary[from][item.Key]];
                }
            }

            return localizationObjs;
        }

        public LocalizationObject Translate(LocalizationObject localizationObj, LanguageType from, LanguageType to)
        {
            if (!localizationObj.Dictionary.ContainsKey(to))
                localizationObj.Dictionary.Add(to, new Dictionary<string, string>());

            foreach (var item in localizationObj.Dictionary[from])
            {
                if (!localizationObj.Dictionary[to].ContainsKey(item.Key))
                    localizationObj.Dictionary[to].Add(item.Key, "");

                localizationObj.Dictionary[to][item.Key] = _translateService.Translate(item.Value, from, to);
            }

            return localizationObj;
        }
    }
}
