﻿using TravelRecon.Localization.Job.Services;

namespace TravelRecon.Localization.Job.Installers
{
    public class ServicesInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Component.For<TravelReconDbContext>()
                    .LifestylePerThread());

            container.Register(
                Component.For<IComponentResolver>()
                     .ImplementedBy<WindsorComponentResolver>()
                     .LifestyleTransient());

            container.Register(
                Component.For<ITranslateService>()
                     .ImplementedBy<GoogleTranslateService>()
                     .LifestyleTransient());

            container.Register(
                Component.For<ILocalizationService>()
                     .ImplementedBy<LocalizationService>()
                     .LifestyleTransient());
        }
    }
}


