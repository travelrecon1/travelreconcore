﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TravelRecon.AppServices.Ecommerce.Dtos;
using TravelRecon.AppServices.IntelReports;
using TravelRecon.AppServices.IntelReports.ViewDtos;
using TravelRecon.AppServices.SharedDtos;
using TravelRecon.AppServices.SubscriptionOffers.Queries;
using TravelRecon.AppServices.SubscriptionOffers.ViewDtos;
using TravelRecon.AppServices.Users.ViewDtos;
using TravelRecon.Common.DynamicLinq;
using TravelRecon.Data;
using TravelRecon.Domain.Ecommerce;
using TravelRecon.Domain.Identity;
using TravelRecon.Domain.IntelReports;
using TravelRecon.Domain.RepositoryInterfaces;
using TravelRecon.Domain.SubscriptionOffers;
using TravelRecon.Web.Areas.SubscriptionsManagment.Models;

namespace TravelRecon.AppServices.SubscriptionsManagment
{
    public class UserSubscriptionService:IAppService
    {
        private readonly IRepository<UserSubscription> _userSubscriptonRepository;
        private readonly IRepository<UserDestinations> _userDestinationsRepository;
        private readonly IRepository<SubscriptionOffer> _offerRepository;
        private readonly IRepository<Destination> _destinationsRepository;
        private readonly DestinationAppService _destinationAppService;
        private readonly IRepository<User> _usersRepository;
        private readonly TravelReconDbContext _dbContext;
        private readonly IRepository<StripePayment> _paymentsRepository;

        public UserSubscriptionService(IRepository<UserSubscription> userSubscriptonRepository,
                                       IRepository<UserDestinations> userDestinationsRepository,
                                       IRepository<SubscriptionOffer> offerRepository,
                                       IRepository<Destination> destinationsRepository,
                                       DestinationAppService destinationAppService,
                                       IRepository<User> usersRepository,
                                       IRepository<StripePayment> paymentsRepository)
        {
            _userSubscriptonRepository = userSubscriptonRepository;
            _userDestinationsRepository = userDestinationsRepository;
            _offerRepository = offerRepository;
            _destinationsRepository = destinationsRepository;
            _destinationAppService = destinationAppService;
            _usersRepository = usersRepository;
            _paymentsRepository = paymentsRepository;
        }

        public async Task<DataSourceResult<ManageUserSubscriptionModel>> GetPaymentHistoryDataSourceAsync(int id, DataSourceRequest request)
        {
            return await _userSubscriptonRepository.GetAll()
                .Where(order => order.UserFk == id && order.IsActivated)
                .OrderByDescending(x => x.ExpirationDate)
                .SelectManageUserSubscriptionVm().ToDataSourceResultAsync(request);
        }

        public async Task<ManageUserSubscriptionModel> GetActiveSubscription(int userId)
        {
            var subscription = await _userSubscriptonRepository.GetAll()
                .Where(order => order.UserFk == userId && order.IsActivated)
                .OrderByDescending(x => x.ExpirationDate)
                .SelectManageUserSubscriptionVm().FirstOrDefaultAsync();
            if (subscription != null)
            {
                subscription.Destinations = subscription.SubscriptionType == SubscriptionTypes.Monthly
                    ? _userDestinationsRepository.GetAll()
                        .Where(x => x.UserFk == userId)
                        .OrderByDescending(x => x.ExpirationDate)
                        .Select(destination => new PureDestinationDto()
                        {
                            City = destination.City,
                            ExpirationDate = destination.ExpirationDate
                            
                        }).ToList()
                    : new List<PureDestinationDto>();
                var pureDestinationDto = subscription.Destinations.FirstOrDefault();
                if (pureDestinationDto != null && pureDestinationDto.ExpirationDate != null)
                    subscription.ExpirationDate = pureDestinationDto.ExpirationDate.Value;
            }
            

            return subscription;

        }

        public async Task<List<LocalizedDestination>> GetDestinationsForSetUp()
        {
            return await _destinationAppService.GetAllDestinationsAsync();
        }

        private UserSubscription GetUserSubscription(User currentUser, SubscriptionTypes subscriptionType)
        {
            if (subscriptionType == SubscriptionTypes.Annual)
            {
                var existingSubscriptions = _userSubscriptonRepository.GetAll()
                    .Where(x => x.UserFk == currentUser.Id
                    && x.IsActivated
                    && x.SubscriptionType == SubscriptionTypes.Annual
                    && x.ExpirationDate.HasValue
                    && x.ExpirationDate >= DateTime.UtcNow)
                    .ToList();

                // if user has active annual subscription we should update it
                if (existingSubscriptions.Any())
                {
                    var latestSubscription = existingSubscriptions.OrderBy(x => x.ExpirationDate).Last();
                    latestSubscription.ExpirationDate = latestSubscription.ExpirationDate.Value.AddYears(1);
                    return latestSubscription;
                }

                // otherwise create a new subscription
                return new UserSubscription
                {
                    UserFk = currentUser.Id,
                    IsActivated = true, // set Active to true in difference with strine charge method
                    SubscriptionType = SubscriptionTypes.Annual,
                    PurchaseDate = DateTime.UtcNow,
                    ExpirationDate = DateTime.UtcNow.AddYears(1)
                };
            }

            return new UserSubscription
            {
                UserFk = currentUser.Id,
                IsActivated = true,
                SubscriptionType = SubscriptionTypes.Monthly,
                PurchaseDate = DateTime.UtcNow
            };
        }

        private int CountPaymentAmount(decimal priceUsd)
        {
            return decimal.ToInt32(priceUsd * 100);
        }

        public async Task AddSubscription(int id, SubscriptionDto purchase)
        {
            var currentUser = _usersRepository.Get(id);
            var subscription = GetUserSubscription(currentUser, purchase.SubscriptionType);
            decimal totalPriceDecimal;
            var paymentDescription = "Subscription purchase. Username: " + currentUser.UserName +
                                     "; Subscription type: ";
            if (purchase.SubscriptionType == SubscriptionTypes.Annual)
            {
                var annualOffer = _offerRepository.GetAll()
                    .FirstOrDefault(x => x.SubscriptionType.HasValue
                                         && x.SubscriptionType.Value == SubscriptionTypes.Annual
                                         && x.Price.HasValue);

                totalPriceDecimal = annualOffer?.Price ?? 129;
            }
            else
            {
                var destinations = _destinationsRepository.GetAll()
                    .Where(x => purchase.DestinationIds.Contains(x.Id))
                    .ToList();

                totalPriceDecimal = destinations.Select(x => x.Cost).ToList().Sum();

            }

            var payment = new StripePayment
            {
                UserFk = currentUser.Id,
                AmountPaid = totalPriceDecimal,
                Date = DateTime.UtcNow,
                PaymentType = PaymentTypes.Other,
                IsPaid = false,
                IsTestPayment = false,
                PaymentId = "add from admin site_" + Guid.NewGuid(),
                ReceiptEmail = "travel recon support email",
                Status = "Added by admin"
            };

            var newPayment = _paymentsRepository.Add(payment);

            subscription.PaymentFk = newPayment.Id;


            if (subscription.Id == 0)
                _userSubscriptonRepository.Add(subscription);
            else
                _userSubscriptonRepository.Modified(subscription);

            // if it was monthly subscription we need to update the ExpirationDate and PaymentFk for existing user destinations and
            // create new user destination if needed 
            if (purchase.SubscriptionType == SubscriptionTypes.Monthly)
            {
                var existingDestinations = _userDestinationsRepository.GetAll()
                    .Where(x => x.UserFk == id
                                && x.ExpirationDate.HasValue
                                && x.DestinationId.HasValue
                                && purchase.DestinationIds.Contains(x.DestinationId.Value))
                    .ToList();

                if (existingDestinations.Any())
                {
                    foreach (var existingDestination in existingDestinations)
                    {
                       
                        existingDestination.ExpirationDate = existingDestination.ExpirationDate.Value.AddMonths(1);                     
                        existingDestination.SubscriptionFk = subscription.Id;
                    }
                }

                var existingDestinationIds = existingDestinations.Select(x => x.DestinationId.Value).ToList();

                // get destinations that are not in the UserDestinations table
                var newDestinations = await _destinationsRepository.GetAll()
                    .Where(x => purchase.DestinationIds.Contains(x.Id) && !existingDestinationIds.Contains(x.Id))
                    .SelectLocalized()
                    .ToListAsync();

                if (newDestinations.Any())
                {
                    var userDestinations = newDestinations.Select(UserDestinations.ConvertFrom).Select(x =>
                    {
                        x.UserFk = currentUser.Id;
                        x.IsAvailable = true;
                        x.ReceiveNotifications = true;
                        x.SubscriptionFk = subscription.Id;
                        x.ExpirationDate = DateTime.Now.AddMonths(1);
                        return x;
                    }).ToList();

                    _userDestinationsRepository.AddRange(userDestinations);
                }
            }

            _userDestinationsRepository.SaveChanges();
        }

        public async Task AddSubscriptions(BulkSubscriptionDto purchase)
        {
            using (_userSubscriptonRepository.BeginTransaction())
            {
                foreach (var user in purchase.UsersIds)
                {
                    await this.AddSubscription(user, purchase);
                }

                _userSubscriptonRepository.CommitTransaction();
            }          
        }
    }
}
