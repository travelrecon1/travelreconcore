﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using TravelRecon.AppServices.Mail;
using TravelRecon.AppServices.Mail.ViewDtos;
using TravelRecon.AppServices.Points;
using TravelRecon.AppServices.Profile.ViewDtos;
using TravelRecon.Domain.Identity;
using TravelRecon.Domain.Localization;
using TravelRecon.Domain.Localization.QueryObjects;
using TravelRecon.Domain.Points;
using TravelRecon.Domain.RepositoryInterfaces;

namespace TravelRecon.AppServices.Profile
{
    public class ProfileService : IAppService
    {
        private readonly IRepository<Label> _labelRepository;
        private readonly PointService _pointService;
        private readonly MailService _mailService;
        private readonly AppSettingsService _appSettingsService;

        public ProfileService(IRepository<Label> labelRepository, PointService pointService, MailService mailService, AppSettingsService appSettingsService)
        {
            _labelRepository = labelRepository;
            _pointService = pointService;
            _mailService = mailService;
            _appSettingsService = appSettingsService;
        }

        public InviteUsersPageViewModel GetInviteUsersPageViewModel(User currentUser)
        {
            var targetLang = currentUser.GetLanguageSetting();
            var labels = new[]
            {
                LabelType.SeparateEmailsWithAComma, LabelType.PersonalizeYourEmail, LabelType.InviteContacts,
                LabelType.SendInvite, LabelType.JoinMyTeamAndHelp, LabelType.YouEarned, LabelType.Points, LabelType.InviteContactsCaption
            };

            var localized = _labelRepository.GetAll().FindLabelsLocalizedDictionary(labels, targetLang);

            return new InviteUsersPageViewModel
            {
                Labels = localized
            };
        }

        private bool ValidateEmailAddress(string emailAddress)
        {
            var reg = new Regex(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$");
            return reg.IsMatch(emailAddress);
        }

        public SendInvitationsResultViewModel SendInvitations(User currentUser, string emails, string emailBody)
        {
            var targLang = currentUser.GetLanguageSetting();

            var labels = new[] {LabelType.EmailAddressWasntSentTo, LabelType.IncorrectEmailAdresses, LabelType.DownloadTheApp };
            var localizedLabels = _labelRepository.GetAll().FindLabelsLocalizedDictionary(labels, targLang);

            var result = new SendInvitationsResultViewModel();

            var emailSeporators = new[] {',', ';', ' '};
            var recepients = emails.Split(emailSeporators, StringSplitOptions.RemoveEmptyEntries).Select(m => m.Trim());

            if (recepients.Any(x => !ValidateEmailAddress(x)) || string.IsNullOrWhiteSpace(emails))
            {
                result.Errors.Add(localizedLabels[(int)LabelType.IncorrectEmailAdresses]);
                return result;
            }

            var recepientsDictionary=new HashSet<string>();

            foreach (var rec in recepients)
            {
                try
                {
                    if (!recepientsDictionary.Contains(rec))
                    {
                        recepientsDictionary.Add(rec);
                        var isAlreadyInvited=_mailService.SendFriendInvite(currentUser, rec, emailBody, currentUser.GetLanguageSetting());

                        if(!isAlreadyInvited)
                            result.Points += _pointService.Create(PointType.Referral, currentUser);
                    }
                }
                catch (Exception e)
                {
                    //log it

                    result.Errors.Add($"{localizedLabels[(int)LabelType.EmailAddressWasntSentTo]} {rec}");
                }

               
            }

            return result;
        }
    }
}
