﻿using System.Collections.Generic;

namespace TravelRecon.AppServices.Profile.ViewDtos
{
    public class InviteUsersPageViewModel
    {
        public Dictionary<int, string> Labels { get; set; }
    }
}
