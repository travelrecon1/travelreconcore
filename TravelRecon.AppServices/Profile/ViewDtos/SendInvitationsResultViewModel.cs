﻿using TravelRecon.AppServices.SharedDtos;

namespace TravelRecon.AppServices.Profile.ViewDtos
{
    public class SendInvitationsResultViewModel: BaseApiResultViewModel
    {
        public int Points { get; set; }
    }
}
