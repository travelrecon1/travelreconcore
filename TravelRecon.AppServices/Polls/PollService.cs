﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using TravelRecon.AppServices.Polls.ViewDtos;
using TravelRecon.Common.DynamicLinq;
using TravelRecon.Common.Exceptions;
using TravelRecon.Domain.Polls;
using TravelRecon.Domain.RepositoryInterfaces;

namespace TravelRecon.AppServices.Polls
{
    public class PollService: IAppService
    {
        private readonly IRepository<Poll> _pollRepository;
        private readonly IRepository<PollAnswer> _pollAnswerRepository;

        public PollService(
            IRepository<Poll> pollRepository,
            IRepository<PollAnswer> pollAnswerRepository
            )
        {
            _pollRepository = pollRepository;
            _pollAnswerRepository = pollAnswerRepository;
        }

        public Task<DataSourceResult<PollDto>> GetManagedPollsAsync(DataSourceRequest request, int userId)
        {
            return _pollRepository.GetAll()
                .FindByCreatedById(userId)
                .SelectPollDto()
                .ToDataSourceResultAsync(request);
        }

        public Task<DataSourceResult<PollAnswerDto>> GetManagedPollAnswersAsync(DataSourceRequest request, int userId)
        {
            return _pollAnswerRepository.GetAll()
                .Where(x => x.Poll.CreatedById == userId)
                .SelectPollAnswerDto()
                .ToDataSourceResultAsync(request);
        }

        public Task<List<PollDto>> GetUserPollsAsync(int userId)
        {
            return _pollRepository.GetAll()
                .Where(x => x.PollAnswers.Any(pa => pa.UserId == userId && pa.Status == PollAnswerStatusEnum.Pending))
                .SelectPollDto()
                .ToListAsync();
        }

        public Task<List<PollAnswerDto>> GetPollAnswersByPollIdAsync(int pollId)
        {
            return _pollAnswerRepository.GetAll()
                .FindByPollId(pollId)
                .SelectPollAnswerDto()
                .ToListAsync();
        }

        public async Task CreatePollAsync(CreatePollVm model, int userId)
        {
            if (!model.UsersIds.Any())
                throw new ValidationException("Users Ids required");

            using (_pollRepository.BeginTransaction())
            {
                var poll = new Poll
                {
                    CreatedById = userId,
                    CreatedDate = DateTime.UtcNow,
                    ExpirationDate = model.ExpirationDate,
                    Question = model.Question
                };

                _pollRepository.Add(poll);
                await _pollRepository.SaveChangesAsync();

                var pollAnswers = model.UsersIds.Select(x => new PollAnswer
                {
                    UserId = x,
                    CreatedDate = DateTime.UtcNow,
                    Answer = string.Empty,
                    UpdatedDate = DateTime.UtcNow,
                    PollId = poll.Id,
                    Status = PollAnswerStatusEnum.Pending
                });

                _pollAnswerRepository.AddRange(pollAnswers);
                await _pollAnswerRepository.SaveChangesAsync();

                _pollRepository.CommitTransaction();
            }
        }

        public async Task<PollAnswerDto> CreatePollAnswerAsync(CreatePollAnswerVm model, int userId)
        {
            var pollAnswer = await _pollAnswerRepository.GetAll()
                .FindByUserId(userId)
                .FindByPollId(model.PollId)
                .FirstOrDefaultAsync();

            pollAnswer.Answer = model.Answer;
            pollAnswer.Status = PollAnswerStatusEnum.Answered;
            pollAnswer.UpdatedDate = DateTime.UtcNow;

            _pollAnswerRepository.Modified(pollAnswer);
            await _pollAnswerRepository.SaveChangesAsync();

            return await _pollAnswerRepository.GetAll()
                .Where(x => x.PollId == model.PollId && x.UserId == userId)
                .SelectPollAnswerDto()
                .FirstOrDefaultAsync();
        }

        public Task<PollDto> GetPollAsync(int pollId)
        {
            return _pollRepository.GetAll()
                .Where(x => x.Id == pollId)
                .SelectPollDto()
                .FirstOrDefaultAsync();
        }
    }
}
