﻿using System;

namespace TravelRecon.AppServices.Polls.ViewDtos
{
    public class PollDto
    {
        public int Id { get; set; }
        public int CreatedById { get; set; }
        public string Creator { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ExpirationDate { get; set; }
        public string Question { get; set; }
        public string CreatorAvatarUrl { get; set; }

        public int AnsweredCount { get; set; }
        public int NotAnsweredCount { get; set; }

        public bool IsClosed => ExpirationDate < DateTime.UtcNow;
    }
}
