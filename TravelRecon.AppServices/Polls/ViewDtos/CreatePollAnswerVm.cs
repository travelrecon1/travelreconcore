﻿using System.ComponentModel.DataAnnotations;

namespace TravelRecon.AppServices.Polls.ViewDtos
{
    public class CreatePollAnswerVm
    {
        [Required]
        public int PollId { get; set; }
        [Required]
        public string Answer { get; set; }
    }
}
