﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TravelRecon.AppServices.Polls.ViewDtos
{
    public class CreatePollVm
    {
        [Required]
        public DateTime ExpirationDate { get; set; }
        [Required]
        public string Question { get; set; }
        [Required]
        public int[] UsersIds { get; set; }
    }
}
