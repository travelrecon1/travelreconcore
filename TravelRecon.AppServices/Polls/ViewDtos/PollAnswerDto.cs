﻿using System;
using TravelRecon.Domain.Polls;

namespace TravelRecon.AppServices.Polls.ViewDtos
{
    public class PollAnswerDto
    {
        public int Id { get; set; }
        public int PollId { get; set; }
        public int UserId { get; set; }

        public string UserFullName { get; set; }
        public string UserAvatarUrl { get; set; }

        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }

        public string Answer { get; set; }
        public int Status { get; set; }
    }
}
