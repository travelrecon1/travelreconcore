﻿using System.Linq;
using LinqKit;
using TravelRecon.AppServices.Polls.ViewDtos;
using TravelRecon.AppServices.Users.Queries;
using TravelRecon.Domain.Polls;

namespace TravelRecon.AppServices.Polls
{
    public static class PollQueries
    {
        public static IQueryable<Poll> FindByCreatedById(this IQueryable<Poll> query, int userId)
        {
            return query.Where(x => x.CreatedById == userId);
        }

        public static IQueryable<PollDto> SelectPollDto(this IQueryable<Poll> query)
        {
            var q = query.AsExpandable();
            var userNameExpr = UserQueries.GetFullNameExpr();

            return q.Select(x => new PollDto
            {
                CreatedDate = x.CreatedDate,
                ExpirationDate = x.ExpirationDate,
                Creator = userNameExpr.Invoke(x.CreatedBy),
                Id = x.Id,
                CreatedById = x.CreatedById,
                Question = x.Question,
                AnsweredCount = x.PollAnswers.Count(pa => pa.Status == PollAnswerStatusEnum.Answered),
                NotAnsweredCount = x.PollAnswers.Count(pa => pa.Status == PollAnswerStatusEnum.Pending),
                CreatorAvatarUrl = "http://www.snappertech.com/images/prashant-surana.jpg"
            });
        }

        public static IQueryable<PollAnswer> FindByPollId(this IQueryable<PollAnswer> query, int pollId)
        {
            return query.Where(x => x.PollId == pollId);
        }

        public static IQueryable<PollAnswer> FindByUserId(this IQueryable<PollAnswer> query, int userId)
        {
            return query.Where(x => x.UserId == userId);
        }

        public static IQueryable<PollAnswerDto> SelectPollAnswerDto(this IQueryable<PollAnswer> query)
        {
            var q = query.AsExpandable();
            var userNameExpr = UserQueries.GetFullNameExpr();

            return q.Select(x => new PollAnswerDto
            {
                CreatedDate = x.CreatedDate,
                UpdatedDate = x.UpdatedDate,
                UserFullName = userNameExpr.Invoke(x.User),
                Id = x.Id,
                UserId = x.UserId,
                Status = (int)x.Status,
                Answer = x.Answer,
                PollId = x.PollId,
                UserAvatarUrl = "http://www.snappertech.com/images/prashant-surana.jpg"
            });
        }
    }
}
