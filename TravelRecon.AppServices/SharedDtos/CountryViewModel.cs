﻿using System.Collections.Generic;
using TravelRecon.AppServices.IntelReports.ViewDtos;

namespace TravelRecon.AppServices.SharedDtos
{
    public class CountryViewModel
    {
        public string Name { get; set; }
        public IList<DestinationDto> DestinationsList { get; set; }
        public double RiskFactor { get; internal set; }
    }
}
