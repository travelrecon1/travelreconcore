﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TravelRecon.Domain.Identity;

namespace TravelRecon.AppServices.SharedDtos
{
    public class ClientCompanyDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string LegalName { get; set; }
        public string Abbreviation { get; set; }
        public int ClientFk { get; set; }
        public string ClientFirstName { get; set; }
        public string ClientLastName { get; set; }
        public string ClientTitle { get; set; }
        public string ClientDepartment { get; set; }
        public string PhoneNumber { get; set; }
        public string MobileNumber { get; set; }
        public string Email { get; set; }
        public string EmergencyInformation { get; set; }
        public string Notes { get; set; }
        public int NumberOfUsersLicenses { get; set; }
        public int NumberOfManagersLicenses { get; set; }

        public bool IsClient { get; set; }
        public bool IsAdministrator { get; set; }

        public DateTime UserLicenseExpirationDate { get; set; }

        public AddressDto ClientPhysicalAddress { get; set; }
        public int? ClientPhysicalAddressId { get; set; }
    }
}
