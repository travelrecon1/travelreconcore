﻿namespace TravelRecon.AppServices.SharedDtos
{
    public enum DeviceTypeEnum
    {
        None = 0,
        Android = 1,
        Ios = 2
    }
}
