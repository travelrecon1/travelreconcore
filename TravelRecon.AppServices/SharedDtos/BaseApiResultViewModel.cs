﻿using System.Collections.Generic;
using System.Linq;

namespace TravelRecon.AppServices.SharedDtos
{
    public class BaseApiResultViewModel
    {
        public BaseApiResultViewModel()
        {
            Errors = new List<string>();
        }

        public bool Success { get { return string.IsNullOrWhiteSpace(Error); } }
        public string Error { get { return Errors.FirstOrDefault(); } }
        public IList<string> Errors { get; set; }
    }
}
