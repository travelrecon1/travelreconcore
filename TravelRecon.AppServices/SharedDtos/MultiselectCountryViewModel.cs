﻿using System.Collections.Generic;
using TravelRecon.AppServices.IntelReports.ViewDtos;

namespace TravelRecon.AppServices.SharedDtos
{
    public class MultiselectCountryViewModel
	{
        public string Name { get; set; }
        public IList<UserDestinationDto> DestinationsList { get; set; } 
    }
}
