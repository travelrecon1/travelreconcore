﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TravelRecon.AppServices.SharedDtos
{
    public class CreateClientCompanyVm
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string LegalName { get; set; }
        [Required]
        public string Abbreviation { get; set; }
        [Required]
        public string ClientFirstName { get; set; }
        [Required]
        public string ClientLastName { get; set; }
        [Required]
        public string ClientTitle { get; set; }
        [Required]
        public string ClientDepartment { get; set; }
        [Required]
        public AddressDto ClientPhysicalAddress { get; set; }
        public int? ClientPhysicalAddressId { get; set; }
        [Required]
        public string ClientCity { get; set; }
        [Required]
        public string ClientState { get; set; }
        [Required]
        public string ClientCounry { get; set; }
        [Required]
        public string PhoneNumber { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        public string Notes { get; set; }
    }
}
