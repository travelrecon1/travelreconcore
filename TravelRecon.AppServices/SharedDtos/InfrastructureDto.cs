﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TravelRecon.AppServices.Infrastructure.Dtos;

namespace TravelRecon.AppServices.SharedDtos
{
    public class InfrastructureDto
    {
        public int Id { get; set; }
        public int TypeId { get; set; }

        public  string Name { get; set; }

        public string Country { get; set; }

        public string City { get; set; }

        public double Lat { get; set; }

        public double Long { get; set; }

        public int UserId { get; set; }

        public string Type { get; set; }
        public string TypeIcon { get; set; }

        public double RiskRating { get; set; }

        public List<CustomFieldsValuesDto> CustomFieldsValuesCollection { get; set; }
        public string Description { get; set; }
        public string Address { get; set; }
    }
}
