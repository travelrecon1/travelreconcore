﻿using System.Collections.Generic;

namespace TravelRecon.AppServices.SharedDtos
{
    public class InfrastructureTypeDto
    {
        public InfrastructureTypeDto()
        {
            CustomFields = new  List<string>();
        }
        public  int Id { get; set; }
        public string Name { get; set; }
        public string Icon { get; set; }

        public List<string> CustomFields { get; set; }
    }
}
