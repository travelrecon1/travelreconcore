﻿namespace TravelRecon.AppServices.SharedDtos
{
    public class DeviceDto
    {
        public DeviceDto()
        {
        }

        public DeviceDto(string deviceId, int deviceType)
        {
            DeviceId = deviceId;
            DeviceType = (DeviceTypeEnum) deviceType;
        }

        public string DeviceId { get; set; }
        public DeviceTypeEnum DeviceType { get; set; }
    }
}
