﻿using TravelRecon.Domain.Tutorial;

namespace TravelRecon.AppServices.SharedDtos
{
    public abstract class BasePageViewModel
    {
        public bool IsViewed { get; set; }
        public abstract PageTypeEnum PageType { get; }
    }
}
