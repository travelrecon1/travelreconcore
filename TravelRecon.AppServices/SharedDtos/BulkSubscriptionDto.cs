﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TravelRecon.AppServices.SharedDtos
{
    public class BulkSubscriptionDto: SubscriptionDto
    {
        public List<int> UsersIds { get; set; }
    }
}
