﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TravelRecon.Domain.SubscriptionOffers;

namespace TravelRecon.AppServices.SharedDtos
{
    public class SubscriptionDto
    {
        public SubscriptionTypes SubscriptionType { get; set; }
        public List<int> DestinationIds { get; set; }
    }
}
