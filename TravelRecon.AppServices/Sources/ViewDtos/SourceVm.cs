﻿namespace TravelRecon.AppServices.Sources.ViewDtos
{
    public class SourceVm
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double? AvgRealibility { get; set; }
        public int? Rating { get; set; }
    }
}
