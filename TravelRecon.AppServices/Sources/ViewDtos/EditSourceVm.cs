﻿using System.ComponentModel.DataAnnotations;

namespace TravelRecon.AppServices.Sources.ViewDtos
{
    public class EditSourceVm
    {
        [Required]
        public string Name { get; set; }

        public int Rating { get; set; }
    }
}
