﻿using System;

namespace TravelRecon.AppServices.Sources.ViewDtos
{
    public class AlertSourceVm
    {
        public int Id { get; set; }
        public int SourceId { get; set; }
        public int AlertId { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public string Creator { get; set; }
        public int Realibility { get; set; }
    }
}
