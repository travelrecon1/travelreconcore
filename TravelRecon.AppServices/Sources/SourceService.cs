﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using TravelRecon.AppServices.Sources.Queries;
using TravelRecon.AppServices.Sources.ViewDtos;
using TravelRecon.Domain.Alerts;
using TravelRecon.Domain.RepositoryInterfaces;

namespace TravelRecon.AppServices.Sources
{
    public class SourceService: IAppService
    {
        private readonly IRepository<Source> _sourceRepository;
        private readonly IRepository<AlertSource> _alertSourceRepository;

        public SourceService(
            IRepository<Source> sourceRepository,
            IRepository<AlertSource> alertSourceRepository)
        {
            _sourceRepository = sourceRepository;
            _alertSourceRepository = alertSourceRepository;
        }

        public Task<SourceVm> GetAsync(int id)
        {
            return _sourceRepository.GetAll()
                .SelectVm()
                .FirstOrDefaultAsync(x => x.Id == id);
        }

        public Task<List<SourceVm>> GetListAsync()
        {
            return _sourceRepository.GetAll()
                .SelectVm()
                .ToListAsync();
        }

        public async Task<SourceVm> CreateAsync(CreateSourceVm model)
        {
            var source = new Source
            {
                Name = model.Name,
                Rating = model.Rating
            };

            _sourceRepository.Add(source);
            await _sourceRepository.SaveChangesAsync();

            return await GetAsync(source.Id);
        }

        public async Task<SourceVm> EditAsync(int id, EditSourceVm model)
        {
            var source = await _sourceRepository.GetAll()
                .FirstOrDefaultAsync(x => x.Id == id);

            source.Name = model.Name;
            source.Rating = model.Rating;
            
            _sourceRepository.Modified(source);
            await _sourceRepository.SaveChangesAsync();

            return await GetAsync(id);
        }

        public async Task DeleteAsync(int id)
        {
            var source = await _sourceRepository.GetAll()
                .FirstOrDefaultAsync(x => x.Id == id);

            _sourceRepository.Delete(source);
            await _sourceRepository.SaveChangesAsync();
        }

        public Task<List<AlertSourceVm>> GetAlertSourcesAsync(int sourceId)
        {
            return _alertSourceRepository.GetAll()
                .SelectVm()
                .Where(x => x.SourceId == sourceId)
                .ToListAsync();
        }
    }
}
