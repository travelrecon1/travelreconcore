﻿using System.Linq;
using TravelRecon.AppServices.Sources.ViewDtos;
using TravelRecon.Domain.Alerts;

namespace TravelRecon.AppServices.Sources.Queries
{
    public static class SourceExtensions
    {
        public static IQueryable<SourceVm> SelectVm(this IQueryable<Source> query)
        {
            return query.Select(x => new SourceVm
            {
                Id = x.Id,
                Name = x.Name,
                AvgRealibility = x.AlertSources.Average(s => s.Realibility),
                Rating = x.Rating
            });
        }

        public static IQueryable<AlertSourceVm> SelectVm(this IQueryable<AlertSource> query)
        {
            return query.Select(x => new AlertSourceVm
            {
                Id = x.Id,
                CreatedBy = x.CreatedBy,
                AlertId = x.AlertId,
                CreatedOn = x.CreatedOn,
                Creator = x.Creator.UserName,
                SourceId = x.SourceId,
                Realibility = x.Realibility
            });
        }
    }
}
