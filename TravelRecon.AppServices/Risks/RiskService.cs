﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using TravelRecon.AppServices.IntelReports;
using TravelRecon.AppServices.SharedDtos;
using TravelRecon.Common.Enums;
using TravelRecon.Common.Exceptions;
using TravelRecon.Data.Repositories;
using TravelRecon.Domain.Identity;
using TravelRecon.Domain.IntelReports;
using TravelRecon.Domain.Localization;
using TravelRecon.Domain.RepositoryInterfaces;
using TravelRecon.Domain.Risk;

namespace TravelRecon.AppServices.Risks
{
    public class RiskService : IAppService
    {
        private readonly DestinationAppService _destinationAppService;
        private readonly IRepository<RiskRatingHistory> _riskHistoryRepository;
        private readonly IRepository<User> _userRepository;
        private readonly IRepository<Domain.IntelReports.Country> _countryRepository;
        private readonly IRepository<Destination> _destinationRepository;

        public RiskService(
            IRepository<User> userRepository,
            IRepository<Domain.IntelReports.Country> countryRepository,
            IRepository<Destination> destinationRepository,
            DestinationAppService destinationAppService,
            IRepository<RiskRatingHistory> riskHistoryRepository)
        {
            _destinationAppService = destinationAppService;
            _riskHistoryRepository = riskHistoryRepository;
            _userRepository = userRepository;
            _countryRepository = countryRepository;
            _destinationRepository = destinationRepository;
        }

        public async Task SaveUserRisk(int id, double risk)
        {
            var user = await _userRepository.GetAll()
                .FirstOrDefaultAsync(x => x.Id == id);

            user.RiskFactor = risk;
            _userRepository.Modified(user);

            await _userRepository.SaveChangesAsync();
        }

        public async Task SaveDestinationRisk(int id, double risk, int currentUserId)
        {
            var destination = await _destinationRepository.GetAll()
                .FirstOrDefaultAsync(x => x.Id == id);

            var currentRiskFactor = destination.RiskFactor;
            destination.RiskFactor = risk;
            _destinationRepository.Modified(destination);

            await _destinationRepository.SaveChangesAsync();

            var shift = (risk - currentRiskFactor ?? 0) < 0
              ? Math.Abs(risk - currentRiskFactor ?? 0)
              : (risk - currentRiskFactor ?? 0);
            await SaveRiskFactorHistory(HistoryTypeEnum.Destination, currentUserId, risk, shift, destination.Id);
        }

        public async Task SaveCountryRisk(int id, double risk, int currentUserId)
        {
            var country = await _countryRepository.GetAll()
                .FirstOrDefaultAsync(x => x.Id == id);

            var currentRiskFactor = country.RiskFactor;
            country.RiskFactor = risk;
            _countryRepository.Modified(country);
            await _countryRepository.SaveChangesAsync();
            var shift = (risk - currentRiskFactor) < 0 
                ? Math.Abs(risk - currentRiskFactor)
                : (risk - currentRiskFactor);
            await SaveRiskFactorHistory(HistoryTypeEnum.Country, currentUserId, risk, shift, country.Id);
        }

        public async Task<double> GetUserRisk(int userId)
        {
            var user = _userRepository.Get(userId);
            LocalizedDestination destination = new LocalizedDestination();
            if (user.RiskFactor == null)
            {
                if (user.CurrentLocation != null)
                {
                    destination = _destinationAppService.GetDestinationFor(user.CurrentLocation, LanguageType.English);
                }               
                else
                {
                    destination = null;
                } 
                if (destination != null)
                    user.RiskFactor = destination.RiskFactor;

                if (user.RiskFactor != null) return user.RiskFactor.Value;
            }
            else
            {
                return user.RiskFactor.Value;
            }

            if (user.RiskFactor == null && destination != null)
            {
                var risk = await _countryRepository
               .GetAll()
               .Where(x => x.Id == destination.CountryId)
               .Select(x => x.RiskFactor)
               .FirstOrDefaultAsync();

                return risk;
            }

            return user.RiskFactor ?? 0;
        }

        public async Task<double> GetDestinationRisk(int destinationId)
        {
            var destination = _destinationRepository.Get(destinationId);
            if (destination != null && destination.RiskFactor == null)
            {
                var risk = await _countryRepository
                .GetAll()
                .Where(x => x.Id == destination.CountryId)
                .Select(x => x.RiskFactor)
                .FirstOrDefaultAsync();

                destination.RiskFactor = risk;

                if (destination.RiskFactor != null)
                    return destination.RiskFactor.Value;
            }

            if (destination.RiskFactor != null)
            {
                return destination.RiskFactor.Value;
            }
            else
            {
                return 0;
            }
        }

        public Task<double> GetCountryRisk(int countryId)
        {
            return _countryRepository
                   .GetAll()
                   .Where(x => x.Id == countryId)
                   .Select(x => x.RiskFactor)
                   .FirstOrDefaultAsync();
        }


        public Task SaveRiskFactorHistory(
            HistoryTypeEnum type,
            int userId, double newRate,
            double shift,
            int entityId)
        {
           return Task.Run(() =>
            {

                try
                {
                    var historyTask = new RiskRatingHistory()
                    {
                        UserId = userId,
                        CurrentValue = newRate,
                        Shift = shift,
                        Created = DateTimeOffset.UtcNow
                    };

                    if (type == HistoryTypeEnum.Destination)
                    {
                        historyTask.DestinationId = entityId;
                        historyTask.CountryId = null;
                    }
                    else
                    {
                        historyTask.DestinationId = null;
                        historyTask.CountryId = entityId;
                    }

                    _riskHistoryRepository.Add(historyTask);
                    _riskHistoryRepository.SaveChanges();
                    return true;
                }
                catch (Exception e)
                {
                    throw new BusinessLogicException("Can't add log history");
                }
            });
        }
    }
}
