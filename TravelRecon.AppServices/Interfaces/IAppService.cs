﻿namespace TravelRecon.AppServices.Interfaces
{
    /// <summary>
    /// Exists to indicate which classes are services, and to register them via IoC, accordingly
    /// </summary>
    public interface IAppService { }
}
