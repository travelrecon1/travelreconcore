﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TravelRecon.AppServices.Users;
using TravelRecon.Domain.Groups;

namespace TravelRecon.AppServices.Extensions
{
    public static class UserExtension
    {
        public static UserInviteDto ConvertToUserInviteDto(this UserEmailInvite invite)
        {
            return new UserInviteDto()
            {
                UserFk = invite.UserFk,
                UserGroup = invite.InvitedUserGroup
            };
        }

    }
}
