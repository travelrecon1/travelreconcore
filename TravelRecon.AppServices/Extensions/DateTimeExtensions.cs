﻿using System;

namespace TravelRecon.AppServices.Extensions
{
    public static class DateTimeExtensions
    {
        public static int GetUnixTicks(this DateTime date)
        {
            return (int)date.Subtract(new DateTime(1970, 1, 1)).TotalSeconds;
        }
    }
}
