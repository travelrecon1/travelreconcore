﻿using TravelRecon.AppServices.SharedDtos;
using TravelRecon.Domain.Locale;

namespace TravelRecon.AppServices.Extensions
{
    public static class AddressExtensions
    {
        public static Address ConvertFromDto(this AddressDto dto)
        {
            if (dto == null)
            {
                return null;
            }

            return new Address
            {
                Id = dto.Id,
                Address1 = dto.Address1,
                City = dto.City,
                State = dto.State,
                Country = dto.Country
            };
        }

        public static AddressDto ConvertToDto(this Address entity)
        {
            if (entity == null)
            {
                return null;
            }

            return new AddressDto
            {

                Id = entity.Id,
                Address1 = entity.Address1,
                City = entity.City,
                State = entity.State,
                Country = entity.Country
            };
        }

        public static Address Copy(this Address input)
        {
            if (input == null)
            {
                return null;
            }

            return new Address
            {

                Id = input.Id,
                Address1 = input.Address1,
                City = input.City,
                State = input.State,
                Country = input.Country
            };
        }
    }
}