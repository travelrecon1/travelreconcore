﻿using System.Data.Entity;
using System.Linq;
using TravelRecon.AppServices.SharedDtos;
using TravelRecon.Domain.Identity;
using TravelRecon.Domain.License;

namespace TravelRecon.AppServices.Extensions
{
    public static class ClientCompanyExtensions
    {
        public static IQueryable<ClientCompanyDto> SelectClientCompanyDto(
            this IQueryable<Domain.ClientCompany.ClientCompany> companyQuery, 
            IQueryable<UserLicense> userLicensesQuery)
        {
            return companyQuery.Include(c=>c.ClientPhysicalAddress).Select(x => new ClientCompanyDto()
            {
                Id = x.Id,
                Name = x.Name,
                LegalName = x.LegalName,
                Abbreviation = x.Abbreviation,
                ClientFirstName = x.ClientFirstName,
                ClientLastName = x.ClientLastName,
                ClientTitle = x.ClientTitle,
                ClientDepartment = x.ClientDepartment,
                ClientPhysicalAddress = x.ClientPhysicalAddress == null ? null : new AddressDto
                {
                    Id = x.Id,
                    Address1=x.ClientPhysicalAddress.Address1,
                    City = x.ClientPhysicalAddress.City,
                    State = x.ClientPhysicalAddress.State,
                    Country = x.ClientPhysicalAddress.Country
                },
                ClientPhysicalAddressId = x.ClientPhysicalAddressId,
                PhoneNumber = x.PhoneNumber,
                MobileNumber = x.MobileNumber,
                Email = x.Email,
                EmergencyInformation = x.EmergencyInformation,
                Notes = x.Notes,
                NumberOfUsersLicenses = userLicensesQuery.Count(ul => ul.AssignedById == x.UserId && ul.License.Type != LicenseType.TrpManagersLicense && (!x.AffiliatedClientId.HasValue || ul.ClientCompanyId == x.AffiliatedClientId)),
                NumberOfManagersLicenses = userLicensesQuery.Count(ul => ul.AssignedById == x.UserId && ul.License.Type == LicenseType.TrpManagersLicense && (!x.AffiliatedClientId.HasValue || ul.ClientCompanyId == x.AffiliatedClientId)),

                IsAdministrator = x.User.UserRoles.Any(ur => ur.RoleId == (int)UserRoleTypeEnum.TravelReconProAdministrator),
                IsClient = x.User.UserRoles.Any(ur => ur.RoleId == (int)UserRoleTypeEnum.TRPClient)
            });
        }

        public static Domain.ClientCompany.ClientCompany ConvertFromDto(this ClientCompanyDto company)
        {
            return new Domain.ClientCompany.ClientCompany()
            {
                Name = company.Name,
                LegalName = company.LegalName,
                Abbreviation = company.Abbreviation,
                ClientFirstName = company.ClientFirstName,
                ClientLastName = company.ClientLastName,
                ClientTitle = company.ClientTitle,
                ClientDepartment = company.ClientDepartment,
                ClientPhysicalAddress = company.ClientPhysicalAddress.ConvertFromDto(),
                ClientPhysicalAddressId = company.ClientPhysicalAddressId,
                PhoneNumber = company.PhoneNumber,
                MobileNumber = company.MobileNumber,
                Email = company.Email,
                EmergencyInformation = company.EmergencyInformation,
                Notes = company.Notes,
            };
        }

        public static Domain.ClientCompany.ClientCompany ConvertFromVm(this CreateClientCompanyVm company)
        {
            return new Domain.ClientCompany.ClientCompany()
            {
                Name = company.Name,
                LegalName = company.LegalName,
                Abbreviation = company.Abbreviation,
                ClientFirstName = company.ClientFirstName,
                ClientLastName = company.ClientLastName,
                ClientTitle = company.ClientTitle,
                ClientDepartment = company.ClientDepartment,
                ClientPhysicalAddress = company.ClientPhysicalAddress.ConvertFromDto(),
                ClientPhysicalAddressId=company.ClientPhysicalAddressId,
                PhoneNumber = company.PhoneNumber,
                MobileNumber = "",
                Email = company.Email,
                EmergencyInformation = "",
                Notes = company.Notes
            };
        }
    }
}
