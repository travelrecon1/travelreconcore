﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TravelRecon.AppServices.Users
{
    public class PureUserDto
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
