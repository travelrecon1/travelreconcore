using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using TravelRecon.AppServices.ClientCompany;
using TravelRecon.AppServices.Groups; 
using TravelRecon.AppServices.Groups.ViewDtos;
using TravelRecon.AppServices.Mail;
using TravelRecon.AppServices.Identity;
using TravelRecon.AppServices.Itineraries;
using TravelRecon.AppServices.Missions;
using TravelRecon.AppServices.Permissions;
using TravelRecon.AppServices.Push;
using TravelRecon.AppServices.Risks;
using TravelRecon.AppServices.SharedDtos;
using TravelRecon.AppServices.Users.ViewDtos;
using TravelRecon.Domain.Extensions;
using TravelRecon.Domain.Identity;
using TravelRecon.Domain.Identity.UserRelationships;
using TravelRecon.Domain.Localization;
using TravelRecon.Domain.Localization.QueryObjects;
using TravelRecon.Domain.Points;
using TravelRecon.Domain.RepositoryInterfaces;
using Z.EntityFramework.Plus;
using TravelRecon.AppServices.Points;
using TravelRecon.AppServices.Shared;
using TravelRecon.AppServices.Shared.Extensions;
using TravelRecon.AppServices.Users.Queries;
using TravelRecon.Common.DynamicLinq;
using TravelRecon.Common.Exceptions;
using TravelRecon.Domain.Alerts;
using TravelRecon.Domain.Groups;
using TravelRecon.Domain.IntelReports;
using TravelRecon.Domain.Itinerary;
using TravelRecon.Domain.License;

namespace TravelRecon.AppServices.Users
{
    public class UserService : IAppService
    {
        private readonly IRepository<User> _userRepository;
        private readonly IRepository<Label> _labelRepository;
        private readonly IRepository<Point> _pointRepository;
        private readonly IRepository<UserRelationship> _userRelationshipRepository;
        private readonly IRepository<Rank> _rankRepository;
        private readonly IRepository<AnalystsUser> _assignedUsersRepository;
        private readonly IRepository<Alert> _alertRepository;
        private readonly IRepository<Destination> _destinationRepository;
        private readonly IRepository<UserLicense> _userLicenseRepository;
        private readonly IRepository<License> _licenseRepository;
        private readonly IRepository<UserGroupRelation> _userGroupRelationRepository;
        private readonly IRepository<Itinerary> _itineraryRepository;
        private readonly IRepository<UserDetails> _userDetailsRepository;
        private readonly AppSettingsService _appSettingsService;
        private readonly UserRelationshipService _userRelationshipService;
        private readonly ItinerariesService _itinerariesService;
        private readonly RiskService _riskService;
        private readonly MissionsService _missionsService;
        private readonly MailService _mailService;
        private readonly TravelReconUserManager _userManager;
        private readonly PointService _pointsService;
        private readonly GroupsService _groupsService;

        public UserService(IRepository<User> userRepository,
            IRepository<Label> labelRepository, 
            IRepository<Point> pointRepository,
            IRepository<UserRelationship> userRelationshipRepository,
			IRepository<Rank> rankRepository,
            IRepository<AnalystsUser> assignedUsersRepository,
            IRepository<Alert> alertRepository, 
            IRepository<Destination> destinationRepository, 
            IRepository<UserLicense> userLicenseRepository, 
            IRepository<License> licenseRepository,  
            IRepository<UserGroupRelation> userGroupRelationRepository, 
            IRepository<Itinerary> itineraryRepository,
            IRepository<UserDetails> userDetailsRepository,
            AppSettingsService appSettingsService,
            UserAuthorizationService permissionsService,
            UserRelationshipService userRelationshipService,
            ItinerariesService itinerariesService,
            RiskService riskService,
            MissionsService missionsService,
            MailService mailService,
            TravelReconUserManager userManager, 
            PointService pointsService, GroupsService groupsService)
        {
            _userRepository = userRepository;
            _labelRepository = labelRepository;
            _pointRepository = pointRepository;
            _userRelationshipRepository = userRelationshipRepository;
	        _rankRepository = rankRepository;
            _assignedUsersRepository = assignedUsersRepository;
            _alertRepository = alertRepository;
            _destinationRepository = destinationRepository;
            _userLicenseRepository = userLicenseRepository;
            _licenseRepository = licenseRepository;
            _userGroupRelationRepository = userGroupRelationRepository;
            _itineraryRepository = itineraryRepository;
            _userDetailsRepository = userDetailsRepository;
            _appSettingsService = appSettingsService;
            _userRelationshipService = userRelationshipService;
            _itinerariesService = itinerariesService;
            _riskService = riskService;
            _missionsService = missionsService;
            _mailService = mailService;
            _userManager = userManager;
            _pointsService = pointsService;
            _groupsService = groupsService;
        }

        public Task<List<UserRiskFactorDto>> GetAll()
        {
            return _userRepository.GetAll().Select(x => new UserRiskFactorDto
                {
                    Id = x.Id,
                    RiskFactor = x.RiskFactor,
                    UserName = x.UserName
                })
                .ToListAsync();
        }

        public User GetUser(string username)
        {
            return _userRepository.GetAll().FirstOrDefault(x => x.UserName.Contains(username));
        }

        public User AddNewUser(User user)
        {
            var add = _userRepository.Add(user);
            _userRepository.SaveChanges();
            return add;
        }

        public async Task<IEnumerable<string>> RegisterNewUser(string email, 
            string username,
            string firstname,
            string lastname,
            string password,
            bool isTravelRecon)
        {
            if (string.IsNullOrWhiteSpace(username) || string.IsNullOrWhiteSpace(password))
                return new List<string> { "Username and password cannot be empty" };

            if (isTravelRecon && string.IsNullOrWhiteSpace(email))
                return new List<string> { "Email cannot be empty" };

            if (isTravelRecon && string.IsNullOrWhiteSpace(firstname))
                return new List<string> { "First name cannot be empty" };

            if (isTravelRecon && string.IsNullOrWhiteSpace(lastname))
                return new List<string> { "Last name cannot be empty" };

            var newUser = new User(username);

            if (isTravelRecon)
            {
                newUser.FirstName = firstname;
                newUser.LastName = lastname;
                newUser.Email = email;
                newUser.EmailConfirmed = false;
                newUser.UserRoles.Add(new UserRoles {RoleId = (int)UserRoleTypeEnum.TravelRecon});
            }
            else
            {
                if (!string.IsNullOrWhiteSpace(email))
                    newUser.Email = email;

                newUser.EmailConfirmed = true;
                newUser.UserRoles.Add(new UserRoles { RoleId = (int)UserRoleTypeEnum.GoRecon});
            }

            var result = await _userManager.CreateAsync(newUser, password);

            if (isTravelRecon && result.Succeeded)
                await VerificateEmailAsync(newUser.UserName);

            return result.Errors;
        }

        
        public bool UserHasAccessToSubscription(UserRoleTypeEnum userRole, User user)
        {
            return user.HasRole(userRole);
        }

        public bool IsFirstLogin(User user)
        {
            return user.Language == null;
        }

        public async Task<UserViewModel> GetUserInfoAsync(int userId)
        {
            var points = await _pointRepository.GetAll()
                .Where(m => m.OwnerId == userId)
                .Select(m => m.Value)
                .DefaultIfEmpty()
                .SumAsync(m => m);

            var user = await _userRepository.GetAll()
                .Where(m => m.Id == userId)
                .Include(x => x.UserRoles)
                .FirstOrDefaultAsync();

	        var targetLanguage = user.GetLanguageSetting();

			var ranks = await _rankRepository.GetAll()
                .Include(x => x.Label)
                .ToListAsync();

			var ownedRanks = (from rank in ranks
							  where points >= rank.MinPoints
							  select RankDto.ConvertFrom(rank, targetLanguage)).ToList();

	        RankDto nextRank = null;

			if (ranks.Any(x => x.Id == ownedRanks.Last().Id + 1))
				nextRank = RankDto.ConvertFrom(ranks.FirstOrDefault(x => x.Id == ownedRanks.Last().Id + 1), targetLanguage);

            var availableEmergencyDate = DateTime.UtcNow.AddHours(-4);
            var hasEmergencyOnLastFourHours = await _alertRepository.GetAll()
                .AnyAsync(x => x.IsEmergency 
                    && x.ReportedByFk == userId 
                    && x.ReportedOn > availableEmergencyDate);

            return new UserViewModel
            {
                Id = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                UserName = user.UserName,
                UserRoles = user.UserRoles.Select(UserRoleViewModel.ConvertFrom).ToList(),
                SubscriptionLevelType = new UserRoleViewModel { Id = (int)user.GetMaxReconRole(), Title = user.GetMaxReconRole().ConvertToString() },
                IsAgree = user.AgreementDate != null,
                IsLiabilityComplete = !string.IsNullOrWhiteSpace(user.ElectronicSignature),
                IsFirstLogin = IsFirstLogin(user),
                LocationUpdateInterval = !hasEmergencyOnLastFourHours ? 
                    user.LocationUpdateFrequecySeconds 
                    : Math.Min(user.LocationUpdateFrequecySeconds, 5 * 60),
                Email = user.Email,
                UpgradePackageId = user.HasMaxReconRole(UserRoleTypeEnum.GoRecon) ?
                UserRoleTypeEnum.TravelRecon : (UserRoleTypeEnum?)null,
                Points = points,
                OwnedRanks = ownedRanks,
				NextRank = nextRank,
                Language = targetLanguage,
				ObfuscateLocation = user.ObfuscateLocation,
                MapZoom = user.MapZoom,
                AddressFormatted = user.CurrentAddressFormatted,
                CreatedDate = user.CreatedDate,
                AvatarUrl = "http://www.snappertech.com/images/prashant-surana.jpg",
                FullName = string.IsNullOrWhiteSpace(user.FirstName) && string.IsNullOrWhiteSpace(user.LastName) ?
                    user.UserName : user.FirstName + " " + user.LastName,
                EmergencyEmail = user.EmergencyEmail
            };
        }

        public async Task<DashboardViewModel> GetDashboardViewModel(User currentUser)
        {
            var targetLanguage = currentUser.GetLanguageSetting();

            var points = _pointRepository.GetAll()
                .Where(m => m.OwnerId == currentUser.Id)
                .Select(m => m.Value)
                .DefaultIfEmpty()
                .DeferredSum(m => m)
                .FutureValue();

            var ranks = await _rankRepository.GetAll().Include("Label").ToListAsync();
            var ownedRanks = (from rank in ranks
                              where points.Value >= rank.MinPoints
                              select RankDto.ConvertFrom(rank, targetLanguage)).ToList();

            var viewModel = new DashboardViewModel
            {
                Rank = ownedRanks.Last().Name,
                Points = points.Value,
                RiskRating = Convert.ToInt32(await _riskService.GetUserRisk(currentUser.Id)),
                FriendsRisk = new List<ContactRiskViewModel>(),
                SuggestedMissions = new List<MissionDto>()
            };

            var contacts = _userRelationshipService.GetContacts(currentUser.Id);

            foreach (var contact in contacts)
            {
                if (contact.UserName != "TravelRecon_Support")
                    viewModel.FriendsRisk.Add(new ContactRiskViewModel
                    {
                        Username = string.IsNullOrWhiteSpace(contact.FullName) ? contact.UserName : contact.FullName,
                        Location = contact.Location,
                        Risk = Convert.ToInt32(await _riskService.GetUserRisk(contact.Id))
                    });
            }

            viewModel.FriendsRisk = viewModel.FriendsRisk.OrderByDescending(x => x.Risk).Take(3).ToList();

            var missions = await _missionsService.GetAll(targetLanguage);

            viewModel.SuggestedMissions = missions;
            viewModel.CurrentAddress = currentUser.CurrentAddressFormatted;

            viewModel.UpcomingItinerary = await _itinerariesService.GetUpcomingItineraryForUserAsync(currentUser.Id);

            return viewModel;
        }

        public void SetRadius(int userId, double radius)
        {
            var user = _userRepository.Get(userId);
            user.RadiusInMeters = radius;
            _userRepository.SaveChanges();
        }

        public void SetAgreement(string initials, User currentUser)
        {
            var user = _userRepository.Get(currentUser.Id);
            user.Initials = initials;
            user.AgreementDate = DateTime.UtcNow;
            _userRepository.Modified(user);
            _userRepository.SaveChanges();
        }

        public void SetElectronicSignature(string electronicSignature, User currentUser)
        {
            var user = _userRepository.Get(currentUser.Id);
            user.ElectronicSignature = electronicSignature;
            _userRepository.Modified(user);
            _userRepository.SaveChanges();
        }

        public int GetLocationUpdateFrequency(User currentUser)
        {
            var user = _userRepository.Get(currentUser.Id);
            return user.LocationUpdateFrequecySeconds * 1000;
        }

        public void UpdateDevices(int userId, string devices)
        {
            var user = _userRepository.Get(userId);
            user.Devices = devices;
            _userRepository.Modified(user);
            _userRepository.SaveChanges();
        }

        public User SaveUserLocally(User user, DeviceDto device)
        {
            //set default values for users
            if (user.LocationUpdateFrequecySeconds == 0)
                user.LocationUpdateFrequecySeconds = 30;

            if (user.MaxAlertPopups == 0)
                user.MaxAlertPopups = 5;

            if (user.MapZoom == 0)
                user.MapZoom = 15;

            user.Devices = PushService.UpdateDevices(user.Devices, device);
            _userRepository.Modified(user);

            _userRepository.SaveChanges();

            AddDefaultContact(user);
          
            return user;
        }

        public void AddDefaultContact(User user)
        {
            if (user.UserName.Equals(_appSettingsService.DefaultContact, StringComparison.InvariantCultureIgnoreCase) ||
                !IsFirstLogin(user))
                return;

            var defaultContact = _userRepository.GetAll()
               .FirstOrDefault(x => x.UserName == _appSettingsService.DefaultContact);

            if (defaultContact == null)
            {
                defaultContact = new User(_appSettingsService.DefaultContact)
                {
                    Email = _appSettingsService.SupportEmailAddress
                };
                _userRepository.Add(defaultContact);
                _userRepository.SaveChanges();
            }

            var hasDefaultRelationship = _userRelationshipRepository.GetAll()
              .Any(x => (x.UserInvitedFk == user.Id && x.UserRequesterFk == defaultContact.Id)
                                   || (x.UserInvitedFk == defaultContact.Id && x.UserRequesterFk == user.Id));

            if (hasDefaultRelationship)
                return;

            var defaultRelation = new UserRelationship
            {
                UserInvitedFk = user.Id,
                AcceptedDate = DateTime.UtcNow,
                CreatedDate = DateTime.UtcNow,
                UserRelationshipStatusFk = (int) UserRelationshipStatusEnum.Accepted,
                UserRequesterFk = defaultContact.Id
            };

            _userRelationshipRepository.Add(defaultRelation);
            _userRelationshipRepository.SaveChanges();
        }

	    public bool CheckUsernameExists(string username)
	    {
	        return _userRepository.GetAll().Any(user => user.UserName == username);
	    }

        public bool CheckEmailExists(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
                return false;

            return _userRepository.GetAll().Any(user => user.Email == email);
        }

        public void PostAvatar(User currentUser, byte[] avatar)
        {
            // TODO needs to be reworked
            //var accessToken = GetAccessToken(currentUser.DrupalId);
            //var postUrl = _appSettingsService.AvatarPostUrl;

            //UTF8Encoding encoding = new UTF8Encoding();
            //var postData = new { file = avatar, access_token = accessToken };
            //var postDataJson = JsonConvert.SerializeObject(postData);
            //var postDataBytes = encoding.GetBytes(postDataJson);

            //var request = (HttpWebRequest)WebRequest.Create(postUrl);

            //request.ContentLength = postDataJson.Length;
            //request.Credentials = CredentialCache.DefaultCredentials;
            //request.ContentType = "application/json";
            //request.Method = WebRequestMethods.Http.Post;

            //request.ServerCertificateValidationCallback += (s, ce, ch, ss) => true;

            //using (var stream = request.GetRequestStream())
            //{
            //    stream.Write(postDataBytes, 0, postDataBytes.Length);
            //    stream.Flush();
            //}

            //request.GetResponse();
        }

        public async Task VerificateEmailAsync(string username)
        {
            var user = await _userRepository.GetAll()
                .FirstOrDefaultAsync(x => x.UserName == username);

            if (user == null)
                throw new Exception("User not found");

            var token = await _userManager.GenerateEmailConfirmationTokenAsync(user.Id);

            var emailTemplate = _labelRepository.GetAll()
                .FindLabelsLocalized(new[] { LabelType.EmailVerification }, LanguageType.English)
                .First();

            var emailMessage = string.Format(emailTemplate.Content,
               user.UserName,
               $"{_appSettingsService.WebSiteUrl}/email-verification/{Uri.EscapeDataString(user.UserName)}/{Uri.EscapeDataString(token)}");

            _mailService.SendConfirmationEmail(user.Email, emailMessage, user.Language ?? LanguageType.English);
        }

        public async Task VerificateEmailAsync(string username, string token)
        {
            var user = await _userRepository.GetAll()
                .FirstOrDefaultAsync(x => x.UserName == username);

            if (user == null)
                throw new Exception("User not found");

            var result = await _userManager.ConfirmEmailAsync(user.Id, token);
            result.Validate();
        }

        public async Task<bool> CheckEmailConfirmedAsync(string username)
        {
            var user = await _userRepository.GetAll()
                .FirstOrDefaultAsync(x => x.UserName == username);

            return await _userManager.IsEmailConfirmedAsync(user.Id);
        }

        public async Task RecoverUsernameAsync(string email)
        {
            var user = await _userRepository.GetAll()
                .FirstOrDefaultAsync(x => x.Email == email);

            if (user == null)
                throw new Exception("User not found");

            var emailTemplate = _labelRepository.GetAll()
                .FindLabelsLocalized(new[] { LabelType.RecoverUsernameEmail }, LanguageType.English)
                .First();

            var emailMessage = string.Format(emailTemplate.Content,
                user.UserName,
                $"{_appSettingsService.WebSiteUrl}/login");

            _mailService.SendUsernameRecovery(user.Email, emailMessage, user.GetLanguageSetting());
        }

        private async Task ResetPasswordAsync(User user)
        {
            if (user == null)
                throw new Exception("User not found");

            var token = await _userManager.GeneratePasswordResetTokenAsync(user.Id);

            var emailTemplate = _labelRepository.GetAll()
                .FindLabelsLocalized(new[] { LabelType.ResetPasswordEmail }, LanguageType.English)
                .First();

            var emailMessage = string.Format(emailTemplate.Content,
                user.UserName,
                $"{_appSettingsService.WebSiteUrl}/reset-password-confirmation/{Uri.EscapeDataString(user.UserName)}/{Uri.EscapeDataString(token)}");

            _mailService.SendResetPassword(user.Email, emailMessage, user.GetLanguageSetting());
        }

        public async Task ResetPasswordAsync(int userId)
        {
            var user = await _userRepository.GetAll()
                .FirstOrDefaultAsync(x => x.Id == userId);

            await ResetPasswordAsync(user);
        }

        public async Task ResetPasswordAsync(string username)
        {
            var user = await _userRepository.GetAll()
                .FirstOrDefaultAsync(x => x.Email == username || x.UserName == username);

            await ResetPasswordAsync(user);
        }

        public async Task ResetPasswordConfirmationAsync(string username, string token, string password)
        {
            var user = await _userRepository.GetAll()
                .FirstOrDefaultAsync(x => x.Email == username || x.UserName == username);

            if (user == null)
                throw new Exception("User not found");

            var result = await _userManager.ResetPasswordAsync(user.Id, token, password);
            result.Validate();
        }

        public Task<UserOverviewVm> GetUserOverviewAsync(int userId)
        {
            return _userRepository.GetAll()
                .Where(x => x.Id == userId)
                .Select(x => new UserOverviewVm
                {
                    AddressFormatted = x.CurrentAddressFormatted,
                    FirstName = x.FirstName,
                    LastName = x.LastName,
                    Points = x.Points.Sum(p => p.Value),
                    UserName = x.UserName
                })
                .FirstOrDefaultAsync();
        }

        public async Task ChangePasswordAsync(int userId, string currentPassword, string newPassword)
        {
            var result = await _userManager.ChangePasswordAsync(userId, currentPassword, newPassword);
            result.Validate();
        }

        public async Task UpdateUserInfoAsync(int userId, LanguageType language, string email)
        {
            var user = await _userRepository.GetAll()
                .FirstOrDefaultAsync(x => x.Id == userId);

            user.Language = language;
            user.Email = email;

            _userRepository.Modified(user);

            await _userRepository.SaveChangesAsync();
        }


        public void CheckInvitationAndProceed(RegistarationDto registrationParams)
        {
            var invite = CheckIfUserInvited(registrationParams);
            AddUserToGroup(registrationParams, invite);
        }

        private void AddUserToGroup(RegistarationDto registrationParams, UserInviteDto invite)
        {
            //add new user to group
            var user = GetUser(registrationParams.Username);
            if (invite != null && invite.UserGroup != null)
                _groupsService.AddToGroupAfterRegisterIfInvited(invite.UserFk,
                    new InivteToGroupRequestDto() { GroupId = invite.UserGroup.Value, UserId = user.Id });
        }

        private UserInviteDto CheckIfUserInvited(RegistarationDto registrationParams)
        {
            //if thera are no errors during registration
            //check if user email in
            var invite = _mailService.GetUserEmailInvite(registrationParams.Email);
            if (invite != null)
                _pointsService.Create(PointType.Newcomer, invite.UserFk);
            return invite;
        }

        public Task<DataSourceResult<PersonnelDto>> GetPersonnelDataSourceAsync(DataSourceRequest request, int userId)
        {
            var destinationsQuery = _destinationRepository.GetAll();
            var userLicenseQuery = _userLicenseRepository.GetAll();
            var itirinaryQuery = _itineraryRepository.GetAll();
            var userGroupRelationsQuery = _userGroupRelationRepository.GetAll();

            return _userRepository.GetAll()
                .Where(x => userLicenseQuery.Any(ul => ul.OwnerId == x.Id && ul.ManagerId == userId))
                .SelectPersonnelDto(destinationsQuery, userGroupRelationsQuery, itirinaryQuery, LanguageType.English)
                .OrderBy(x => x.Id)
                .FilterList(request, x => x.Groups)
                .ToDataSourceResultAsync(request);
        }

        public Task<List<PersonnelDto>> GetDashboardPersonnelList(User currentUser)
        {
            var destinationsQuery = _destinationRepository.GetAll();
            var userLicenseQuery = _userLicenseRepository.GetAll();
            var itirinaryQuery = _itineraryRepository.GetAll();
            var userGroupRelationsQuery = _userGroupRelationRepository.GetAll();

            return _userRepository.GetAll()
                .Where(x => userLicenseQuery.Any(ul => ul.OwnerId == x.Id 
                    && ul.ManagerId == currentUser.Id
                    && ul.ActivatedDate.HasValue
                    && ul.ExpirationDate > DateTime.Now))
                .SelectPersonnelDto(destinationsQuery, userGroupRelationsQuery, itirinaryQuery, LanguageType.English)
                .OrderByDescending(x => x.Risk)
                .Take(10)
                .ToListAsync();
        }

        public Task<DataSourceResult<UserListItemDto>> GetUserDataSourceAsync(DataSourceRequest request)
        {
            return _userRepository.GetAll()
                .SelectUserListItemDto()
                .FilterList(request, u => u.Roles)
                .OrderBy(x => x.Id)
                .ToDataSourceResultAsync(request);
        }

        public async Task AddToRolesAsync(int userId, List<string> roles)
        {
            var userRoles = await _userManager.GetRolesAsync(userId);

            if (userRoles.Any())
            {
                var result = await _userManager.RemoveFromRolesAsync(userId, userRoles.ToArray());
                result.Validate();
            }

            if (roles.Any())
            {
                var result = await _userManager.AddToRolesAsync(userId, roles.ToArray());
                result.Validate();
            }
        }

        public async Task ManageUserAsync(int userId, string email, List<string> roles)
        {
            using (_userRepository.BeginTransaction())
            {
                var user = await _userRepository.GetAll()
                  .FirstOrDefaultAsync(x => x.Id == userId);

                user.Email = email;

                _userRepository.Modified(user);

                await AddToRolesAsync(userId, roles);
                await _userRepository.SaveChangesAsync();

                _userRepository.CommitTransaction();
            }
        }

        public Task<TrpUserViewModel> GetTrpUserInfo(int userId)
        {
            // TODO allow to view info only for TRP users

            var alerts = _alertRepository.GetAll();
            var details = _userDetailsRepository.GetAll();
            var licenses = _userLicenseRepository.GetAll();

            return _userRepository.GetAll()
                .Where(x => x.Id == userId)
                .SelectTrpUserViewModel(alerts, details, licenses)
                .FirstOrDefaultAsync();
        }

        public async Task DeleteTrpUserAsync(int userId)
        {
            var user = _userRepository.Get(userId);
            var license = await _userLicenseRepository.GetAll()
                .Where(x => x.OwnerId == user.Id)
                .ToListAsync();

            license.ForEach(x =>
            {
                x.OwnerId = null;
                x.ActivatedDate = null;
                x.AssignedDate = null;
                _userLicenseRepository.Modified(x);
            });

            await _userLicenseRepository.SaveChangesAsync();

            _userRepository.Delete(user);
           await _userRepository.SaveChangesAsync();
        }
    }
}