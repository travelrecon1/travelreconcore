﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TravelRecon.AppServices.Groups;
using TravelRecon.AppServices.Users.ViewDtos;
using TravelRecon.Domain.Identity;
using TravelRecon.Domain.Identity.UserRelationships;
using TravelRecon.Domain.Localization;
using TravelRecon.Domain.Localization.QueryObjects;
using TravelRecon.Domain.RepositoryInterfaces;

namespace TravelRecon.AppServices.Users
{
    public class ContactsService : IAppService
    {
        private readonly GroupsService _groupsService;
        private readonly UserRelationshipService _userRelationshipService;
        private readonly UserService _userService;
        private readonly IRepository<User> _userRepository;
        private readonly IRepository<Label> _labelRepository;

        public ContactsService(GroupsService groupsService, 
            UserRelationshipService userRelationshipService, 
            UserService userService,
            IRepository<User> userRepository, 
            IRepository<Label> labelRepository)
        {
            _groupsService = groupsService;
            _userRelationshipService = userRelationshipService;
            _userService = userService;
            _userRepository = userRepository;
            _labelRepository = labelRepository;
        }

        /// <summary>
        /// constructs the user search results view model. This search takes place when a user enters
        /// characters into the contacts screen and the drupal database is queried to find matching users
        /// </summary>
        /// <param name="searchCharacters"></param>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        public ContactSearchResultsViewModel GetContactSearchResultsViewModel(string searchCharacters, User currentUser)
        {
            var userSearchResultsViewModel = new ContactSearchResultsViewModel();

            var results = _userRepository.GetAll().Where(x => x.UserName.Contains(searchCharacters)
                    || x.FirstName.Contains(searchCharacters)
                    || x.LastName.Contains(searchCharacters))
                    .ToList();

            if (!results.Any()) return userSearchResultsViewModel;

            //map usrs to view model
            foreach (var user in results)
            {
                if (user.Id == currentUser.Id)
                {
                    //filter out our own existing user from the drupal search results
                    continue;
                }
                //convert drupal user to our contact dto
                var contact = ContactSearchResultDto.ConvertFrom(user);
                userSearchResultsViewModel.Contacts.Add(contact);
            }

            var activeContacts = _userRelationshipService.GetUsersInRelationshipWith(currentUser).ToList();
            var pendingContacts = _userRelationshipService.GetUsersInRelationshipWith(currentUser, true, UserRelationshipStatusEnum.Pending).ToList();

            var ids = activeContacts.Select(x => x.Id).ToList();

            if (ids.Any())
            {
                foreach (var contactSearchResultDto in userSearchResultsViewModel.Contacts)
                    contactSearchResultDto.IsContactEstablished = ids.Any(x => x == contactSearchResultDto.Id);
            }

            ids = pendingContacts.Select(x => x.Id).ToList();

            if (ids.Any())
            {
                foreach (var contactSearchResultDto in userSearchResultsViewModel.Contacts)
                    contactSearchResultDto.IsInvitePending = ids.Any(x => x == contactSearchResultDto.Id);
            }

            return userSearchResultsViewModel;
        }

        /// <summary>
        /// this is a test method that can be deleted when the drupal integration is working flawlessly
        /// </summary>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        private ContactSearchResultsViewModel GetStubbedSearchResults(User currentUser)
        {
            var result = new ContactSearchResultsViewModel();

            var contactsAvailable = _userRepository.GetAll().Where(x => x.Id != currentUser.Id).ToList();

            //hack to remove users with existing relationsihps for demo
            var currentRelationships = _userRelationshipService.GetCurrentUserContactRelationshipViewModel(currentUser);
            IEnumerable<int> existing = currentRelationships.Contacts.Where(w => w.Id != currentUser.Id).Select(x => x.Id).Distinct();
            contactsAvailable.RemoveAll(x => existing.Any(y => y == x.Id));
            var filteredSearchResults = contactsAvailable.Select(ContactSearchResultDto.ConvertFrom);
            result.Contacts.AddRange(filteredSearchResults);

            return result;
        }

        public void AcceptInvitation(User currentUser, int userId)
        {
            _userRelationshipService.AcceptUserRelationshipInvitation(userId, currentUser.Id);
        }

        public Task<List<ContactRelationshipDto>> GetInvitesAsync(int userId)
        {
            return _userRelationshipService.GetInvitesAsync(userId);
        }

        public Task<List<ContactRelationshipDto>> GetContactsAsync(int userId)
        {
            return _userRelationshipService.GetContactsAsync(userId);
        }

        [Obsolete("Old method")]
        public ContactRelationshipsViewModel GetContactsForCurrentUser(User currentUser)
        {
            return _userRelationshipService.GetCurrentUserContactRelationshipViewModel(currentUser);
        }

        public void RejectContactRelationship(User currentUser, int userId)
        {
            _userRelationshipService.RejectUserRelationship(userId, currentUser.Id);
        }

		public async Task DeleteContactRelationship(User currentUser, int userId)
		{
			await _userRelationshipService.DeleteUserRelationship(userId, currentUser.Id);
			await _groupsService.RemoveUserFromAllGroups(currentUser, userId);
		}

		public void Invite(ContactSearchResultDto invitee, User currentUser)
		{
		    var existingUser = _userRepository.Get(invitee.Id);
            _userRelationshipService.Invite(existingUser, currentUser);
        }

        public Task InviteAsync(int userId, int currentUserId)
        {
            return _userRelationshipService.InviteAsync(userId, currentUserId);
        }

        public void IgnoreContactRelationship(User currentUser, int userId)
        {
            _userRelationshipService.IgnoreUserRelationship(userId, currentUser.Id);
        }

        public IEnumerable<UserMinDto> GetAcceptedContactForCurrentUser(User currentUser)
        {
            return _userRelationshipService.GetContacts(currentUser.Id);
        }

        public async Task<ContactDetailsViewModel> GetContactDetails(int contactId, User currentUser)
        {
            var singleOrDefault = _userRepository.GetAll().Single(x => x.Id == contactId);
            var contactViewModel = new ContactDetailsViewModel {Contact = ContactDto.ConvertFrom(singleOrDefault)};
            var targetLanguage = currentUser.Language ?? LanguageType.English;

            var labelsToRetrive = new []
            {
                LabelType.GoBack,
                LabelType.LastCheckIn,
                LabelType.LastEmergency,
                LabelType.LastAlert,
                LabelType.Reject,
                LabelType.Accept,
                LabelType.Uninvite,
                LabelType.Delete,
                LabelType.AddToGroup,
                LabelType.UserHasNoActivity,
                LabelType.NotAvailableAcronym
            };

            contactViewModel.Labels = _labelRepository.GetAll().FindLabelsLocalizedDictionary(labelsToRetrive, targetLanguage);
            contactViewModel.IsCurrentUser = currentUser.Id == contactId;

            //we don't need all this info, if we are showing current user contact
            if (contactViewModel.IsCurrentUser == false)
            {
                var userConsistsGroups = await _groupsService.GetUserConsistsGroups(currentUser, contactId);
                var ur = _userRelationshipService.GetUserRelationship(contactId, currentUser.Id);
                contactViewModel.RelationshipStatus = (UserRelationshipStatusEnum)(ur?.UserRelationshipStatusFk ?? 0);
                contactViewModel.IsRelationshipStatusOwner = (ur?.UserRequesterFk ?? -1) == currentUser.Id;
                contactViewModel.HasAvailableGroups = contactViewModel.RelationshipStatus == UserRelationshipStatusEnum.Accepted
                    && _groupsService.HasAvailableGroups(currentUser, contactId);
	            contactViewModel.ConsistsInGroups = contactViewModel.RelationshipStatus == UserRelationshipStatusEnum.Accepted && userConsistsGroups.Any();
            }

            return contactViewModel;
        }

        public ContactsPageViewModel GetContactPage(User currentUser)
        {
            var targetLanguage = currentUser.GetLanguageSetting();

            var vm = new ContactsPageViewModel();

            var labelsToRetrive = new[]
            {
                LabelType.ContactsTitle,
                LabelType.Groups,
                LabelType.Search, 
                LabelType.CreateGroup,
                LabelType.ContactsInvitePending,
                LabelType.ContactsInviteReject,
                LabelType.InviteHasNotBeenAccepted,
                LabelType.Invites,
                LabelType.AddContacts,
				LabelType.Accept,
				LabelType.Decline,
				LabelType.NoUsersFound,
				LabelType.HasSentYouContactInvite,
				LabelType.YouHaveBeenInvitedBy,
				LabelType.ToJoin
            };
            var localizedLabels = _labelRepository.GetAll().FindLabelsLocalizedDictionary(labelsToRetrive, targetLanguage);

            vm.Labels = localizedLabels;

            return vm;
        }
    }
}