using System.Collections.Generic;
using TravelRecon.Domain.Localization;

namespace TravelRecon.AppServices.Users.ViewDtos
{
    public struct SubscriptionLevelTypeIdCollection
    {
        public static readonly Dictionary<LabelType, int> SortTypeId = new Dictionary<LabelType, int>
        {
            {LabelType.GoRecon, 1},
            {LabelType.TravelRecon, 2},
            {LabelType.TravelReconPro, 3},
            {LabelType.Administrator, 4}
        };
    }
}