using System.Collections.Generic;
using TravelRecon.AppServices.Groups.ViewDtos;

namespace TravelRecon.AppServices.Users.ViewDtos
{
    public class GroupRelationshipsViewModel
    {
        public GroupRelationshipsViewModel()
        {
            Labels = new Dictionary<int, string>();
        }

        public List<UserGroupRelationMaxDto> Invitations { get; set; }
        public List<UserMinDto> Individuals { get; set; }
        public List<GroupDto> Groups { get; set; }
        public Dictionary<int, string> Labels { get; set; }
    }
}