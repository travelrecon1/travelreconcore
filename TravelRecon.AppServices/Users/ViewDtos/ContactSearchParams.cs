﻿namespace TravelRecon.AppServices.Users.ViewDtos
{
    public class ContactSearchParams
    {
        public string SearchTerms { get; set; }
        public ContactOrderEnum? Order { get; set; }
    }
}