﻿using System;
using System.ComponentModel.DataAnnotations;
using TravelRecon.AppServices.Map.ViewDtos;
using TravelRecon.Domain.Extensions;
using TravelRecon.Domain.Identity;
using TravelRecon.Domain.Map;

namespace TravelRecon.AppServices.Users.ViewDtos
{
    public class ContactDto
    {
        [Required]
        public string Username { get; set; }
        [Required]
        public int Id { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string FullName { get; set; }
        public string MapAddress { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public bool IsOnline { get; set; }
        public int DrupalId { get; set; }

        public static ContactDto ConvertFrom(User user)
        {
            var longitude = 0.0;
            if (user.Location != null && user.Location.Longitude.HasValue)
            {
                longitude = user.Location.Longitude.Value;
            }

            var latitude = 0.0;
            if (user.Location != null &&  user.Location.Latitude.HasValue)
            {
                latitude = user.Location.Latitude.Value;
            }

            if (user == null) throw new ArgumentNullException(nameof(user));
            var contactDto = new ContactDto();
            contactDto.Username = user.UserName;
            contactDto.Id = user.Id;
            contactDto.Address = user.CurrentAddressFormatted;
            contactDto.Email = user.Email;
            contactDto.FullName = user.Fullname();
            contactDto.MapAddress= user.CurrentMapAddressFormatted;
            contactDto.Latitude = latitude;
            contactDto.Longitude = longitude;
            //contactDto.IsOnline = latitude > 0 && longitude > 0;
            contactDto.IsOnline = user.IsOnline();
            
            return contactDto;
        }

        
    }
}