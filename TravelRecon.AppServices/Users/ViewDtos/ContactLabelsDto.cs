namespace TravelRecon.AppServices.Users.ViewDtos
{
    public class ContactLabelsDto
    {
        public string Title { get; set; }
        public string InvitePending { get; set; }
        public string InviteRejected { get; set; }
        public string ContactAlreadyEstablished { get; set; }
        public string ContactNotEstablished { get; set; }
        public string ContactShowOnMap { get; set; }
        public string ContactIsNotOnline { get; set; }
        public string ContactDetail { get; set; }
        public string GoBack { get; set; }
        public string All { get; set; }
        public string SearchUsers { get; set; }
        public string LastCheckIn { get; set; }
        public string LastEmergency { get; set; }
        public string LastAlert { get; set; }
    }
}