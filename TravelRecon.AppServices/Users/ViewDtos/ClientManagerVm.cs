﻿using System.ComponentModel.DataAnnotations;

namespace TravelRecon.AppServices.Users.ViewDtos
{
    public class ClientManagerVm
    {
        public int UserId { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string Username { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        public int LicenseId { get; set; }
        public string LicenseNumber { get; set; }
    }
}