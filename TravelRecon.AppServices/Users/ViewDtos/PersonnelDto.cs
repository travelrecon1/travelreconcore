﻿using System;
using System.Collections.Generic;

namespace TravelRecon.AppServices.Users.ViewDtos
{
    public class PersonnelDto
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public DateTime? LastCheckIn { get; set; }
        public List<string> Groups { get; set; }
        public double? Risk { get; set; }
        public string AvatarUrl { get; set; }
        public int EmergencyStatus { get; set; }
        public string LastLocation { get; set; }
        public string FutureLocation { get; set; }
        public double? Longitude { get; set; }
        public double? Latitude { get; set; }
        public DateTime? LocationUpdated { get; set; }
    }
}