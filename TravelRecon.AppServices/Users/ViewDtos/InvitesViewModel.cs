using System.Collections.Generic;

namespace TravelRecon.AppServices.Users.ViewDtos
{
    public class ContactRelationshipsViewModel
    {
        public List<ContactRelationshipDto> Contacts = new List<ContactRelationshipDto>();
    }
}