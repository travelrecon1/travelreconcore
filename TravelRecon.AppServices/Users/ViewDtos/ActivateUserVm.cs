﻿using System.ComponentModel.DataAnnotations;

namespace TravelRecon.AppServices.Users.ViewDtos
{
    public class ActivateUserVm
    {
        [Required]
        public string Username { get; set; }

        [StringLength(255, MinimumLength = 6, ErrorMessage = "Min 6 symbols")]
        [Required]
        public string Password { get; set; }
    }
}