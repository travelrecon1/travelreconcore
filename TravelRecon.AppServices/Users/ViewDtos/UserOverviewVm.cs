﻿namespace TravelRecon.AppServices.Users.ViewDtos
{
    public class UserOverviewVm
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public int Points { get; set; }
        public string AddressFormatted { get; set; }
    }
}
