﻿using System.Collections.Generic;

namespace TravelRecon.AppServices.Users.ViewDtos
{
    public class UserListItemDto
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public IList<int> Roles { get; set; }
    }
}