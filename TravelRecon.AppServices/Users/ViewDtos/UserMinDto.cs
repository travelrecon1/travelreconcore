﻿namespace TravelRecon.AppServices.Users.ViewDtos
{
    public class UserMinDto
    {
        public int DrupalId { get; set; }
        public string UserName { get; set; }
        public int Id { get; set; }
        public string FullName { get; set; }
        public bool CanInviteToGroup { get; set; }
	    public bool HasCheckIn { get; set; }
	    public bool HasEmergency { get; set; }
        public string Location { get; set; }
    }
}