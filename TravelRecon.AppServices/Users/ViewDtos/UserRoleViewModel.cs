﻿using TravelRecon.Domain.Extensions;
using TravelRecon.Domain.Identity;

namespace TravelRecon.AppServices.Users.ViewDtos
{
    public class UserRoleViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }

        public static UserRoleViewModel ConvertFrom(UserRoles roleType)
        {
            return new UserRoleViewModel
            {
                Id = roleType.RoleId,
                Title = roleType.UserRole.Name
            };
        }
    }
}
