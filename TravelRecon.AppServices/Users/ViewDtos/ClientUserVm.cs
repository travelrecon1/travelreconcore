﻿using System;
using System.ComponentModel.DataAnnotations;
using TravelRecon.Domain.Identity;

namespace TravelRecon.AppServices.Users.ViewDtos
{
    public class ClientUserVm
    {
        public int UserId { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string Username { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        public int LicenseId { get; set; }
        [Required]
        public int ManagerId { get; set; }

        public string Dependent { get; set; }
        public string Employee { get; set; }
        public string EmployeeNumber { get; set; }
        public string PhoneNumber { get; set; }
        public string EmergencyContactInfo { get; set; }
        public string Passport { get; set; }
        public string GovtPassport { get; set; }
        public string PersonalPassport { get; set; }
        public string CountryOfPassport { get; set; }
        public DateTime? PassportExpiration { get; set; }
        public string PassportNumber { get; set; }
        public BloodTypeEnum Blood { get; set; }
        public string Allergies { get; set; }
        public string Disabilities { get; set; }
    }
}