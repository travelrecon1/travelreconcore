﻿using System;
using System.Collections.Generic;
using TravelRecon.Domain.Identity;
using TravelRecon.Domain.Localization;

namespace TravelRecon.AppServices.Users.ViewDtos
{
    public class UserViewModel
    {
        public int Id { get; set; }
        public int DrupalId { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public bool IsAgree { get; set; }
        public string AddressFormatted { get; set; }
        public List<UserRoleViewModel> UserRoles { get; set; }
        public UserRoleViewModel SubscriptionLevelType { get; set; }
        public bool IsFirstLogin { get; set; }
        public int LocationUpdateInterval { get; set; }
        public bool IsLiabilityComplete { get; set; }
        public UserRoleTypeEnum? UpgradePackageId { get; set; }
        public int Points { get; set; }
        public List<RankDto> OwnedRanks { get; set; }
        public RankDto NextRank { get; set; }
        public LanguageType Language { get; set; }
	    public bool ObfuscateLocation { get; set; }

		//obfuscate to 2 mile width circle = 1 mile radius = 1.60934km
		private const double obfuscationRadius = 1.60934;
		public double ObfuscationRadius { get { return obfuscationRadius; } }
        public int MapZoom { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string AvatarUrl { get; set; }
        public string EmergencyEmail { get; set; }
	}
}
