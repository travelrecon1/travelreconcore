using System.Collections.Generic;
using TravelRecon.Domain.Identity.UserRelationships;

namespace TravelRecon.AppServices.Users.ViewDtos
{
    public class ContactDetailsViewModel
    {
        public ContactDto Contact { get; set; }
        public Dictionary<int, string> Labels { get; set; }
        public UserRelationshipStatusEnum RelationshipStatus { get; set; }
        public bool IsRelationshipStatusOwner { get; set; }
        public bool HasAvailableGroups { get; set; }
        public bool IsCurrentUser { get; set; }
        public bool ConsistsInGroups { get; set; }
    }
}