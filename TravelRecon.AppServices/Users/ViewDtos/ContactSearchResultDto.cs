﻿using System.ComponentModel.DataAnnotations;
using TravelRecon.Domain.Identity;

namespace TravelRecon.AppServices.Users.ViewDtos
{
    public class ContactSearchResultDto
    {
        public static ContactSearchResultDto ConvertFrom(User user)
        {
            var contactDto = new ContactSearchResultDto
            {
                Id = user.Id,
                Username = user.UserName,
                Email = user.Email,
            };

            return contactDto;
        }



        public string Email { get; set; }
        [Required]
        public string Username { get; set; }

        [Required]
        public int Id { get; set; }

        public bool IsContactEstablished { get; set; }
        public bool IsInvitePending { get; set; }
    }
}