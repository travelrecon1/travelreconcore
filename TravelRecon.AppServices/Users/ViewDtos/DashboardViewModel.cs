﻿using System.Collections.Generic;
using TravelRecon.AppServices.Itineraries.ViewDtos;
using TravelRecon.AppServices.Missions;

namespace TravelRecon.AppServices.Users.ViewDtos
{
    public class DashboardViewModel
    {
        public int Points { get; set; }
        public string Rank { get; set; }
        public int RiskRating { get; set; }
        public List<ContactRiskViewModel> FriendsRisk { get; set; }
        public List<MissionDto> SuggestedMissions { get; set; }
        public string CurrentAddress { get; set; }
        public ItineraryViewModel UpcomingItinerary { get; set; }
    }

    public class ContactRiskViewModel
    {
        public string Username { get; set; }
        public string Location { get; set; }
        public int Risk { get; set; }
    }
}
