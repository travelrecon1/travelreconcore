﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TravelRecon.AppServices.Users.ViewDtos
{
    public class UserRiskFactorDto
    {

        public int Id { get; set; }
        public string UserName { get; set; }

        public double? RiskFactor { get; set; }
    }
}
