﻿using System;
using System.ComponentModel.DataAnnotations;
using TravelRecon.Domain.Identity.UserRelationships;

namespace TravelRecon.AppServices.Users.ViewDtos
{
    public class ContactRelationshipDto
    {
        [Required]
        public string Username { get; set; }
        [Required]
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public int UserId { get; set; }

        public bool IsRelationshipStatusOwner { get; set; }
        public UserRelationshipStatusEnum RelationshipStatus { get; set; }
        public string MapAddress { get; set; }
        public double? Longitude { get; set; }
        public double? Latitude { get; set; }
        public DateTime? LocationTimeStamp { get; set; }
        public bool IsOnline => LocationTimeStamp.HasValue && ((LocationTimeStamp.Value - DateTime.UtcNow).TotalMinutes <= 15 );
        public DateTime LastCheckIn { get; set; }
        public double? Risk { get; set; }
        public int EmergencyStatus { get; set; }
        public string AvatarUrl { get; set; }

        public string LastCheckInFormatted
        {
            get
            {
                return LastCheckIn == default(DateTime)
                    ? null
                    : LastCheckIn.ToString("MM/dd/yyyy @ HH:mm");
            }
        }
    }
}