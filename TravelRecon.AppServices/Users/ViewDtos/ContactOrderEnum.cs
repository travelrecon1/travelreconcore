﻿namespace TravelRecon.AppServices.Users.ViewDtos
{
    public enum ContactOrderEnum
    {
        FirstName = 1,
        LastName = 2,
        RiskRating = 3
    }
}
