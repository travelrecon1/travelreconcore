﻿using System;

namespace TravelRecon.AppServices.Users.ViewDtos
{
    public class UserLicenseInfoDto
    {
        public int LicenseId { get; set; }
        public DateTime ExpirationDate { get; set; }
        public DateTime? ActivatedDate { get; set; }
        public int TotalManagerLicensesCount { get; set; }
        public int TotalUserLicensesCount { get; set; }
        public int AssignedManagerLicensesCount { get; set; }
        public int AssignedUserLicensesCount { get; set; }
        public int ActivatedLicensesManagerCount { get; set; }
        public int ActivatedLicensesUserCount { get; set; }
        public bool CanAssignLicenses { get; set; }
    }
}