﻿using TravelRecon.Domain.Groups;

namespace TravelRecon.AppServices.Users.ViewDtos
{
	public class GroupContactDto
	{
		public int UserId { get; set; }
		public int DrupalId { get; set; }
		public string UserName { get; set; }
		public UserGroupRelationType GroupRelationType { get; set; }
	}
}
