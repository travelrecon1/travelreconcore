﻿using System.Linq;
using System.Reflection.Emit;
using TravelRecon.Domain.Localization;
using TravelRecon.Domain.Points;

namespace TravelRecon.AppServices.Users.ViewDtos
{
	public class RankDto
	{
		public int Id { get; set; }
		public string Label { get; set; }
		public int MinPoints { get; set; }
		public string Name { get; set; }

		public static RankDto ConvertFrom(Rank rank, LanguageType? language)
		{
			var dto = new RankDto
			{
				Id = rank.Id,
				Label = rank.Label.Translations.FirstOrDefault(x => x.Language == language)?.Content,
				MinPoints = rank.MinPoints,
				Name = rank.Name
			};

			if (dto.Label == null)
				dto.Label = dto.Name;

			return dto;
		}
	}
}
