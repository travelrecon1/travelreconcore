﻿using System;
using TravelRecon.Domain.Identity;

namespace TravelRecon.AppServices.Users.ViewDtos
{
    public class TrpUserViewModel
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string CurrentLocation { get; set; }
        public string AvatarUrl { get; set; }
        public double? Risk { get; set; }
        public int EmergencyStatus { get; set; }
        public DateTime? LastCheckinDate { get; set; }
        public DateTime? LastEmergencyDate { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime? LicenseAssignedDate { get; set; }
        public DateTime? LicenseExpirationDate { get; set; }
        public string Passport { get; set; }
        public string GovtPassport { get; set; }
        public string PersonalPassport { get; set; }
        public string CountryOfPassword { get; set; }
        public DateTime? PassportExpiration { get; set; }
        public string PassportNumber { get; set; }
        public string Employee { get; set; }
        public string EmployeeNumber { get; set; }
        public string Dependent { get; set; }
        public BloodTypeEnum? BloodType { get; set; }
        public string Allergies { get; set; }
        public string Disabilities { get; set; }
        public string EmergencyEmail { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}
