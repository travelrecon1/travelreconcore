﻿using System;

namespace TravelRecon.AppServices.Users.ViewDtos
{
    public class ManagerLicenseInfo
    {
        public int ManagerId { get; set; }
        public int LicenseId { get; set; }
        public DateTime ExpirationDate { get; set; }
        public DateTime? ActivatedDate { get; set; }
        public string LicenseNumber { get; set; }
        public int TotalUserLicensesCount { get; set; }
        public int AssignedUserLicensesCount { get; set; }
        public int ActivatedUserLicensesCount { get; set; }
        public bool CanCreateUsers { get; set; }
        public int TotalManagedUsers { get; set; }
    }
}