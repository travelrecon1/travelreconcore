using System.Collections.Generic;
using TravelRecon.AppServices.Groups.ViewDtos;

namespace TravelRecon.AppServices.Users.ViewDtos
{
    public class InvitesViewModel
    {
        public List<ContactRelationshipDto> Invites { get; set; }

	    public List<GroupInviteDto> GroupInvites { get; set; }
    }
}