﻿using System.Collections.Generic;

namespace TravelRecon.AppServices.Users.ViewDtos
{
    public class ContactSearchResultsViewModel
    {
        public ContactSearchResultsViewModel()
        {
            Contacts = new List<ContactSearchResultDto>();
        }

        public List<ContactSearchResultDto> Contacts { get; set; }
    }
}