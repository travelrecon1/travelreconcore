﻿using System.Collections.Generic;
using TravelRecon.AppServices.SharedDtos;
using TravelRecon.Domain.Tutorial;

namespace TravelRecon.AppServices.Users.ViewDtos
{
    public class ContactsPageViewModel: BasePageViewModel
    {
        public Dictionary<int, string> Labels { get; set; }
        public override PageTypeEnum PageType => PageTypeEnum.Contacts;
    }
}
