﻿namespace TravelRecon.AppServices.Users
{
    public class UserInviteDto
    {
        public int UserFk { get; internal set; }
        public int? UserGroup { get; set; }
    }
}