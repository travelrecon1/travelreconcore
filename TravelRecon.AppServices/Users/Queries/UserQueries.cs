﻿using System;
using System.Linq;
using System.Linq.Expressions;
using LinqKit;
using TravelRecon.AppServices.ClientCompany;
using TravelRecon.AppServices.Users.ViewDtos;
using TravelRecon.Domain.Alerts;
using TravelRecon.Domain.Groups;
using TravelRecon.Domain.Identity;
using TravelRecon.Domain.Identity.UserRelationships;
using TravelRecon.Domain.IntelReports;
using TravelRecon.Domain.Itinerary;
using TravelRecon.Domain.License;
using TravelRecon.Domain.Localization;
using TravelRecon.Domain.Statuses;

namespace TravelRecon.AppServices.Users.Queries
{
    public static class UserQueries
    {
        public static IQueryable<ClientUserDto> SelectClientUserDto(
            this IQueryable<UserLicense> usersQuery)
        {
            var q = usersQuery.AsExpandable();
            var fullNameExpr = GetFullNameExpr();

            return q.Select(x => new ClientUserDto
            {
                Id = x.OwnerId.Value,
                FullName = fullNameExpr.Invoke(x.Owner),
                AvatarUrl = "http://www.snappertech.com/images/prashant-surana.jpg",
                LicenseId = x.Id,
                LicenseType = (int) x.License.Type,
                ActivatedDate = x.ActivatedDate,
                AssignedDate = x.AssignedDate,
                ExpirationDate = x.ExpirationDate,
                LicenseNumber = x.LicenseKey,
                CanRevokeLicense = !usersQuery.Any(l => l.ManagerId == x.OwnerId && l.ActivatedDate.HasValue && l.ExpirationDate < DateTime.Now),
                AssignedLicenses = usersQuery.Count(m => m.ManagerId == x.OwnerId && x.AssignedDate.HasValue),
                ActivatedLicenses = usersQuery.Count(m => m.ManagerId == x.OwnerId && x.ActivatedDate.HasValue),
                TotalLicenses = usersQuery.Count(m => m.ManagerId == x.OwnerId)
            });
        }

        public static IQueryable<ClientUserDto> SelectManagerUserDto(this IQueryable<UserLicense> usersQuery)
        {
            var q = usersQuery.AsExpandable();
            var fullNameExpr = GetFullNameExpr();

            return q.Select(x => new ClientUserDto
            {
                Id = x.OwnerId.Value,
                FullName = fullNameExpr.Invoke(x.Owner),
                AvatarUrl = "http://www.snappertech.com/images/prashant-surana.jpg",
                LicenseId = x.Id,
                LicenseType = (int) x.License.Type,
                ActivatedDate = x.ActivatedDate,
                AssignedDate = x.AssignedDate,
                ExpirationDate = x.ExpirationDate,
                LicenseNumber = x.LicenseKey,
                CanRevokeLicense = true
            });
        }

        public static IQueryable<UserListItemDto> SelectUserListItemDto(
            this IQueryable<User> usersQuery
        )
        {
            var q = usersQuery.AsExpandable();
            var fullNameExpr = GetFullNameExpr();

            return q.Select(x => new UserListItemDto
            {
                Id = x.Id,
                FullName = fullNameExpr.Invoke(x),
                Roles = x.UserRoles.Select(ur => ur.RoleId).ToList(),
                UserName = x.UserName
            });
        }

        public static IQueryable<PersonnelDto> SelectPersonnelDto(
            this IQueryable<User> usersQuery,
            IQueryable<Destination> destinationsQueries,
            IQueryable<UserGroupRelation> userGroupRelationsQueries,
            IQueryable<Itinerary> itirinaryQueries, 
            LanguageType language)
        {
            var q = usersQuery.AsExpandable();
            var nearByDestinationExpr = GetNearByDestinationExpr(destinationsQueries);
            var selectPersonnelDtoExpr = GetSelectPersonnelDtoExpr(userGroupRelationsQueries, itirinaryQueries, language);

            return q.Select(u => selectPersonnelDtoExpr.Invoke(u, nearByDestinationExpr.Invoke(u)));
        }

        public static IQueryable<TrpUserViewModel> SelectTrpUserViewModel(this IQueryable<User> userQuery,
            IQueryable<Alert> alertQuery, IQueryable<UserDetails> detailsQuery, IQueryable<UserLicense> licenseQuery)
        {
            var q = userQuery.AsExpandable();

            var emergencyStatusExpr = GetEmergencyStatusExpr();
            var lastCheckinExpr = GetLastCheckinDateTimeExpression();
            var lastEmergencyExpr = GetLastEmergencyDateTimeExpression(alertQuery);

            return q.Select(x => new TrpUserViewModel
            {
                Id = x.Id,
                FirstName = x.FirstName,
                LastName = x.LastName,
                UserName = x.UserName,
                Email = x.Email,
                CurrentLocation = x.CurrentAddressFormatted,
                Risk = x.RiskFactor.HasValue & x.RiskFactor > 0 ? x.RiskFactor : (x.ComputedRiskFactor ?? 0),
                EmergencyStatus = emergencyStatusExpr.Invoke(x.RiskFactor ?? x.ComputedRiskFactor),
                LastCheckinDate = lastCheckinExpr.Invoke(x),
                LastEmergencyDate = lastEmergencyExpr.Invoke(x),
                AvatarUrl = "http://www.snappertech.com/images/prashant-surana.jpg",
                PhoneNumber = detailsQuery.FirstOrDefault(d => d.UserId == x.Id).PhoneNumber,
                Passport = detailsQuery.FirstOrDefault(d => d.UserId == x.Id).Passport,
                GovtPassport = detailsQuery.FirstOrDefault(d => d.UserId == x.Id).GovtPassport,
                PersonalPassport = detailsQuery.FirstOrDefault(d => d.UserId == x.Id).PersonalPassport,
                CountryOfPassword = detailsQuery.FirstOrDefault(d => d.UserId == x.Id).CountryOfPassport,
                PassportExpiration = detailsQuery.FirstOrDefault(d => d.UserId == x.Id).PassportExpiration,
                PassportNumber = detailsQuery.FirstOrDefault(d => d.UserId == x.Id).PassportNumber,
                Employee = detailsQuery.FirstOrDefault(d => d.UserId == x.Id).Employee,
                EmployeeNumber = detailsQuery.FirstOrDefault(d => d.UserId == x.Id).EmployeeNumber,
                Dependent = detailsQuery.FirstOrDefault(d => d.UserId == x.Id).Dependent,
                BloodType = detailsQuery.FirstOrDefault(d => d.UserId == x.Id).Blood,
                Allergies = detailsQuery.FirstOrDefault(d => d.UserId == x.Id).Allergies,
                Disabilities = detailsQuery.FirstOrDefault(d => d.UserId == x.Id).Disabilities,
                LicenseAssignedDate = licenseQuery.FirstOrDefault(d => d.OwnerId == x.Id).AssignedDate,
                LicenseExpirationDate = licenseQuery.FirstOrDefault(d => d.OwnerId == x.Id).ExpirationDate,
                EmergencyEmail = x.EmergencyEmail
            });
        }

        public static IQueryable<ClientManagerVm> SelectManagerVm(this IQueryable<User> userQuery,
            IQueryable<UserLicense> licenseQuery)
        {
            return userQuery.Select(x => new ClientManagerVm
            {
                UserId = x.Id,
                Email = x.Email,
                FirstName = x.FirstName,
                LastName = x.LastName,
                Username = x.UserName,
                LicenseId = licenseQuery.FirstOrDefault(l => l.OwnerId == x.Id).Id,
                LicenseNumber = licenseQuery.FirstOrDefault(l => l.OwnerId == x.Id).LicenseKey
            });
        }

        private static Expression<Func<User, Destination, PersonnelDto>> GetSelectPersonnelDtoExpr(
            IQueryable<UserGroupRelation> userGroupRelationsQueries,
            IQueryable<Itinerary> itirinaryQueries,
            LanguageType language)
        {
            var cityExpr = GetDestinationCityExpr(language);
            var countryExpr = GetDestinationCountryExpr(language);
            var emergencyStatusExpr = GetEmergencyStatusExpr();
            var fullNameExpr = GetFullNameExpr();
            var futureLocationExpr = GetFutureLocationExpr(itirinaryQueries);

            return (u, d) => new PersonnelDto
            {
                Id = u.Id,
                City = cityExpr.Invoke(d),
                Country = countryExpr.Invoke(d),
                UserName = u.UserName,
                FullName = fullNameExpr.Invoke(u),
                LastCheckIn = u.Statuses.FirstOrDefault(s => s.Type == StatusType.CheckIn).ReportedOn,
                Groups = userGroupRelationsQueries.Where(ugr => ugr.UserId == u.Id && ugr.UserGroupRelationType == UserGroupRelationType.Accepted)
                    .Select(g => g.Group.Name).ToList(),
                Risk = u.RiskFactor ?? u.ComputedRiskFactor,
                EmergencyStatus = emergencyStatusExpr.Invoke(u.RiskFactor ?? u.ComputedRiskFactor),
                AvatarUrl = "http://www.snappertech.com/images/prashant-surana.jpg",
                LastLocation = u.CurrentAddressFormatted,
                FutureLocation = futureLocationExpr.Invoke(u),
                Longitude = u.CurrentLocation.Longitude,
                Latitude = u.CurrentLocation.Latitude,
                LocationUpdated = u.CurrentLocationDateTime
            };
        }

        public static Expression<Func<User, string>> GetFullNameExpr()
        {
            return (u) => (u.FirstName == null || u.FirstName == "")
                          && (u.LastName == null || u.LastName == "")
                ? u.UserName
                : u.FirstName + " " + u.LastName;
        }

        public static Expression<Func<User, Itinerary>> GetItineraryExpr(IQueryable<Itinerary> itirinaryQueries)
        {
            return (u) => itirinaryQueries
                .Where(x => x.UserId == u.Id)
                .OrderByDescending(x => x.ArrivalDateTime)
                .FirstOrDefault(x => x.ArrivalDateTime > DateTime.UtcNow);
        }

        public static Expression<Func<User, string>> GetFutureLocationExpr(IQueryable<Itinerary> itirinaryQueries)
        {
            var itineraryExpr = GetItineraryExpr(itirinaryQueries);
            return (u) => itineraryExpr.Invoke(u) == null
                ? null
                : itineraryExpr.Invoke(u).ArrivalCountry + ", " + itineraryExpr.Invoke(u).ArrivalCity;
        }

        private static Expression<Func<User, Destination>> GetNearByDestinationExpr(IQueryable<Destination> destinationsQuery)
        {
            return (u) =>
                destinationsQuery
                .OrderBy(d => u.CurrentLocation.Distance(d.Location))
                .FirstOrDefault();
        }

        private static Expression<Func<Destination, string>> GetDestinationCityExpr(LanguageType language)
        {
            return (d) =>
                d == null ? null :
                d.Translations.Where(t => t.Language == language)
                .Select(x => x.City).FirstOrDefault();
        }

        private static Expression<Func<Destination, string>> GetDestinationCountryExpr(LanguageType language)
        {
            return (d) =>
                d == null ? null :
                d.Country.Translations.Where(t => t.Language == language)
                .Select(x => x.Name).FirstOrDefault();
        }

        private static Expression<Func<double?, int>> GetEmergencyStatusExpr()
        {
            return (r) => r == null
                    ? (int)EmergencyStatusEnum.Unknown : (r < 50)
                        ? (int)EmergencyStatusEnum.Safe : (r < 75)
                            ? (int)EmergencyStatusEnum.InDanger : (int)EmergencyStatusEnum.Critical;
        }

        private static Expression<Func<User, DateTime?>> GetLastCheckinDateTimeExpression()
        {
            return (u) => u.Statuses.OrderByDescending(x => x.ReportedOn)
                .Select(x => (DateTime?) x.ReportedOn)
                .FirstOrDefault();
        }

        private static Expression<Func<User, DateTime?>> GetLastEmergencyDateTimeExpression(IQueryable<Alert> alertQuery)
        {
            return (u) => alertQuery
                .Where(x => x.IsEmergency && x.ReportedByFk == u.Id)
                .OrderByDescending(x => x.ReportedOn)
                .Select(x => (DateTime?)x.ReportedOn)
                .FirstOrDefault();
        }

        public static IQueryable<ContactRelationshipDto> SelectContactRelationshipDto(this IQueryable<UserRelationship> query, int userId)
        {
            query = query.AsExpandable();
            var emergencyStatusExpr = GetEmergencyStatusExpr();

            return query.Select(x => new ContactRelationshipDto
            {
                Id = x.Id,
                Username = x.UserInvitedFk == userId ? x.UserRequester.UserName : x.UserInvited.UserName,
                FirstName = x.UserInvitedFk == userId ? x.UserRequester.FirstName : x.UserInvited.FirstName,
                LastName = x.UserInvitedFk == userId ? x.UserRequester.LastName : x.UserInvited.LastName,
                FullName = x.UserInvitedFk == userId ? 
                ((x.UserRequester.FirstName == null || x.UserRequester.FirstName == "") && (x.UserRequester.LastName == null || x.UserRequester.LastName == "") ?
                    x.UserRequester.UserName : x.UserRequester.FirstName + " " + x.UserRequester.LastName)
                    : ((x.UserInvited.FirstName == null || x.UserInvited.FirstName == "") && (x.UserInvited.LastName == null || x.UserInvited.LastName == "") ?
                     x.UserInvited.UserName : x.UserInvited.FirstName + " " + x.UserInvited.LastName),
                MapAddress = x.UserInvitedFk == userId ? x.UserRequester.CurrentMapAddressFormatted : x.UserInvited.CurrentMapAddressFormatted,
                Latitude = x.UserInvitedFk == userId ? x.UserRequester.CurrentLocation.Latitude : x.UserInvited.CurrentLocation.Latitude,
                Longitude = x.UserInvitedFk == userId ? x.UserRequester.CurrentLocation.Longitude : x.UserInvited.CurrentLocation.Longitude,
                LocationTimeStamp = x.UserInvitedFk == userId ? x.UserRequester.CurrentLocationDateTime : x.UserInvited.CurrentLocationDateTime,
                RelationshipStatus = (UserRelationshipStatusEnum)x.UserRelationshipStatusFk,
                IsRelationshipStatusOwner = x.UserRequesterFk == userId,
                Risk = x.UserInvitedFk == userId ? x.UserRequester.RiskFactor ?? x.UserRequester.ComputedRiskFactor : 
                    x.UserInvited.RiskFactor ?? x.UserInvited.ComputedRiskFactor,
                EmergencyStatus = emergencyStatusExpr.Invoke(x.UserInvitedFk == userId ? x.UserRequester.RiskFactor ?? x.UserRequester.ComputedRiskFactor :
                    x.UserInvited.RiskFactor ?? x.UserInvited.ComputedRiskFactor),
                AvatarUrl = "http://www.snappertech.com/images/prashant-surana.jpg",
                UserId = x.UserInvitedFk == userId ? x.UserRequester.Id : x.UserInvited.Id
            });
        }

        public static IQueryable<ContactRelationshipDto> SelectContactRelationshipDto(this IQueryable<User> query, int userId)
        {
            query = query.AsExpandable();
            var emergencyStatusExpr = GetEmergencyStatusExpr();

            return query.Select(x => new ContactRelationshipDto
            {
                Id = x.Id,
                Username = x.UserName,
                FirstName = x.FirstName,
                LastName = x.LastName,
                FullName = (x.FirstName == null || x.FirstName == "") && (x.LastName == null || x.LastName == "") ?
                    x.UserName : x.FirstName + " " + x.LastName,
                MapAddress = x.CurrentMapAddressFormatted,
                Latitude = x.CurrentLocation.Latitude,
                Longitude = x.CurrentLocation.Longitude,
                LocationTimeStamp = x.CurrentLocationDateTime,
                RelationshipStatus = (int)UserRelationshipStatusEnum.None,
                IsRelationshipStatusOwner = false,
                Risk = x.RiskFactor,
                EmergencyStatus = emergencyStatusExpr.Invoke(x.RiskFactor ?? x.ComputedRiskFactor),
                AvatarUrl = "http://www.snappertech.com/images/prashant-surana.jpg",
                UserId = x.Id
            });
        }

        public static IQueryable<ContactRelationshipDto> Order(this IQueryable<ContactRelationshipDto> query, ContactOrderEnum? order)
        {
            if (order == null)
                return query.OrderBy(x => x.Id);

            if (order == ContactOrderEnum.FirstName)
                return query.OrderBy(x => x.FirstName)
                    .ThenBy(x => x.Username);

            if (order == ContactOrderEnum.LastName)
                return query.OrderBy(x => x.LastName)
                    .ThenBy(x => x.Username);

            if (order == ContactOrderEnum.RiskRating)
                return query.OrderBy(x => x.Risk);

            return query.OrderBy(x => x.Id);
        }
    }
}

