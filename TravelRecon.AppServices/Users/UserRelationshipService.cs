using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using TravelRecon.AppServices.Users.Queries;
using TravelRecon.AppServices.Users.ViewDtos;
using TravelRecon.Data;
using TravelRecon.Domain.Identity;
using TravelRecon.Domain.Identity.UserRelationships;
using TravelRecon.Domain.RepositoryInterfaces;

namespace TravelRecon.AppServices.Users
{
    public class UserRelationshipService : IAppService
    {
        private readonly IRepository<UserRelationship> _userRelationshipRepository;
        private readonly TravelReconDbContext _dbContext;
        private readonly IRepository<User> _userRepository;


	    public UserRelationshipService(
            IRepository<UserRelationship> userRelationshipRepository,
            TravelReconDbContext dbContext,
            IRepository<User> userRepository)
        {
            _userRepository = userRepository;
	        _userRelationshipRepository = userRelationshipRepository;
            _dbContext = dbContext;
        }

        public IEnumerable<User> GetUsersInRelationshipWith(User currentUser, bool restrictBasedOnStatus = true, UserRelationshipStatusEnum userRelationshipStatus = UserRelationshipStatusEnum.Accepted)
        {
            IEnumerable<User> contacts;

            var currentUserId = currentUser.Id;
            var contactRelationships = _userRelationshipRepository.GetAll()
                .Where(ur => ur.IsIgnored == false
                             && (ur.UserRequesterFk == currentUserId || ur.UserInvitedFk == currentUserId));

            if (restrictBasedOnStatus)
            {
                contactRelationships = contactRelationships.Where(ur => ur.UserRelationshipStatusFk == (int)userRelationshipStatus);
            }


            if (!contactRelationships.Any())
            {
                contacts = new List<User>();
            }
            else
            {
                var uniqueContactIds = contactRelationships
                    .Select(ur => ur.UserRequesterFk)
                    .Union(
                        contactRelationships.Select(ur => ur.UserInvitedFk)
                    );

                // Get all their accepted contacts
                contacts = _userRepository.GetAll()
                    .Where(u => uniqueContactIds.Contains(u.Id) &&
                                u.Id != currentUserId);
            }

            return contacts;
        }

        public IEnumerable<UserMinDto> GetContacts(int userId)
        {
            var result =
                _dbContext.UserRelationship
                    .Where(x => x.UserRequesterFk == userId && x.UserRelationshipStatusFk == (int)UserRelationshipStatusEnum.Accepted && !x.IsIgnored)
                    .Select(x => new UserMinDto
                    {
                        Id = x.UserInvited.Id,
                        UserName = x.UserInvited.UserName,
                        FullName = x.UserInvited.FirstName + " " + x.UserInvited.LastName,
                      
                        Location = x.UserInvited.CurrentAddressFormatted
                    })

                    .Union(
                        _dbContext.UserRelationship
                            .Where(x => x.UserInvitedFk == userId && x.UserRelationshipStatusFk == (int)UserRelationshipStatusEnum.Accepted && !x.IsIgnored)
                            .Select(x => new UserMinDto
                            {
                                Id = x.UserRequester.Id,
                                UserName = x.UserRequester.UserName,
                                FullName = x.UserRequester.FirstName + " " + x.UserRequester.LastName,
                             
                                Location = x.UserRequester.CurrentAddressFormatted
                            })
                    );

	        var checkins = _dbContext.Statuses
		        .Where(s => result.Any(user => user.Id == s.ReportedBy))
				.ToList()
				.Where(s => s.ReportedOn.CompareTo(DateTime.Now.AddDays(-1)) >= 0);

	        var emergencies = _dbContext.Alerts
		        .Where(e => result.Any(user => user.Id == e.ReportedByFk) && e.IsEmergency)
		        .ToList()
				.Where(e => e.ReportedOn.CompareTo(DateTime.Now.AddDays(-1)) >= 0);

	        var users = result.ToList();

	        foreach (var user in users)
	        {
		        if (emergencies.Any(e => e.ReportedByFk == user.Id))
			        user.HasEmergency = true;

		        if (checkins.Any(c => c.ReportedBy == user.Id))
			        user.HasCheckIn = true;
	        }

            return users;
        }

        public Task<List<ContactRelationshipDto>> GetInvitesAsync(int userId)
        {
            return _userRelationshipRepository.GetAll()
                .Where(x => x.UserRelationshipStatusFk == (int) UserRelationshipStatusEnum.Pending
                            && x.IsIgnored == false
                            && x.UserInvitedFk == userId)
                .SelectContactRelationshipDto(userId)
                .ToListAsync();
        }

        public Task<List<ContactRelationshipDto>> GetContactsAsync(int userId)
        {
            return _userRelationshipRepository.GetAll()
                .Where(x => x.UserRelationshipStatusFk == (int) UserRelationshipStatusEnum.Accepted
                            && x.IsIgnored == false
                            && (x.UserRequesterFk == userId || x.UserInvitedFk == userId))
                .SelectContactRelationshipDto(userId)
                .ToListAsync();
        }

        public Task<List<ContactRelationshipDto>> GetUsersForInviteAsync(string searchTerms, ContactOrderEnum? order, int userId)
        {
            var invitations = _userRelationshipRepository.GetAll()
                .Where(x => (x.UserRelationshipStatusFk == (int) UserRelationshipStatusEnum.Pending
                             || x.UserRelationshipStatusFk == (int) UserRelationshipStatusEnum.Accepted) &&
                            (x.UserInvitedFk == userId || x.UserRequesterFk == userId))
                .Select(x => x.UserInvitedFk == userId ? x.UserRequesterFk : x.UserInvitedFk);

            var query = _userRepository.GetAll()
                .Where(x => !invitations.Contains(x.Id))
                .SelectContactRelationshipDto(userId)
                .Order(order);

            if (!string.IsNullOrWhiteSpace(searchTerms)) 
                query = query.Where(x => x.FullName.ToLower().Contains(searchTerms.ToLower()));

            return query.Take(100)
                .ToListAsync();
        }

        public ContactRelationshipsViewModel GetCurrentUserContactRelationshipViewModel(User currentUser)
        {
            var currentUserContactRelationships = new ContactRelationshipsViewModel();

            int userId = currentUser.Id;

            var contacts =
                //first determine any contacts this person has accepted or requested 
                (from initiatedContacts in _dbContext.Users
                 join ur in _dbContext.UserRelationship on initiatedContacts.Id equals ur.UserInvitedFk
                 where (ur.UserRequesterFk == userId)
                       && ur.IsIgnored == false 
                       && (ur.UserRelationshipStatusFk == (int)UserRelationshipStatusEnum.Accepted ||
                        ur.UserRelationshipStatusFk == (int)UserRelationshipStatusEnum.Pending)
                 select new ContactRelationshipDto
                 {
                     Id = initiatedContacts.Id,
                     Username = initiatedContacts.UserName,
                     FullName = initiatedContacts.FirstName + " " + initiatedContacts.LastName,
                     MapAddress = initiatedContacts.CurrentMapAddressFormatted,
                     Latitude = initiatedContacts.CurrentLocation.Latitude,
                     Longitude = initiatedContacts.CurrentLocation.Longitude,
                     LocationTimeStamp = initiatedContacts.CurrentLocationDateTime,
                     RelationshipStatus = (UserRelationshipStatusEnum)ur.UserRelationshipStatusFk,
                     IsRelationshipStatusOwner = ur.UserRequesterFk == userId
                 })

                    //this query is for requests going in the opposite direction
                    .Union(from pendingRequests in _dbContext.Users
                           join ur in _dbContext.UserRelationship on pendingRequests.Id equals ur.UserRequesterFk
                           where (ur.UserInvitedFk == userId)
                                 && ur.IsIgnored == false
                                 && ur.UserRelationshipStatusFk == (int)UserRelationshipStatusEnum.Accepted
                           select new ContactRelationshipDto
                           {
                               Id = pendingRequests.Id,
                               Username = pendingRequests.UserName,
                               FullName = pendingRequests.FirstName + " " + pendingRequests.LastName,
                               MapAddress = pendingRequests.CurrentMapAddressFormatted,
                               Latitude = pendingRequests.CurrentLocation.Latitude,
                               Longitude = pendingRequests.CurrentLocation.Longitude,
                               LocationTimeStamp = pendingRequests.CurrentLocationDateTime,
                               RelationshipStatus = (UserRelationshipStatusEnum)ur.UserRelationshipStatusFk,
                               IsRelationshipStatusOwner = ur.UserRequesterFk == userId
                           })
                           .OrderByDescending(x => x.RelationshipStatus).ToList();

            currentUserContactRelationships.Contacts.AddRange(contacts.ToList());

            return currentUserContactRelationships;
        }

        public void AcceptUserRelationshipInvitation(int userRequestorId, int userInvitedId)
        {
            UserRelationship userRelationship = GetUserRelationship(userRequestorId, userInvitedId,
                UserRelationshipStatusEnum.Pending);
            userRelationship.AcceptInvitation();
            _userRelationshipRepository.SaveChanges();
        }

        private UserRelationship CreateRelationship(int userRequestorId, int userInvitedId, UserRelationshipStatusEnum userRelationshipStatusEnum = UserRelationshipStatusEnum.Pending)
        {
            var userRelationship = new UserRelationship
            {
                IsIgnored = false,
                UserInvitedFk = userInvitedId,
                UserRequesterFk = userRequestorId,
                UserRelationshipStatusFk = (int)userRelationshipStatusEnum,
                CreatedDate = DateTime.Now
            };
            return userRelationship;
        }


        public void RejectUserRelationship(int userId, int currentUserId)
        {
            var userRelationship = GetUserRelationship(userId, currentUserId);
            userRelationship.Reject();
            _userRelationshipRepository.SaveChanges();
        }

	    public Task DeleteUserRelationship(int userId, int currentUserId)
	    {
            var userRelationship = GetUserRelationship(userId, currentUserId);
			_userRelationshipRepository.Delete(userRelationship);
			return _userRelationshipRepository.SaveChangesAsync();
		}

		public void IgnoreUserRelationship(int userId, int currentUserId)
        {
            var userRelationship = GetUserRelationship(userId, currentUserId);
            userRelationship.Ignore();
            _userRelationshipRepository.SaveChanges();
        }

        public void Invite(User invitee, User currentUser)
        {
            var isExistingUserRelationship = IsExistingUserRelationship(currentUser.Id, invitee.Id);
            if (isExistingUserRelationship)
            {
                return;
            }

            var userRelationship = CreateRelationship(currentUser.Id, invitee.Id);
            _userRelationshipRepository.Add(userRelationship);
            _userRelationshipRepository.SaveChanges();

        }

        public Task InviteAsync(int userId, int currentUserId)
        {
            var isExistingUserRelationship = IsExistingUserRelationship(currentUserId, userId);
            if (isExistingUserRelationship)
                return Task.FromResult(0);

            var userRelationship = CreateRelationship(currentUserId, userId);
            _userRelationshipRepository.Add(userRelationship);
            return _userRelationshipRepository.SaveChangesAsync();
        }



        /// <summary>
        /// This method determines if there is any type of relationship that is not ignored
        /// 
        /// </summary>
        /// <param name="userRequesterId"></param>
        /// <param name="userInvitedId"></param>
        /// <returns></returns>
        private bool IsExistingUserRelationship(int userRequesterId, int userInvitedId)
        {
            return _userRelationshipRepository.GetAll()
                                .Any(
                                    relationship => relationship.IsIgnored == false
                                    && (
                                        relationship.UserInvitedFk == userInvitedId &&
                                        relationship.UserRequesterFk == userRequesterId
                                        ||
                                        relationship.UserRequesterFk == userInvitedId &&
                                        relationship.UserInvitedFk == userRequesterId)
                                );
        }

        public UserRelationship GetUserRelationship(int userRequesterId, int userInvitedId,
            UserRelationshipStatusEnum userRelationshipStatus)
        {
            var userRelationships =
                _userRelationshipRepository.GetAll()
                    .Where(
                        relationship =>
                            (relationship.IsIgnored == false) &&
                            relationship.UserInvitedFk == userInvitedId &&
                            relationship.UserRequesterFk == userRequesterId &&
                            relationship.UserRelationshipStatusFk == (int)userRelationshipStatus);

            if (!userRelationships.Any())
            {
                throw new ArgumentException("Invalid request for a nonexistent relationship");
            }

            return userRelationships.First();
        }

        public UserRelationship GetUserRelationship(int userId, int currentUserId)
        {
            return _userRelationshipRepository.GetAll()
                .FirstOrDefault(x => (x.UserInvitedFk == userId && x.UserRequesterFk == currentUserId) 
                    || (x.UserInvitedFk == currentUserId && x.UserRequesterFk == userId));
        }

        public bool IsPermittedToViewContactDetailsFor(int contactId, User currentUser)
        {
            return _userRelationshipRepository.GetAll()
                           .Any(
                               relationship => relationship.IsIgnored == false
                               && (
                                   relationship.UserInvitedFk == contactId &&
                                   relationship.UserRequesterFk == currentUser.Id &&
                                   relationship.UserRelationshipStatusFk == (int)UserRelationshipStatusEnum.Accepted
                                   ||
                                   relationship.UserRequesterFk == contactId &&
                                   relationship.UserInvitedFk == currentUser.Id &&
                                   (
                                   relationship.UserRelationshipStatusFk == (int)UserRelationshipStatusEnum.Pending ||
                                   relationship.UserRelationshipStatusFk == (int)UserRelationshipStatusEnum.Accepted

                                   )
                                   )

                           );
        }
    }

}