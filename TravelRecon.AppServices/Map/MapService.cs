﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Caching;
using TravelRecon.AppServices.IntelReports;
using TravelRecon.AppServices.IntelReports.ViewDtos;
using TravelRecon.AppServices.Map.ViewDtos;
using TravelRecon.AppServices.SharedDtos;
using TravelRecon.AppServices.SubscriptionOffers;
using TravelRecon.AppServices.Users;
using TravelRecon.Common.Extensions;
using TravelRecon.Domain.Alerts;
using TravelRecon.Domain.Alerts.QueryObjects;
using TravelRecon.Domain.Factories;
using TravelRecon.Domain.Identity;
using TravelRecon.Domain.IntelReports;
using TravelRecon.Domain.Localization;
using TravelRecon.Domain.Localization.QueryObjects;
using TravelRecon.Domain.Map;
using TravelRecon.Domain.RepositoryInterfaces;

namespace TravelRecon.AppServices.Map
{
    public class MapService : IAppService
    {
        private readonly UserRelationshipService _userRelationshipService;
        private readonly DestinationAppService _destinationAppService;
        private readonly SubscriptionOffersService _subscriptionOffersService;
        private readonly UserService _userService;
        private readonly IRepository<Domain.IntelReports.Country> _countryRepository;
        private readonly IRepository<User> _userRepository;
        private readonly IRepository<Label> _labelRepository;
        private readonly IRepository<Alert> _alertRepository;
        private readonly IRepository<UserLocationHistory> _userLocationHistoryRepository;
        private readonly IRepository<UserDestinations> _userDestinationRepository;

        public MapService(IRepository<UserDestinations> userDestinationRepository, 
			IRepository<User> userRepository, 
			IRepository<Label> labelRepository, 
			IRepository<Alert> alertRepository, 
            IRepository<UserLocationHistory> userLocationHistoryRepository,
            UserRelationshipService userRelationshipService, 
			DestinationAppService destinationAppService, 
            SubscriptionOffersService subscriptionOffersService,
			UserService userService,
            IRepository<Domain.IntelReports.Country> countryRepository)
        {
            _userRelationshipService = userRelationshipService;
            _alertRepository = alertRepository;
            _userLocationHistoryRepository = userLocationHistoryRepository;
            _labelRepository = labelRepository;
            _userRepository = userRepository;
            _destinationAppService = destinationAppService;
            _subscriptionOffersService = subscriptionOffersService;
            _userService = userService;
            _countryRepository = countryRepository;
            _userDestinationRepository = userDestinationRepository;
        }

        public async Task SaveCurrentLocation(Location location, string ipAddr, User currentUser)
        {
            using (_userRepository.BeginTransaction())
            {
                currentUser.CurrentLocation = DbGeographyFactory.GenerateDbGeographyValue(location.Latitude, location.Longitude);
                currentUser.CurrentLocationDateTime = DateTime.UtcNow;
                currentUser.CurrentAddressFormatted = location.AddressFormatted;
                currentUser.IpAddr = ipAddr;

                _userRepository.Modified(currentUser);
                await TrackLocationHistory(location, currentUser.Id);
                await _userRepository.SaveChangesAsync();

                _userRepository.CommitTransaction();
            }
        }

        private async Task TrackLocationHistory(Location location, int userId)
        {
            var availableEmergencyDate = DateTime.UtcNow.AddHours(-4);

            var lastEmegency = await _alertRepository.GetAll()
                .Where(x => x.ReportedByFk == userId 
                    && x.IsEmergency
                    && x.ReportedOn > availableEmergencyDate)
                .OrderByDescending(x => x.ReportedOn)
                .FirstOrDefaultAsync();

            var lastLocationHistory = await _userLocationHistoryRepository.GetAll()
                .Where(x => x.UserId == userId)
                .OrderByDescending(x => x.CreatedOn)
                .FirstOrDefaultAsync();

            var availableTrackDate = DateTime.UtcNow;
            var timeoutDays = 1;
            var criticalEmergencyHours = 1;
            var firstHourEmergencyTimeoutMins = 5;
            var afterFirstHourEmergencyTimeoutMins = 30;

            if (lastEmegency == null)
                availableTrackDate = availableTrackDate.AddDays(-timeoutDays);

            if (lastEmegency != null && lastEmegency.ReportedOn > DateTime.UtcNow.AddHours(-criticalEmergencyHours))
                availableTrackDate = availableTrackDate.AddMinutes(-firstHourEmergencyTimeoutMins);

            if (lastEmegency != null && lastEmegency.ReportedOn <= DateTime.UtcNow.AddHours(-criticalEmergencyHours))
                availableTrackDate = availableTrackDate.AddMinutes(-afterFirstHourEmergencyTimeoutMins);

            if (lastLocationHistory != null && lastLocationHistory.CreatedOn > availableTrackDate)
                return;

            var geography = DbGeographyFactory.GenerateDbGeographyValue(location.Latitude, location.Longitude);
            var destination = _destinationAppService.GetDestination(geography);

            var address = location.AddressFormatted ?? string.Empty;
            var addressParts = address.Split(',');
            var city = addressParts.FirstOrDefault()
                .GetValueOrEmpty()
                .Trim();
            var country = addressParts.LastOrDefault()
                .GetValueOrEmpty()
                .Trim();

            var userLocationHistory = new UserLocationHistory
            {
                DestinationId = destination?.Id,
                Address = address,
                Country = country,
                City = city,
                CreatedOn = DateTime.UtcNow,
                Location = geography,
                UserId = userId,
                AlertId = lastEmegency?.Id
            };

            _userLocationHistoryRepository.Add(userLocationHistory);
            await _userLocationHistoryRepository.SaveChangesAsync();
        }

        [Obsolete]
        public void SaveMapCenterPoint(Location location, User currentUser)
        {
            //todo this method will be deleted after all users gets last version of application

            var currentMapCenterPoint = DbGeographyFactory.GenerateDbGeographyValue(location);
            currentUser.CurrentMapAddressFormatted = location.AddressFormatted;
            currentUser.CurrentMapLocation = currentMapCenterPoint;
            _userRepository.SaveChanges();
        }

		public CurrentLocationViewModel GetCurrentLocation(int userId)
		{
			var user = _userRepository.GetAll().SingleOrDefault(m => m.Id == userId);
			return new CurrentLocationViewModel
			{
				Location = user.Location,
				AddressFormatted = user.CurrentAddressFormatted,
				LocationDateTime = user.CurrentLocationDateTime
			};
		}

		public MapViewModel GetMapViewModel(User currentUser)
        {
            var viewModel = new MapViewModel();

            var destinations = _destinationAppService.GetDestinationsFor(currentUser);
            viewModel.Countries =
                destinations.ConvertAll(DestinationDto.ConvertFrom)
                    .GroupBy(x => x.Country)
                    .Select(x =>
                    {
                        var firstOrDefault = x.FirstOrDefault();
                        if (firstOrDefault != null)
                            return new CountryViewModel
                            {
                                Name = x.Key,
                                RiskFactor = _countryRepository.Get(firstOrDefault.CountryId).RiskFactor, //todo fix it
                                DestinationsList = x.OrderBy(c => c.City).ToList()
                            };
                        return new CountryViewModel();
                    })
                    .OrderBy(x => x.Name)
                    .ToList();

            viewModel.IsFirstLogin = _userService.IsFirstLogin(currentUser);
            viewModel.UpgradePackageId = currentUser.GetMaxReconRole();
            viewModel.CanUpgradeSubscription = _subscriptionOffersService.CanUpgradeSubscription(currentUser.Id);

            return viewModel;
        }

	    public bool ShouldShowDisablePushModal(User currentUser)
	    {
			var result = !currentUser.PushNotificationsModalViewed 
				&& _destinationAppService.GetDestinationsFor(currentUser).Any();

			if (result)
			{
				currentUser.PushNotificationsModalViewed = true;
				var user = _userRepository.Get(currentUser.Id);
				user.PushNotificationsModalViewed = true;

				_userRepository.Modified(user);
				_userRepository.SaveChanges();
			}

			return result;
	    }


		public LayersViewModel GetLayers(User currentUser)
        {
            var targetLanguage = currentUser.GetLanguageSetting();
            var viewModel = new LayersViewModel();

            var labelsToRetrieve = new[] { LabelType.RoadView, LabelType.Satellite };
            var localizedLabels = _labelRepository.GetAll()
                .FindLabelsLocalized(labelsToRetrieve, targetLanguage).ToDictionary(x => x.LabelType, x => x.Content);

            viewModel.Layers = new List<LayerDto>
            {
                new LayerDto
                {
                    Title = localizedLabels.ContainsKey(LabelType.RoadView) ? localizedLabels[LabelType.RoadView] : "",
                    Type = "mapbox.streets"
                },
                new LayerDto
                {
                    Title = localizedLabels.ContainsKey(LabelType.Satellite) ? localizedLabels[LabelType.Satellite] : "",
                    Type = "mapbox.satellite"
                }
            };

            return viewModel;
        }

        public MapPinsViewModel GetMapPins(
            int? alertTypeId,
            bool scoutAlert,
            bool analystAlert,
            bool pushAlert,
            FilterDateTypes? filterDateType,
            AnalystAlertPriorityType? analystAlertPriorityType,
            double currentMapRadiusInMeters,
            Location currentMapLocation,
            User currentUser)
        {
            //var currentMapLocation = currentUser.CurrentMapLocation;
            //var radiusInMeters = currentUser.GetRadiusInMetersSetting();

            var location = DbGeographyFactory.GenerateDbGeographyValue(currentMapLocation.Latitude,
                currentMapLocation.Longitude);

            var purchasedDestinations = _destinationAppService.GetDestinationsFor(currentUser).Select(x => (int?)x.Id).ToArray();

            var targetLang = currentUser.GetLanguageSetting();
            var alertsNearby = _alertRepository.GetAll()
                .FindActiveAlertsNearby(location, currentMapRadiusInMeters, currentUser)
                .Filter(alertTypeId, scoutAlert, analystAlert, pushAlert, filterDateType, analystAlertPriorityType, currentUser.UserRoles, purchasedDestinations)
                .OrderByDescending(sr => sr.ReportedOn)
                .Include(a => a.AlertType)
                .ToList();

            var alertsAsPins = alertsNearby.Select(x => AlertPinDto.ConvertToDto(x, targetLang));
            var labelsToRetrive = new[] {LabelType.SeeMore, LabelType.AnalystAlert, LabelType.ScoutAlert, LabelType.Emergency};
            var labels = _labelRepository.GetAll().FindLabelsLocalizedDictionary(labelsToRetrive, targetLang);

            var viewModel = new MapPinsViewModel
            {
                AlertPins = alertsAsPins,
                Labels = labels
            };

            return viewModel;
        }

        public ContactPinsViewModel GetContactPins(User currentUser)
        {
            var contacts = _userRelationshipService.GetUsersInRelationshipWith(currentUser);

            var viewModel = new ContactPinsViewModel
            {
                ContactPins = contacts
                    .Where(c => c.CurrentLocation != null)
                    .ToList()
                    .ConvertAll(ContactPinDto.ConvertToDto)
            };

            return viewModel;
        }

	    public DestinationPinsViewModel GetDestinationPins(User currentUser)
	    {
			// no destination pins should be displayed for GoRecon users
			if (currentUser.HasMaxReconRole(UserRoleTypeEnum.GoRecon))
				return new DestinationPinsViewModel {DestinationPins = new List<DestinationPinDto>()};

		    var destinations = _destinationAppService.GetAllDestinations().ConvertAll(DestinationPinDto.ConvertFrom);
		    var userDestinations = _destinationAppService.GetDestinationsFor(currentUser);

		    foreach (var destination in destinations)
		    {
			    destination.IsPurchased = userDestinations.Any(x => x.Id == destination.Id);
		    }

			return new DestinationPinsViewModel { DestinationPins = destinations.Where(x => !x.IsPurchased).ToList()};
	    }

        public LockedDestinationsViewModel GetLockedDestinationsViewModel(User currentUser, Location location, double radiusInMeters)
        {
            if (currentUser.HasMinReconRole(UserRoleTypeEnum.TravelRecon))
                return new LockedDestinationsViewModel
                {
                     LockedDestinations = new List<DestinationDto>()
                };

            return new LockedDestinationsViewModel
            {
                LockedDestinations = _destinationAppService.GetLockedDestinations(location, radiusInMeters)
            };
        }

    }
}
