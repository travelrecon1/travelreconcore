﻿using System.Collections.Generic;

namespace TravelRecon.AppServices.Map.ViewDtos
{
    public class LayersViewModel
    {
        public IList<LayerDto> Layers { get; set; }
    }
}
