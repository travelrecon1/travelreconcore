﻿namespace TravelRecon.AppServices.Map.ViewDtos
{
    public class LayerDto
    {
        public string Title { get; set; }
        public string Type { get; set; }
    }
}
