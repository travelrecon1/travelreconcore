﻿using System.Collections.Generic;
using TravelRecon.AppServices.IntelReports.ViewDtos;

namespace TravelRecon.AppServices.Map.ViewDtos
{
    public class LockedDestinationsViewModel
    {
        public IEnumerable<DestinationDto> LockedDestinations { get; set; } 
    }
}
