﻿using System;
using System.Linq;
using TravelRecon.Domain.Alerts;
using TravelRecon.Domain.Localization;

namespace TravelRecon.AppServices.Map.ViewDtos
{
    public class AlertPinDto
    {
        public int Id { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public bool IsEmergency { get; set; }
        public short EmergencyExpiration { get; set; }
        public bool IsAnalystAlert { get; set; }
        public bool IsScoutAlert { get; set; }
        public string Title { get; set; }
        public int Priority { get; set; }

        public static AlertPinDto ConvertToDto(Alert entity, LanguageType language)
        {
            if (entity == null)
                return null;

            var dto = new AlertPinDto
            {
                Id = entity.Id,
                Latitude = entity.Location.Latitude.Value,
                Longitude = entity.Location.Longitude.Value,
                IsAnalystAlert = entity.Source == AlertSourceType.Analyst,
                IsScoutAlert = entity.Source == AlertSourceType.Scout,
                Title = entity.AlertType.Translations
                    .Where(x => x.Language == language)
                    .Select(x => x.Name)
                    .FirstOrDefault(),
                Priority = entity.AlertType.AlertPriorityFk,
                IsEmergency = entity.IsEmergency
            };

            //after 30 min emergency alert become general alert
            if (entity.IsEmergency)
            {
                var min = (DateTime.UtcNow - entity.ReportedOn).TotalMinutes;
                if (min < 30)
                {
                    dto.IsEmergency = true;
                    dto.EmergencyExpiration = (short)(30 - Math.Floor(min));
                }
            }

            return dto;
        }
    }
}
