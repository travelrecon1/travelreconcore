﻿using System.Collections.Generic;

namespace TravelRecon.AppServices.Map.ViewDtos
{
    public class ContactPinsViewModel
    {
        public ContactPinsViewModel() {
            ContactPins = new List<ContactPinDto>();
        }

        public IEnumerable<ContactPinDto> ContactPins { get; set; }
    }
}
