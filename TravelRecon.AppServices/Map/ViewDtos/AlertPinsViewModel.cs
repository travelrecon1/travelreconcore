﻿using System.Collections.Generic;

namespace TravelRecon.AppServices.Map.ViewDtos
{
    public class MapPinsViewModel
    {
        public IEnumerable<AlertPinDto> AlertPins { get; set; }
        public Dictionary<int, string> Labels { get; set; } 
    }
}
