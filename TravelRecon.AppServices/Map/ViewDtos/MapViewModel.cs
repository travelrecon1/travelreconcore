﻿using System.Collections.Generic;
using TravelRecon.AppServices.SharedDtos;
using TravelRecon.Domain.Identity;
using TravelRecon.Domain.Tutorial;

namespace TravelRecon.AppServices.Map.ViewDtos
{
    public class MapViewModel: BasePageViewModel
    {
        public IList<CountryViewModel> Countries { get; set; }
        public override PageTypeEnum PageType => PageTypeEnum.Map;
        public bool IsFirstLogin { get; set; }
        public UserRoleTypeEnum? UpgradePackageId { get; set; }
        public bool CanUpgradeSubscription { get; set; }
    }
}
