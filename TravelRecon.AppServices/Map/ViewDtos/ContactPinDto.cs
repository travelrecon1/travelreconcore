﻿using System;
using System.Data.Entity.Spatial;
using TravelRecon.Domain.Extensions;
using TravelRecon.Domain.Factories;
using TravelRecon.Domain.Identity;
using TravelRecon.Domain.Map;

namespace TravelRecon.AppServices.Map.ViewDtos
{
    public class ContactPinDto
    {
        public int Id { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string Username { get; set; }
        public string FullName { get; set; }
        public bool ObfuscateLocation { get; set; }
	    public DateTime? LocationUpdatedTime { get; set; }

        private static readonly Random random = new Random();

        //obfuscate to 2 mile width circle = 1 mile radius = 1.60934km
        private const double obfuscationRadius = 1.60934;
        public double ObfuscationRadius { get { return obfuscationRadius; } }

        public static ContactPinDto ConvertToDto(IEntityWithLocation entityWithLocation)
        {
            if (entityWithLocation == null)
                return null;

            var dto = new ContactPinDto();
            dto.Id = entityWithLocation.Id;
            dto.Latitude = entityWithLocation.Location.Latitude.Value;
            dto.Longitude = entityWithLocation.Location.Longitude.Value;
			

            var user = entityWithLocation as User;
            if (user != null)
            {
                dto.Username = user.UserName;
                dto.FullName = user.Fullname();
	            dto.LocationUpdatedTime = user.CurrentLocationDateTime;

                dto.ObfuscateLocation = user.ObfuscateLocation;
                if (dto.ObfuscateLocation)
                {
                    var obfuscatedLocation = GetObfuscatedLocation(user.Location);
                    dto.Latitude = obfuscatedLocation.Latitude.Value;
                    dto.Longitude = obfuscatedLocation.Longitude.Value;
                }
            }

            return dto;
        }

        public static DbGeography GetObfuscatedLocation(DbGeography location)
        {
            double metersInLat = location.Distance(DbGeographyFactory.GenerateDbGeographyValue(location.Latitude.Value + 1, location.Longitude.Value)).Value;
            double metersInLong = location.Distance(DbGeographyFactory.GenerateDbGeographyValue(location.Latitude.Value, location.Longitude.Value + 1)).Value;

            double latDegreeInKm = 1000 / metersInLat;
            double longDegreeInKm = 1000 / metersInLong;

            //user's position will be moved within a square area inside a circle with obfuscation radius
            double kmOffset = obfuscationRadius*Math.Sqrt(2)/2;

            double latOffset = latDegreeInKm * random.NextDouble() * kmOffset * (random.Next(0, 2) == 0 ? 1 : -1);
            double longOffset = longDegreeInKm * random.NextDouble() * kmOffset * (random.Next(0, 2) == 0 ? 1 : -1);

            return DbGeographyFactory.GenerateDbGeographyValue(location.Latitude.Value + latOffset, location.Longitude.Value + longOffset);
        }
    }
}
