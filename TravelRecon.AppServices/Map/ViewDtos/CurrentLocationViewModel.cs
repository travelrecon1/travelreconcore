﻿using System;
using System.Data.Entity.Spatial;

namespace TravelRecon.AppServices.Map.ViewDtos
{
    public class CurrentLocationViewModel
    {
        public DbGeography Location { get; set; }
        public string AddressFormatted { get; set; }
        public DateTime? LocationDateTime { get; set; }
    }
}
