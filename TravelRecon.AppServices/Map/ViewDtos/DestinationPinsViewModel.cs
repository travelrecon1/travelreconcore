﻿using System.Collections.Generic;
using TravelRecon.Domain.IntelReports;

namespace TravelRecon.AppServices.Map.ViewDtos
{
	public class DestinationPinsViewModel
	{
		public DestinationPinsViewModel()
		{
			DestinationPins = new List<DestinationPinDto>();
		}

		public List<DestinationPinDto> DestinationPins { get; set; }
	}

	public class DestinationPinDto
	{
		public int Id { get; set; }
		public int DrupalId { get; set; }
		public string Country { get; set; }
		public string City { get; set; }
		public double Latitude { get; set; }
		public double Longitude { get; set; }
		public bool IsLocked { get; set; }
		public bool IsComingSoon { get; set; }
		public bool IsPurchased { get; set; }

		public static DestinationPinDto ConvertFrom(LocalizedDestination destination)
		{
			return new DestinationPinDto
			{
				Id = destination.Id,
				Country = destination.Country,
				City = destination.City,
				Latitude = destination.Latitude,
				Longitude = destination.Longitude,
				IsLocked = destination.IsLocked,
				IsComingSoon = destination.IsComingSoon
			};
		}
	}
}
