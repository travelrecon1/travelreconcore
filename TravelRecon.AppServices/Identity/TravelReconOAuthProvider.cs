﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security.OAuth;
using TravelRecon.AppServices.SharedDtos;
using TravelRecon.AppServices.Users;
using TravelRecon.Domain.RepositoryInterfaces;
using TravelRecon.Domain.Identity;

namespace TravelRecon.AppServices.Identity
{
    public class TravelReconOAuthProvider : OAuthAuthorizationServerProvider
    {
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context) {
            var username = context.Parameters["username"];
            var password = context.Parameters["password"];
            var deviceType = context.Parameters["devicetype"];
            var deviceId = context.Parameters["deviceid"];
            bool isTravelRecon = false;

            if (context.Parameters["isTravelRecon"] != null)
            {
                isTravelRecon = Convert.ToBoolean(context.Parameters["isTravelRecon"]);
            }

            DeviceDto device = null;
            int dType;

            if (!string.IsNullOrWhiteSpace(deviceId) && int.TryParse(deviceType, out dType))
                device = new DeviceDto(deviceId, dType);

            if (string.IsNullOrEmpty(username) && string.IsNullOrEmpty(password)) {
				ReturnAuthError(context, "missing_credentials", "Client credentials could not be retrieved. Please, fill the login form");
				return;
            }

            var authenticationService = DependencyResolver.Current.GetService<AuthenticationService>();
            var authenticationConfirmation = authenticationService.Authenticate(username, password);

            if (authenticationConfirmation.WasSuccessful) {
                var authenticatedUser = authenticationConfirmation.Value;
                if (isTravelRecon && !authenticatedUser.HasMinReconRole(UserRoleTypeEnum.TravelRecon))
                {
					ReturnAuthError(context, "invalid_credentials", "TravelRecon account needed.");
					return;
                }

                var userService = DependencyResolver.Current.GetService<UserService>();
                userService.SaveUserLocally(authenticatedUser, device);

                context.Validated();
            }
            else
            {
	            ReturnAuthError(context, "invalid_credentials", "Incorrect username or password.");
            }
        }

        /// <summary>
        /// Invoked by the OAuth infrastructure, after ValidateClientAuthentication.  So we know 
        /// the user has been validated and exists in the local DB.
        /// </summary>
        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context) {
            var userManager = DependencyResolver.Current.GetService<TravelReconUserManager>();
            var userRepository = DependencyResolver.Current.GetService<IRepository<User>>();

            var user = userRepository.GetAll().Single(u => u.UserName == context.UserName);
            var identity = await userManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ExternalBearer);
            context.Validated(identity);
        }

	    private void ReturnAuthError(OAuthValidateClientAuthenticationContext context, string error, string message)
	    {
			context.SetError(error, message);
			context.Response.Headers.Add("AuthorizationResponse", new[] { "Failed" });
		}
    }
}
