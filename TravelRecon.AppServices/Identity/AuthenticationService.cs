﻿using System.Threading.Tasks;
using System.Web.Security;
using TravelRecon.AppServices.Interfaces;
using TravelRecon.AppServices.Push;
using TravelRecon.AppServices.SharedDtos;
using TravelRecon.AppServices.Users;
using TravelRecon.Domain;
using TravelRecon.Domain.Identity;
using TravelRecon.Domain.RepositoryInterfaces;

namespace TravelRecon.AppServices.Identity
{
    public class AuthenticationService : IAppService
    {
        public AuthenticationService(TravelReconUserManager userManager, UserService userService, IRepository<User> userRepository) {
            _userManager = userManager;
            _userService = userService;
            _userRepository = userRepository;
        }

        /// <summary>
        /// Checks to see if the username / password authenticate against Drupal;
        /// if so, then returns the Drupal-authenticated user information. If the Id
        /// of the returned user is 0, this means that the user successfully authenticated
        /// against Drupal but was not yet found in the local DB.
        /// This ONLY authenticates, it does NOT ensure the Drupal-authenticated user exists
        /// in the local DB.
        /// </summary>
        internal ActionConfirmation<User> Authenticate(string username, string password) {
            // Task.Run turns the async method into a synchronous invocation
            
            var authenticatedUser = Task.Run(() => _userManager.FindAsync(username, password)).Result;

            return authenticatedUser != null
                ? ActionConfirmation<User>.CreateSuccessConfirmation(authenticatedUser)
                : ActionConfirmation<User>.CreateFailureConfirmation("Invalid username or password");
        }

        private readonly IRepository<User> _userRepository;
        private readonly TravelReconUserManager _userManager;
        private readonly UserService _userService;
    }
}
