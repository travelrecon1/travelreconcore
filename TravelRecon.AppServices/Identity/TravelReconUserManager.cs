﻿using System;
using System.Web.Mvc;
using TravelRecon.Domain.Identity;
using Microsoft.AspNet.Identity;
using TravelRecon.Domain.RepositoryInterfaces;
using System.Threading.Tasks;
using System.Security.Claims;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security.DataProtection;

namespace TravelRecon.AppServices.Identity
{
    // Configure the application user manager used in this application. UserManager is defined in ASP.NET Identity and is used by the application.
    public class TravelReconUserManager : UserManager<User, int>
    {
        public static IDataProtectionProvider DataProtectionProvider { get; set; }
        
        public TravelReconUserManager(IRepository<User> userRepository, IRepository<UserRoles> userRolesRepository)
            : base(new TravelReconUserStore(userRepository, userRolesRepository)) {

            var dataProtectionProvider = DataProtectionProvider;
            UserTokenProvider = new DataProtectorTokenProvider<User, int>(dataProtectionProvider.Create("PasswordRecovery"))
            {
                TokenLifespan = TimeSpan.FromHours(24)
            };
        }

        /// <summary>
        /// Invoked to create the identity token; overridden just to show that it's used
        /// </summary>
        public override Task<ClaimsIdentity> CreateIdentityAsync(User user, string authenticationType) {
            return base.CreateIdentityAsync(user, authenticationType);
        }

        public static TravelReconUserManager Create() {
            var userRepository = DependencyResolver.Current.GetService<IRepository<User>>();
            var userRolesRepository = DependencyResolver.Current.GetService<IRepository<UserRoles>>();
            return new TravelReconUserManager(userRepository, userRolesRepository);
        }
    }
}
