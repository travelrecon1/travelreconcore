﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using TravelRecon.Domain.Extensions;
using TravelRecon.Domain.Identity;
using TravelRecon.Domain.RepositoryInterfaces;

namespace TravelRecon.AppServices.Identity
{
    /// <summary>
    /// Provides a custom user store for retrieving users;
    /// this is because authentication is currently being provided via a Drupal integration.
    /// </summary>
    public class TravelReconUserStore : 
        IUserPasswordStore<User, int>, 
        IUserRoleStore<User, int>, 
        IUserLockoutStore<User, int>, 
        IUserTwoFactorStore<User, int>,
        IUserEmailStore<User, int>
    {
        public TravelReconUserStore(IRepository<User> userRepository, IRepository<UserRoles> userRolesRepository)
        {
            _userRepository = userRepository;
            _userRolesRepository = userRolesRepository;
        }

        public IQueryable<User> Users
        {
            get { return _userRepository.GetAll(); }
        }

        public void Dispose()
        {
        }

        public Task CreateAsync(User user)
        {
            _userRepository.Add(user);
            return _userRepository.SaveChangesAsync();
        }

        public Task UpdateAsync(User user)
        {
            _userRepository.Modified(user);
            return _userRepository.SaveChangesAsync();
        }

        public Task DeleteAsync(User user)
        {
            _userRepository.Delete(user);
            return _userRepository.SaveChangesAsync();
        }

        public Task<string> GetPasswordHashAsync(User user)
        {
            return Task.FromResult(user.PasswordHash);
        }

        public Task SetPasswordHashAsync(User user, string passwordHash)
        {
            user.PasswordHash = passwordHash;
            return Task.FromResult(0);
        }

        public Task<bool> HasPasswordAsync(User user)
        {
            return Task.FromResult(!string.IsNullOrWhiteSpace(user.PasswordHash));
        }
        public async Task<IList<string>> GetRolesAsync(User user)
        {
            var roles = await _userRolesRepository.GetAll()
                .Where(x => x.UserId == user.Id)
                .Select(x => x.UserRole.Name)
                .ToListAsync();

            return roles;
        }

        public async Task RemoveFromRoleAsync(User user, string roleName)
        {
            var isInRole = await IsInRoleAsync(user, roleName);
            if (!isInRole)
                return;

            var userRole = await _userRolesRepository.GetAll()
                .FirstOrDefaultAsync(x => x.UserRole.Name == roleName && x.UserId == user.Id);

            _userRolesRepository.Delete(userRole);

            await _userRolesRepository.SaveChangesAsync();
        }

        public async Task AddToRoleAsync(User user, string roleName)
        {
            var isInRole = await IsInRoleAsync(user, roleName);
            if (isInRole)
                return;

            _userRolesRepository.Add(new UserRoles
            {
                UserId = user.Id,
                RoleId = (int) roleName.ConverToEnum<UserRoleTypeEnum>()
            });

            await _userRolesRepository.SaveChangesAsync();
        }

        public Task<User> FindByIdAsync(string userId)
        {
            var uid = int.Parse(userId);
            return _userRepository.GetAll()
                .FirstOrDefaultAsync(x => x.Id == uid);
        }

        public Task<User> FindByIdAsync(int userId)
        {
            return _userRepository.GetAll()
                .FirstOrDefaultAsync(x => x.Id == userId);
        }

        public Task<User> FindByNameAsync(string userName)
        {
            return _userRepository.GetAll()
                .Include(x => x.UserRoles)
                .FirstOrDefaultAsync(x => x.UserName == userName);
        }

        public Task SetEmailAsync(User user, string email)
        {
            user.Email = email;
            return Task.FromResult(0);
        }

        public Task<string> GetEmailAsync(User user)
        {
            return Task.FromResult(user.Email);
        }

        public Task<bool> GetEmailConfirmedAsync(User user)
        {
            return Task.FromResult(user.EmailConfirmed);
        }

        public Task SetEmailConfirmedAsync(User user, bool confirmed)
        {
            user.EmailConfirmed = confirmed;
            return Task.FromResult(0);
        }

        public Task<User> FindByEmailAsync(string email)
        {
            return _userRepository.GetAll()
                .Where(x => x.Email == email)
                .SingleOrDefaultAsync();
        }

        public Task<bool> IsInRoleAsync(User user, string roleName)
        {
            return _userRolesRepository.GetAll()
                .AnyAsync(x =>
                    x.UserId == user.Id
                    && x.UserRole.Name == roleName);
        }

        private readonly IRepository<User> _userRepository;
        private readonly IRepository<UserRoles> _userRolesRepository;

        #region Not Implemented

        public Task<DateTimeOffset> GetLockoutEndDateAsync(User user)
        {
            throw new NotImplementedException();
        }

        public Task SetLockoutEndDateAsync(User user, DateTimeOffset lockoutEnd)
        {
            throw new NotImplementedException();
        }

        public Task<int> IncrementAccessFailedCountAsync(User user)
        {
            throw new NotImplementedException();
        }

        public Task ResetAccessFailedCountAsync(User user)
        {
            throw new NotImplementedException();
        }

        public Task<int> GetAccessFailedCountAsync(User user)
        {
            throw new NotImplementedException();
        }

        public Task<bool> GetLockoutEnabledAsync(User user)
        {
            throw new NotImplementedException();
        }

        public Task SetLockoutEnabledAsync(User user, bool enabled)
        {
            throw new NotImplementedException();
        }

        public Task SetTwoFactorEnabledAsync(User user, bool enabled)
        {
            throw new NotImplementedException();
        }

        public Task<bool> GetTwoFactorEnabledAsync(User user)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
