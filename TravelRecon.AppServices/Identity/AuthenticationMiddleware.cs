﻿using System.Threading.Tasks;
using Microsoft.Owin;

namespace TravelRecon.AppServices.Identity
{
	public class AuthenticationMiddleware : OwinMiddleware
	{
		public AuthenticationMiddleware(OwinMiddleware next) : base(next)
		{
		}

		public override async Task Invoke(IOwinContext context)
		{
			await Next.Invoke(context);

			if (context.Response.StatusCode == 400 && context.Response.Headers.ContainsKey("AuthorizationResponse"))
			{
				context.Response.Headers.Remove("AuthorizationResponse");
				context.Response.StatusCode = 403;
			}
		}
	}
}
