﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using TravelRecon.Api.Model;
using TravelRecon.Api.ServiceIntarfaces;
using TravelRecon.AppServices.Push.Dtos;
using TravelRecon.AppServices.SharedDtos;

namespace TravelRecon.AppServices.Push
{
    public class PushService: IPushService
    {
        public const string UndefinedDeviceId = "undefined";

        private readonly IAndroidPushService _androidPushService;
        private readonly IIosPushService _iosPushService;

        public PushService(IAndroidPushService androidPushService, IIosPushService iosPushService)
        {
            _androidPushService = androidPushService;
            _iosPushService = iosPushService;
        }

        public void Push(PushMessageDto pushMessage)
        {
            var notification = GetNotification(pushMessage);

            switch (pushMessage.Device.DeviceType)
            {
                case DeviceTypeEnum.Android:
                    _androidPushService.Push(notification);
                    break;
                case DeviceTypeEnum.Ios:
                    _iosPushService.Push(notification);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private PushNotification GetNotification(PushMessageDto pushMessage)
        {
            return new PushNotification
            {
                DeviceId = pushMessage.Device.DeviceId,
                Data = pushMessage.Data
            };
        }

        public void Push(IEnumerable<PushMessageDto> pushMessages)
        {
            foreach (var msg in pushMessages)
                Push(msg);
        }

        public static string UpdateDevices(string userDevices, DeviceDto device)
        {
            var devices = GetDevices(userDevices).ToList();

            if (device == null || devices.Any(x => x.DeviceType == device.DeviceType && x.DeviceId == device.DeviceId))
                return userDevices;

            devices.Add(device);

            userDevices = JsonConvert.SerializeObject(devices, new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            });

            return userDevices;
        }

        public static string RemoveDevice(string userDevices, DeviceDto device)
        {
            var devices = GetDevices(userDevices).ToList();

            if (device == null)
                return userDevices;

            devices = devices.Where(x => x.DeviceType != device.DeviceType && x.DeviceId != device.DeviceId).ToList();

            userDevices = JsonConvert.SerializeObject(devices, new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            });

            return userDevices;
        }

        public static IEnumerable<DeviceDto> GetDevices(string userDevices)
        {
            if (string.IsNullOrWhiteSpace(userDevices))
                return new List<DeviceDto>();

            var devices = JsonConvert.DeserializeObject<List<DeviceDto>>(userDevices);
            return devices.Where(m => m.DeviceId != UndefinedDeviceId);
        }
    }
}
