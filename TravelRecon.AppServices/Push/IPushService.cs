﻿using System.Collections.Generic;
using TravelRecon.AppServices.Push.Dtos;

namespace TravelRecon.AppServices.Push
{
    public interface IPushService
    {
        void Push(PushMessageDto pushMessage);
        void Push(IEnumerable<PushMessageDto> pushMessages);
    }
}
