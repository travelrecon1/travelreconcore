﻿using TravelRecon.Api.Model;

namespace TravelRecon.AppServices.Push.Dtos
{
    public class AlertNotificationDto: NotificationBaseDto
    {
        public override NotificationType Type { get { return NotificationType.Alert; } }

        public int AlertId { get; set; }
        public string Description { get; set; }
        public string AddressFormatted { get; set; }
        public string ReportedOnDateFormatted { get; set; }
        public string ReportedOnTimeFormatted { get; set; }
        public string AlertTypeCategoryName { get; set; }
        public string AlertTypeName { get; set; }
    }
}
