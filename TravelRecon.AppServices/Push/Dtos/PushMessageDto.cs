﻿using TravelRecon.Api.Model;
using TravelRecon.AppServices.SharedDtos;

namespace TravelRecon.AppServices.Push.Dtos
{
    public class PushMessageDto
    {
        public DeviceDto Device { get; set; }
        public NotificationBaseDto Data { get; set; }
    }
}