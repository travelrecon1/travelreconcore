﻿using System;
using TravelRecon.Domain.Itinerary;

namespace TravelRecon.AppSeenurvices.Itineraries.ViewDtos
{
    public class CreateItineraryTrpManagerDto
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string ReservationNumber { get; set; }

        public string DepartureCountry { get; set; }
        public string DepartureCity { get; set; }
        public string DepartureAirport { get; set; }
        public DateTime DepartureDateTime { get; set; }
        public double? DepartureLongitude { get; set; }
        public double? DepartureLatitude { get; set; }

        public string ArrivalCountry { get; set; }
        public string ArrivalCity { get; set; }
        public string ArrivalAirport { get; set; }
        public DateTime ArrivalDateTime { get; set; }
        public double? ArrivalLongitude { get; set; }
        public double? ArrivalLatitude { get; set; }

        public string HotelReservationNumber { get; set; }
        public string HotelCountry { get; set; }
        public string HotelCity { get; set; }
        public string HotelName { get; set; }
        public string HotelAddress { get; set; }
        public DateTime? HotelCheckinDate { get; set; }
        public DateTime? HotelCheckoutDate { get; set; }

        public int ItineraryType { get; set; }

        public Itinerary ConvertTo()
        {
            return new Itinerary
            {
                UserId = UserId,
                ReservationNumber = ReservationNumber,
                DepartureCountry = DepartureCountry,
                DepartureCity = DepartureCity,
                DepartureAirport = DepartureAirport,
                DepartureDateTime = DepartureDateTime,
                ArrivalCountry = ArrivalCountry,
                ArrivalCity = ArrivalCity,
                ArrivalAirport = ArrivalAirport,
                ArrivalDateTime = ArrivalDateTime,
                HotelReservationNumber = HotelReservationNumber,
                HotelCountry = HotelCountry,
                HotelCity = HotelCity,
                HotelName = HotelName,
                HotelAddress = HotelAddress,
                HotelCheckinDate = HotelCheckinDate,
                HotelCheckoutDate = HotelCheckoutDate,
                ItineraryType = (ItineraryType)ItineraryType
            };
        }
    }
}