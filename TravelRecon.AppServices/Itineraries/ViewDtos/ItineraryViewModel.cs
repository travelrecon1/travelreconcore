﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using TravelRecon.Domain.Identity;
using TravelRecon.Domain.Itinerary;

namespace TravelRecon.AppServices.Itineraries.ViewDtos
{
    public class ItineraryViewModel
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public ItineraryType ItineraryType { get; set; }
        public string ReservationNumber { get; set; }

        public string DepartureCountry { get; set; }
        public string DepartureCity { get; set; }
        public string DepartureAirport { get; set; }
        public DateTime DepartureDateTime { get; set; }
        public double? DepartureRiskRating { get; set; }

        public string ArrivalCountry { get; set; }
        public string ArrivalCity { get; set; }
        public string ArrivalAirport { get; set; }
        public DateTime ArrivalDateTime { get; set; }
        public double? ArrivalRiskRating { get; set; }

        public string HotelReservationNumber { get; set; }
        public string HotelCountry { get; set; }
        public string HotelCity { get; set; }
        public string HotelName { get; set; }
        public string HotelAddress { get; set; }
        public DateTime? HotelCheckinDate { get; set; }
        public DateTime? HotelCheckoutDate { get; set; }


        public DateTime CreatedOn { get; set; }
        public DateTime ModifedOn { get; set; }
        public int CreatedById { get; set; }
        public int ModifiedById { get; set; }
    }
}
