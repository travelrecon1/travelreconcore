﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Threading.Tasks;
using TravelRecon.AppSeenurvices.Itineraries.ViewDtos;
using TravelRecon.AppServices.IntelReports;
using TravelRecon.AppServices.Interfaces;
using TravelRecon.AppServices.Itineraries.Queries;
using TravelRecon.AppServices.Itineraries.ViewDtos;
using TravelRecon.Domain.Factories;
using TravelRecon.Domain.Identity;
using TravelRecon.Domain.IntelReports;
using TravelRecon.Domain.Itinerary;
using TravelRecon.Domain.RepositoryInterfaces;

namespace TravelRecon.AppServices.Itineraries
{
    public class ItinerariesService : IAppService
    {
        private readonly IRepository<Itinerary> _itinerariesRepository;
        private readonly IRepository<Destination> _destinationsRepository;

        public ItinerariesService(
            IRepository<Itinerary> itinerariesRepository
            , IRepository<Destination> destinationsRepository)
        {
            _itinerariesRepository = itinerariesRepository;
            _destinationsRepository = destinationsRepository;
        }

        public Task<ItineraryViewModel> GetItineraryByIdAsync(int id)
        {
            var destinations = _destinationsRepository.GetAll();

            return _itinerariesRepository.GetAll()
                .Where(x => x.Id == id)
                .SelectItineraryViewModel(destinations)
                .FirstOrDefaultAsync();
        }

        public async Task<ItineraryViewModel> CreateItineraryTrpManagerAsync(User currentUser, CreateItineraryTrpManagerDto model)
        {
            var itinerary = model.ConvertTo();

            itinerary.ItineraryType = (ItineraryType)model.ItineraryType;
            itinerary.CreatedOn = DateTime.UtcNow;
            itinerary.CreatedById = currentUser.Id;
            itinerary.ModifedOn = DateTime.UtcNow;
            itinerary.ModifiedById = currentUser.Id;

            if (model.DepartureLatitude.HasValue && model.DepartureLongitude.HasValue)
            {
                itinerary.DepartureLocation = DbGeographyFactory
                    .GenerateDbGeographyValue(model.DepartureLatitude.Value, model.DepartureLongitude.Value);
            }

            if (model.ArrivalLatitude.HasValue && model.ArrivalLongitude.HasValue)
            {
                itinerary.ArrivalLocation = DbGeographyFactory
                    .GenerateDbGeographyValue(model.ArrivalLatitude.Value, model.ArrivalLongitude.Value);
            }

            _itinerariesRepository.Add(itinerary);
            await _itinerariesRepository.SaveChangesAsync();

            return await GetItineraryByIdAsync(itinerary.Id);
        }

        public async Task<ItineraryViewModel> EditItineraryTrpManagerAsync(User currentUser, int id,
            CreateItineraryTrpManagerDto model)
        {
            var itinerary = await _itinerariesRepository.GetAll().FirstOrDefaultAsync(x => x.Id == id);

            itinerary.ReservationNumber = model.ReservationNumber;
            itinerary.DepartureCountry = model.DepartureCountry;
            itinerary.DepartureCity = model.DepartureCity;
            itinerary.DepartureAirport = model.DepartureAirport;
            itinerary.DepartureDateTime = model.DepartureDateTime;

            itinerary.ArrivalCountry = model.ArrivalCountry;
            itinerary.ArrivalCity = model.ArrivalCity;
            itinerary.ArrivalAirport = model.ArrivalAirport;
            itinerary.ArrivalDateTime = model.ArrivalDateTime;

            itinerary.HotelReservationNumber = model.HotelReservationNumber;
            itinerary.HotelCountry = model.HotelCountry;
            itinerary.HotelCity = model.HotelCity;
            itinerary.HotelName = model.HotelName;
            itinerary.HotelAddress = model.HotelAddress;
            itinerary.HotelCheckinDate = model.HotelCheckinDate;
            itinerary.HotelCheckoutDate = model.HotelCheckoutDate;

            itinerary.ModifedOn = DateTime.UtcNow;
            itinerary.ModifiedById = currentUser.Id;

            itinerary.ItineraryType = (ItineraryType)model.ItineraryType;

            if (model.DepartureLatitude.HasValue && model.DepartureLongitude.HasValue)
            {
                itinerary.DepartureLocation = DbGeographyFactory
                    .GenerateDbGeographyValue(model.DepartureLatitude.Value, model.DepartureLongitude.Value);
            }

            if (model.ArrivalLatitude.HasValue && model.ArrivalLongitude.HasValue)
            {
                itinerary.ArrivalLocation = DbGeographyFactory
                    .GenerateDbGeographyValue(model.ArrivalLatitude.Value, model.ArrivalLongitude.Value);
            }

            _itinerariesRepository.Modified(itinerary);
            await _itinerariesRepository.SaveChangesAsync();

            return await GetItineraryByIdAsync(itinerary.Id);
        }

        public Task<List<ItineraryViewModel>> GetItinerariesForUser(int userId)
        {
            var destinations = _destinationsRepository.GetAll();

            return _itinerariesRepository.GetAll()
                .Where(x => x.UserId == userId)
                .SelectItineraryViewModel(destinations)
                .OrderByDescending(x => x.ModifedOn)
                .ToListAsync();
        }

        public Task<ItineraryViewModel> GetUpcomingItineraryForUserAsync(int userId)
        {
            var destinations = _destinationsRepository.GetAll();

            return _itinerariesRepository.GetAll()
                .Where(x => x.UserId == userId && x.DepartureDateTime > DateTime.Now)
                .SelectItineraryViewModel(destinations)
                .OrderByDescending(x => x.DepartureDateTime)
                .FirstOrDefaultAsync();
        }
    }
}
