﻿using System;
using System.Data.Entity;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Linq.Expressions;
using LinqKit;
using TravelRecon.AppServices.Itineraries.ViewDtos;
using TravelRecon.Domain.IntelReports;
using TravelRecon.Domain.Itinerary;

namespace TravelRecon.AppServices.Itineraries.Queries
{
    public static class ItinerariesQueries
    {
        public static IQueryable<ItineraryViewModel> SelectItineraryViewModel(this IQueryable<Itinerary> itineraries, 
            IQueryable<Destination> destinations)
        {
            var query = itineraries.AsExpandable();
            var destinationExpr = GetItineraryPointDesinationExpression(destinations);
            double distance = 50000; //nearest destincation within 50 km radius


            return query.Select(x => new ItineraryViewModel
            {
                Id = x.Id,
                UserId = x.UserId,
                ItineraryType = x.ItineraryType,
                ReservationNumber = x.ReservationNumber,
                DepartureCountry = x.DepartureCountry,
                DepartureCity = x.DepartureCity,
                DepartureAirport = x.DepartureAirport,
                DepartureDateTime = x.DepartureDateTime,
                DepartureRiskRating = destinationExpr.Invoke(x.DepartureLocation, distance).RiskFactor 
                    ?? destinationExpr.Invoke(x.DepartureLocation, distance).Country.RiskFactor,
                ArrivalCountry = x.ArrivalCountry,
                ArrivalCity = x.ArrivalCity,
                ArrivalAirport = x.ArrivalAirport,
                ArrivalDateTime = x.ArrivalDateTime,
                ArrivalRiskRating = destinationExpr.Invoke(x.ArrivalLocation, distance).RiskFactor
                    ?? destinationExpr.Invoke(x.ArrivalLocation, distance).Country.RiskFactor,
                HotelReservationNumber = x.HotelReservationNumber,
                HotelCountry = x.HotelCountry,
                HotelCity = x.HotelCity,
                HotelName = x.HotelName,
                HotelAddress = x.HotelAddress,
                HotelCheckinDate = x.HotelCheckinDate,
                HotelCheckoutDate = x.HotelCheckoutDate,
                CreatedOn = x.CreatedOn,
                ModifedOn = x.ModifedOn,
                CreatedById = x.CreatedById,
                ModifiedById = x.ModifiedById
            });
        }

        public static Expression<Func<DbGeography, double, Destination>> GetItineraryPointDesinationExpression(
            IQueryable<Destination> destinations)
        {
            return (location, distance) => destinations
                .Where(x => x.Location.Distance(location) < distance)
                .OrderBy(x => x.Location.Distance(location))
                .FirstOrDefault();
        }
    }
}