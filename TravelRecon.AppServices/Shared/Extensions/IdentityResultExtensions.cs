﻿using System;
using Microsoft.AspNet.Identity;

namespace TravelRecon.AppServices.Shared.Extensions
{
    public static class IdentityResultExtensions
    {
        public static void Validate(this IdentityResult result)
        {
            if (!result.Succeeded)
                throw new Exception(string.Join(", ", result.Errors));
        }
    }
}
