﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using LinqKit;
using TravelRecon.Common.DynamicLinq;

namespace TravelRecon.AppServices.Shared
{
    public static class SharedQueries
    {
        public static IQueryable<T> FilterList<T, TList>(this IQueryable<T> query, DataSourceRequest request,
            Expression<Func<T, IEnumerable<TList>>> exprProp)
        {
            var expression = (MemberExpression)exprProp.Body;
            string propName = expression.Member.Name;

            var filters = request.GetFiltersByField(propName);
            request.RemoveFiltersByField(propName);

            var isString = typeof(TList) == typeof(string);

            foreach (var f in filters)
            {
                var value = (TList)f.GetValue();

                if (!isString)
                    query = query.Where(x => exprProp.Invoke(x).Contains(value));
                else
                {
                    var strArr = value.ToString().Split(',').Cast<TList>().ToList();
                    query = query.Where(x => exprProp.Invoke(x).Any(v => strArr.Contains(v)));
                }
            }

            return query;
        }
    }
}
