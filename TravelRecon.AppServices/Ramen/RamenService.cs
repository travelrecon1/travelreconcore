﻿using System;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using TravelRecon.AppServices.Extensions;
using TravelRecon.AppServices.Ramen.ViewDtos;
using TravelRecon.Domain;
using TravelRecon.Domain.Identity;

namespace TravelRecon.AppServices.Ramen
{
    public class RamenService : IAppService
    {
        private readonly string _orgId;
        private readonly string _orgApiKey;

        public RamenService(): this(ConfigSettings.RamenOrganizationId, ConfigSettings.RamenOrganizationApiKey)
        {
        }

        public RamenService(string orgId, string orgApiKey)
        {
            _orgId = orgId;
            _orgApiKey = orgApiKey;
        }

        private string GetAuthHash(long timespan, RamenUser ramenUser)
        {
            var arrParams = new[]
            {
                ramenUser.Email,
                ramenUser.Id.ToString(),
                ramenUser.Name,
                timespan.ToString(),
                _orgApiKey
            };

            var str = string.Join(":", arrParams);

            using (var shaManaged = new SHA256Managed())
            {
                byte[] data = Encoding.UTF8.GetBytes(str);

                var bytes = shaManaged.ComputeHash(data);
                return BitConverter.ToString(bytes).Replace("-", "").ToLower();
            }
        }

        public Task<RamenSettings> GetRamenSettings(User currentUser)
        {
            var ramenUser = new RamenUser
            {
                Email = currentUser.Email,
                Id = currentUser.Id,
                Name = currentUser.UserName
            };

            var unixTicks = DateTime.UtcNow.GetUnixTicks();

            var settings = new RamenSettings
            {
                Auth_hash = GetAuthHash(unixTicks, ramenUser),
                Organization_id = _orgId,
                Timestamp = unixTicks,
                User = ramenUser
            };

            return Task.FromResult(settings);
        }
    }
}
