﻿namespace TravelRecon.AppServices.Ramen.ViewDtos
{
    public class RamenUser
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
    }
}
