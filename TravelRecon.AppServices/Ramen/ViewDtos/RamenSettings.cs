﻿namespace TravelRecon.AppServices.Ramen.ViewDtos
{
    public class RamenSettings
    {
        public string Organization_id { get; set; }
        public RamenUser User { get; set; }
        public long Timestamp { get; set; }
        public string Auth_hash { get; set; }
    }
}
