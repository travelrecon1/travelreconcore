﻿using System.Collections.Generic;
using TravelRecon.Domain.Localization.QueryObjects;

namespace TravelRecon.AppServices.Statuses.ViewDtos
{
    public class AddEmergencyPageDto
    {
        public UserStatusDto Status { get; set; }
        public IEnumerable<LabelLocalizedDto> Labels { get; set; }
    }
}
