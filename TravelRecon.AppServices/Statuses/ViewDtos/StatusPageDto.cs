﻿using System.Collections.Generic;
using TravelRecon.Domain.Localization.QueryObjects;

namespace TravelRecon.AppServices.Statuses.ViewDtos
{
    public class StatusPageDto
    {
        public UserStatusDto LastCheckInFormatted { get; set; }
        public UserStatusDto LastEmergencyFormatted { get; set; }
        public UserStatusDto LastAlertFormatted { get; set; }
    }
}
