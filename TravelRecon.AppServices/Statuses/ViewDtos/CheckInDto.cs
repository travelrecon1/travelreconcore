﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TravelRecon.Domain.Statuses;

namespace TravelRecon.AppServices.Statuses.ViewDtos
{
    public class CheckInDto
    {
        public int Id { get; set; }

        public string Description { get; set; }

        [Range(-90, 90)]
        public double? Latitude { get; set; }

        [Range(-180, 180)]
        public double? Longitude { get; set; }

        public string AddressFormatted { get; set; }

        public DateTime ReportedOn { get; set; }

        public string ReportedOnFormatted
        {
            get { return ReportedOn.ToString("o"); }
        }

        public static CheckInDto ConvertToDto(Status status)
        {
            if (status == null)
                return null;

            var dto = new CheckInDto
            {
                Id = status.Id,
                AddressFormatted = status.AddressFormatted,
                Description = status.Description,
                ReportedOn = status.ReportedOn
            };

            if (status.Location != null)
            {
                dto.Latitude = status.Location.Latitude.Value;
                dto.Longitude = status.Location.Longitude.Value;
            }

            return dto;
        }
    }
}
