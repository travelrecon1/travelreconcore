﻿using System;
using TravelRecon.Domain.Statuses;

namespace TravelRecon.AppServices.Statuses.ViewDtos
{
    public class UserStatusDto
    {
        public StatusType Type { get; set; }
        public string UserName { get; set; }
        public string AddressFormatted { get; set; }
        public DateTime ReportedOn { get; set; }
        public int ReportedBy { get; set; }
        public string DateTimeFormatted { get { return ReportedOn.ToString("o"); } }
        public string DateFormatted { get { return $"{ReportedOn:d MMM yyyy}"; } }
        public int EntityId { get; set; }
    }
}
