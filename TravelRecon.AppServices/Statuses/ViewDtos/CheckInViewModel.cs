﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TravelRecon.Domain.Localization.QueryObjects;

namespace TravelRecon.AppServices.Statuses.ViewDtos
{
    public class CheckInViewModel
    {
        public CheckInDto CheckIn { get; set; }
        public IEnumerable<LabelLocalizedDto> Labels { get; set; }
    }
}
