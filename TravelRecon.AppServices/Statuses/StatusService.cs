﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Web.Caching;
using LinqKit;
using TravelRecon.AppServices.Alerts.ViewDtos;
using TravelRecon.AppServices.Communications.ViewDtos;
using TravelRecon.AppServices.Statuses.ViewDtos;
using TravelRecon.Common.Extensions;
using TravelRecon.Common.Models;
using TravelRecon.Data;
using TravelRecon.Data.Repositories;
using TravelRecon.Domain.Alerts;
using TravelRecon.Domain.Alerts.QueryObjects;
using TravelRecon.Domain.Factories;
using TravelRecon.Domain.Identity;
using TravelRecon.Domain.Identity.UserRelationships;
using TravelRecon.Domain.License;
using TravelRecon.Domain.Localization;
using TravelRecon.Domain.Localization.QueryObjects;
using TravelRecon.Domain.Map;
using TravelRecon.Domain.Polls;
using TravelRecon.Domain.RepositoryInterfaces;
using TravelRecon.Domain.Statuses;
using Z.EntityFramework.Plus;

namespace TravelRecon.AppServices.Statuses
{
    public class StatusService : IAppService
    {
        private readonly IRepository<Status> _statusRepository;
        private readonly IRepository<Label> _labelRepository;
        private readonly IRepository<User> _userRepository;
        private readonly IRepository<ReadCommunication> _readCommunicationsRepository;
        private readonly IRepository<UserLicense> _userLicenseRepository;
        private readonly IRepository<PollAnswer> _pollAnswerRepository;
        private readonly IRepository<Alert> _alertRepository;
        private readonly TravelReconDbContext _dbContext;

        public StatusService(IRepository<Status> statusRepository, 
            IRepository<Label> labelRepository, 
            IRepository<Alert> alertRepository, 
            IRepository<User> userRepository, 
            IRepository<ReadCommunication> readCommunicationsRepository,
            IRepository<UserLicense> userLicenseRepository,
            IRepository<PollAnswer> pollAnswerRepository,  
            TravelReconDbContext dbContext)
        {
            _statusRepository = statusRepository;
            _labelRepository = labelRepository;
            _userRepository = userRepository;
            _readCommunicationsRepository = readCommunicationsRepository;
            _userLicenseRepository = userLicenseRepository;
            _pollAnswerRepository = pollAnswerRepository;
            _alertRepository = alertRepository;
            _dbContext = dbContext;
        }

        public UserStatusDto CheckIn(CheckInRequestParams checkInRequest, User currentUser)
        {
            if (checkInRequest.MeansForLocationInput != MeansForLocationInputType.UseCurrentLocation
                && (!checkInRequest.SelectedLatitude.HasValue || !checkInRequest.SelectedLongitude.HasValue))
                throw new ArgumentException();

            var location = checkInRequest.MeansForLocationInput == MeansForLocationInputType.UseCurrentLocation
                 ? currentUser.CurrentLocation
                 : DbGeographyFactory.GenerateDbGeographyValue(checkInRequest.SelectedLatitude.Value, checkInRequest.SelectedLongitude.Value);

            var address = checkInRequest.MeansForLocationInput == MeansForLocationInputType.UseCurrentLocation
                 ? currentUser.CurrentAddressFormatted
                 : checkInRequest.SelectedAddressFormatted;

            var status = new Status
            {
                AddressFormatted = address,
                Location = location,
                ReportedBy = currentUser.Id,
                ReportedOn = DateTime.UtcNow,
                Type = StatusType.CheckIn,
                Description = checkInRequest.Description
            };

            status = _statusRepository.Add(status);
            _statusRepository.SaveChanges();

            var statusDto = GetStatusById(status.Id);
            return statusDto;
        }

        private UserStatusDto GetStatusById(int id)
        {
            var status = _statusRepository.GetAll()
                 .Where(x => x.Id == id)
                 .Select(x => new UserStatusDto

                 {
                     Type = x.Type,
                     AddressFormatted = x.AddressFormatted,
                     ReportedOn = x.ReportedOn,
                     UserName = x.Reporter.UserName
                 })
                 .FirstOrDefault();

            return status;
        }

        public string Emergency(User currentUser)
        {
            var status = _statusRepository.Add(new Status
            {
                AddressFormatted = currentUser.CurrentAddressFormatted,
                Location = currentUser.CurrentLocation,
                ReportedBy = currentUser.Id,
                ReportedOn = DateTime.UtcNow,
                Type = StatusType.Emergency
            });

            _statusRepository.SaveChanges();

            var dateFormatted = status.ReportedOn.ToString("o");
            return dateFormatted;
        }

        private IEnumerable<int> GetContanctIds(int userId)
        {
            var contactIds = _dbContext.UserRelationship.Where(
               x => x.UserRelationshipStatusFk == (int)UserRelationshipStatusEnum.Accepted &&
                   (x.UserInvitedFk == userId)).Select(x => x.UserRequesterFk).Union(_dbContext.UserRelationship.Where(
               x => x.UserRelationshipStatusFk == (int)UserRelationshipStatusEnum.Accepted &&
                   (x.UserRequesterFk == userId)).Select(x => x.UserInvitedFk)).Distinct().ToList();

            return contactIds;
        }

        public List<UserStatusDto> GetCheckIns(User currentUser)
        {
            var contactIds = GetContanctIds(currentUser.Id);

            var statusQueries = _userRepository.GetAll()
                    .Where(x => contactIds.Contains(x.Id))
                    .SelectMany(x => x.Statuses
                        .Select(s => new UserStatusDto
                        {
                            Type = s.Type,
                            AddressFormatted = s.AddressFormatted,
                            ReportedOn = s.ReportedOn,
                            UserName = x.UserName,
                            ReportedBy = x.Id,
                            EntityId = s.Id
                        }));

            return statusQueries
                .OrderByDescending(s => s.ReportedOn)
                .ToList();
        }

        public List<UserStatusDto> GetEmergencies(User currentUser)
        {
            var contactIds = GetContanctIds(currentUser.Id);

            var emergenciesQuery =
                _alertRepository.GetAll()
                    .Where(em => contactIds.Contains(em.ReportedByFk) && em.IsEmergency)
                    .Select(em => new UserStatusDto
                    {
                        Type = StatusType.Emergency,
                        AddressFormatted = em.AddressFormatted,
                        ReportedOn = em.ReportedOn,
                        UserName = em.ReportedBy.UserName,
                        ReportedBy = em.ReportedByFk,
                        EntityId = em.Id
                    });

            return emergenciesQuery
                .OrderByDescending(s => s.ReportedOn)
                .ToList();
        }

        public StatusPageDto GetLasts(int userId)
        {
            var lastEmergency =
                _alertRepository.GetAll()
                    .Where(m => m.ReportedByFk == userId && m.IsEmergency)
                    .OrderByDescending(m => m.ReportedOn)
                    .Select(em => new UserStatusDto
                    {
                        Type = StatusType.Emergency,
                        AddressFormatted = em.AddressFormatted,
                        ReportedOn = em.ReportedOn,
                        UserName = em.ReportedBy.UserName,
                        ReportedBy = em.ReportedByFk,
                        EntityId = em.Id
                    }).DeferredFirstOrDefault().FutureValue();

            var lastAlert =
                _alertRepository.GetAll()
                    .Where(m => m.ReportedByFk == userId &&  m.IsEmergency == false)
                    .OrderByDescending(m => m.ReportedOn)
                    .Select(em => new UserStatusDto
                    {
                        AddressFormatted = em.AddressFormatted,
                        ReportedOn = em.ReportedOn,
                        UserName = em.ReportedBy.UserName,
                        EntityId = em.Id
                    }).DeferredFirstOrDefault().FutureValue();

            var lastCheckIn = _statusRepository.GetAll()
                .Where(m => m.ReportedBy == userId && m.Type == StatusType.CheckIn)
                .OrderByDescending(m => m.ReportedOn)
                .Select(m => new UserStatusDto
                {
                    Type = m.Type,
                    AddressFormatted = m.AddressFormatted,
                    ReportedOn = m.ReportedOn,
                    ReportedBy = m.Id,
                    EntityId = m.Id
                }).DeferredFirstOrDefault().FutureValue();

            return new StatusPageDto
            {
                LastAlertFormatted = lastAlert.Value,
                LastCheckInFormatted = lastCheckIn.Value,
                LastEmergencyFormatted = lastEmergency.Value
            };
        }

        public CheckInPageViewModel GetCheckInPage(User currentUser)
        {
            var targetLanguage = currentUser.GetLanguageSetting();

            var vm = new CheckInPageViewModel();

            var labelsToRetrieve = new[] {
                 LabelType.CheckIn, LabelType.Description,
                 LabelType.Location, LabelType.Save, LabelType.DescriptionIsRequired,
                 LabelType.Date, LabelType.Time,
                 LabelType.UseCurrentLocation, LabelType.ClickLocationOnMap, LabelType.TypeInAnAddress,
                 LabelType.ClickTheMapToSelectALocation, LabelType.ValidAddressRequired
             };

            vm.Labels = _labelRepository.GetAll().FindLabelsLocalizedDictionary(labelsToRetrieve, targetLanguage);

            return vm;
        }

        public CheckInViewModel GetCheckInViewModel(int checkInId, User currentUser)
        {
            var viewModel = new CheckInViewModel();

            var status = _statusRepository.Get(checkInId);

            var checkInDto = CheckInDto.ConvertToDto(status);

            viewModel.CheckIn = checkInDto;

            var targetLanguage = currentUser.GetLanguageSetting();

            var labelsToRetrieve = new[] {
                LabelType.Location, LabelType.Description, LabelType.Alert, LabelType.Date, LabelType.Time, LabelType.AlertType, LabelType.ShowAlertOnMap, LabelType.Reject, LabelType.Emergency, LabelType.CheckIn,LabelType.ShowCheckInOnMap
            };
            viewModel.Labels = _labelRepository.GetAll().FindLabelsLocalized(labelsToRetrieve, targetLanguage);

            return viewModel;
        }

        public Task<RangeResult<CommunicationDto>> GetCommunicationsRange(int skip, int take)
        {
            var statuses = _statusRepository.GetAll()
                .Select(x => new CommunicationDto
                {
                    Id = x.Id,
                    AddressFormatted = x.AddressFormatted,
                    Description = x.Description,
                    ReportedOn = x.ReportedOn,
                    Type = StatusType.CheckIn,
                    UserFirstName = x.Reporter.FirstName,
                    UserLastName = x.Reporter.LastName,
                    Username = x.Reporter.UserName,
                    Location = new Location { Longitude = x.Location.Longitude.Value, Latitude = x.Location.Latitude.Value},
                    AvatarUrl = "http://www.snappertech.com/images/prashant-surana.jpg"
                });

            var emergencies = _alertRepository.GetAll()
                .Where(x => x.IsEmergency)
                .FindActiveAlerts()
                .Select(x => new CommunicationDto
                {
                    Id = x.Id,
                    AddressFormatted = x.AddressFormatted,
                    Description = x.Description,
                    ReportedOn = x.ReportedOn,
                    Type = StatusType.Emergency,
                    UserFirstName = x.ReportedBy.FirstName,
                    UserLastName = x.ReportedBy.LastName,
                    Username = x.ReportedBy.UserName,
                    Location = new Location { Longitude = x.Location.Longitude.Value, Latitude = x.Location.Latitude.Value },
                    AvatarUrl = "http://www.snappertech.com/images/prashant-surana.jpg"
                });

            return statuses.Union(emergencies)
                .OrderByDescending(x => x.ReportedOn)
                .ToRangeResultAsync(skip, take);
        }

        public Task<RangeResult<CommunicationDto>> GetUnreadCommunications(User currentUser, int skip, int take)
        {
            var userLicenseQuery = _userLicenseRepository.GetAll();

            var readCheckinsIds = _readCommunicationsRepository.GetAll()
                .Where(x => x.StatusType == StatusType.CheckIn && x.UserId == currentUser.Id)
                .Select(x => x.StatusId);

            var readEmergenciesIds = _readCommunicationsRepository.GetAll()
                .Where(x => x.StatusType == StatusType.Emergency && x.UserId == currentUser.Id)
                .Select(x => x.StatusId);

            var readPollAnswersIds = _readCommunicationsRepository.GetAll()
                .Where(x => (x.StatusType == StatusType.PollAnswer || x.StatusType == StatusType.NotRespondedPollAnswer) 
                    && x.UserId == currentUser.Id)
                .Select(x => x.StatusId);

            var statuses = _statusRepository.GetAll()
                .Where(x => userLicenseQuery.Any(ul => ul.OwnerId == x.ReportedBy && ul.ManagerId == currentUser.Id))
                .Where(x => !readCheckinsIds.Contains(x.Id))
                .GroupBy(x => x.ReportedBy)
                .Select(x => x.OrderByDescending(r => r.ReportedOn).FirstOrDefault())
                .Select(x => new CommunicationDto
                {
                    Id = x.Id,
                    AddressFormatted = x.AddressFormatted,
                    Description = x.Description,
                    ReportedOn = x.ReportedOn,
                    Type = StatusType.CheckIn,
                    UserFirstName = x.Reporter.FirstName,
                    UserLastName = x.Reporter.LastName,
                    Username = x.Reporter.UserName,
                    Location =
                        new Location {Longitude = x.Location.Longitude.Value, Latitude = x.Location.Latitude.Value},
                    AvatarUrl = "http://www.snappertech.com/images/prashant-surana.jpg"
                });

            var emergencies = _alertRepository.GetAll()
                .Where(x => userLicenseQuery.Any(ul => ul.OwnerId == x.ReportedByFk && ul.ManagerId == currentUser.Id))
                .Where(x => x.IsEmergency && !readEmergenciesIds.Contains(x.Id))
                .FindActiveAlerts()
                .Select(x => new CommunicationDto
                {
                    Id = x.Id,
                    AddressFormatted = x.AddressFormatted,
                    Description = x.Description,
                    ReportedOn = x.ReportedOn,
                    Type = StatusType.Emergency,
                    UserFirstName = x.ReportedBy.FirstName,
                    UserLastName = x.ReportedBy.LastName,
                    Username = x.ReportedBy.UserName,
                    Location = new Location { Longitude = x.Location.Longitude.Value, Latitude = x.Location.Latitude.Value },
                    AvatarUrl = "http://www.snappertech.com/images/prashant-surana.jpg"
                });

            var pollAnswers = _pollAnswerRepository.GetAll()
                .Where(x => userLicenseQuery.Any(ul => ul.OwnerId == x.UserId && ul.ManagerId == currentUser.Id))
                .Where(x => !readPollAnswersIds.Contains(x.Id))
                .Where(x => x.Status == PollAnswerStatusEnum.Answered || x.Poll.ExpirationDate < DateTime.UtcNow)
                .Select(x => new CommunicationDto
                {
                    Id = x.Id,
                    AddressFormatted = "",
                    Description = x.Status == PollAnswerStatusEnum.Answered ? x.Answer : "No Answer",
                    ReportedOn =  x.UpdatedDate,
                    Type = x.Status == PollAnswerStatusEnum.Answered ? StatusType.PollAnswer : StatusType.NotRespondedPollAnswer,
                    UserFirstName = x.User.FirstName,
                    UserLastName = x.User.LastName,
                    Username = x.User.UserName,
                    Location = new Location { Longitude = 0, Latitude = 0 },
                    AvatarUrl = "http://www.snappertech.com/images/prashant-surana.jpg"
                });

            return statuses.Union(emergencies).Union(pollAnswers)
                .OrderByDescending(x => x.ReportedOn)
                .ToRangeResultAsync(skip, take);
        }

        public Task SetCommunicationRead(User currentUser, StatusType communicationType, int communicationId)
        {
            var readCommunication = new ReadCommunication
            {
                StatusType = communicationType,
                StatusId = communicationId,
                UserId = currentUser.Id
            };

            _readCommunicationsRepository.Add(readCommunication);
            return _readCommunicationsRepository.SaveChangesAsync();
        }

        public Task<List<CommunicationDto>> GetCommunicationsAsync(int userId, CommunicationOrderType orderType)
        {
            var contactIds = GetContanctIds(userId);

            var statusQueries = _userRepository.GetAll()
                    .Where(x => contactIds.Contains(x.Id))
                    .SelectMany(u => u.Statuses.Select(x => new CommunicationDto
                    {
                        Id = x.Id,
                        AddressFormatted = x.AddressFormatted,
                        Description = x.Description,
                        ReportedOn = x.ReportedOn,
                        Type = StatusType.CheckIn,
                        UserFirstName = x.Reporter.FirstName,
                        UserLastName = x.Reporter.LastName,
                        Username = x.Reporter.UserName,
                        Location =
                        new Location { Longitude = x.Location.Longitude.Value, Latitude = x.Location.Latitude.Value },
                        AvatarUrl = "http://www.snappertech.com/images/prashant-surana.jpg",
                        ExpirationDate = null
                    }));

            var emergenciesQuery =
              _alertRepository.GetAll()
                  .Where(em => contactIds.Contains(em.ReportedByFk) && em.IsEmergency)
                  .Select(x => new CommunicationDto
                  {
                      Id = x.Id,
                      AddressFormatted = x.AddressFormatted,
                      Description = x.Description,
                      ReportedOn = x.ReportedOn,
                      Type = StatusType.Emergency,
                      UserFirstName = x.ReportedBy.FirstName,
                      UserLastName = x.ReportedBy.LastName,
                      Username = x.ReportedBy.UserName,
                      Location = new Location { Longitude = x.Location.Longitude.Value, Latitude = x.Location.Latitude.Value },
                      AvatarUrl = "http://www.snappertech.com/images/prashant-surana.jpg",
                      ExpirationDate = null
                  });

            var pollsQuery = _pollAnswerRepository.GetAll()
                .Where(x => x.UserId == userId 
                    && x.Status == PollAnswerStatusEnum.Pending 
                    && x.Poll.ExpirationDate > DateTime.UtcNow)
                .Select(x => new CommunicationDto
                {
                    Id = x.Poll.Id,
                    AddressFormatted = "",
                    Description = x.Poll.Question,
                    ReportedOn = x.Poll.CreatedDate,
                    Type = StatusType.NotRespondedPollAnswer,
                    UserFirstName = x.Poll.CreatedBy.FirstName,
                    UserLastName = x.Poll.CreatedBy.LastName,
                    Username = x.Poll.CreatedBy.UserName,
                    Location = new Location { Longitude = 0, Latitude = 0 },
                    AvatarUrl = "http://www.snappertech.com/images/prashant-surana.jpg",
                    ExpirationDate = x.Poll.ExpirationDate
                });

            var query = statusQueries.Union(emergenciesQuery).Union(pollsQuery);
            query = query.AsExpandable();

            Expression<Func<CommunicationDto, int>> cdtoExpr =
                (c) => c.Type == StatusType.Emergency
                        ? 100 : c.Type == StatusType.PollAnswer 
                            || c.Type == StatusType.NotRespondedPollAnswer ? 50 : 1;

            var orderedQuery = query.OrderByDescending(x => cdtoExpr.Invoke(x));

            switch (orderType)
            {
                case CommunicationOrderType.FirstName:
                    query = orderedQuery.ThenBy(x => x.UserFirstName);
                    break;
                case CommunicationOrderType.LastName:
                    query = orderedQuery.ThenBy(x => x.UserLastName);
                    break;
                case CommunicationOrderType.Recent:
                    query = orderedQuery.ThenByDescending(x => x.ReportedOn);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(orderType), orderType, null);
            }

            return query.ToListAsync();
        }

        public Task<CheckInDto> GetCheckInById(int id)
        {
            return _statusRepository.GetAll()
                .Select(x => new CheckInDto
                {
                    AddressFormatted = x.AddressFormatted,
                    Description = x.Description,
                    Id = x.Id,
                    Latitude = x.Location.Latitude,
                    Longitude = x.Location.Longitude,
                    ReportedOn = x.ReportedOn
                })
                .FirstOrDefaultAsync(x => x.Id == id);
        }
    }
}
