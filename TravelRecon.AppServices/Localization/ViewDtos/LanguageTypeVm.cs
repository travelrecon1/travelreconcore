﻿using TravelRecon.Domain.Localization;

namespace TravelRecon.AppServices.Localization.ViewDtos
{
    public class LanguageTypeVm
    {
        public LanguageType Id { get; set; }
        public string Name { get; set; }
    }
}
