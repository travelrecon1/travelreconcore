﻿using System.Collections.Generic;
using TravelRecon.AppServices.Interfaces;
using TravelRecon.Domain.Localization;
using TravelRecon.Domain.Localization.QueryObjects;
using TravelRecon.Domain.RepositoryInterfaces;

namespace TravelRecon.AppServices.Localization
{
    public class LocalizationService : IAppService
    {
        private readonly IRepository<Label> _labelRepository;

        public LocalizationService(IRepository<Label> labelRepository) {

            _labelRepository = labelRepository;
        }
        
        public IDictionary<int, string> Find(LanguageType language, LabelType[] labels)
        {
            return _labelRepository
                .GetAll()
                .FindLabelsLocalizedDictionary(labels, language);
        }
    }
}