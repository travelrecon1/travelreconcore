using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using TravelRecon.AppServices.Interfaces;
using TravelRecon.AppServices.Localization.ViewDtos;
using TravelRecon.Domain.Extensions;
using TravelRecon.Domain.Identity;
using TravelRecon.Domain.Localization;
using TravelRecon.Domain.Localization.QueryObjects;
using TravelRecon.Domain.RepositoryInterfaces;
using TravelRecon.Logger;

namespace TravelRecon.AppServices.Labels
{
    public class LabelService : IAppService
    {
        private readonly IRepository<Label> _labelRepository;
        private readonly IRepository<User> _userRepository;

        public LabelService(IRepository<Label> labelRepository, IRepository<User> userRepository)
        {
            _labelRepository = labelRepository;
            _userRepository = userRepository;
        }

        public static string GetContentFor(List<LabelLocalizedDto> labelCollection, LabelType labelType)
        {
            var labelLocalizedDto = labelCollection.FirstOrDefault(x => x.LabelType == labelType);
            if (labelLocalizedDto != null)
                return labelLocalizedDto.Content;

            return string.Empty;
        }

        public Dictionary<int, string> GetAllLocalizedLabels(LanguageType language)
        {
            return _labelRepository.GetAll()
                .Include(x => x.Translations)
				.Where(x => x.Translations.Any())
				.OrderBy(x => x.Id)
                .Select(x => new LabelLocalizedDto
                {
                    LabelType = (LabelType) x.Id,
                    Content = x.Translations.FirstOrDefault(t => t.Language == language).Content
                })
				.ToDictionary(x => (int) x.LabelType, x => x.Content);
        }

        public List<LanguageTypeVm> GetLanguages()
        {
            var ltType = typeof(LanguageType);
            var names = Enum.GetNames(ltType);

            return names.Select(x => new LanguageTypeVm
            {
                Id = (LanguageType) Enum.Parse(ltType, x),
                Name = x
            })
            .Where(x => x.Id != LanguageType.None)
            .ToList();
        }
    }
}