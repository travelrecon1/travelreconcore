﻿using System.Threading.Tasks;
using TravelRecon.AppServices.ContactUs.ViewDtos;
using TravelRecon.AppServices.Interfaces;
using TravelRecon.AppServices.Mail;

namespace TravelRecon.AppServices.ContactUs
{
    public class ContactUsService: IAppService
    {
        private readonly MailService _mailService;

        public ContactUsService(MailService mailService)
        {
            _mailService = mailService;
        }

        public void SendEmail(ContactUsEmailDto email)
        {
            _mailService.SendContactUsEmail(email.Name, email.Email, email.Message);
        }
    }
}
