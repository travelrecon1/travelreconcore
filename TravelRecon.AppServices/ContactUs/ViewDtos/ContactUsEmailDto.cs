﻿using System.ComponentModel.DataAnnotations;

namespace TravelRecon.AppServices.ContactUs.ViewDtos
{
    public class ContactUsEmailDto
    {
        [Required]
        public string Name { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        public string Message { get; set; }
    }
}
