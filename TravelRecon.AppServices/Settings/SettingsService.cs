using System.Collections.Generic;
using System.Linq;
using TravelRecon.AppServices.IntelReports;
using TravelRecon.AppServices.IntelReports.ViewDtos;
using TravelRecon.AppServices.Settings.ViewDtos;
using TravelRecon.AppServices.SharedDtos;
using TravelRecon.AppServices.SubscriptionOffers;
using TravelRecon.Domain.Identity;
using TravelRecon.Domain.IntelReports;
using TravelRecon.Domain.Localization;
using TravelRecon.Domain.Localization.QueryObjects;
using TravelRecon.Domain.RepositoryInterfaces;
using Label = TravelRecon.Domain.Localization.Label;

namespace TravelRecon.AppServices.Settings
{
	public class SettingsService : IAppService
	{
		private readonly IRepository<Label> _labelRepository;
		private readonly IRepository<User> _userRepository;
        private readonly IRepository<Destination> _destinationsRepository;
        private readonly IRepository<UserDestinations> _userDestinationsRepository;
		private readonly DestinationAppService _destinationAppService;
	    private readonly SubscriptionOffersService _subscriptionOffersService;
	    private readonly AppSettingsService _appSettingsService;
	    private readonly IRepository<Domain.IntelReports.Country> _countryRepository;


	    public SettingsService(
            IRepository<Label> labelRepository, 
            IRepository<UserDestinations> userDestinationsRepository, 
            IRepository<Destination> destinations,
			IRepository<User> userRepository, 
            DestinationAppService destinationAppService, 
            SubscriptionOffersService subscriptionOffersService,
            AppSettingsService appSettingsService,
            IRepository<Domain.IntelReports.Country> countryRepository)
		{
			_labelRepository = labelRepository;
			_userDestinationsRepository = userDestinationsRepository;
			_userRepository = userRepository;
		    _destinationsRepository = destinations;
			_destinationAppService = destinationAppService;
		    _subscriptionOffersService = subscriptionOffersService;
		    _appSettingsService = appSettingsService;
		    _countryRepository = countryRepository;
		}

		public SettingsViewModel GetSettingsPage(User currentUser)
		{
			var settingsViewModel = new SettingsViewModel();
			var languageSetting = currentUser.GetLanguageSetting();

			settingsViewModel.LanguagesList.AddRange(LanguageViewModel.GetList());

			settingsViewModel.UpgradePackageId = currentUser.HasMaxReconRole(UserRoleTypeEnum.GoRecon)
				? UserRoleTypeEnum.TravelRecon
				: (UserRoleTypeEnum?) null;
			settingsViewModel.UserLanguage =
				settingsViewModel.LanguagesList.FirstOrDefault(x => x.Id == (int?) currentUser.Language);
			settingsViewModel.DestinationSettingsViewModel = BuildDestinationViewModel(currentUser);
			settingsViewModel.LocationUpdateFrequencyViewModel = GetLocationUpdateFrequencyList(currentUser, languageSetting);
			settingsViewModel.ObfuscateLocationViewModel = BuildObfuscateLocationViewModel(currentUser);
			settingsViewModel.AlertsNotificationsSettingsViewModel = BuildAlertsNotificationsSettingsViewModel(currentUser,
				languageSetting);
			settingsViewModel.EmergencyNotificationsSettingsViewModel = BuildEmergencyNotificationsSettingsViewModel(
				currentUser, languageSetting);

			settingsViewModel.PurchasedDestinationsViewModel = BuildUserDestionationsViewModel(currentUser);
		    settingsViewModel.MapZoom = currentUser.MapZoom;

            settingsViewModel.CanUpgradeSubscription = _subscriptionOffersService.CanUpgradeSubscription(currentUser.Id);


            return settingsViewModel;
		}

	    private PurchasedDestinationsViewModel BuildUserDestionationsViewModel(User currentUser)
	    {
	        PurchasedDestinationsViewModel destinationViewModel = null;

	        if (currentUser.HasRole(UserRoleTypeEnum.GoRecon))
	        {
                destinationViewModel = new PurchasedDestinationsViewModel
                {
                    Countries = new List<MultiselectCountryViewModel>()
                };
            }
	        else
	        {
                var destinations = _userDestinationsRepository.GetAll()
                .Where(d => d.UserFk == currentUser.Id && d.IsAvailable)
                .ToList();

                destinationViewModel = new PurchasedDestinationsViewModel
                {
                    Countries = destinations.ConvertAll(UserDestinationDto.ConvertFrom)
                        .GroupBy(x => x.Country)
                        .Select(x => new MultiselectCountryViewModel
                        {
                            Name = x.Key,
                            DestinationsList = x.OrderBy(c => c.City).ToList()
                        })
                        .OrderBy(x => x.Name)
                        .ToList()
                };
            }

			var labelsToRetrieve = new List<LabelType>
			{
				LabelType.NotificationsAndAlerts,
				LabelType.ChooseDestinations,
                LabelType.SelectAll
			}.ToArray();

			var languageSetting = currentUser.GetLanguageSetting();
			var localizedLabels = _labelRepository.GetAll().FindLabelsLocalized(labelsToRetrieve, languageSetting).ToList();

			destinationViewModel.ChooseDestinationsLabel =
				localizedLabels.Single(x => x.LabelType == LabelType.ChooseDestinations).Content;

			destinationViewModel.NotificationsAndAlertsLabel =
				localizedLabels.Single(x => x.LabelType == LabelType.NotificationsAndAlerts).Content;

            destinationViewModel.ChooseAllDestinationLabel = 
                localizedLabels.Single(x => x.LabelType == LabelType.SelectAll).Content; 

			return destinationViewModel;
		}

		private List<LocationFrequencyViewModel> GetLocationUpdateFrequencyList(User currentUser, LanguageType lang)
		{
			var freqLabels = GetLocationFrequencyLabels();
			var localizedFreqLabels = _labelRepository.GetAll().FindLabelsLocalized(freqLabels, lang).ToList();
			var selectedFrequency = currentUser.LocationUpdateFrequecySeconds == 0
				? 30
				: currentUser.LocationUpdateFrequecySeconds;

			return localizedFreqLabels.Select(x => new LocationFrequencyViewModel
			{
				Label = x.Content,
				Value = GetFreqValueInSecByType(x.LabelType),
				IsSelected = GetFreqValueInSecByType(x.LabelType) == selectedFrequency
			}).OrderBy(x => x.Value).ToList();
		}

		private int GetFreqValueInSecByType(LabelType freqType)
		{
			const int secondsInHour = 3600;

			switch (freqType)
			{
				case LabelType.Immediate:
					return 0;
				case LabelType.EveryFiveSecondsDigits:
					return 5;
				case LabelType.EveryThirySecondsDigitsDefault:
					return 30;
				case LabelType.EveryTwoMinutesDigits:
					return 120;
				case LabelType.EveryThreeMinutes:
					return 180;
				case LabelType.EveryFiveMinues:
				case LabelType.EveryFiveMinutesDigits:
					return 300;
				case LabelType.EveryTenMinutesDigits:
					return 600;
				case LabelType.EveryTwentyMinutes:
					return 1200;
				case LabelType.EveryHour:
					return secondsInHour;
				case LabelType.EveryFiveHoursDigits:
					return 5*secondsInHour;
				case LabelType.EveryTwelveHoursDigits:
					return 12*secondsInHour;
				case LabelType.EveryDay:
					return 24*secondsInHour;
				default:
					return 30;
			}
		}

		private int GetFreqValueInMinByType(LabelType freqType)
		{
			int secondInMin = 60;
			var seconds = GetFreqValueInSecByType(freqType);
			return seconds/secondInMin;
		}

		private ObfuscateLocationViewModel BuildObfuscateLocationViewModel(User currentUser)
		{
			var viewModel = new ObfuscateLocationViewModel();

			viewModel.IsAvailable = _userRepository.Get(currentUser.Id).HasMinReconRole(UserRoleTypeEnum.TravelRecon);

			if (viewModel.IsAvailable)
				viewModel.ObfuscateLocation = _userRepository.Get(currentUser.Id).ObfuscateLocation;
			else
				viewModel.ObfuscateLocation = false;

			return viewModel;
		}

		private DestinationSettingsViewModel BuildDestinationViewModel(User currentUser)
		{
			var destinationViewModel = new DestinationSettingsViewModel();

			var destinations = _destinationAppService.GetDestinationsFor(currentUser);
			destinationViewModel.Countries = destinations.ConvertAll(DestinationDto.ConvertFrom)
				.GroupBy(x => x.Country)
                .Select(x =>
                {
                    var firstOrDefault = x.FirstOrDefault();
                    if (firstOrDefault != null)
                        return new CountryViewModel
                        {
                            Name = x.Key,
                            RiskFactor = _countryRepository.Get(firstOrDefault.CountryId).RiskFactor,
                            DestinationsList = x.OrderBy(c => c.City).ToList()
                        };
                    return new CountryViewModel();
                })
                .OrderBy(x => x.Name)
				.ToList();

			var labelsToRetrieve = GetDestinationLabels().ToArray();
			var languageSetting = currentUser.GetLanguageSetting();
			var localizedLabels = _labelRepository.GetAll().FindLabelsLocalized(labelsToRetrieve, languageSetting).ToList();
			var destinationSettingLabelsDto = new DestinationSettingLabelsDto
			{
				CurrentLocation = localizedLabels.Single(x => x.LabelType == LabelType.DestinationSettingCurrentLocation).Content,
				SelectDestination =
					localizedLabels.Single(x => x.LabelType == LabelType.DestinationSettingSelectDestination).Content,
				Destination = localizedLabels.Single(x => x.LabelType == LabelType.Destination).Content,
			};


			destinationViewModel.Labels = destinationSettingLabelsDto;

			return destinationViewModel;
		}

		private EmergencyNotificationsSettingsViewModel BuildEmergencyNotificationsSettingsViewModel(User currentUser,
			LanguageType lang)
		{
			return new EmergencyNotificationsSettingsViewModel
			{
				Email = currentUser.EmergencyEmail,
				Phone = currentUser.EmergencyPhone
			};
		}

		private AlertsNotificationsSettingsViewModel BuildAlertsNotificationsSettingsViewModel(User currentUser,
			LanguageType lang)
		{
			var viewModel = new AlertsNotificationsSettingsViewModel();

			var captionLabels = new[]
			{
				LabelType.TurnOnOffNotifications,
				LabelType.AlertsNotificationsFrequency,
				LabelType.MaxNumberOfAlertsNotifications
			};

			var freqLabels = new[]
			{
				LabelType.Immediate,
				LabelType.EveryThreeMinutes,
				LabelType.EveryFiveMinues,
				LabelType.EveryTwentyMinutes,
				LabelType.EveryHour,
				LabelType.EveryDay
			};

			var localizedCaptionLabels = _labelRepository.GetAll().FindLabelsLocalized(captionLabels, lang).ToList();

			viewModel.TurnOnOffNotificationsCaption =
				localizedCaptionLabels.Single(x => x.LabelType == LabelType.TurnOnOffNotifications).Content;
			viewModel.AlertPopupsShowIntervalMinutesCaption =
				localizedCaptionLabels.Single(x => x.LabelType == LabelType.AlertsNotificationsFrequency).Content;
			viewModel.MaxAlertPopupsCaption =
				localizedCaptionLabels.Single(x => x.LabelType == LabelType.MaxNumberOfAlertsNotifications).Content;

			var user = _userRepository.Get(currentUser.Id);

			viewModel.TurnOnOffNotificationsValue = user.ShowAlertPopups;
			viewModel.MaxAlertPopupsSelectedValue = user.MaxAlertPopups;

			var localizedFreqLabels = _labelRepository.GetAll().FindLabelsLocalized(freqLabels, lang).ToList();

			viewModel.AlertsNotificationsFrequencies =
				localizedFreqLabels.Select(x => new AlertsNotificationsFrequencyViewModel
				{
					Label = x.Content,
					Value = GetFreqValueInMinByType(x.LabelType),
					IsSelected = GetFreqValueInMinByType(x.LabelType) == user.AlertPopupsShowIntervalMinutes
				}).ToList();

			return viewModel;
		}


		private List<LabelType> GetDestinationLabels()
		{
			var labelTypes = new List<LabelType>
			{
				LabelType.DestinationSettingCurrentLocation,
				LabelType.DestinationSettingSelectDestination,
				LabelType.Destination,
			};
			return labelTypes;
		}

		private LabelType[] GetLocationFrequencyLabels()
		{
			return new[]
			{
				LabelType.EveryThirySecondsDigitsDefault,
				LabelType.EveryTwoMinutesDigits,
				LabelType.EveryFiveMinutesDigits,
				LabelType.EveryTenMinutesDigits,
				LabelType.EveryHour,
				LabelType.EveryFiveHoursDigits,
				LabelType.EveryTwelveHoursDigits
			};
		}

		public void ChangeDestinationNotification(User user, List<UserDestinationViewModel>  userDestinations)
		{
		    var userDestinationIds = userDestinations.Select(m => m.UserDestinationId);
            var destinations = _userDestinationsRepository.GetAll().Where(m =>m.UserFk == user.Id && userDestinationIds.Contains(m.Id)).ToList();

		    foreach (var d in destinations)
		    {
		        d.ReceiveNotifications = userDestinations.Single(m => m.UserDestinationId == d.Id).IsSelected;
                _userDestinationsRepository.Modified(d);
            }
			
			_userDestinationsRepository.SaveChanges();
		}

        public void SetUserLanguage(User currentUser, LanguageType language)
		{
			var user = _userRepository.Get(currentUser.Id);
			user.Language = language;
			_userRepository.Modified(user);
			_userRepository.SaveChanges();
		}

	    public void ClearUserLanguage(User currentUser)
	    {
            var user = _userRepository.Get(currentUser.Id);
            user.Language = null;
            _userRepository.Modified(user);
            _userRepository.SaveChanges();
        }

		public void SetUserLocationUpdateInterval(User currentUser, int seconds)
		{
			var user = _userRepository.Get(currentUser.Id);
			user.LocationUpdateFrequecySeconds = seconds;
			_userRepository.Modified(user);
			_userRepository.SaveChanges();
		}

		public void ChangeUserAlertsNotificationsShow(User currentUser, bool showNotifications)
		{
			var user = _userRepository.Get(currentUser.Id);
			user.ShowAlertPopups = showNotifications;
			_userRepository.Modified(user);
			_userRepository.SaveChanges();
		}

		public void SetUserAlertsNotificationsInterval(User currentUser, int showInterval)
		{
			var user = _userRepository.Get(currentUser.Id);
			user.AlertPopupsShowIntervalMinutes = showInterval;
			_userRepository.Modified(user);
			_userRepository.SaveChanges();
		}

		public void SetUserAlertsNotificationsMaxNumber(User currentUser, int maxAlertsNumber)
		{
			var user = _userRepository.Get(currentUser.Id);
			user.MaxAlertPopups = maxAlertsNumber;
			_userRepository.Modified(user);
			_userRepository.SaveChanges();
		}

		public void SetEmergencyNotificationEmail(User currentUser, string email)
		{
			var user = _userRepository.Get(currentUser.Id);
			if (user.HasMinReconRole(UserRoleTypeEnum.TravelRecon))
			{
				user.EmergencyEmail = email;
				_userRepository.Modified(user);
				_userRepository.SaveChanges();
			}
		}

		public void SetEmergencyNotificationPhone(User currentUser, string phone)
		{
			var user = _userRepository.Get(currentUser.Id);
			if (user.HasMinReconRole(UserRoleTypeEnum.TravelRecon))
			{
				user.EmergencyPhone = phone;
				_userRepository.Modified(user);
				_userRepository.SaveChanges();
			}
		}

		private static List<LabelType> GetLanguageLabels()
		{
			return new List<LabelType>
			{
				LabelType.LanguageEnglish,
				LabelType.LanguageSpanish,
                LabelType.LanguagePortuguese
                
            };
		}

		public DestinationDto GetSelectedDestination(User currentUser)
		{
			var selectedDestination = _destinationAppService.GetDestination(currentUser.CurrentMapLocationDrupalDestinationId,
				currentUser.GetLanguageSetting());
			return DestinationDto.ConvertFrom(selectedDestination);
		}

		public void ChangeObfuscateLocationValue(User currentUser, bool obfusctate)
		{
			var user = _userRepository.Get(currentUser.Id);

			if (user.ObfuscateLocation == obfusctate)
				return;

			user.ObfuscateLocation = obfusctate;
			_userRepository.Modified(user);
			_userRepository.SaveChanges();
		}

		public ClientSettingsViewModel GetClientSettings()
		{
			var vm = new ClientSettingsViewModel
			{
				WebSiteAppUrl = _appSettingsService.WebSiteAppUrl,
				ImagesRootUrl = _appSettingsService.ImagesRootUrl,
				SupportEmailAddress = _appSettingsService.SupportEmailAddress,
				WebSiteHelpUrl = _appSettingsService.WebSiteHelpUrl,
				WebSitePurchaseUrl = _appSettingsService.WebSitePurchaseUrl,
				WebSiteUrl = _appSettingsService.WebSiteUrl,
				WebSiteRequestLocationUrl = _appSettingsService.WebSiteRequestLocationUrl,
				RegisterUrl = _appSettingsService.RegisterUrl,
				ForgotPasswordUrl = _appSettingsService.ForgotPasswordUrl,
				AvatarGetUrl = _appSettingsService.AvatarGetUrl,
                StripeIsLiveMode = _appSettingsService.StripeIsLiveMode
			};

			return vm;
		}

	    public void SetUserMapZoom(User currentUser, int mapZoom)
	    {
            var user = _userRepository.Get(currentUser.Id);
            user.MapZoom = mapZoom;
            _userRepository.Modified(user);
            _userRepository.SaveChanges();
	    }
	}
}