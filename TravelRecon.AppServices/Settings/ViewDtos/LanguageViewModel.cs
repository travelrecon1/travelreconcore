﻿using System;
using System.Collections.Generic;
using TravelRecon.Domain.Localization;

namespace TravelRecon.AppServices.Settings.ViewDtos
{
    public class LanguageViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }

        public static List<LanguageViewModel> GetList()
        {
            return new List<LanguageViewModel>
            {
                ConvertFrom(LanguageType.English),
                ConvertFrom(LanguageType.Spanish),
                ConvertFrom(LanguageType.Portuguese),
                ConvertFrom(LanguageType.French)
            };
        }

        public static LanguageViewModel ConvertFrom(LanguageType languageType)
        {
            var title = "";

            switch (languageType)
            {
                case LanguageType.English:
                    title = "English";
                    break;
                case LanguageType.Spanish:
                    title = "Spanish";
                    break;
                case LanguageType.Portuguese:
                    title = "Portuguese";
                    break;
                case LanguageType.French:
                    title = "French";
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(languageType), languageType, null);
            }

            return new LanguageViewModel
            {
                Id = (int)languageType,
                Title = title
            };
        }
    }
}
