﻿using System.Collections.Generic;

namespace TravelRecon.AppServices.Settings.ViewDtos
{
    public class AlertsNotificationsSettingsViewModel
    {
        public string TurnOnOffNotificationsCaption { get; set; }
        public bool TurnOnOffNotificationsValue { get; set; }
        public string AlertPopupsShowIntervalMinutesCaption { get; set; }
        public string MaxAlertPopupsCaption { get; set; }
        public int MaxAlertPopupsSelectedValue { get; set; }
        public List<AlertsNotificationsFrequencyViewModel> AlertsNotificationsFrequencies { get; set; }
    }

    public class AlertsNotificationsFrequencyViewModel
    {
        public string Label { get; set; }
        public int Value { get; set; }
        public bool IsSelected { get; set; }
    }
}
