namespace TravelRecon.AppServices.Settings.ViewDtos
{
    public class DestinationSettingLabelsDto
    {
        public string CurrentLocation { get; set; }
        public string SelectDestination { get; set; }
        public string Destination { get; set; }
    }
}