﻿using System.Collections.Generic;
using TravelRecon.AppServices.SharedDtos;
using TravelRecon.Domain.Localization.QueryObjects;

namespace TravelRecon.AppServices.Settings.ViewDtos
{
	public class PurchasedDestinationsViewModel
	{
		public IList<MultiselectCountryViewModel> Countries { get; set; }
		public string NotificationsAndAlertsLabel { get; set; }
		public string ChooseDestinationsLabel { get; set; }
        public string ChooseAllDestinationLabel { get; set; }
	}
}
