﻿using System;
using System.Collections.Generic;
using TravelRecon.Domain.SubscriptionOffers;

namespace TravelRecon.AppServices.Settings.ViewDtos
{
    public class PurchasedDestinationVm
    {
        public int Id { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public DateTime? ExpirationDate { get; set; }
    }

    public class PurchasedDestinationsVm
    {
        public PurchasedDestinationsVm()
        {
            Destinations = new List<PurchasedDestinationVm>();
        }

		public IList<PurchasedDestinationVm> Destinations { get; set; }
        public SubscriptionTypes SubscriptionType { get; set; }
	}
}
