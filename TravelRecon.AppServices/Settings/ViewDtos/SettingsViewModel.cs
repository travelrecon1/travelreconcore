﻿using System.Collections.Generic;
using TravelRecon.Domain.Identity;
using TravelRecon.Domain.Localization.QueryObjects;

namespace TravelRecon.AppServices.Settings.ViewDtos
{
    public class SettingsViewModel
    {
        public SettingsViewModel()
        {
            LanguagesList = new List<LanguageViewModel>();
        }

        public LanguageViewModel UserLanguage { get; set; }
        public List<LanguageViewModel> LanguagesList { get; set; }
        public DestinationSettingsViewModel DestinationSettingsViewModel { get; set; }
        public PurchasedDestinationsViewModel PurchasedDestinationsViewModel { get; set; }
        public UserRoleTypeEnum? UpgradePackageId { get; set; }
        public List<LocationFrequencyViewModel> LocationUpdateFrequencyViewModel { get; set; }
        public ObfuscateLocationViewModel ObfuscateLocationViewModel { get; set; }
        public AlertsNotificationsSettingsViewModel AlertsNotificationsSettingsViewModel { get; set; }
        public EmergencyNotificationsSettingsViewModel EmergencyNotificationsSettingsViewModel { get; set; }
        public int MapZoom { get; set; }
        public bool CanUpgradeSubscription { get; set; }
    }
}