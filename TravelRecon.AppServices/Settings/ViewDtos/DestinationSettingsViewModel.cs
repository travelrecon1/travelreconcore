﻿using System.Collections.Generic;
using TravelRecon.AppServices.SharedDtos;

namespace TravelRecon.AppServices.Settings.ViewDtos
{
    public class DestinationSettingsViewModel
    {
        public IList<CountryViewModel> Countries { get; set; }
        public DestinationSettingLabelsDto Labels { get; set; }
    }
}