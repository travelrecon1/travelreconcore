﻿namespace TravelRecon.AppServices.Settings.ViewDtos
{
    public class LocationFrequencyViewModel
    {
        public int Value { get; set; }
        public string Label { get; set; }
        public bool IsSelected { get; set; }
    }
}
