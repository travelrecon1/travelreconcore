﻿namespace TravelRecon.AppServices.Settings.ViewDtos
{
    public class ObfuscateLocationViewModel
    {
        public bool IsAvailable { get; set; }
        public bool ObfuscateLocation { get; set; }
    }
}
