﻿namespace TravelRecon.AppServices.Settings.ViewDtos
{
    public class ClientSettingsViewModel
    {
        public string WebSiteHelpUrl { get; set; }
        public string WebSiteUrl { get; set; }
        public string WebSiteRequestLocationUrl { get; set; }
        public string WebSitePurchaseUrl { get; set; }
        public string WebSiteAppUrl { get; set; }
        public string ImagesRootUrl { get; set; }
        public string SupportEmailAddress { get; set; }
        public string RegisterUrl { get; set; }
        public string ForgotPasswordUrl { get; set; }
        public string AvatarGetUrl { get; set; }
        public bool StripeIsLiveMode { get; set; }
    }
}
