﻿namespace TravelRecon.AppServices.Settings.ViewDtos
{
    public class UserDestinationViewModel
    {
        public int UserDestinationId { get; set; }
        public bool IsSelected { get; set; }
    }
}
