﻿namespace TravelRecon.AppServices.Settings.ViewDtos
{
    public class EmergencyNotificationsSettingsViewModel
    {
        public string Email { get; set; }
        public string Phone { get; set; }
    }
}
