﻿using System.Collections.Generic;

namespace TravelRecon.AppServices.Alerts.ViewDtos
{
	public class AlertCategoryAndTypeViewModel
	{
		public List<AlertCategoryWithTypesDto> AlertCategories { get; set; }
	}
}
