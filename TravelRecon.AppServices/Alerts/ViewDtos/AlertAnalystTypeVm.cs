﻿namespace TravelRecon.AppServices.Alerts.ViewDtos
{
    public class AlertAnalystTypeVm
    {
        public int Id { get; set; }
        public int CategoryId { get; set; }
        public string Name { get; set; }
    }
}
