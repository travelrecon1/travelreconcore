﻿using System.Collections.Generic;
using TravelRecon.AppServices.SharedDtos;
using TravelRecon.Domain.Identity;
using TravelRecon.Domain.Localization.QueryObjects;

namespace TravelRecon.AppServices.Alerts.ViewDtos
{
    public class FilterAlertsViewModel
    {
        public FilterAlertsViewModel()
        {
            FilterDateTypes = new List<FilterDateTypeViewModel>();
        }

        public IEnumerable<LabelLocalizedDto> Labels { get; set; }

        public List<FilterDateTypeViewModel> FilterDateTypes { get; set; }

        public IList<CountryViewModel> Countries { get; set; }

        public bool AnalystAlertEnable { get; set; }
        public bool PushAlertEnable { get; set; }

        public int AlertTypeId { get; set; }

        public LabelLocalizedDto UpgradeButtonLabel { get; set; }
        public UserRoleTypeEnum UpgradePackageId { get; set; }
        public bool CanUpgradeSubscription { get; set; }
    }
}
