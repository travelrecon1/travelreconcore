﻿using System;
using System.ComponentModel.DataAnnotations;
using TravelRecon.AppServices.Alerts.Attributes;
using TravelRecon.Domain.Alerts;

namespace TravelRecon.AppServices.Alerts.ViewDtos
{
    public class CreateAlertVm
    {
        [Required(ErrorMessage = "Title required field.")]
        public string Title { get; set; }
        [RequiredOnSubmit("IsSubmit", "true", ErrorMessage = "Description required field.")]
        public string Description { get; set; }
        [RequiredOnSubmit("IsSubmit", "true",ErrorMessage = "Category required field.")]
        public int CategoryId { get; set; }
        [Required(ErrorMessage = "Type required field.")]
        public int TypeId { get; set; }
        public int AnalystCategoryId { get; set; }
        public int AnalystTypeId { get; set; }
        public AlertWorkflow? Workflow { get; set; }
        public string Address { get; set; }
        [RequiredOnSubmit("IsSubmit", "true",ErrorMessage = "Country required field.")]
        public string Country { get; set; }
        public string City { get; set; }
        [Range(-90, 90, ErrorMessage = "Latitude may be only floating point value. In range (-90, 90).")]
        [RequiredOnSubmit("IsSubmit", "true",ErrorMessage = "Latitude required field.")]
        public double Latitude { get; set; }
        [Range(-180, 180, ErrorMessage = "Longitude may be only floating point value. In range (-180, 180).")]
        [RequiredOnSubmit("IsSubmit", "true",ErrorMessage = "Longitude required field.")]
        public double Longitude { get; set; }
        [RequiredOnSubmit("IsSubmit", "true",ErrorMessage = "Expires On required field.")]
        public DateTime ExpiresOn { get; set; }
        public bool ShouldPushToUsers { get; set; }
        public bool ShouldShowOnMap { get; set; }
        [Required]
        public int SourceId { get; set; }
        [RequiredOnSubmit("IsSubmit", "true")]
        public int Accuracy { get; set; }

        public int? DestinationId { get; set; }

        [Range(0, 100, ErrorMessage = "ConfidenceRating may be only int value. In range (0, 100).")]
        public int? ConfidenceRating { get; set; }
        public int? AnalystAlertPriorityId { get; set; }
        public string Tags { get; set; }
        [Range(0.5, 5, ErrorMessage = "ImpactRating may be only floating point value. In range (0.5, 5).")]
        public double? ImpactRating { get; set; }

        public bool IsSubmit { get; set; }

        [Range(0, 100, ErrorMessage = "Initial Impact Percentage may be only int value. In range (0, 100).")]
        public int? InitialImpactPercentage { get; set; }
        public DateTime? PeakImpactTime { get; set; }
    } 
}
