﻿using System.Collections.Generic;
using TravelRecon.AppServices.SharedDtos;
using TravelRecon.Domain.Alerts.QueryObjects;
using TravelRecon.Domain.Localization.QueryObjects;
using TravelRecon.Domain.Tutorial;

namespace TravelRecon.AppServices.Alerts.ViewDtos
{
    public class ListAlertsViewModel: BasePageViewModel
    {
        public ListAlertsViewModel()
        {
            SortAlertTypes = new List<SortAlertTypeViewModel>();
        }

        public SortAlertTypes SortAlertType { get; set; }
        public string MapCenterAddress { get; set; }
        public IEnumerable<AlertDto> Alerts { get; set; }
        public IEnumerable<LabelLocalizedDto> Labels { get; set; }
        public List<SortAlertTypeViewModel> SortAlertTypes { get; set; }
        public override PageTypeEnum PageType => PageTypeEnum.Alerts;
    }
}
