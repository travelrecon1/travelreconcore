﻿using System.ComponentModel.DataAnnotations;

namespace TravelRecon.AppServices.Alerts.ViewDtos
{
    public class CreateManageAlertVm
    {
        [Required(ErrorMessage = "Title required field.")]
        public string Title { get; set; }
        public string Description { get; set; }
        [Required(ErrorMessage = "Category required field.")]
        public int CategoryId { get; set; }
        [Required(ErrorMessage = "Type required field.")]
        public int TypeId { get; set; }
        [Required(ErrorMessage = "Destination required field.")]
        public int DestinationId { get; set; }
        [Required(ErrorMessage = "Priority required field.")]
        public int PriorityId { get; set; }
    }
}
