﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using TravelRecon.AppServices.Users.ViewDtos;
using TravelRecon.Domain.Alerts;
using TravelRecon.Domain.Identity;

namespace TravelRecon.AppServices.Alerts.ViewDtos
{
    public class AlertVm
    {
        public int Id { get; set; }

        public string Description { get; set; }
        public int AlertTypeCategoryId { get; set; }
        public string AlertTypeCategoryName { get; set; }

        public int? AlertAnalystCategoryId { get; set; }
        public string AlertAnalystCategoryName { get; set; }

        public int AlertTypeId { get; set; }
        public string AlertTypeName { get; set; }

        public int? AlertAnalystTypeId { get; set; }
        public string AlertAnalystTypeName { get; set; }

        public DateTime? ReportedOn { get; set; }
        public DateTime? ExpiresOn { get; set; }

        public string AddressFormatted { get; set; }

        [Range(-90, 90)]
        public double Latitude { get; set; }
        [Range(-180, 180)]
        public double Longitude { get; set; }

        public bool ShouldShowOnMap { get; set; }
        public bool ShouldPushToUsers { get; set; }
        public AlertSourceType? Source { get; set; }
        public AlertWorkflow? Workflow { get; set; }

        public string Country { get; set; }
        public string City { get; set; }

        public bool IsEmergency { get; set; }

        public int ReportedBy { get; set; }
        public string Reporter { get; set; }
        public string ReporterFullName { get; set; }
        public string Title { get; set; }
        public int? EmergencyExpiration { get; set; }
        public int? SourceId { get; set; }
        public int? DestinationId { get; set; }
        public int? Accuracy { get; set; }
        public int? ConfidenceRating { get; set; }
        public int? AnalystAlertPriorityId { get; set; }
        public int AlertTypePriorityId { get; set; }
        public string Tags { get; set; }
        public double? ImpactRating { get; set; }

        public string Priority { get; set; }

        public string SourceName { get; set; }
        public double? Severity { get; set; }
        public double? ProximityStandoff { get; set; }

        public bool CanValidate { get; set; }
        public bool CanThank { get; set; }
        public int ThanksCount { get; set; }
        public int ValidationsCount { get; set; }
        public List<User> PeopleAffectedDetails { get; set; }

        public int PeopleAffected { get; set; }

        public int? InitialImpactPercentage { get; set; }
        public DateTime? PeakImpactTime { get; set; }
    }
}
