﻿namespace TravelRecon.AppServices.Alerts.ViewDtos
{
    public class AnalystAlertPriorityVm
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
