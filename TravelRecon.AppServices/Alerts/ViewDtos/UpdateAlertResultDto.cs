﻿using TravelRecon.AppServices.IntelReports.ViewDtos;

namespace TravelRecon.AppServices.Alerts.ViewDtos
{
    public class UpdateAlertResultDto
    {
        public int AlertId { get; set; }
        public DestinationUnlockResultDto UnlockDestinationReslut { get; set; }
    }
}
