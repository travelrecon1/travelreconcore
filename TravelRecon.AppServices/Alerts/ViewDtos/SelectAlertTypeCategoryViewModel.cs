﻿using System.Collections.Generic;
using TravelRecon.Domain.Localization.QueryObjects;

namespace TravelRecon.AppServices.Alerts.ViewDtos
{
    public class SelectAlertTypeCategoryViewModel
    {
        public int AlertId { get; set; }
        public int SelectedAlertTypeCategoryId { get; set; }
        public IEnumerable<AlertTypeCategoryDto> AlertTypeCategories { get; set; }
        public IEnumerable<LabelLocalizedDto> Labels { get; set; }
    }
}
