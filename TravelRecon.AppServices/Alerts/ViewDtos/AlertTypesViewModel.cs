﻿using System.Collections.Generic;

namespace TravelRecon.AppServices.Alerts.ViewDtos
{
    public class AlertTypesViewModel
    {
        public IEnumerable<AlertTypeDto> AlertTypes { get; set; }
    }
}
