﻿using System;

namespace TravelRecon.AppServices.Alerts.ViewDtos
{
    public class AnalystPushedGroupVm
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public DateTime CreatedDate { get; set; }
        public int ConfidenceRating { get; set; }
        public int Accurancy { get; set; }
        public int DestinationId { get; set; }
        public int CreatedById { get; set; }
        public string CreatedBy { get; set; }
        public string Country { get; set; }
        public string City { get; set; }

        public int GroupsCount { get; set; }
        public int UsersCount { get; set; }
        public int AlertsCount { get; set; }
    }
}
