﻿using TravelRecon.Domain.Localization;

namespace TravelRecon.AppServices.Alerts.ViewDtos
{
    public class AlertTypeCategoryTranslationVm
    {
        public int Id { get; set; }
        public LanguageType Language { get; set; }
        public string Name { get; set; }
    }
}
