﻿using System.Collections.Generic;

namespace TravelRecon.AppServices.Alerts.ViewDtos
{
	public class AlertCategoryWithTypesDto : AlertTypeCategoryDto
	{
		public List<AlertTypeDto> AlertTypes { get; set; }
	}
}
