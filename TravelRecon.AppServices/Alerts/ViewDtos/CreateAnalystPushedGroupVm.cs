﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TravelRecon.AppServices.Alerts.ViewDtos
{
    public class CreateAnalystPushedGroupVm
    {
        [Required]
        public string Title { get; set; }
        [Required]
        public int ConfidenceRating { get; set; }
        [Required]
        public int Accurancy { get; set; }
        [Required]
        public int DestinationId { get; set; }
        [Required]
        public bool ShouldPush { get; set; }
        [Required]
        public IList<int> Alerts { get; set; }
        public IList<int> Groups { get; set; }
        public IList<int> Users { get; set; }
    }
}
