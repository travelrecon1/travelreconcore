﻿using System.Collections.Generic;
using TravelRecon.Domain.Localization.QueryObjects;

namespace TravelRecon.AppServices.Alerts.ViewDtos
{
    public class SelectAlertTypeViewModel
    {
        public int AlertId { get; set; }
        public int SelectedAlertCategoryId { get; set; }
        public IEnumerable<AlertTypeDto> AlertTypes { get; set; }
        public IEnumerable<LabelLocalizedDto> Labels { get; set; }
    }
}
