﻿using System.Linq;
using TravelRecon.Domain.Localization.QueryObjects;

namespace TravelRecon.AppServices.Alerts.ViewDtos
{
    public class SortAlertTypeViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }

        public static SortAlertTypeViewModel ConvertFrom(LabelLocalizedDto labelLocalizedDto)
        {
            return new SortAlertTypeViewModel
            {
                Id = SortAlertTypeIdCollection.SortTypeId.Single(x => x.Key == labelLocalizedDto.LabelType).Value,
                Title = labelLocalizedDto.Content
            };
        }
    }
}
