﻿using System.Linq;
using TravelRecon.Domain.Localization.QueryObjects;

namespace TravelRecon.AppServices.Alerts.ViewDtos
{
    public class FilterDateTypeViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }

        public static FilterDateTypeViewModel ConvertFrom(LabelLocalizedDto labelLocalizedDto)
        {
            return new FilterDateTypeViewModel
            {
                Id = FilterDateTypeIdCollection.SortTypeId.Single(x => x.Key == labelLocalizedDto.LabelType).Value,
                Title = labelLocalizedDto.Content
            };
        }
    }
}
