﻿using System;
using System.ComponentModel.DataAnnotations;
using TravelRecon.Domain.Alerts;
using TravelRecon.Domain.Alerts.QueryObjects;
using TravelRecon.Domain.Localization;

namespace TravelRecon.AppServices.Alerts.ViewDtos
{
    public class AlertDto
    {
        public AlertDto() {
            MeansForLocationInput = MeansForLocationInputType.ViaApi;
            ShouldPushToUsers = false;
            ShouldShowOnMap = true;
        }

        public int Id { get; set; }

        public string Description { get; set; }

        public int AlertTypeCategoryId { get; set; }
        public string AlertTypeCategoryName { get; set; }

        [Range(1, int.MaxValue)]
        public int AlertTypeId { get; set; }
        public string AlertTypeName { get; set; }

        public DateTime? ReportedOn { get; set; }
        public DateTime? ExpiresOn { get; set; }

        public string ReportedOnFormatted { get; set; }

        public string AddressFormatted { get; set; }

        public double? Latitude { get; set; }
        public double? Longitude { get; set; }

        public bool ShouldShowOnMap { get; set; }
        public bool ShouldPushToUsers { get; set; }
        public AlertSourceType Source { get; set; }

        public MeansForLocationInputType MeansForLocationInput { get; set; }
        public string SelectedAddressFormatted { get; set; }
        public string TypedInAddress { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public double? SelectedLatitude { get; set; }
        public double? SelectedLongitude { get; set; }
        public bool IsEmergency { get; set; }

		public int Priority { get; set; }

        public bool CanValidate { get; set; }
        public bool CanThank { get; set; }
        public int ThanksCount { get; set; }
        public int ValidationsCount { get; set; }

        public static AlertDto ConvertToDto(Alert alert, LanguageType targetLanguage)
        {
            if (alert == null)
                return null;

            var dto = new AlertDto {
                Id = alert.Id,
                Description = alert.Description,
                AddressFormatted = alert.AddressFormatted,
                ReportedOnFormatted = alert.ReportedOn.ToString("o"),
                IsEmergency = alert.IsEmergency,
                ShouldPushToUsers = alert.ShouldPushToUsers,
				City = alert.City,
				Country = alert.Country
            };

            if (alert.Location != null) {
                dto.Latitude = alert.Location.Latitude.Value;
                dto.Longitude = alert.Location.Longitude.Value;
            }

            if (alert.AlertType != null) {
                dto.AlertTypeId = alert.AlertType.Id;
                dto.AlertTypeName = alert.AlertType.Translations
                    .FindAlertTypeLocalized(targetLanguage).Name;

	            dto.Priority = alert.AlertType.AlertPriorityFk;

                if (alert.AlertType.Category != null) {
                    dto.AlertTypeCategoryId = alert.AlertType.Category.Id;
                    dto.AlertTypeCategoryName = alert.AlertType.Category.Translations
                        .FindAlertTypeCategoryLocalized(targetLanguage).Name;
                }
            }

            return dto;
        }
    }
}
