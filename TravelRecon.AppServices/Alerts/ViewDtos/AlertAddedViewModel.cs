﻿using TravelRecon.AppServices.IntelReports.ViewDtos;

namespace TravelRecon.AppServices.Alerts.ViewDtos
{
    public class AlertAddedViewModel
    {
        public int AlertId { get; set; }
        public short Points { get; set; }
        public DestinationUnlockResultDto UnlockDestinationResult { get; set; }
    }
}
