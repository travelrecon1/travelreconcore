﻿using System.Collections.Generic;
using System.Linq;
using TravelRecon.Domain.Localization;

namespace TravelRecon.AppServices.Alerts.ViewDtos
{
    public class AlertTypeCategoryVm
    {
        public LanguageType? Language { get; set; }

        public AlertTypeCategoryVm()
        {
            Translations = new List<AlertTypeCategoryTranslationVm>();
        }

        public int Id { get; set; }

        public string Name
        {
            get
            {
                var trnaslation = Translations.FirstOrDefault(x => x.Language == (Language ?? LanguageType.English)) ??
                                  Translations.FirstOrDefault(x => !string.IsNullOrWhiteSpace(x.Name));

                return trnaslation?.Name;
            }
        }

        public List<AlertTypeCategoryTranslationVm> Translations { get; set; }
    }
}
