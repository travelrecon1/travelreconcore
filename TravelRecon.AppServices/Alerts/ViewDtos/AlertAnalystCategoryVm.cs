﻿namespace TravelRecon.AppServices.Alerts.ViewDtos
{
    public class AlertAnalystCategoryVm
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
