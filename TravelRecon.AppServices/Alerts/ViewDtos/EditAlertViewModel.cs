﻿using System.Collections.Generic;
using TravelRecon.Domain.Localization.QueryObjects;

namespace TravelRecon.AppServices.Alerts.ViewDtos
{
    public class EditAlertViewModel
    {
        public AlertDto Alert { get; set; }
        public IEnumerable<LabelLocalizedDto> Labels { get; set; }

        public IEnumerable<AlertTypeCategoryDto> AlertTypeCategories { get; set; }
        public IEnumerable<AlertTypeDto> AlertTypes { get; set; }
    }
}
