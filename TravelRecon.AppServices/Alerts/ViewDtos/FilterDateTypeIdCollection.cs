using System.Collections.Generic;
using TravelRecon.Domain.Localization;

namespace TravelRecon.AppServices.Alerts.ViewDtos
{
    public struct FilterDateTypeIdCollection
    {
        public static readonly Dictionary<LabelType, int> SortTypeId = new Dictionary<LabelType, int>
        {
            {LabelType.SevenDays, 1},
            {LabelType.ThirtyDays, 2},
            {LabelType.SixMonths, 3}
        };
    }
}