﻿using System.Collections.Generic;
using TravelRecon.Domain.Localization.QueryObjects;

namespace TravelRecon.AppServices.Alerts.ViewDtos
{
    public class ViewAlertViewModel
    {
        public AlertDto Alert { get; set; }
        public IEnumerable<LabelLocalizedDto> Labels { get; set; }
        public bool CanReject { get; set; }
    }
}
