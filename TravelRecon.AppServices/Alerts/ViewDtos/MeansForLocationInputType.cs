﻿namespace TravelRecon.AppServices.Alerts.ViewDtos
{
    public enum MeansForLocationInputType
    {
        ViaApi,
        UseCurrentLocation,
        ClickLocationOnMap,
        TypeInAnAddress
    }
}
