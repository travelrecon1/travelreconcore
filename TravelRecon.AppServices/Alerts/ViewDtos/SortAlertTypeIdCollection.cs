using System.Collections.Generic;
using TravelRecon.Domain.Localization;

namespace TravelRecon.AppServices.Alerts.ViewDtos
{
    public struct SortAlertTypeIdCollection
    {
        public static readonly Dictionary<LabelType, int> SortTypeId = new Dictionary<LabelType, int>
        {
            {LabelType.ByMostRecentFirst, 1},
            {LabelType.ByProximity, 2}
        };
    }
}