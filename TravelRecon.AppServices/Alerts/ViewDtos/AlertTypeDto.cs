﻿using TravelRecon.Domain.Alerts;

namespace TravelRecon.AppServices.Alerts.ViewDtos
{
    public class AlertTypeDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int CategoryId { get; set; }

        public static AlertTypeDto ConvertToDto(int alertTypeId, int alertTypeCategoryId, AlertTypeTranslation translation) {
            var dto = new AlertTypeDto {
                Id = alertTypeId,
                Name = (translation ?? new AlertTypeTranslation {Name = "Empty translation"}).Name,
                CategoryId = alertTypeCategoryId
            };

            return dto;
        }
    }
}
