﻿using System.Collections.Generic;

namespace TravelRecon.AppServices.Alerts.ViewDtos
{
    public class AlertTypeCategoriesViewModel
    {
        public IEnumerable<AlertTypeCategoryDto> AlertTypeCategories { get; set; }
    }
}
