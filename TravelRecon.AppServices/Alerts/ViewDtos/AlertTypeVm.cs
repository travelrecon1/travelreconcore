﻿using System.Collections.Generic;
using System.Linq;
using TravelRecon.Domain.Localization;

namespace TravelRecon.AppServices.Alerts.ViewDtos
{
    public class AlertTypeVm
    {
        public LanguageType? Language { get; set; }

        public AlertTypeVm()
        {
            Translations = new List<AlertTypeTranslationVm>();
        }

        public int Id { get; set; }

        public string Name
        {
            get
            {
                var trnaslation = Translations.FirstOrDefault(x => x.Language == (Language ?? LanguageType.English)) ??
                                  Translations.FirstOrDefault(x => !string.IsNullOrWhiteSpace(x.Name));

                return trnaslation?.Name;
            }
        }

        public int? ExpirationTime { get; set; }

        public List<AlertTypeTranslationVm> Translations { get; set; }
    }
}
