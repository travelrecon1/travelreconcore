﻿using TravelRecon.Domain.Alerts;
using TravelRecon.Domain.Alerts.QueryObjects;
using TravelRecon.Domain.Localization;

namespace TravelRecon.AppServices.Alerts.ViewDtos
{
    public class AlertTypeCategoryDto
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public static AlertTypeCategoryDto ConvertToDto(AlertTypeCategory alertTypeCategory, LanguageType targetLanguage) {
            if (alertTypeCategory == null)
                return null;

            var dto = new AlertTypeCategoryDto() {
                Id = alertTypeCategory.Id,
                Name = alertTypeCategory.Translations
                    .FindAlertTypeCategoryLocalized(targetLanguage).Name
            };

            return dto;
        }
    }
}
