﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Threading.Tasks;
using TravelRecon.AppServices.Alerts.Queries;
using TravelRecon.AppServices.Alerts.ViewDtos;
using TravelRecon.AppServices.IntelReports;
using TravelRecon.AppServices.IntelReports.ViewDtos;
using TravelRecon.AppServices.Interfaces;
using TravelRecon.AppServices.Points;
using TravelRecon.AppServices.SharedDtos;
using TravelRecon.AppServices.SubscriptionOffers;
using TravelRecon.Common.DynamicLinq;
using TravelRecon.Common.Exceptions;
using TravelRecon.Common.Extensions;
using TravelRecon.Common.Models;
using TravelRecon.Domain.Alerts;
using TravelRecon.Domain.Alerts.QueryObjects;
using TravelRecon.Domain.Factories;
using TravelRecon.Domain.Identity;
using TravelRecon.Domain.IntelReports;
using TravelRecon.Domain.License;
using TravelRecon.Domain.Localization;
using TravelRecon.Domain.Localization.QueryObjects;
using TravelRecon.Domain.Map;
using TravelRecon.Domain.Points;
using TravelRecon.Domain.RepositoryInterfaces;

namespace TravelRecon.AppServices.Alerts
{
    public class ManageAlertsService : IAppService
    {
        private readonly PointService _pointService;
        private readonly IRepository<Country> _countryRepository;
        private readonly IRepository<AlertSource> _alertSourceRepository;
        private readonly IRepository<AnalystAlertPriority> _analystAlertPriorityRepository;
        private readonly IRepository<AlertsThank> _alertThanksRepository;
        private readonly IRepository<AlertsValidation> _alertValidationsRepository;
        private readonly IRepository<AnalystPushedGroup> _analystPushedGroupRepository;
        private readonly IRepository<AnalystPushedAlert> _analystPushedAlertRepository;
        private readonly IRepository<User> _usersRepository;
        private readonly IRepository<UserLicense> _userLicenseRepository;
        private readonly IRepository<PushedAlert> _pushedAlertRepository;
        private readonly IRepository<Alert> _alertRepository;
        private readonly IRepository<Label> _labelRepository;
        private readonly IRepository<AlertTypeCategory> _alertTypeCategoryRepository;
        private readonly IRepository<AlertType> _alertTypeRepository;
        private readonly IRepository<AnalystAlertCategory> _alertAnalystCategoryRepository;
        private readonly IRepository<AnalystAlertType> _alertAnalystTypeRepository;
        private readonly IRepository<UserDestinations> _userDestinationRepository;
        private readonly ILanguageTranslator _languageTranslator;
        private readonly DestinationAppService _destinationAppService;
        private readonly SubscriptionOffersService _subscriptionOffersService;

        /// <summary>
        /// 10 mile radius
        /// </summary>
        public static double RadiusFromCurrentLocationForPullingAlertUpdates = 16093.44;

        public ManageAlertsService(
            IRepository<Alert> alertRepository,
            IRepository<Label> labelRepository,
            IRepository<AlertTypeCategory> alertTypeCategoryRepository,
            IRepository<AlertType> alertTypeRepository,
            IRepository<AnalystAlertCategory> alertAnalystCategoryRepository,
            IRepository<AnalystAlertType> alertAnalystTypeRepository,
            ILanguageTranslator languageTranslator,
            IRepository<PushedAlert> pushedAlertRepository,
            DestinationAppService destinationAppService,
            SubscriptionOffersService subscriptionOffersService,
            IRepository<UserDestinations> userDestinationsRepository,
            PointService pointService,
            IRepository<Country> countryRepository,
            IRepository<AlertSource> alertSourceRepository,
            IRepository<AnalystAlertPriority> analystAlertPriorityRepository,
            IRepository<AlertsThank> alertThanksRepository,
            IRepository<AlertsValidation> alertValidationsRepository, 
            IRepository<AnalystPushedGroup> analystPushedGroupRepository,
            IRepository<AnalystPushedAlert> analystPushedAlertRepository,
            IRepository<User> usersRepository,
            IRepository<UserLicense> userLicenseRepository)
        {

            _pushedAlertRepository = pushedAlertRepository;
            _destinationAppService = destinationAppService;
            _subscriptionOffersService = subscriptionOffersService;
            _languageTranslator = languageTranslator;
            _alertTypeRepository = alertTypeRepository;
            _alertAnalystCategoryRepository = alertAnalystCategoryRepository;
            _alertAnalystTypeRepository = alertAnalystTypeRepository;
            _alertTypeCategoryRepository = alertTypeCategoryRepository;
            _alertRepository = alertRepository;
            _labelRepository = labelRepository;
            _pointService = pointService;
            _countryRepository = countryRepository;
            _alertSourceRepository = alertSourceRepository;
            _analystAlertPriorityRepository = analystAlertPriorityRepository;
            _alertThanksRepository = alertThanksRepository;
            _alertValidationsRepository = alertValidationsRepository;
            _analystPushedGroupRepository = analystPushedGroupRepository;
            _analystPushedAlertRepository = analystPushedAlertRepository;
            _usersRepository = usersRepository;
            _userLicenseRepository = userLicenseRepository;
            _userDestinationRepository = userDestinationsRepository;
        }

        public EditAlertViewModel GetEditAlertViewModel(int alertId, User currentUser)
        {
            var targetLanguage = currentUser.GetLanguageSetting();
            var alertToEdit = GetAlertToEdit(alertId, currentUser);

            var viewModel = new EditAlertViewModel();
            viewModel.Alert = AlertDto.ConvertToDto(alertToEdit, targetLanguage);

            var labelsToRetrieve = new[] {
                LabelType.Alert, LabelType.Description, LabelType.AlertType,
                LabelType.Location, LabelType.Save, LabelType.DescriptionIsRequired,
                LabelType.Date, LabelType.Time,
                LabelType.UseCurrentLocation, LabelType.ClickLocationOnMap, LabelType.TypeInAnAddress,
                LabelType.ClickTheMapToSelectALocation, LabelType.ValidAddressRequired,
                LabelType.YouEarned, LabelType.Points
            };
            viewModel.Labels = _labelRepository.GetAll().FindLabelsLocalized(labelsToRetrieve, targetLanguage);

            var alertTypeCategories = _alertTypeCategoryRepository.GetAll()
                .Include(at => at.Translations)
                .ToList();

            viewModel.AlertTypeCategories = alertTypeCategories
                .Select(atc => AlertTypeCategoryDto.ConvertToDto(atc, targetLanguage))
                .OrderBy(atc => atc.Name)
                .ToList();

            var alertTypes = _alertTypeRepository.GetAll()
                .Include(at => at.Translations)
                .Select(x => new
                {
                    AlertTypeId = x.Id,
                    AlertTypeCategoryId = x.AlertTypeCategoryFk,
                    Translation = x.Translations.FirstOrDefault(t => t.Language == targetLanguage)
                })
                .ToList();

            viewModel.AlertTypes = alertTypes
                .Select(at => AlertTypeDto.ConvertToDto(at.AlertTypeId, at.AlertTypeCategoryId, at.Translation))
                .OrderBy(at => at.Name)
                .ToList();

            return viewModel;
        }

        public EditAlertViewModel GetAddEmergencyViewModel(User currentUser)
        {
            var targetLanguage = currentUser.GetLanguageSetting();
            var alertToEdit = new Alert { AddressFormatted = currentUser.CurrentAddressFormatted, IsEmergency = true, ShouldPushToUsers = true };

            var viewModel = new EditAlertViewModel();
            viewModel.Alert = AlertDto.ConvertToDto(alertToEdit, targetLanguage);

            var labelsToRetrieve = new[] {
                LabelType.Emergency, LabelType.Description, LabelType.AlertType, LabelType.AlertCategory,
                LabelType.Location, LabelType.Submit, LabelType.Confirm, LabelType.Yes, LabelType.No, LabelType.Cancel, LabelType.DescriptionIsRequired,
                LabelType.Date, LabelType.Time,
                LabelType.UseCurrentLocation, LabelType.ClickLocationOnMap, LabelType.TypeInAnAddress,
                LabelType.ClickTheMapToSelectALocation, LabelType.ValidAddressRequired,
                LabelType.PressBelowToConfirmYouWantToSendAnEmergencyNotification, LabelType.DoYouWishToCancelYourEmergency
            };
            viewModel.Labels = _labelRepository.GetAll().FindLabelsLocalized(labelsToRetrieve, targetLanguage);

            var alertTypeCategories = _alertTypeCategoryRepository.GetAll()
                .Include(at => at.Translations)
                .ToList();

            viewModel.AlertTypeCategories = alertTypeCategories
                .Select(atc => AlertTypeCategoryDto.ConvertToDto(atc, targetLanguage))
                .OrderBy(atc => atc.Name)
                .ToList();

            var alertTypes = _alertTypeRepository.GetAll()
                .Include(at => at.Translations)
                .Select(x => new
                {
                    AlertTypeId = x.Id,
                    AlertTypeCategoryId = x.AlertTypeCategoryFk,
                    Translation = x.Translations.FirstOrDefault(t => t.Language == targetLanguage)
                })
                .ToList();

            viewModel.AlertTypes = alertTypes
                .Select(at => AlertTypeDto.ConvertToDto(at.AlertTypeId, at.AlertTypeCategoryId, at.Translation))
                .OrderBy(at => at.Name)
                .ToList();

            return viewModel;
        }

        private Alert GetAlertToEdit(int alertId, User currentUser)
        {
            Alert alertToEdit;

            if (alertId > 0)
            {
                alertToEdit = _alertRepository.Get(alertId);
            }
            else
            {
                alertToEdit = new Alert();
                alertToEdit.AddressFormatted = currentUser.CurrentAddressFormatted;
            }

            return alertToEdit;
        }

        public AlertTypeCategoriesViewModel GetAlertTypeCategoriesViewModel(LanguageType languageType)
        {

            var alertTypeCategories = _alertTypeCategoryRepository.GetAll()
                .Include(at => at.Translations)
                .ToList();

            var alertTypeCategoryDtos = alertTypeCategories
                .Select(atc => AlertTypeCategoryDto.ConvertToDto(atc, languageType))
                .OrderBy(atc => atc.Name)
                .ToList();

            var viewModel = new AlertTypeCategoriesViewModel();
            viewModel.AlertTypeCategories = alertTypeCategoryDtos;

            return viewModel;
        }

        public AlertCategoryAndTypeViewModel GetAlertCategoriesWithTypes(LanguageType languageType)
        {
            var alertTypeCategories = _alertTypeCategoryRepository.GetAll()
                .Include(at => at.Translations)
                .ToList();

            var alertTypeCategoryDtos = alertTypeCategories
                .Select(atc => AlertTypeCategoryDto.ConvertToDto(atc, languageType))
                .OrderBy(atc => atc.Name)
                .ToList();

            var alertTypes = _alertTypeRepository.GetAll()
                .Include(at => at.Translations)
                .Select(x => new
                {
                    AlertTypeId = x.Id,
                    AlertTypeCategoryId = x.AlertTypeCategoryFk,
                    Translation = x.Translations.FirstOrDefault(t => t.Language == languageType)
                })
                .ToList();

            var viewModel = new AlertCategoryAndTypeViewModel();
            viewModel.AlertCategories = new List<AlertCategoryWithTypesDto>();

            foreach (var category in alertTypeCategoryDtos)
            {
                viewModel.AlertCategories.Add(new AlertCategoryWithTypesDto
                {
                    Id = category.Id,
                    Name = category.Name,
                    AlertTypes = alertTypes.Where(x => x.AlertTypeCategoryId == category.Id)
                        .Select(at => AlertTypeDto.ConvertToDto(at.AlertTypeId, at.AlertTypeCategoryId, at.Translation))
                        .ToList()
                });
            }


            return viewModel;
        }

        public SelectAlertTypeCategoryViewModel GetSelectAlertTypeCategoryViewModel(int alertId, User currentUser)
        {
            var targetLanguage = currentUser.GetLanguageSetting();
            var alertTypeCategories = GetAlertTypeCategoriesViewModel(targetLanguage);

            var viewModel = new SelectAlertTypeCategoryViewModel();
            viewModel.AlertTypeCategories = alertTypeCategories.AlertTypeCategories;

            var labelsToRetrieve = new[] { LabelType.SelectAlertTypeCategory };
            viewModel.Labels = _labelRepository.GetAll().FindLabelsLocalized(labelsToRetrieve, targetLanguage);

            return viewModel;
        }

        public AlertTypesViewModel GetAlertTypesViewModel(int? alertTypeCategoryId, LanguageType languageType)
        {
            if (!alertTypeCategoryId.HasValue)
                return new AlertTypesViewModel
                {
                    AlertTypes = new List<AlertTypeDto>()
                };

            var alertTypesWithLabels = _alertTypeRepository.GetAll()
                .Include(at => at.Translations)
                .Where(at => at.AlertTypeCategoryFk == alertTypeCategoryId)
                .Select(x => new
                {
                    AlertTypeId = x.Id,
                    AlertTypeCategoryId = x.AlertTypeCategoryFk,
                    Translation = x.Translations.FirstOrDefault(t => t.Language == languageType)
                })
                .ToList();

            var alertTypeDtos = alertTypesWithLabels
                .Select(at => AlertTypeDto.ConvertToDto(at.AlertTypeId, at.AlertTypeCategoryId, at.Translation))
                .OrderBy(at => at.Name)
                .ToList();

            var viewModel = new AlertTypesViewModel();
            viewModel.AlertTypes = alertTypeDtos;

            return viewModel;
        }

        public SelectAlertTypeViewModel GetSelectAlertTypeViewModel(int alertId, int selectedAlertTypeCategoryId, User currentUser)
        {
            var targetLanguage = currentUser.GetLanguageSetting();
            var alertTypes = GetAlertTypesViewModel(selectedAlertTypeCategoryId, targetLanguage);

            var viewModel = new SelectAlertTypeViewModel();
            viewModel.AlertTypes = alertTypes.AlertTypes;

            var labelsToRetrieve = new[] { LabelType.SelectAlertType };
            viewModel.Labels = _labelRepository.GetAll().FindLabelsLocalized(labelsToRetrieve, targetLanguage);

            return viewModel;
        }

        public AlertAddedViewModel AddAlert(AlertDto alertForm, User currentUser)
        {
            var result = CreateOrUpdate(alertForm, currentUser);

            var model = new AlertAddedViewModel
            {
                AlertId = result.AlertId,
                UnlockDestinationResult = result.UnlockDestinationReslut
            };

            if (alertForm.Id == 0 && !alertForm.IsEmergency)
            {
                //remove hardcode
                var pointType = alertForm.AlertTypeCategoryId == 6 || alertForm.AlertTypeCategoryId == 1
                    ? PointType.CriminalScoutReport
                    : PointType.InfoScoutReport;

                model.Points = _pointService.Create(pointType, currentUser);
            }

            return model;
        }

        private UpdateAlertResultDto CreateOrUpdate(AlertDto alertForm, User currentUser)
        {
            var result = new UpdateAlertResultDto();

            var alert = GetAlertToEdit(alertForm.Id, currentUser);

            alert.Description = (alertForm.Description ?? "").Trim();
            alert.AlertTypeFk = alertForm.AlertTypeId;
            alert.ReportedByFk = currentUser.Id;
            alert.ReportedOn = DateTime.UtcNow;
            alert.ShouldShowOnMap = alertForm.ShouldShowOnMap;
            alert.ShouldPushToUsers = alertForm.ShouldPushToUsers;
            alert.IsEmergency = alertForm.IsEmergency;
            alert.Source = alertForm.Source == AlertSourceType.None ? AlertSourceType.Scout : alertForm.Source;
            alert.Country = alertForm.Country;
            alert.City = alertForm.City;

            var alertType = _alertTypeRepository.GetAll().FirstOrDefault(type => type.Id == alert.AlertTypeFk);

            // there's a magic number for now - 24 hours as default alert expiration time
            alert.ExpiresOn = alert.ReportedOn.AddHours(alertType?.ExpirationTime ?? 24);

            if (alertForm.MeansForLocationInput == MeansForLocationInputType.ViaApi 
                || (alertForm.MeansForLocationInput == MeansForLocationInputType.UseCurrentLocation 
                        && alertForm.Latitude.HasValue && alertForm.Longitude.HasValue))
            {
                alert.Location = DbGeographyFactory.GenerateDbGeographyValue(alertForm.Latitude.Value, alertForm.Longitude.Value);
                alert.AddressFormatted = alertForm.AddressFormatted;
            }
            else if (alertForm.MeansForLocationInput == MeansForLocationInputType.UseCurrentLocation)
            {
                alert.Location = currentUser.CurrentLocation;
                alert.AddressFormatted = currentUser.CurrentAddressFormatted;
            }
            else if (alertForm.MeansForLocationInput == MeansForLocationInputType.TypeInAnAddress)
            {
                alert.Location = DbGeographyFactory.GenerateDbGeographyValue(alertForm.SelectedLatitude.Value, alertForm.SelectedLongitude.Value);
                alert.AddressFormatted = alertForm.TypedInAddress;
            }
            else if (alertForm.MeansForLocationInput == MeansForLocationInputType.ClickLocationOnMap)
            {
                alert.Location = DbGeographyFactory.GenerateDbGeographyValue(alertForm.SelectedLatitude.Value, alertForm.SelectedLongitude.Value);
                alert.AddressFormatted = alertForm.SelectedAddressFormatted;
            }

            //alert.DestinationId = _destinationAppService.GetDestinationFor(alert.Location, currentUser.GetLanguageSetting()).Id;

            var destination = _destinationAppService.GetDestination(alert.Location);
            alert.DestinationId = destination?.Id;

            if (string.IsNullOrWhiteSpace(alert.Country) || string.IsNullOrWhiteSpace(alert.City))
            {
                alert.Country = destination?.Country;
                alert.City = destination?.City;
            }

            if (alert.Id == 0)
                _alertRepository.Add(alert);

            _alertRepository.SaveChanges();

            //try to unlock destination
            var unlockResult = _destinationAppService.TryToUnlockDestination(destination);

            if (currentUser.HasMinReconRole(UserRoleTypeEnum.TravelRecon))
                unlockResult.NeedForUnlock = 0;

            result.UnlockDestinationReslut = unlockResult;
            result.AlertId = alert.Id;

            return result;
        }

        public ViewAlertViewModel GetViewAlertViewModel(int alertId, User currentUser)
        {
            var targetLanguage = currentUser.GetLanguageSetting();
            var alert = _alertRepository.Get(alertId);
            var alertDto = AlertDto.ConvertToDto(alert, targetLanguage);

            if (!string.IsNullOrWhiteSpace(alertDto.Description))
                alertDto.Description = _languageTranslator.Translate(alertDto.Description, targetLanguage);

            var viewModel = new ViewAlertViewModel();
            viewModel.Alert = alertDto;

            var labelsToRetrieve = new[] {
                LabelType.Location, LabelType.Description, LabelType.Alert, LabelType.Date, LabelType.Time, LabelType.AlertType, LabelType.ShowAlertOnMap, LabelType.Reject, LabelType.Emergency, LabelType.ShowEmergencyOnMap, LabelType.AlertCategory
            };
            viewModel.Labels = _labelRepository.GetAll().FindLabelsLocalized(labelsToRetrieve, targetLanguage);
            viewModel.CanReject = currentUser.HasRole(UserRoleTypeEnum.Administrator);

            viewModel.Alert.CanThank = CanThankAlert(currentUser, alertId);
            viewModel.Alert.CanValidate = CanValidateAlert(currentUser, alertId);
            viewModel.Alert.ThanksCount = GetAlertThanks(alertId);
            viewModel.Alert.ValidationsCount = GetAlertValidation(alertId);

            return viewModel;
        }

        public ListAlertsViewModel GetPushedUnreadAlerts(User currentUser)
        {
            var language = currentUser.GetLanguageSetting();
            var pushedUnreadAlerts = _pushedAlertRepository.GetAll()
                .Where(x => x.UserFk == currentUser.Id && !x.IsRead)
                .Select(x => x.Alert)
                .ToList();

            return new ListAlertsViewModel
            {
                Alerts = pushedUnreadAlerts.Select(x => AlertDto.ConvertToDto(x, language))
            };
        }

        public ListAlertsViewModel GetPushedAlertsFeed(User currentUser)
        {
            var language = currentUser.GetLanguageSetting();

            var now = DateTime.UtcNow;

            var pushedUnreadAlerts = _pushedAlertRepository.GetAll()
                .Where(x => x.UserFk == currentUser.Id &&
                    ((x.Alert.ExpiresOn.HasValue && x.Alert.ExpiresOn > now) ||
                    (!x.Alert.AlertType.ExpirationTime.HasValue || DbFunctions.AddHours(x.Alert.ReportedOn, x.Alert.AlertType.ExpirationTime) > now)))
                .Distinct()
                .Select(x => x.Alert)
                .ToList();

            return new ListAlertsViewModel
            {
                Alerts = pushedUnreadAlerts.Select(x => AlertDto.ConvertToDto(x, language))
            };
        }

        public bool HasPushedUnreadAlerts(User currentUser)
        {
            return _pushedAlertRepository.GetAll().Any(x => x.UserFk == currentUser.Id && !x.IsRead);
        }

        public void SetPushedReadAlerts(User currentUser)
        {
            var pushedUnreadAlerts = _pushedAlertRepository.GetAll()
                .Where(x => x.UserFk == currentUser.Id && !x.IsRead);

            foreach (var alert in pushedUnreadAlerts)
            {
                alert.IsRead = true;
                _pushedAlertRepository.Modified(alert);
            }

            _pushedAlertRepository.SaveChanges();
        }

        public Task<List<AlertVm>> GetListAlertsViewModelAsync(
            int? alertTypeId,
            bool scoutAlert,
            bool analystAlert,
            bool pushAlert,
            FilterDateTypes? filterDateType,
            AnalystAlertPriorityType? analystAlertPriorityType,
            SortAlertTypes sortAlertType,
            Location destination,
            User currentUser,
            Location currentMapLocation,
            double currentMapRadiusInMeters
            )
        {
            if (currentMapLocation == null)
            {
                currentMapLocation = new Location
                {
                    Longitude = currentUser.Location.Longitude.Value,
                    Latitude = currentUser.Location.Latitude.Value,
                    AddressFormatted = currentUser.CurrentAddressFormatted
                };
            }

            if (currentMapRadiusInMeters < 100)
                currentMapRadiusInMeters = currentUser.GetRadiusInMetersSetting();

            var location = DbGeographyFactory.GenerateDbGeographyValue(currentMapLocation.Latitude,
                currentMapLocation.Longitude);

            var purchasedDestinations = _destinationAppService.GetDestinationsFor(currentUser).Select(x => (int?)x.Id).ToArray();

            var alertsNearby = _alertRepository.GetAll()
                .FindActiveAlertsNearby(location, currentMapRadiusInMeters, currentUser)
                .Include(sr => sr.AlertType)
                .Include(sr => sr.AlertType.Translations)
                .Include(sr => sr.AlertType.Category)
                .Include(sr => sr.AlertType.Category.Translations)
                .Include(sr => sr.AlertType.Priority)
                .Filter(alertTypeId, scoutAlert, analystAlert, pushAlert, filterDateType, analystAlertPriorityType, currentUser.UserRoles, purchasedDestinations)
                .Sort(location, sortAlertType)
                .SelectVm(currentUser.Id, currentUser.GetLanguageSetting())
                .ToListAsync();

            return alertsNearby;
        }

        public void RejectAlert(int alertId, User currentUser)
        {
            var alertToReject = _alertRepository.Get(alertId);

            alertToReject.IsRejected = true;
            alertToReject.RejectedOn = DateTime.UtcNow;
            alertToReject.RejectedBy = currentUser;

            _alertRepository.SaveChanges();
        }

        public AlertTypeCategoriesViewModel GetAlertSourcesViewModel(LanguageType languageType)
        {
            var alertTypeCategories = _alertTypeCategoryRepository.GetAll().Include(at => at.Translations).ToList();

            var alertTypeCategoryDtos = alertTypeCategories.Select(atc => AlertTypeCategoryDto.ConvertToDto(atc, languageType)).OrderBy(atc => atc.Name).ToList();

            var viewModel = new AlertTypeCategoriesViewModel();
            viewModel.AlertTypeCategories = alertTypeCategoryDtos;

            return viewModel;
        }

        public FilterAlertsViewModel GetFilterAlertsViewModel(User currentUser)
        {
            var targetLanguage = currentUser.GetLanguageSetting();

            var viewModel = new FilterAlertsViewModel();

            var filterDateTypeLabels = GetFilterDateTypeLabels();
            var filterDateTypeLocalizedLabels = _labelRepository.GetAll().FindLabelsLocalized(filterDateTypeLabels, targetLanguage).ToList();

            foreach (var localizedLabel in filterDateTypeLocalizedLabels)
            {
                var filterDateType = FilterDateTypeViewModel.ConvertFrom(localizedLabel);
                viewModel.FilterDateTypes.Add(filterDateType);
            }

            var destinations = _destinationAppService.GetDestinationsFor(currentUser);
            viewModel.Countries =
                destinations.ConvertAll(DestinationDto.ConvertFrom)
                    .GroupBy(x => x.Country)
                    .Select(x =>
                      {
                          var firstOrDefault = x.FirstOrDefault();
                          if (firstOrDefault != null)
                              return new CountryViewModel
                              {
                                  Name = x.Key,
                                  RiskFactor = _countryRepository.Get(firstOrDefault.CountryId).RiskFactor,
                                  DestinationsList = x.OrderBy(c => c.City).ToList()
                              };
                          return new CountryViewModel();
                      })
                    .OrderBy(x => x.Name)
                    .ToList();

            viewModel.AnalystAlertEnable = currentUser.HasRole(UserRoleTypeEnum.TravelRecon);
            viewModel.PushAlertEnable = currentUser.HasRole(UserRoleTypeEnum.TravelRecon);
            viewModel.UpgradePackageId = UserRoleTypeEnum.TravelReconPro;
            viewModel.CanUpgradeSubscription = _subscriptionOffersService.CanUpgradeSubscription(currentUser.Id);

            var labelsToRetrieve = new[]
            {
                LabelType.AllFilterItem, LabelType.FilterAlerts, LabelType.AlertCategory, LabelType.AlertType, LabelType.AlertSource, LabelType.ScoutAlert,
                LabelType.AnalystAlert, LabelType.PushAlert, LabelType.Date, LabelType.ApplyFilters, LabelType.FilterAlerts, LabelType.AllDates, LabelType.Destination,
                LabelType.DestinationSettingCurrentLocation, LabelType.DestinationSettingSelectDestination, LabelType.TravelRecon, LabelType.Upgrade, LabelType.TravelReconPro,
                LabelType.TravelReconOrTravelReconPro, LabelType.ClickHereToPurchaseTravelRecon, LabelType.Apply, LabelType.Reset, LabelType.PurchaseMoreDestinations,
                LabelType.SelectAlertCategoriesAndTypes
            };

            viewModel.Labels = _labelRepository.GetAll().FindLabelsLocalized(labelsToRetrieve, targetLanguage);

            viewModel.UpgradeButtonLabel = currentUser.HasMaxReconRole(UserRoleTypeEnum.GoRecon)
                ? viewModel.Labels.FirstOrDefault(x => x.LabelType == LabelType.TravelReconOrTravelReconPro)
                : viewModel.Labels.FirstOrDefault(x => x.LabelType == LabelType.TravelReconPro);

            return viewModel;
        }

        private List<Alert> GetAlertsToPush(DbGeography location, IQueryable<int> pushedAlertIds)
        {
            var now = DateTime.UtcNow;
            return _alertRepository.GetAll().Where(a => a.ShouldPushToUsers
                    && (a.ExpiresOn.HasValue && a.ExpiresOn >= now) ||
                            (!a.AlertType.ExpirationTime.HasValue || DbFunctions.AddHours(a.ReportedOn, a.AlertType.ExpirationTime) >= now)
                    && a.Location.Distance(location) <= RadiusFromCurrentLocationForPullingAlertUpdates
                    && !pushedAlertIds.Contains(a.Id)).ToList();
        }

        private LabelType[] GetFilterDateTypeLabels()
        {
            return new[]
            {
                LabelType.SevenDays, LabelType.ThirtyDays, LabelType.SixMonths
            };
        }

        private LabelType[] GetSortAlertTypeLabels()
        {
            return new[]
            {
                LabelType.ByMostRecentFirst, LabelType.ByProximity
            };
        }

        public Task<AlertVm> GetAlertAsync(int id, int userId, LanguageType language)
        {
            return _alertRepository.GetAll()
                .Where(x => x.Id == id)
                .SelectVm(userId, language)
                .FirstOrDefaultAsync();
        }

        public Task<AlertVm> GetAlertAsync(int id, User user)
        {
            return _alertRepository
                .GetAll()
                .SelectVm(user.Id, user.GetLanguageSetting())
                .FirstOrDefaultAsync(x => x.Id == id);
        }

        public Task<AlertVm> GetAnalystAlertAsync(int id, User user)
        {
            var isEditorLead = user.HasRole(UserRoleTypeEnum.EditorLead);

            var query = _alertRepository
                .GetAll();

            query = isEditorLead ? query.GetAlertForEditorLead(user.Id) : query.GetAlertForAnalyst(user.Id);

            return query.SelectVm(user.Id, LanguageType.English)
                .FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task DeleteAsync(int id)
        {
            var alert = await _alertRepository.GetAll()
                .FirstOrDefaultAsync(x => x.Id == id);

            _alertRepository.Delete(alert);
            await _alertRepository.SaveChangesAsync();
        }

        public Task<List<AlertTypeCategoryVm>> GetCategoriesAsync(LanguageType language)
        {
            return _alertTypeCategoryRepository.GetAll()
                .Select(x => new AlertTypeCategoryVm
                {
                    Id = x.Id,
                    Language = language,
                    Translations = x.Translations.Select(t => new AlertTypeCategoryTranslationVm
                    {
                        Id = t.Id,
                        Language = t.Language,
                        Name = t.Name
                    }).ToList()
                })
                .ToListAsync();
        }

        public Task<List<AlertAnalystCategoryVm>> GetAnalystCategoriesAsync()
        {
            return _alertAnalystCategoryRepository.GetAll()
                .Select(x => new AlertAnalystCategoryVm
                {
                    Id = x.Id,
                    Name = x.Name
                })
                .ToListAsync();
        }

        public Task<List<AlertTypeVm>> GetTypesAsync(int categoryId, LanguageType language)
        {
            return _alertTypeRepository.GetAll()
                .Where(x => x.AlertTypeCategoryFk == categoryId)
                .Select(x => new AlertTypeVm
                {
                    Id = x.Id,
                    Language = language,
                    ExpirationTime = x.ExpirationTime,
                    Translations = x.Translations.Select(t => new AlertTypeTranslationVm
                    {
                        Id = t.Id,
                        Language = t.Language,
                        Name = t.Name
                    }).ToList()
                })
                .ToListAsync();
        }

        public Task<List<AlertAnalystTypeVm>> GetAnalystTypesAsync(int categoryId)
        {
            return _alertAnalystTypeRepository.GetAll()
                .Where(x => x.AnalystCategoryFk == categoryId)
                .Select(x => new AlertAnalystTypeVm
                {
                    Id = x.Id,
                    Name = x.Name,
                    CategoryId = x.AnalystCategoryFk
                })
                .ToListAsync();
        }

        private AlertWorkflow? ComputeWorkflow(AlertWorkflow? workflow, User user)
        {
            if (workflow == null && !user.HasRole(UserRoleTypeEnum.EditorLead))
                return AlertWorkflow.Draft;

            if (workflow == AlertWorkflow.None)
                return AlertWorkflow.Draft;

            return workflow;
        }

        public async Task<AlertVm> CreateAnalystAlertAsync(CreateAlertVm model, User user)
        {
            var location = DbGeographyFactory.GenerateDbGeographyValue(model.Latitude, model.Longitude);
            var destination = _destinationAppService.GetDestination(location);

            if (model.PeakImpactTime.HasValue)
            {
                if (model.PeakImpactTime.Value < DateTime.UtcNow || model.PeakImpactTime.Value > model.ExpiresOn)
                    throw new ValidationException("Peak Impact Time can be between the start date/time of the alert and the expiration time of the alert");
            }

            var alert = new Alert
            {
                AddressFormatted = model.Address,
                AlertTypeFk = model.TypeId,
                AnalystAlertTypeFk = model.AnalystTypeId,
                City = model.City ?? destination?.City,
                Country = model.Country ?? destination?.Country,
                Description = model.Description,
                Source = AlertSourceType.Analyst,
                DestinationId = model.DestinationId ?? destination?.Id,
                ExpiresOn = model.ExpiresOn,
                Workflow = ComputeWorkflow(model.Workflow, user),
                Location = location,
                Title = model.Title,
                ShouldPushToUsers = user.HasRole(UserRoleTypeEnum.EditorLead) && model.ShouldPushToUsers,
                ShouldShowOnMap = user.HasRole(UserRoleTypeEnum.EditorLead) && model.ShouldShowOnMap,
                ReportedByFk = user.Id,
                ReportedOn = DateTime.UtcNow,
                AnalystAlertPriorityId = model.AnalystAlertPriorityId,
                ConfidenceRating = model.ConfidenceRating,
                ImpactRating = model.ImpactRating,
                Tags = model.Tags,
                InitialImpactPercentage = model.InitialImpactPercentage,
                PeakImpactTime = model.PeakImpactTime
            };

            var alertSource = new AlertSource
            {
                CreatedOn = DateTime.UtcNow,
                CreatedBy = user.Id,
                Realibility = model.Accuracy,
                SourceId = model.SourceId
            };

            using (_alertRepository.BeginTransaction())
            {
                _alertRepository.Add(alert);
                await _alertRepository.SaveChangesAsync();

                alertSource.AlertId = alert.Id;

                _alertSourceRepository.Add(alertSource);
                await _alertSourceRepository.SaveChangesAsync();

                _alertRepository.CommitTransaction();
            }

            return await GetAlertAsync(alert.Id, user.Id, user.GetLanguageSetting());
        }

        public async Task<AlertVm> EditAnalystAlertAsync(int id, EditAlertVm model, User user)
        {
            var alert = await _alertRepository.GetAll().FirstOrDefaultAsync(x => x.Id == id);
            var location = DbGeographyFactory.GenerateDbGeographyValue(model.Latitude, model.Longitude);
            var destination = _destinationAppService.GetDestination(location);

            if (model.PeakImpactTime.HasValue)
            {
                if (model.PeakImpactTime.Value < alert.ReportedOn || model.PeakImpactTime.Value > model.ExpiresOn)
                    throw new ValidationException("Peak Impact Time can be between the start date/time of the alert and the expiration time of the alert");
            }

            alert.AddressFormatted = model.Address;
            alert.AlertTypeFk = model.TypeId;
            alert.AnalystAlertTypeFk = model.AnalystTypeId;
            alert.City = model.City ?? destination?.City;
            alert.Country = model.Country ?? destination?.Country;
            alert.Description = model.Description;
            alert.DestinationId = destination?.Id;
            alert.ExpiresOn = model.ExpiresOn;
            alert.Workflow = ComputeWorkflow(model.Workflow, user);
            alert.Location = location;
            alert.Title = model.Title;
            alert.ShouldPushToUsers = user.HasRole(UserRoleTypeEnum.EditorLead) && model.ShouldPushToUsers;
            alert.ShouldShowOnMap = user.HasRole(UserRoleTypeEnum.EditorLead) && model.ShouldShowOnMap;
            alert.DestinationId = model.DestinationId;
            alert.AnalystAlertPriorityId = model.AnalystAlertPriorityId;
            alert.ConfidenceRating = model.ConfidenceRating;
            alert.ImpactRating = model.ImpactRating;
            alert.Tags = model.Tags;
            alert.InitialImpactPercentage = model.InitialImpactPercentage;
            alert.PeakImpactTime = model.PeakImpactTime;

            var alertSource = new AlertSource
            {
                AlertId = id,
                CreatedOn = DateTime.UtcNow,
                CreatedBy = user.Id,
                Realibility = model.Accuracy,
                SourceId = model.SourceId
            };

            using (_alertRepository.BeginTransaction())
            {
                _alertRepository.Modified(alert);
                await _alertRepository.SaveChangesAsync();

                var alertSources = await _alertSourceRepository.GetAll()
                    .Where(x => x.AlertId == alert.Id)
                    .ToListAsync();

                _alertSourceRepository.DeleteRange(alertSources);
                _alertSourceRepository.Add(alertSource);

                await _alertSourceRepository.SaveChangesAsync();

                // re-send pushes for edited alert
                if (alert.Workflow == null && alert.ShouldPushToUsers)
                    await SetAlertForResendingPushNotification(alert.Id);

                _alertRepository.CommitTransaction();
            }

            return await GetAlertAsync(alert.Id, user.Id, user.GetLanguageSetting());
        }

        public async Task RemoveAlertFromApp(int alertId)
        {
            var alert = await _alertRepository.GetAll().FirstOrDefaultAsync(x => x.Id == alertId);

            if (alert == null)
                return;

            alert.Workflow = AlertWorkflow.Review;
            _alertRepository.Modified(alert);
            await _alertRepository.SaveChangesAsync();
        }

        public Task<RangeResult<AlertVm>> GetAlertsRangeAsync(int skip, int take, LanguageType language)
        {
            return _alertRepository
                .GetAll()
                .FindActiveAlerts()
                .Where(x => !x.IsEmergency)
                .OrderByDescending(x => x.Id)
                .SelectVm(default(int), language)
                .ToRangeResultAsync(skip, take);
        }

        public Task<List<AlertVm>> GetAlertsByLocationAsync(double latitude, double longitude, double radius, User user)
        {
            var location = DbGeographyFactory.GenerateDbGeographyValue(latitude, longitude);

            return _alertRepository.GetAll()
                .FindActiveAlertsNearby(location, radius, user)
                .SelectVm(user.Id, user.GetLanguageSetting())
                .ToListAsync();
        }

        public Task<List<AnalystAlertPriorityVm>> GetAnalystAlertPrioritiesAsync()
        {
            return _analystAlertPriorityRepository.GetAll()
                .SelectVm()
                .ToListAsync();
        }

        public bool CanThankAlert(User currentUser, int alertId)
        {
            var canThank = _alertThanksRepository.GetAll()
                .Where(x => x.AlertId == alertId && x.UserId == currentUser.Id);

            return !canThank.Any();
        }

        public bool CanValidateAlert(User currentUser, int alertId)
        {
            var canValidate = _alertValidationsRepository.GetAll()
                .Where(x => x.AlertId == alertId && x.UserId == currentUser.Id);

            return !canValidate.Any();
        }

        public Task ThankAlert(User currentUser, int alertId)
        {
            if (!CanThankAlert(currentUser, alertId))
                throw new ArgumentException("Current user already sent Thank Alert request to this alert");

            _alertThanksRepository.Add(new AlertsThank
            {
                AlertId = alertId,
                UserId = currentUser.Id,
                Date = DateTime.UtcNow
            });

            return _alertThanksRepository.SaveChangesAsync();
        }

        public Task ValidateAlert(User currentUser, int alertId)
        {
            if (!CanValidateAlert(currentUser, alertId))
                throw new ArgumentException("Current user already validated this alert");

            _alertValidationsRepository.Add(new AlertsValidation
            {
                AlertId = alertId,
                UserId = currentUser.Id,
                Date = DateTime.UtcNow
            });

            return _alertValidationsRepository.SaveChangesAsync();
        }

        public int GetAlertThanks(int alertId)
        {
            var thanksCount = _alertThanksRepository.GetAll()
                .Where(x => x.AlertId == alertId);

            return thanksCount.Count();
        }

        public int GetAlertValidation(int alertId)
        {
            var validationsCount = _alertValidationsRepository.GetAll()
                .Where(x => x.AlertId == alertId);

            return validationsCount.Count();
        }

        public Task<int> GetAlertThanksAsync(int alertId)
        {
            var thanksCount = _alertThanksRepository.GetAll()
                .Where(x => x.AlertId == alertId);

            return thanksCount.CountAsync();
        }

        public Task<int> GetAlertValidationsAsync(int alertId)
        {
            var validationsCount = _alertValidationsRepository.GetAll()
                .Where(x => x.AlertId == alertId);

            return validationsCount.CountAsync();
        }

        public Task<DataSourceResult<AlertVm>> GetAnalystAlertsAsync(DataSourceRequest request, User user)
        {
            var isEditorLead = user.HasRole(UserRoleTypeEnum.EditorLead);

            var query = _alertRepository
                .GetAll();

            query = isEditorLead 
                ? query.GetAlertForEditorLead(user.Id) 
                : query.GetAlertForAnalyst(user.Id);

            query = query.OrderByDescending(x => x.ReportedOn);

            return query.SelectVm(user.Id, LanguageType.English)
                .ToDataSourceResultAsync(request);
        }

        public Task<DataSourceResult<AlertVm>> GetAlertsAsync(DataSourceRequest request, User user)
        {
            var usersQuery = _userLicenseRepository.GetAll()
                .Where(x => x.ManagerId == user.Id 
                    && x.ActivatedDate.HasValue 
                    && x.ExpirationDate > DateTime.UtcNow
                    && x.OwnerId.HasValue)
                .Select(x => x.Owner);

            var latFilter = request.GetFiltersByField("latitude").FirstOrDefault();
            var longFilter = request.GetFiltersByField("longitude").FirstOrDefault();
            var radiusFilter = request.GetFiltersByField("radius").FirstOrDefault();
            var isEmergencyFilter = request.GetFiltersByField("isEmergency").FirstOrDefault();

            request.RemoveFiltersByField("latitude");
            request.RemoveFiltersByField("longitude");
            request.RemoveFiltersByField("radius");
            request.RemoveFiltersByField("isEmergency");

            var query = _alertRepository.GetAll();

            if (latFilter != null && longFilter != null && radiusFilter != null)
            {
                var location = DbGeographyFactory.GenerateDbGeographyValue((double) latFilter.Value,
                    (double) longFilter.Value);
                query = query.FindActiveAlertsNearby(location, (double) radiusFilter.Value, user);
            }
            else
                query = query.FindActiveAlerts();

            if (isEmergencyFilter != null)
            {
                query = query.Where(x => !x.IsEmergency || (x.IsEmergency && (bool)isEmergencyFilter.Value));
            }

            return query.SelectVm(usersQuery, user.Id, user.GetLanguageSetting())
                .OrderBy(x => x.Id)
                .ToDataSourceResultAsync(request);
        }

        public Task<DataSourceResult<AlertVm>> GetAnalystAlertsForManageAsync(DataSourceRequest request, User user)
        {
            var users = _usersRepository.GetAll().Where(x => x.UserRoles.Any(role => role.RoleId == (int)UserRoleTypeEnum.TravelReconPro));
            if (request.FilterList != null && request.FilterList.ListType != null &&
                request.FilterList.Field == "alertTypeId"
                && request.FilterList.ListType == "int" && request.FilterList.IdList != null
                && request.FilterList.IdList.Count > 0 )
            {
                IQueryable<Alert> alertsForManage = _alertRepository
                    .GetAll()
                    .GetAlertForManage();

                IQueryable<Alert> alertsForType = alertsForManage.GetAlertsForTypeList(request.FilterList.IdList);

                return alertsForType
                    .OrderByDescending(x => x.ReportedOn)
                    .SelectVm(users, user.Id, LanguageType.English)
                    .ToDataSourceResultAsync(request);
            }
            return _alertRepository
                .GetAll()
                .GetAlertForManage()
                .OrderByDescending(x => x.ReportedOn)
                .SelectVm(users, user.Id, LanguageType.English)
                .ToDataSourceResultAsync(request);
        }

        public Task<AnalystPushedGroupVm> GetAnalystPushedGroupAsync(int groupId)
        {
            return _analystPushedGroupRepository
                .GetAll()
                .Where(x => x.Id == groupId)
                .SelectVm(LanguageType.English)
                .OrderByDescending(x => x.CreatedDate)
                .FirstOrDefaultAsync();
        }

        public Task<DataSourceResult<AnalystPushedGroupVm>> GetAnalystPushedGroupsAsync(
            DataSourceRequest request, 
            int userId)
        {
            return _analystPushedGroupRepository
                .GetAll()
                .Where(x => x.CreatedById == userId)
                .SelectVm(LanguageType.English)
                .OrderByDescending(x => x.CreatedDate)
                .ToDataSourceResultAsync(request);
        }

        public Task<List<AlertVm>> GetAnalystPushedAlertsAsync(
            int groupId)
        {
            return _alertRepository
                .GetAll()
                .Where(x => x.AnalystPushedAlerts.Any(apa => apa.AnalystPushedGroupId == groupId))
                .SelectVm(default(int), LanguageType.English)
                .OrderByDescending(x => x.ReportedOn)
                .ToListAsync();
        }

        public async Task<AnalystPushedGroupVm> CreateAnalystPushedGroupAsync(CreateAnalystPushedGroupVm model, int userId)
        {
            if (model.Alerts.IsNullOrEmpty())
                throw new BusinessLogicException("Required any alert");

            if (model.Groups.IsNullOrEmpty() && model.Users.IsNullOrEmpty())
                throw new BusinessLogicException("Required any group or any user");

            using (_analystPushedGroupRepository.BeginTransaction())
            {
                var analystPushedGroup = new AnalystPushedGroup
                {
                    Accurancy = model.Accurancy,
                    CreatedDate = DateTime.UtcNow,
                    ConfidenceRating = model.ConfidenceRating,
                    DestinationId = model.DestinationId,
                    Title = model.Title,
                    CreatedById = userId,
                    ShouldPush = model.ShouldPush
                };

                _analystPushedGroupRepository.Add(analystPushedGroup);
                await _analystPushedGroupRepository.SaveChangesAsync();

                var analystPushedAlerts = new List<AnalystPushedAlert>();

                foreach (var alertId in model.Alerts)
                {
                    if (!model.Groups.IsNullOrEmpty())
                    {
                        foreach (var gId in model.Groups)
                        {
                            analystPushedAlerts.Add(new AnalystPushedAlert
                            {
                                AlertId = alertId,
                                AnalystPushedGroupId = analystPushedGroup.Id,
                                ReceiverId = gId,
                                ReceiverType = AnalystPushedAlertReceiverType.Group
                            });
                        }
                    }

                    if (!model.Users.IsNullOrEmpty())
                    {
                        foreach (var uId in model.Users)
                        {
                            analystPushedAlerts.Add(new AnalystPushedAlert
                            {
                                AlertId = alertId,
                                AnalystPushedGroupId = analystPushedGroup.Id,
                                ReceiverId = uId,
                                ReceiverType = AnalystPushedAlertReceiverType.User
                            });
                        }
                    }
                }

                _analystPushedAlertRepository.AddRange(analystPushedAlerts);
                await _analystPushedAlertRepository.SaveChangesAsync();

                _analystPushedGroupRepository.CommitTransaction();

                return await GetAnalystPushedGroupAsync(analystPushedGroup.Id);
            }
        }

        private async Task SetAlertForResendingPushNotification(int alertId)
        {
            var pushedAlerts = await _pushedAlertRepository.GetAll()
                .Where(x => x.AlertFk == alertId)
                .ToListAsync();

            if (pushedAlerts.Any())
            {
                _pushedAlertRepository.DeleteRange(pushedAlerts);
                await _pushedAlertRepository.SaveChangesAsync();
            }
        }

        public async Task<AlertVm> CreateManageAnalystAlertAsync(CreateManageAlertVm model, int userId, LanguageType language)
        {
            var destination = await _destinationAppService.GetDestinationAsync(model.DestinationId, language);

            var alert = new Alert
            {
                AlertTypeFk = model.TypeId,
                AddressFormatted = $"{destination.City}, {destination.Country}",
                Workflow = null,
                AnalystAlertPriorityId = model.PriorityId,
                DestinationId = model.DestinationId,
                Location = destination.Location,
                Description = model.Description,
                Title = model.Title,
                ReportedOn = DateTime.UtcNow,
                ReportedByFk = userId,
                ShouldShowOnMap = true,
                Source = AlertSourceType.Analyst,
                City = destination.City,
                Country = destination.Country
            };

            _alertRepository.Add(alert);

            await _alertRepository.SaveChangesAsync();
            return await GetAlertAsync(alert.Id, userId, language);
        }

        public async Task<int> GetDefaultEmergencyTypeIdAsync()
        {
            var categoryOther = await _alertTypeCategoryRepository.GetAll()
                .Include(x => x.Translations)
                .FirstOrDefaultAsync(x => x.Translations.Any(t => t.Name.Trim() == "Other"));

            var dafaultCategory = categoryOther ?? await _alertTypeCategoryRepository.GetAll()
                                      .OrderByDescending(x => x.Id)
                                      .FirstOrDefaultAsync();

            var typeOther = await _alertTypeRepository.GetAll()
                .Include(x => x.Translations)
                .Where(x => x.AlertTypeCategoryFk == dafaultCategory.Id)
                .FirstOrDefaultAsync(x => x.Translations.Any(t => t.Name.Trim() == "Other"));

            if (typeOther == null)
            {
                return await _alertTypeRepository.GetAll()
                    .OrderByDescending(x => x.Id)
                    .Select(x => x.Id)
                    .FirstOrDefaultAsync();
            }

            return typeOther.Id;
        }

        public async Task<List<AlertVm>> GetEmergenciesForWebNotification(User currentUser)
        {
            var managedUsersIds = _userLicenseRepository.GetAll()
                .Where(x => x.ManagerId == currentUser.Id && x.OwnerId.HasValue && x.ExpirationDate > DateTime.Now)
                .Select(x => x.OwnerId)
                .ToList();

            var requestInterval = DateTime.UtcNow.AddMinutes(-5);

            // select emergencies alerted by users linked to current TRP Manager not older than 5 minutes
            var alerts = await _alertRepository.GetAll()
                .Where(x => x.IsEmergency
                        && managedUsersIds.Contains(x.ReportedByFk)
                        && x.ReportedOn > requestInterval)
                .SelectVm(currentUser.Id, currentUser.GetLanguageSetting())
                .ToListAsync();

            return alerts;
        }
    }
}

