﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using LinqKit;
using TravelRecon.AppServices.Alerts.ViewDtos;
using TravelRecon.Domain.Alerts;
using TravelRecon.Domain.Identity;
using TravelRecon.Domain.Localization;
using System.Collections.Generic;

namespace TravelRecon.AppServices.Alerts.Queries
{
    public static class AlertQueries
    {
        public static IQueryable<AlertVm> SelectVm(
            this IQueryable<Alert> query, int userId, LanguageType language
        )
        {
            var date = DateTime.UtcNow.AddMinutes(-30);

            return query
                .Select(x => new AlertVm
                {
                    AddressFormatted = x.AddressFormatted,
                    AlertTypeCategoryId = x.AlertType.AlertTypeCategoryFk,
                    AlertTypeCategoryName = x.AlertType.Category.Translations.FirstOrDefault(t => t.Language == language).Name,
                    AlertTypeId = x.AlertTypeFk,
                    AlertTypeName = x.AlertType.Translations.FirstOrDefault(t => t.Language == language).Name,
                    AlertAnalystCategoryId = x.AnalystAlertType.AnalystCategoryFk,
                    AlertAnalystCategoryName = x.AnalystAlertType.AnalystCategory.Name,
                    AlertAnalystTypeId = x.AnalystAlertTypeFk,
                    AlertAnalystTypeName = x.AnalystAlertType.Name,
                    City = x.City,
                    Country = x.Country,
                    Description = x.Description,
                    ExpiresOn = x.ExpiresOn,
                    Id = x.Id,
                    ReportedOn = x.ReportedOn,
                    ReportedBy = x.ReportedByFk,
                    Reporter = x.ReportedBy.UserName,
                    ReporterFullName = string.Concat(x.ReportedBy.FirstName, " ", x.ReportedBy.LastName).Trim(),
                    Latitude = x.Location.Latitude.Value,
                    Longitude = x.Location.Longitude.Value,
                    ShouldPushToUsers = x.ShouldPushToUsers,
                    ShouldShowOnMap = x.ShouldShowOnMap,
                    Source = x.Source,
                    IsEmergency = x.IsEmergency && x.ReportedOn > date,
                    EmergencyExpiration = x.IsEmergency && x.ReportedOn > date ? DbFunctions.DiffMinutes(x.ReportedOn, date) : null,
                    Title = x.Title,
                    Workflow = x.Workflow,
                    SourceId = x.AlertSources.FirstOrDefault().SourceId,
                    Accuracy = x.AlertSources.FirstOrDefault().Realibility,
                    ConfidenceRating = x.ConfidenceRating,
                    AnalystAlertPriorityId = x.AnalystAlertPriorityId,
                    AlertTypePriorityId = x.AlertType.AlertPriorityFk,
                    Tags = x.Tags,
                    ImpactRating = x.ImpactRating,
                    DestinationId = x.DestinationId,
                    Priority = x.AnalystAlertPriority.Name,
                    SourceName = x.AlertSources.FirstOrDefault().Source.Name,
                    Severity = x.AlertType.Severity,
                    ProximityStandoff = x.AlertType.AffectProximity,
                    CanThank = x.AlertsThanks.All(at => at.UserId != userId),
                    CanValidate = x.AlertsValidations.All(at => at.UserId != userId),
                    ValidationsCount = x.AlertsValidations.Count(),
                    ThanksCount = x.AlertsThanks.Count(),
                    InitialImpactPercentage = x.InitialImpactPercentage,
                    PeakImpactTime = x.PeakImpactTime
                });
        }

        public static IQueryable<AlertVm> SelectVm(
            this IQueryable<Alert> query, IQueryable<User> users, int userId, LanguageType language
        )
        {
            var date = DateTime.UtcNow.AddMinutes(-30);
            var peopleAffectedDetailsExpr = getAffectedUsers(users);
            var peopleAffectedExpr = GetPeopleAffectedCountExpression(users);

            return query
                .AsExpandable()
                .Select(x => new AlertVm
                {
                    AddressFormatted = x.AddressFormatted,
                    AlertTypeCategoryId = x.AlertType.AlertTypeCategoryFk,
                    AlertTypeCategoryName =
                        x.AlertType.Category.Translations.FirstOrDefault(t => t.Language == language).Name,
                    AlertTypeId = x.AlertTypeFk,
                    AlertTypeName = x.AlertType.Translations.FirstOrDefault(t => t.Language == language).Name,
                    AlertAnalystCategoryId = x.AnalystAlertType.AnalystCategoryFk,
                    AlertAnalystCategoryName = x.AnalystAlertType.AnalystCategory.Name,
                    AlertAnalystTypeId = x.AnalystAlertTypeFk,
                    AlertAnalystTypeName = x.AnalystAlertType.Name,
                    City = x.City,
                    Country = x.Country,
                    Description = x.Description,
                    ExpiresOn = x.ExpiresOn,
                    Id = x.Id,
                    ReportedOn = x.ReportedOn,
                    ReportedBy = x.ReportedByFk,
                    Reporter = x.ReportedBy.UserName,
                    Latitude = x.Location.Latitude.Value,
                    Longitude = x.Location.Longitude.Value,
                    ShouldPushToUsers = x.ShouldPushToUsers,
                    ShouldShowOnMap = x.ShouldShowOnMap,
                    Source = x.Source,
                    IsEmergency = x.IsEmergency && x.ReportedOn > date,
                    EmergencyExpiration =
                        x.IsEmergency && x.ReportedOn > date ? DbFunctions.DiffMinutes(x.ReportedOn, date) : null,
                    Title = x.Title,
                    Workflow = x.Workflow,
                    SourceId = x.AlertSources.FirstOrDefault().SourceId,
                    Accuracy = x.AlertSources.FirstOrDefault().Realibility,
                    ConfidenceRating = x.ConfidenceRating,
                    AnalystAlertPriorityId = x.AnalystAlertPriorityId,
                    AlertTypePriorityId = x.AlertType.AlertPriorityFk,
                    Tags = x.Tags,
                    ImpactRating = x.ImpactRating,
                    DestinationId = x.DestinationId,
                    Priority = x.AnalystAlertPriority.Name,
                    SourceName = x.AlertSources.FirstOrDefault().Source.Name,
                    Severity = x.AlertType.Severity,
                    ProximityStandoff = x.AlertType.AffectProximity,
                    CanThank = x.AlertsThanks.All(at => at.UserId != userId),
                    CanValidate = x.AlertsValidations.All(at => at.UserId != userId),
                    ValidationsCount = x.AlertsValidations.Count(),
                    ThanksCount = x.AlertsThanks.Count(),
                    PeopleAffected = peopleAffectedExpr.Invoke(x),
                    PeopleAffectedDetails = peopleAffectedDetailsExpr.Invoke(x)
                });
        }

        public static IQueryable<AnalystPushedGroupVm> SelectVm(
            this IQueryable<AnalystPushedGroup> query,
            LanguageType language
        )
        {
            return query
                .Select(x => new AnalystPushedGroupVm
                {
                    City = x.Destination.Translations.Where(t => t.Language == language)
                        .Select(t => t.City)
                        .FirstOrDefault(),
                    Country = x.Destination.Country.Translations.Where(t => t.Language == language)
                        .Select(t => t.Name)
                        .FirstOrDefault(),
                    Id = x.Id,
                    CreatedDate = x.CreatedDate,
                    CreatedById = x.CreatedById,
                    CreatedBy = x.CreatedBy.UserName,
                    Title = x.Title,
                    ConfidenceRating = x.ConfidenceRating,
                    DestinationId = x.DestinationId,
                    Accurancy = x.Accurancy,

                    AlertsCount = x.AnalystPushedAlerts
                        .Select(apa => apa.AlertId)
                        .Distinct()
                        .Count(),
                    GroupsCount = x.AnalystPushedAlerts
                        .Where(apa => apa.ReceiverType == AnalystPushedAlertReceiverType.Group)
                        .Select(apa => apa.ReceiverId)
                        .Distinct()
                        .Count(),
                    UsersCount = x.AnalystPushedAlerts
                        .Where(apa => apa.ReceiverType == AnalystPushedAlertReceiverType.User)
                        .Select(apa => apa.ReceiverId)
                        .Distinct()
                        .Count()
                });
        }

        public static IQueryable<AnalystAlertPriorityVm> SelectVm(this IQueryable<AnalystAlertPriority> query)
        {
            return query.Select(x => new AnalystAlertPriorityVm
            {
                Id = x.Id,
                Name = x.Name
            });
        }

        public static IQueryable<Alert> GetAnalystAlerts(this IQueryable<Alert> query)
        {
            return query.Where(x => x.Source == AlertSourceType.Analyst);
        }

        public static IQueryable<Alert> GetAlertForEditorLead(this IQueryable<Alert> query, int editorLeadId)
        {
            return query
                .GetAnalystAlerts()
                .Where(x => (x.Workflow != AlertWorkflow.Draft || x.ReportedByFk == editorLeadId)
                    && !x.AnalystPushedAlerts.Any());
        }

        public static IQueryable<Alert> GetAlertForAnalyst(this IQueryable<Alert> query, int analystId)
        {
            return query
                .GetAnalystAlerts()
                .Where(x => x.Workflow != null
                    && x.ReportedByFk == analystId
                    && !x.AnalystPushedAlerts.Any());
        }

        public static IQueryable<Alert> GetAlertForManage(this IQueryable<Alert> query)
        {
            return query.Where(x => x.Workflow == null
                && x.Source == AlertSourceType.Analyst
                && !x.AnalystPushedAlerts.Any());
        }

        public static Expression<Func<Alert, List<User>>> getAffectedUsers (IQueryable<User> users)        {
            return
                (a) => users.Where(u => u.CurrentLocation.Distance(a.Location) <= a.AlertType.AffectProximity * 1609.34).ToList();
        }

        public static Expression<Func<Alert, int>> GetPeopleAffectedCountExpression(IQueryable<User> users)
        {
            // the AffectProximity value in alert types is provided in miles
            // DbGeography.Distance returns distance in meters
            // 1609.34 converts miles in meters
            return
                (a) => users.Count(u => u.CurrentLocation.Distance(a.Location) <= a.AlertType.AffectProximity * 1609.34);
        }

        public static IQueryable<Alert> GetAlertsForTypeList(this IQueryable<Alert> query, List<int> alertTypeIds)
        {
            query = query.Where(a => alertTypeIds.Contains(a.AlertTypeFk));
            return query;
        }
    }
}

