﻿using System.Linq;
using TravelRecon.Domain.Localization.QueryObjects;

namespace TravelRecon.AppServices.Menu.ViewDtos
{
    public class MenuPageDto
    {
        public int MenuId { get; set; }
        public string Title { get; set; }

        public static MenuPageDto ConvertFrom(LabelLocalizedDto labelLocalizedDto)
        {
            var keyValuePair = MenuPageMenuIdCollection.MenuPageMenuId.Single(x => x.Key == labelLocalizedDto.LabelType);
            var menuId = keyValuePair.Value;
            return new MenuPageDto
            {
                MenuId = menuId,
                Title = labelLocalizedDto.Content
            };
        }
    }
}