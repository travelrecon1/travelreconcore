using System.Collections.Generic;
using TravelRecon.Domain.Localization;

namespace TravelRecon.AppServices.Menu.ViewDtos
{
    public struct MenuPageMenuIdCollection
    {
        public static readonly Dictionary<LabelType, int> MenuPageMenuId = new Dictionary<LabelType, int>
        {
            //these align with left-menu-factory.ts 
            {LabelType.ReconMap, 0},
            {LabelType.MenuCommunications, 1},
            {LabelType.MyScoutProfile, 2},
            {LabelType.MenuAlerts, 4},
            {LabelType.MenuContacts, 6},
            {LabelType.MenuReports, 3},
            {LabelType.MenuSettings, 7},
            {LabelType.MenuSupport, 5},
            {LabelType.InviteContacts, 8},
            {LabelType.Dashboard, 9},
            {LabelType.Upgrade, 10},
        };


    }
}