﻿using System.Collections.Generic;
using TravelRecon.Domain.Localization.QueryObjects;

namespace TravelRecon.AppServices.Menu.ViewDtos
{
    public class MenuViewModel
    {
        public List<MenuPageDto> Pages = new List<MenuPageDto>();
        public IEnumerable<LabelLocalizedDto> Labels { get; set; } 
    }
}