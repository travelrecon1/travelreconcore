using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using TravelRecon.AppServices.Menu.ViewDtos;
using TravelRecon.Domain.Identity;
using TravelRecon.Domain.Localization;
using TravelRecon.Domain.Localization.QueryObjects;
using TravelRecon.Domain.RepositoryInterfaces;
using Label = TravelRecon.Domain.Localization.Label;

namespace TravelRecon.AppServices.Menu
{
    public class MenuService : IAppService
    {
        private readonly IRepository<Label> _labelRepository;

        public MenuService(IRepository<Label> labelRepository)
        {
            _labelRepository = labelRepository;
        }

        public MenuViewModel GetMenuPages(User currentUser)
        {
            var menuViewModel = new MenuViewModel();
            var labelsToRetrieve = GetMenuLabels().ToArray();

            var languageSetting = currentUser.GetLanguageSetting();
            List<LabelLocalizedDto> localizedLabels = _labelRepository.GetAll().FindLabelsLocalized(labelsToRetrieve, languageSetting).ToList();

			foreach (var localizedLabel in localizedLabels)
            {
                var convertFrom = MenuPageDto.ConvertFrom(localizedLabel);
                menuViewModel.Pages.Add(convertFrom);
            }

            labelsToRetrieve = new[] { LabelType.Logout, LabelType.Upgrade };
            menuViewModel.Labels = _labelRepository.GetAll().FindLabelsLocalized(labelsToRetrieve, languageSetting).ToList();

            return menuViewModel;
        }

        private static List<LabelType> GetMenuLabels()
        {
            return new List<LabelType>
            {
                LabelType.ReconMap,
                LabelType.MenuAlerts,
                LabelType.MenuContacts,
                LabelType.MenuReports,
                LabelType.MenuSettings,
                LabelType.MenuCommunications,
                LabelType.MenuSupport,
                LabelType.InviteContacts,
				LabelType.Dashboard,
                LabelType.MyScoutProfile,
                LabelType.Upgrade
            };
        }

    }
}