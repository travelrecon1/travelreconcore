using System.Collections.Generic;
using System.Linq;
using TravelRecon.Data;
using TravelRecon.Domain.Identity;
using TravelRecon.Domain.Permissions;
using TravelRecon.Domain.RepositoryInterfaces;

namespace TravelRecon.AppServices.Permissions
{
    public class PermissionsRepository : IAppService
    {
        private readonly IRepository<SubscriptionLevelTypesPermission> _subscriptionLevelTypesPermissionsRepository;
        private readonly PermissionsCache _permissionsCache;
        private readonly TravelReconDbContext _dbContext;

        public PermissionsRepository(IRepository<SubscriptionLevelTypesPermission> subscriptionLevelTypesPermissionsRepository, 
            PermissionsCache permissionsCache, 
            TravelReconDbContext dbContext)
        {
            _subscriptionLevelTypesPermissionsRepository = subscriptionLevelTypesPermissionsRepository;
            _permissionsCache = permissionsCache;
            _dbContext = dbContext;
        }

        private void PersistPermissionsInMemory()
        {
            var permissions = _subscriptionLevelTypesPermissionsRepository.GetAll();
            var goReconPermissions = permissions.Where(x => x.SubscriptionlevelTypeId == (int)UserRoleTypeEnum.GoRecon).ToList();
            var travelReconPermissions = permissions.Where(x => x.SubscriptionlevelTypeId == (int)UserRoleTypeEnum.TravelRecon).ToList();
            var travelReconProPermissions = permissions.Where(x => x.SubscriptionlevelTypeId == (int)UserRoleTypeEnum.TravelReconPro).ToList();
            _permissionsCache.Persist(goReconPermissions, travelReconPermissions, travelReconProPermissions);
        }

        private void UpdateLocalDataStore(List<SubscriptionLevelTypesPermission> results)
        {
            //results.ForEach(x => _repository.Add(x));
            _subscriptionLevelTypesPermissionsRepository.AddRange(results);
            _subscriptionLevelTypesPermissionsRepository.SaveChanges();
        }

        private IEnumerable<SubscriptionLevelTypesPermission> DeleteExistingSubscriptionTypePermissions()
        {
            //When attempting to use the injected _dbContext, EF complains _dbContext has already been disposed
            //using (var dbContext = TravelReconDbContext.Create())
            //{
            //    var deleteExistingSubscriptionTypePermissions = dbContext.SubscriptionLevelTypesPermissions.RemoveRange(dbContext.SubscriptionLevelTypesPermissions);
            //    int saveChanges = dbContext.SaveChanges();
            //    return deleteExistingSubscriptionTypePermissions;
            //}

            var deleteExistingSubscriptionTypePermissions = _dbContext.SubscriptionLevelTypesPermissions.RemoveRange(_dbContext.SubscriptionLevelTypesPermissions);
            int saveChanges = _dbContext.SaveChanges();
            return deleteExistingSubscriptionTypePermissions;


        }
    }
}