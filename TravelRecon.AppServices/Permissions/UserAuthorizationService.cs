using System.Linq;
using TravelRecon.Domain.Permissions;
using TravelRecon.Domain.RepositoryInterfaces;
using User = TravelRecon.Domain.Identity.User;

namespace TravelRecon.AppServices.Permissions
{
    public class UserAuthorizationService : IAppService
    {
        private readonly IRepository<SubscriptionLevelTypesPermission> _repository;
        private readonly PermissionsRepository _permissionsRepository;

        public UserAuthorizationService(IRepository<SubscriptionLevelTypesPermission> repository, PermissionsRepository permissionsRepository)
        {
            _repository = repository;
            _permissionsRepository = permissionsRepository;
        }

        public IQueryable<SubscriptionLevelTypesPermission> GetAll()
        {
            return _repository.GetAll();
        }
    }
}