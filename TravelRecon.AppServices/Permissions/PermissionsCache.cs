using System;
using System.Collections.Generic;
using System.Linq;
using TravelRecon.Domain.Identity;
using TravelRecon.Domain.Permissions;

namespace TravelRecon.AppServices.Permissions
{
    public class PermissionsCache
    {
        private DateTime _lastUpdated;
        private List<SubscriptionLevelTypesPermission> _goReconPermissions;
        private List<SubscriptionLevelTypesPermission> _travelReconPermissions;
        private List<SubscriptionLevelTypesPermission> _travelRecoProPermissions;

        public bool IsItTimeToGetNewPermissionsFromDrupal()
        {

            if (_goReconPermissions == null || !_goReconPermissions.Any())
            {
                return true;
            }

            if (_travelReconPermissions == null || !_travelReconPermissions.Any())
            {
                return true;
            }

            if (_travelRecoProPermissions == null || !_travelRecoProPermissions.Any())
            {
                return true;
            }

            TimeSpan timeSpan = DateTime.UtcNow - _lastUpdated;
            var isTimeExpired = timeSpan.Minutes >= 15;

            return isTimeExpired;
        }

        public void SetLastUpdated()
        {
            _lastUpdated = DateTime.UtcNow.AddMinutes(15);
        }

        public void Persist(List<SubscriptionLevelTypesPermission> goReconPermissions, List<SubscriptionLevelTypesPermission> travelReconPermissions, List<SubscriptionLevelTypesPermission> travelReconProPermissions)
        {
            _goReconPermissions = goReconPermissions;
            _travelRecoProPermissions = travelReconPermissions;
            _travelReconPermissions = travelReconPermissions;
        }

        public List<SubscriptionLevelTypesPermission> GetSubscriptionPermissionsFor(UserRoleTypeEnum subscriptionLevel)
        {
            switch (subscriptionLevel)
            {
                case UserRoleTypeEnum.GoRecon:
                case UserRoleTypeEnum.Administrator:
                    return _goReconPermissions;
                case UserRoleTypeEnum.TravelRecon:
                    return _travelReconPermissions;
                case UserRoleTypeEnum.TravelReconPro:
                    return _travelRecoProPermissions;
                default:
                    return _goReconPermissions;
            }
        }
    }
}