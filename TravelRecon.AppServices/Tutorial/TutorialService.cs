﻿using System.Linq;
using TravelRecon.AppServices.Interfaces;
using TravelRecon.Domain.Identity;
using TravelRecon.Domain.RepositoryInterfaces;
using TravelRecon.Domain.Tutorial;

namespace TravelRecon.AppServices.Tutorial
{
    public class TutorialService: IAppService
    {
        private readonly IRepository<ViewedPage> _viewedPageRepository;

        public TutorialService(IRepository<ViewedPage> viewedPageRepository)
        {
            _viewedPageRepository = viewedPageRepository;
        }

        public bool IsPageViewed(User user, PageTypeEnum pageType)
        {
            var result = _viewedPageRepository.GetAll()
                .Any(x => x.UserId == user.Id && x.PageType == pageType);

            return result;
        }

        public void SetViewedPage(User user, PageTypeEnum page)
        {
            var isViewed = IsPageViewed(user, page);

            if (isViewed)
                return;

            var vPage = new ViewedPage
            {
                PageType = page,
                UserId = user.Id
            };

            _viewedPageRepository.Add(vPage);
            _viewedPageRepository.SaveChanges();
        }
    }
}
