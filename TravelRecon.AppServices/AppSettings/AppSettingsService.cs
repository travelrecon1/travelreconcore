﻿using System.Configuration;
using TravelRecon.AppServices.Interfaces;

namespace TravelRecon.AppServices.AppSettings
{
    public class AppSettingsService: IAppService
    {
        public string WebSiteHelpUrl => Get("webSiteHelpUrl");
        public string WebSiteUrl => Get("webSiteUrl");
        public string WebSiteRequestLocationUrl => Get("webSiteRequestLocationUrl");
        public string WebSitePurchaseUrl => Get("webSitePurchaseUrl");
        public string WebSiteAppUrl => Get("webSiteAppUrl");
        public string ImagesRootUrl => Get("imagesRootUrl");
        public string SupportEmailAddress => Get("supportEmailAddress");
        public string SalesEmailAddress => Get("salesEmailAddress");
        public string RegisterUrl => Get("registerUrl");
        public string ForgotPasswordUrl => Get("forgotPasswordUrl");
        public string AvatarPostUrl => Get("avatarPostUrl");
        public string AvatarGetUrl => Get("avatarGetUrl");
        public string DefaultContact => Get("defaultContact");
        public string SendGridApiKey => Get("sendGridApiKey");
        public string SendGridEmailVerificationTemplate => Get("SendGridEmailVerificationTemplate");
        public string SendGridEmailContactUsTemplate => Get("SendGridEmailContactUsTemplate");
        public string SendGridUsernameRecoveryEmailTemplate => Get("SendGridUsernameRecoveryEmailTemplate");
        public string SendGridResetPasswordEmailTemplate => Get("sendGridResetPasswordEmailTemplate");
        public string SendGridFriendInviteEmailTemplateEnglish => Get("sendGridFriendInviteEmailTemplateEnglish");
        public string SendGridFriendInviteEmailTemplateSpanish => Get("sendGridFriendInviteEmailTemplateSpanish");
        public string SendGridFriendInviteEmailTemplatePortuguese => Get("sendGridFriendInviteEmailTemplatePortuguese");
        public string SendGridSendEmailFrom => Get("sendGridSendEmailFrom");
        public bool StripeIsLiveMode => bool.Parse(Get("StripeIsLiveMode"));

        private string Get(string key)
        {
            return ConfigurationManager.AppSettings[key];
        }
    }
}
