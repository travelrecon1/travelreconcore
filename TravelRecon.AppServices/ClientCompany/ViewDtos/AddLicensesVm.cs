﻿using System;
using System.ComponentModel.DataAnnotations;
using TravelRecon.Domain.License;

namespace TravelRecon.AppServices.ClientCompany.ViewDtos
{
    public class AddLicensesVm
    {
        [Required]
        public LicenseType LicenseType { get; set; }
        [Required]
        public DateTime ExpirationDate { get; set; }
        [Required]
        public int Count { get; set; }
        [Required]
        public int CompanyId { get; set; }
    }
}
