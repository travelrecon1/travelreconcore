﻿namespace TravelRecon.AppServices.ClientCompany.ViewDtos
{
    public class ManagedUsersInfoDto
    {
        public int ManagedUsersNumber { get; set; }
        public int ClientsTotalUsers { get; set; }
    }
}
