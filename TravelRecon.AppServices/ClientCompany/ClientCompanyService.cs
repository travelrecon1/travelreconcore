﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using TravelRecon.AppServices.AppSettings;
using TravelRecon.AppServices.ClientCompany.ViewDtos;
using TravelRecon.AppServices.Extensions;
using TravelRecon.AppServices.Groups.Queries;
using TravelRecon.AppServices.Groups.ViewDtos;
using TravelRecon.AppServices.Identity;
using TravelRecon.AppServices.Interfaces;
using TravelRecon.AppServices.Mail;
using TravelRecon.AppServices.Shared.Extensions;
using TravelRecon.AppServices.SharedDtos;
using TravelRecon.AppServices.Users;
using TravelRecon.AppServices.Users.Queries;
using TravelRecon.AppServices.Users.ViewDtos;
using TravelRecon.Common.DynamicLinq;
using TravelRecon.Common.Exceptions;
using TravelRecon.Domain.Groups;
using TravelRecon.Domain.Identity;
using TravelRecon.Domain.License;
using TravelRecon.Domain.RepositoryInterfaces;

namespace TravelRecon.AppServices.ClientCompany
{
    public class ClientCompanyService :IAppService
    {
        private readonly TravelReconUserManager _userManager;
        private readonly IRepository<Domain.ClientCompany.ClientCompany> _clientCompanyRepository;
        private readonly IRepository<User> _userRepository;
        private readonly UserService _userService;
        private readonly IRepository<UserLicense> _userLicenseRepository;
        private readonly IRepository<License> _licenseRepository;
        private readonly IRepository<UserDetails> _userDetailsRepository;
        private readonly MailService _mailService;
        private readonly AppSettingsService _appSettingsService;
        private readonly IRepository<Group> _groupRepository;

        public ClientCompanyService(
            TravelReconUserManager userManager,
            IRepository<Domain.ClientCompany.ClientCompany> clinetCompanyRepository,
            IRepository<User> userRepository, UserService userService,
            IRepository<UserLicense> userLicenseRepository,
            IRepository<License> licenseRepository,
            IRepository<UserDetails> userDetailsRepository,
            IRepository<Group> groupRepository,
            MailService mailService,
            AppSettingsService appSettingsService)
        {
            _userManager = userManager;
            _clientCompanyRepository = clinetCompanyRepository;
            _userRepository = userRepository;
            _userService = userService;
            _userLicenseRepository = userLicenseRepository;
            _licenseRepository = licenseRepository;
            _userDetailsRepository = userDetailsRepository;
            _mailService = mailService;
            _appSettingsService = appSettingsService;
            _groupRepository = groupRepository;
        }

        public Task<ClientCompanyDto> GetAsync(int id)
        {
            var companyQuery = _clientCompanyRepository.GetAll();
            var userLicenseQuery = _userLicenseRepository.GetAll();
            return companyQuery.SelectClientCompanyDto(userLicenseQuery)
                .FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task SaveAsync(CreateClientCompanyVm model, UserRoleTypeEnum userRoleType, User currentUser)
        {
            using (_userRepository.BeginTransaction())
            {
                if (model.Id == 0)
                {
                    //first we create user 
                    var user = new User()
                    {
                        AlertPopupsShowIntervalMinutes = 5,
                        UserName = model.Name,
                        FirstName = model.ClientFirstName,
                        LastName = model.ClientLastName,
                        Email = model.Email,
                        LocationUpdateFrequecySeconds = 60*5,
                    };

                    var result = await _userManager.CreateAsync(user);
                    result.Validate();

                    result = await _userManager.AddToRolesAsync(user.Id, userRoleType.ToString());
                    result.Validate();

                    var client = model.ConvertFromVm();
                    client.UserId = user.Id;

                    if (currentUser.HasRole(UserRoleTypeEnum.TRPClient) 
                        && !currentUser.HasRole(UserRoleTypeEnum.TRManager)
                        && userRoleType == UserRoleTypeEnum.TravelReconProAdministrator)
                    {
                        var creator = await _clientCompanyRepository.GetAll()
                            .FirstOrDefaultAsync(x => x.UserId == currentUser.Id);

                        if (creator != null)
                            client.AffiliatedClientId = creator.Id;
                    }

                    //then create client company entity
                    _clientCompanyRepository.Add(client);

                    await _clientCompanyRepository.SaveChangesAsync();

                    if (userRoleType == UserRoleTypeEnum.TravelReconProAdministrator)
                    {
                        _mailService.SendActivateAdministratorEmail(client.Email,
                            $"{client.ClientFirstName} {client.ClientLastName}",
                            client.Name,
                            $"{currentUser.FirstName} {currentUser.LastName}",
                            currentUser.Email);

                    }
                    else
                    {
                        _mailService.SendActivateClientEmail(client.Email,
                            $"{client.ClientFirstName} {client.ClientLastName}",
                            client.Name,
                            $"{currentUser.FirstName} {currentUser.LastName}",
                            currentUser.Email);
                    }
                }
                else
                {
                    var client = await _clientCompanyRepository.GetAll()
                        .Where(x => x.Id == model.Id).FirstOrDefaultAsync();

                    if (client == null)
                        throw new ValidationException("Cannot find the client record");

                    var user = _userRepository.Get(client.UserId);
                    
                    var newClient = model.ConvertFromVm();
                    client.Name = newClient.Name;
                    client.LegalName = newClient.LegalName;
                    client.Abbreviation = newClient.Abbreviation;
                    client.ClientFirstName = newClient.ClientFirstName;
                    client.ClientLastName = newClient.ClientLastName;
                    client.ClientTitle = newClient.ClientTitle;
                    client.ClientDepartment = newClient.ClientDepartment;
                    client.ClientPhysicalAddress = newClient.ClientPhysicalAddress.Copy();
                    client.ClientPhysicalAddressId = newClient.ClientPhysicalAddressId;
                    client.PhoneNumber = newClient.PhoneNumber;
                    client.MobileNumber = newClient.MobileNumber;
                    client.Email = newClient.Email;
                    client.Notes = newClient.Notes;

                    user.FirstName = client.ClientFirstName;
                    user.LastName = client.ClientLastName;
                    user.Email = client.Email;

                    _clientCompanyRepository.Modified(client);
                    _userRepository.Modified(user);

                    await _clientCompanyRepository.SaveChangesAsync();
                    await _userRepository.SaveChangesAsync();
                }

                _userRepository.CommitTransaction();
            }
        }

        private int GetNextNumber(string licenseeKey)
        {
            if (string.IsNullOrWhiteSpace(licenseeKey))
                return 0;

            var parts = licenseeKey.Split('-');

            return int.Parse(parts[parts.Length - 1]);
        }

        private string CompleteString(string str, char placeholder, int length, bool onEnd = false)
        {
            while (str.Length < length)
            {
                if (onEnd)
                    str = str + placeholder;
                else
                    str = placeholder + str;
            }

            return str;
        }

        private async Task<List<string>> GenerateLicenseKeys(
            int companyId, 
            string companyCodeClient, 
            LicenseType licenseType, 
            int count)
        {
            var fpart = CompleteString(companyCodeClient, 'С', 5, true);
            var spart = licenseType == LicenseType.TrpUsersLicense ? "U" : "P";
            var licenseKeyPrefix = $"{fpart}-{spart}-".ToUpper();

            var lastLicenseeKey = await _userLicenseRepository.GetAll()
                .Where(x => x.ClientCompanyId == companyId && x.License.Type == licenseType && x.LicenseKey.StartsWith(licenseKeyPrefix))
                .OrderByDescending(x => x.LicenseKey)
                .Select(x => x.LicenseKey)
                .FirstOrDefaultAsync();

            var nextNumber = GetNextNumber(lastLicenseeKey);

            var keys = Enumerable.Range(nextNumber + 1, count)
                    .Select(x => $"{licenseKeyPrefix}{CompleteString(x.ToString(), '0', 6)}")
                    .ToList();
            return keys;
        }

        public Task<DataSourceResult<ClientCompanyDto>> GetDataSourceAsync(DataSourceRequest request)
        {
            var clientQuery = _clientCompanyRepository.GetAll();
            var userLicenseQuery = _userLicenseRepository.GetAll();

            return clientQuery.SelectClientCompanyDto(userLicenseQuery)
                .OrderBy(x => x.Id)
                .ToDataSourceResultAsync(request);
        }

        public async Task AddLicensesAsync(int clientId, AddLicensesVm model)
        {
            var client = await _clientCompanyRepository.GetAll()
                .Include(x => x.User)
                .FirstOrDefaultAsync(x => x.Id == clientId);

            var license = await _licenseRepository.GetAll()
                .FirstOrDefaultAsync(x => x.Type == model.LicenseType);

            var keys = await GenerateLicenseKeys(clientId,client.Abbreviation, model.LicenseType, model.Count);

            var userLicenses = Enumerable.Range(1, model.Count)
                .Select(x => new UserLicense
                {
                    AssignedById = client.UserId,
                    ExpirationDate = model.ExpirationDate,
                    LicenseId = license.Id,
                    ClientCompanyId = clientId,
                    LicenseKey = keys[x-1]
                });

            _userLicenseRepository.AddRange(userLicenses);

            await _userLicenseRepository.SaveChangesAsync();
        }

        public async Task<IEnumerable<AvailableLicenseDto>> GetManagerAvailableLicensesAsync(int userId)
        {
            var licenses = await _userLicenseRepository.GetAll()
                .Where(x => x.AssignedById == userId
                    && x.OwnerId == null
                    && x.License.Type == LicenseType.TrpManagersLicense)
                .GroupBy(x => new { x.ExpirationDate, x.License.Type })
                .ToListAsync();

            return licenses.Select(x => new AvailableLicenseDto
            {
                ExpirationDate = x.Key.ExpirationDate,
                LicenseType = x.Key.Type,
                LicenseId = x.First().Id
            });
        }

        public async Task<IEnumerable<AvailableLicenseDto>> GetUserAvailableLicensesAsync(int userId)
        {
            var licenses = await _userLicenseRepository.GetAll()
                .Where(x => x.ManagerId.HasValue 
                    && x.ManagerId == userId 
                    && x.OwnerId == null
                    && x.License.Type == LicenseType.TrpUsersLicense
                    && x.ExpirationDate > DateTime.Now)
                .GroupBy(x => new { x.ExpirationDate, x.License.Type })
                .ToListAsync();

            return licenses.Select(x => new AvailableLicenseDto
            {
                ExpirationDate = x.Key.ExpirationDate,
                LicenseType = x.Key.Type,
                LicenseId = x.First().Id
            });
        }

        public Task<DataSourceResult<ClientUserDto>> GetClientLicensesAsync(DataSourceRequest request, User user)
        {
            return _userLicenseRepository.GetAll()
                .Where(x => x.AssignedById == user.Id && x.OwnerId != null)
                .OrderBy(x => x.Id)
                .SelectClientUserDto()
                .ToDataSourceResultAsync(request);
        }

        public async Task<DataSourceResult<ClientCompanyDto>> GetClientAdminsAsync(DataSourceRequest request, User user)
        {
            var clientCompany = await _clientCompanyRepository.GetAll()
                .FirstOrDefaultAsync(x => x.UserId == user.Id);

            var licenseQuery = _userLicenseRepository.GetAll();

            return await _clientCompanyRepository.GetAll()
                .Where(x => x.AffiliatedClientId == clientCompany.Id)
                .OrderBy(x => x.Id)
                .SelectClientCompanyDto(licenseQuery)
                .ToDataSourceResultAsync(request);
        }

        public Task<DataSourceResult<ClientUserDto>> GetManagerLicensesAsync(DataSourceRequest request, User user)
        {
            return _userLicenseRepository.GetAll()
                .Where(x => x.ManagerId == user.Id && x.OwnerId != null)
                .OrderBy(x => x.Id)
                .SelectManagerUserDto()
                .ToDataSourceResultAsync(request);
        }

        public async Task CreateClientManagerAsync(ClientManagerVm model, User currentUser)
        {
            var user = new User
            {
                UserName = model.Username,
                Email = model.Email,
                LastName = model.LastName,
                FirstName = model.FirstName
            };

            using (_userRepository.BeginTransaction())
            {
                var result = await _userManager.CreateAsync(user);
                result.Validate();

                await _userManager.AddToRolesAsync(user.Id, UserRoleTypeEnum.TravelReconProManager.ToString());

                var userLicense = await _userLicenseRepository.GetAll()
                    .FirstOrDefaultAsync(x => x.Id == model.LicenseId && x.AssignedById == currentUser.Id && x.OwnerId == null);

                if (userLicense == null)
                    throw new BusinessLogicException("License not found");

                userLicense.OwnerId = user.Id; 
                userLicense.AssignedDate = DateTime.UtcNow;

                _userLicenseRepository.Modified(userLicense);
                await _userLicenseRepository.SaveChangesAsync();

                _mailService.SendActivateManagerLicenseEmail(user.Email, user.FirstName, user.UserName, userLicense.LicenseKey);

                _userRepository.CommitTransaction();
            }
        }

        public async Task EditManagerAsync(ClientManagerVm model, User currentUser)
        {
            var user = _userRepository.Get(model.UserId);

            user.FirstName = model.FirstName;
            user.LastName = model.LastName;
            user.UserName = model.Username;
            user.Email = model.Email;

            _userRepository.Modified(user);
            await _userRepository.SaveChangesAsync();
        }

        public async Task AssignLicensesToManager(int userId, int managerId, int licensesAmount)
        {
            var licenses = await _userLicenseRepository.GetAll()
                .Where(x => x.License.Type == LicenseType.TrpUsersLicense
                            && x.AssignedById == userId
                            && x.ManagerId == null
                            && !x.AssignedDate.HasValue
                            && !x.ActivatedDate.HasValue
                            && x.ExpirationDate > DateTime.Now)
                .Take(licensesAmount)
                .ToListAsync();

            if (licenses.Count < licensesAmount)
                throw new BusinessLogicException("Not enough licenses to assign");

            licenses.ForEach(x =>
            {
                x.ManagerId = managerId;
                _userLicenseRepository.Modified(x);
            });

            await _userLicenseRepository.SaveChangesAsync();
        }

        public async Task AssignLicensesToAdmin(int clienUserId, int adminId, int managerLicensesAmount, int userLicensesAmount)
        {
            var clientCompany = await _clientCompanyRepository.GetAll()
                .FirstOrDefaultAsync(x => x.UserId == clienUserId);

            var adminUser = await _clientCompanyRepository.GetAll()
                .FirstOrDefaultAsync(x => x.Id == adminId);

            var managerLicenses = await _userLicenseRepository.GetAll()
                .Where(x => x.ClientCompanyId == clientCompany.Id
                            && x.License.Type == LicenseType.TrpManagersLicense
                            && !x.AssignedDate.HasValue
                            && x.AssignedById == clienUserId
                            && x.ExpirationDate > DateTime.Now)
                .Take(managerLicensesAmount)
                .ToListAsync();

            if (managerLicenses.Count < managerLicensesAmount)
                throw  new BusinessLogicException("Not enough Manager licenses to assign");

            var userLicenses = await _userLicenseRepository.GetAll()
                .Where(x => x.ClientCompanyId == clientCompany.Id
                            && x.License.Type == LicenseType.TrpUsersLicense
                            && !x.AssignedDate.HasValue
                            && x.AssignedById == clienUserId
                            && x.ExpirationDate > DateTime.Now)
                .Take(userLicensesAmount)
                .ToListAsync();

            if (userLicenses.Count < userLicensesAmount)
                throw new BusinessLogicException("Not enough User licenses to assign");

            managerLicenses.ForEach(x =>
            {
                x.AssignedById = adminUser.UserId;
                _userLicenseRepository.Modified(x);
            });

            userLicenses.ForEach(x =>
            {
                x.AssignedById = adminUser.UserId;
                _userLicenseRepository.Modified(x);
            });

            await _userLicenseRepository.SaveChangesAsync();
        }

        public async Task RevokeLicensesFromAdmin(int clienUserId, int adminId, int managerLicensesAmount, int userLicensesAmount)
        {
            var clientCompany = await _clientCompanyRepository.GetAll()
                .FirstOrDefaultAsync(x => x.UserId == clienUserId);

            var adminUser = await _clientCompanyRepository.GetAll()
                .FirstOrDefaultAsync(x => x.Id == adminId);

            var managerLicenses = await _userLicenseRepository.GetAll()
                .Where(x => x.ClientCompanyId == clientCompany.Id
                            && x.License.Type == LicenseType.TrpManagersLicense
                            && x.AssignedById == adminUser.UserId
                            && !x.OwnerId.HasValue
                            && x.ExpirationDate > DateTime.Now)
                .Take(managerLicensesAmount)
                .ToListAsync();

            if (managerLicenses.Count < managerLicensesAmount)
                throw new BusinessLogicException("Cannot revoke manager licenses: Adminstrator doesn't have enough licenses unassigned");

            var userLicenses = await _userLicenseRepository.GetAll()
                .Where(x => x.ClientCompanyId == clientCompany.Id
                            && x.License.Type == LicenseType.TrpUsersLicense
                            && x.AssignedById == adminUser.UserId
                            && !x.OwnerId.HasValue
                            && x.ExpirationDate > DateTime.Now)
                .Take(userLicensesAmount)
                .ToListAsync();

            if (userLicenses.Count < userLicensesAmount)
                throw new BusinessLogicException("Cannot revoke user licenses: Adminstrator doesn't have enough licenses unassigned");

            managerLicenses.ForEach(x =>
            {
                x.AssignedById = clienUserId;
                _userLicenseRepository.Modified(x);
            });

            userLicenses.ForEach(x =>
            {
                x.AssignedById = clienUserId;
                _userLicenseRepository.Modified(x);
            });

            await _userLicenseRepository.SaveChangesAsync();
        }

        public async Task<UserLicenseInfoDto> GetAdminLicenseInfoAsync(User currentUser)
        {
            var license = await _userLicenseRepository.GetAll()
                .Where(x => x.OwnerId == currentUser.Id)
                .OrderByDescending(x => x.ExpirationDate)
                .FirstOrDefaultAsync();

            var assignedManagerLicenseCounter = await _userLicenseRepository
                .GetAll()
                .CountAsync(x => x.AssignedById == currentUser.Id && x.OwnerId != null && x.License.Type == LicenseType.TrpManagersLicense);

            var assingndUserLicenseCounter = await _userLicenseRepository
                .GetAll()
                .CountAsync(x => x.AssignedById == currentUser.Id && x.ManagerId != null && x.License.Type == LicenseType.TrpUsersLicense);

            var licenseCounters = await _userLicenseRepository.GetAll()
                .Where(x => x.AssignedById == currentUser.Id)
                .Select(x => x.License.Type)
                .GroupBy(x => x)
                .Select(x => new
                {
                    Type = x.Key,
                    Count = x.Count()
                })
                .ToDictionaryAsync(x => x.Type, x => x.Count);

            var activatedLicenseCounters = await _userLicenseRepository.GetAll()
                .Where(x => x.AssignedById == currentUser.Id && x.ActivatedDate.HasValue)
                .Select(x => x.License.Type)
                .GroupBy(x => x)
                .Select(x => new
                {
                    Type = x.Key,
                    Count = x.Count()
                })
                .ToDictionaryAsync(x => x.Type, x => x.Count);

            if (!currentUser.HasRole(UserRoleTypeEnum.TRPClient)
                && !currentUser.HasRole(UserRoleTypeEnum.TravelReconProAdministrator)
                && license == null)
            {
                throw new BusinessLogicException("User don't have license");
            }

            var info = new UserLicenseInfoDto
            {
                TotalManagerLicensesCount = licenseCounters.ContainsKey(LicenseType.TrpManagersLicense)
                    ? licenseCounters[LicenseType.TrpManagersLicense] : 0,
                TotalUserLicensesCount = (licenseCounters.ContainsKey(LicenseType.TrpUsersLicense)
                    ? licenseCounters[LicenseType.TrpUsersLicense] : 0),

                AssignedManagerLicensesCount = assignedManagerLicenseCounter,
                AssignedUserLicensesCount = assingndUserLicenseCounter,

                ActivatedLicensesManagerCount = activatedLicenseCounters.ContainsKey(LicenseType.TrpManagersLicense)
                    ? activatedLicenseCounters[LicenseType.TrpManagersLicense] : 0,
                ActivatedLicensesUserCount = activatedLicenseCounters.ContainsKey(LicenseType.TrpUsersLicense)
                    ? activatedLicenseCounters[LicenseType.TrpUsersLicense] : 0
            };

            info.CanAssignLicenses = info.AssignedUserLicensesCount < info.TotalUserLicensesCount;

            if (!currentUser.HasRole(UserRoleTypeEnum.TRPClient) && !currentUser.HasRole(UserRoleTypeEnum.TravelReconProAdministrator))
            {
                info.LicenseId = license.Id;
                info.ExpirationDate = license.ExpirationDate;
                info.ActivatedDate = license.ActivatedDate;
            }

            return info;
        }

        public async Task<UserLicenseInfoDto> GetClientLicenseInfoAsync(User currentUser)
        {
            var license = await _userLicenseRepository.GetAll()
                .Where(x => x.OwnerId == currentUser.Id)
                .OrderByDescending(x => x.ExpirationDate)
                .FirstOrDefaultAsync();

            var clientCompany = await _clientCompanyRepository.GetAll()
                .FirstOrDefaultAsync(x => x.UserId == currentUser.Id);

            var assignedManagerLicenseCounter = await _userLicenseRepository
                .GetAll()
                .CountAsync(x => x.AssignedById != currentUser.Id
                    && x.License.Type == LicenseType.TrpManagersLicense
                    && x.ClientCompanyId == clientCompany.Id);

            var assingndUserLicenseCounter = await _userLicenseRepository
                .GetAll()
                .CountAsync(x => x.AssignedById != currentUser.Id
                    && x.License.Type == LicenseType.TrpUsersLicense
                    && x.ClientCompanyId == clientCompany.Id);

            var licenseCounters = await _userLicenseRepository.GetAll()
                .Where(x => x.ClientCompanyId == clientCompany.Id)
                .Select(x => x.License.Type)
                .GroupBy(x => x)
                .Select(x => new
                {
                    Type = x.Key,
                    Count = x.Count()
                })
                .ToDictionaryAsync(x => x.Type, x => x.Count);

            var activatedLicenseCounters = await _userLicenseRepository.GetAll()
                .Where(x => x.ActivatedDate.HasValue && x.ClientCompanyId == clientCompany.Id)
                .Select(x => x.License.Type)
                .GroupBy(x => x)
                .Select(x => new
                {
                    Type = x.Key,
                    Count = x.Count()
                })
                .ToDictionaryAsync(x => x.Type, x => x.Count);

            if (!currentUser.HasRole(UserRoleTypeEnum.TRPClient)
                && !currentUser.HasRole(UserRoleTypeEnum.TravelReconProAdministrator)
                && license == null)
            {
                throw new BusinessLogicException("User don't have license");
            }

            var info = new UserLicenseInfoDto
            {
                TotalManagerLicensesCount = licenseCounters.ContainsKey(LicenseType.TrpManagersLicense)
                    ? licenseCounters[LicenseType.TrpManagersLicense] : 0,
                TotalUserLicensesCount = (licenseCounters.ContainsKey(LicenseType.TrpUsersLicense)
                    ? licenseCounters[LicenseType.TrpUsersLicense] : 0),

                AssignedManagerLicensesCount = assignedManagerLicenseCounter,
                AssignedUserLicensesCount = assingndUserLicenseCounter,

                ActivatedLicensesManagerCount = activatedLicenseCounters.ContainsKey(LicenseType.TrpManagersLicense)
                    ? activatedLicenseCounters[LicenseType.TrpManagersLicense] : 0,
                ActivatedLicensesUserCount = activatedLicenseCounters.ContainsKey(LicenseType.TrpUsersLicense)
                    ? activatedLicenseCounters[LicenseType.TrpUsersLicense] : 0
            };

            var adminsCount = await _clientCompanyRepository.GetAll()
                .CountAsync(x => x.AffiliatedClientId == clientCompany.Id);

            info.CanAssignLicenses = adminsCount < info.TotalManagerLicensesCount;

            if (!currentUser.HasRole(UserRoleTypeEnum.TRPClient) && !currentUser.HasRole(UserRoleTypeEnum.TravelReconProAdministrator))
            {
                info.LicenseId = license.Id;
                info.ExpirationDate = license.ExpirationDate;
                info.ActivatedDate = license.ActivatedDate;
            }

            return info;
        }

        public async Task<ManagerLicenseInfo> GetManagerLicenseInfoAsync(int userId)
        {
            var managerLicense = await _userLicenseRepository.GetAll()
                .Where(x => x.License.Type == LicenseType.TrpManagersLicense
                            && x.OwnerId.HasValue
                            && x.OwnerId == userId
                            && x.ExpirationDate > DateTime.Now)
                .OrderByDescending(x => x.ExpirationDate)
                .FirstOrDefaultAsync();

            if (managerLicense == null)
                throw new BusinessLogicException("Your licenses was not found. Please contact your administrator");

            var licenses = _userLicenseRepository.GetAll()
                .Where(x => x.ManagerId == userId && x.ExpirationDate > DateTime.UtcNow && x.License.Type == LicenseType.TrpUsersLicense);

            var licenseInfo = new ManagerLicenseInfo
            {
                ManagerId = userId,
                LicenseId = managerLicense.Id,
                LicenseNumber = managerLicense.LicenseKey,
                ActivatedDate = managerLicense.ActivatedDate,
                ExpirationDate = managerLicense.ExpirationDate,
                TotalUserLicensesCount = await licenses.CountAsync(),
                AssignedUserLicensesCount = await licenses.CountAsync(x => x.AssignedDate.HasValue),
                ActivatedUserLicensesCount = await licenses.CountAsync(x => x.ActivatedDate.HasValue),
            };

            licenseInfo.CanCreateUsers = licenseInfo.TotalUserLicensesCount > licenseInfo.AssignedUserLicensesCount;

            return licenseInfo;
        }

        public Task<List<ClientUserDto>> GetManagersAsync(int userId)
        {
            return _userLicenseRepository.GetAll()
                .Where(x => x.AssignedById == userId 
                    && x.OwnerId != null 
                    && x.License.Type == LicenseType.TrpManagersLicense)
                .OrderBy(x => x.Id)
                .SelectClientUserDto()
                .ToListAsync();
        }

        public Task<ClientManagerVm> GetManagerInfoAsync(int userId)
        {
            var licenses = _userLicenseRepository.GetAll();

            return _userRepository.GetAll()
                .Where(x => x.Id == userId)
                .SelectManagerVm(licenses)
                .FirstOrDefaultAsync();
        }

        public async Task ActivateLicenseAsync(ActivateLicenseVm model)
        {
            if (string.IsNullOrWhiteSpace(model.LicenseKey))
                throw new BusinessLogicException("Invalid License Key");

            using (_userRepository.BeginTransaction())
            {
                var userLicense = await _userLicenseRepository.GetAll()
                    .Where(x => x.LicenseKey == model.LicenseKey && x.OwnerId != null)
                    .FirstOrDefaultAsync();

                if (userLicense == null)
                    throw new BusinessLogicException("Invalid License Key");

                if (userLicense.ActivatedDate.HasValue)
                    throw new BusinessLogicException("License Already Activated");

                userLicense.ActivatedDate = DateTime.UtcNow;
                _userLicenseRepository.Modified(userLicense);

                var userId = userLicense.OwnerId ?? 0;

                var resetPasswordToken = await _userManager.GeneratePasswordResetTokenAsync(userId);
                var resetPasswordResult = await _userManager.ResetPasswordAsync(userId, resetPasswordToken, model.Password);
                resetPasswordResult.Validate();

                _userRepository.CommitTransaction();
            }
        }

        public async Task ActivateUserAsync(ActivateUserVm model)
        {
            if (string.IsNullOrWhiteSpace(model.Username))
                throw new BusinessLogicException("Invalid user name");

            if (string.IsNullOrWhiteSpace(model.Password))
                throw new BusinessLogicException("Invalid password");

            using (_userRepository.BeginTransaction())
            {
                var user = await _userRepository.GetAll()
                    .FirstOrDefaultAsync(x => x.UserName == model.Username);

                if (user == null)
                    throw new BusinessLogicException("Cannot find user with provided username");

                if (!string.IsNullOrWhiteSpace(user.PasswordHash))
                    throw new BusinessLogicException("User has already been activated");

                var resetPasswordToken = await _userManager.GeneratePasswordResetTokenAsync(user.Id);
                var resetPasswordResult = await _userManager.ResetPasswordAsync(user.Id, resetPasswordToken, model.Password);
                resetPasswordResult.Validate();

                _userRepository.CommitTransaction();
            }
        }

        public async Task RevokeLicenseAsync(int userLicenseId, int userId)
        {
            var userLicense = await _userLicenseRepository.GetAll()
                 .Where(x => x.AssignedById == userId && x.Id == userLicenseId)
                 .FirstOrDefaultAsync();

            userLicense.OwnerId = null;
            userLicense.AssignedDate = null;
            userLicense.ActivatedDate = null;

            await _userLicenseRepository.SaveChangesAsync();
        }

        public async Task CreateClientUserAsync(ClientUserVm model, User currentUser)
        {
            var user = new User
            {
                UserName = model.Username,
                Email = model.Email,
                LastName = model.LastName,
                FirstName = model.FirstName
            };

            using (_userRepository.BeginTransaction())
            {
                var result = await _userManager.CreateAsync(user);
                result.Validate();

                //insert details
                var userDetails = new UserDetails
                {
                    Allergies = model.Allergies,
                    Blood = model.Blood,
                    CountryOfPassport = model.CountryOfPassport,
                    Dependent = model.Dependent,
                    Disabilities = model.Disabilities,
                    EmergencyContactInfo = model.EmergencyContactInfo,
                    Employee = model.Employee,
                    EmployeeNumber = model.EmployeeNumber,
                    GovtPassport = model.GovtPassport,
                    PhoneNumber = model.PhoneNumber,
                    Passport = model.Passport,
                    PersonalPassport = model.PersonalPassport,
                    PassportExpiration = model.PassportExpiration,
                    PassportNumber = model.PassportNumber,
                    UserId = user.Id
                };

                _userDetailsRepository.Add(userDetails);
                await _userRepository.SaveChangesAsync();

                await _userManager.AddToRolesAsync(user.Id, UserRoleTypeEnum.TravelReconPro.ToString());

                var userLicense = await _userLicenseRepository.GetAll()
                    .FirstOrDefaultAsync(x => x.Id == model.LicenseId && x.ManagerId == currentUser.Id && x.OwnerId == null);

                if (userLicense == null)
                    throw new BusinessLogicException("License not found");

                userLicense.OwnerId = user.Id;
                userLicense.AssignedDate = DateTime.UtcNow;

                _userLicenseRepository.Modified(userLicense);
                await _userLicenseRepository.SaveChangesAsync();

                _mailService.SendActivateUserLicenseEmail(user.Email, user.FirstName, user.UserName, userLicense.LicenseKey);

                _userRepository.CommitTransaction();
            }
        }

        public async Task EditClientUserAsync(ClientUserVm model)
        {
            var user = _userRepository.Get(model.UserId);

            user.UserName = model.Username;
            user.Email = model.Email;
            user.LastName = model.LastName;
            user.FirstName = model.FirstName;

            _userRepository.Modified(user);
            await _userRepository.SaveChangesAsync();

            var userDetails = await _userDetailsRepository.GetAll()
                .FirstOrDefaultAsync(x => x.UserId == model.UserId);

            userDetails.Allergies = model.Allergies;
            userDetails.Blood = model.Blood;
            userDetails.CountryOfPassport = model.CountryOfPassport;
            userDetails.Dependent = model.Dependent;
            userDetails.Disabilities = model.Disabilities;
            userDetails.EmergencyContactInfo = model.EmergencyContactInfo;
            userDetails.Employee = model.Employee;
            userDetails.EmployeeNumber = model.EmployeeNumber;
            userDetails.GovtPassport = model.GovtPassport;
            userDetails.PhoneNumber = model.PhoneNumber;
            userDetails.Passport = model.Passport;
            userDetails.PersonalPassport = model.PersonalPassport;
            userDetails.PassportExpiration = model.PassportExpiration;
            userDetails.PassportNumber = model.PassportNumber;

            _userDetailsRepository.Modified(userDetails);
            await _userRepository.SaveChangesAsync();
        }

        public Task<DataSourceResult<GroupDto>> GetManagedGroupsDataSourceAsync(DataSourceRequest request, int userId)
        {
            return _groupRepository.GetAll()
                .Where(x => x.UserGroupRelations.Any(ugr => ugr.UserId == userId && ugr.UserGroupRole == UserGroupRole.Administrator))
                .SelectGroupDto()
                .ToDataSourceResultAsync(request);
        }

        public Task<DataSourceResult<UserListItemDto>> GetManagedUsersDataSourceAsync(DataSourceRequest request, int userId)
        {
            var userLicenseQuery = _userLicenseRepository.GetAll();

            return _userRepository.GetAll()
                .Where(x => userLicenseQuery.Any(ul => ul.OwnerId == x.Id && ul.ManagerId == userId))
                .SelectUserListItemDto()
                .ToDataSourceResultAsync(request);
        }

        public Task<List<GroupDto>> GetManagedGroupsListAsync(int userId)
        {
            return _groupRepository.GetAll()
                .Where(x => x.UserGroupRelations.Any(ugr => ugr.UserId == userId && ugr.UserGroupRole == UserGroupRole.Administrator))
                .SelectGroupDto()
                .ToListAsync();
        }

        public async Task<ManagedUsersInfoDto> GetManagedUserInfoAsync(User currentUser)
        {
            var managersAdminId = await _userLicenseRepository.GetAll()
                .Where(x => x.OwnerId.HasValue && x.OwnerId == currentUser.Id)
                .Select(x => x.AssignedById)
                .FirstOrDefaultAsync();

            var totalClientUsers = await _userLicenseRepository.GetAll()
                .CountAsync(x => x.AssignedById == managersAdminId 
                    && x.License.Type == LicenseType.TrpUsersLicense
                    && x.ActivatedDate.HasValue
                    && x.ExpirationDate > DateTime.Now);

            var managedUsers = await _userLicenseRepository.GetAll()
                .CountAsync(
                    x => x.ManagerId == currentUser.Id 
                    && x.License.Type == LicenseType.TrpUsersLicense
                    && x.ActivatedDate.HasValue
                    && x.AssignedDate.HasValue 
                    && x.ExpirationDate > DateTime.Now);

            return new ManagedUsersInfoDto
            {
                ClientsTotalUsers = totalClientUsers,
                ManagedUsersNumber = managedUsers
            };
        }
    }
}
