﻿using System;
using TravelRecon.Domain.License;

namespace TravelRecon.AppServices.ClientCompany
{
    public class AvailableLicenseDto
    {
        public int LicenseId { get; set; }
        public LicenseType LicenseType { get; set; }
        public DateTime ExpirationDate { get; set; }
    }
}