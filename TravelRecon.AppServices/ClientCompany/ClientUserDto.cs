﻿using System;

namespace TravelRecon.AppServices.ClientCompany
{
    public class ClientUserDto
    {
        public int Id { get; set; }
        public int LicenseId { get; set; }
        public int LicenseType { get; set; }
        public string FullName { get; set; }
        public string AvatarUrl { get; set; }
        public string LicenseNumber { get; set; }

        public DateTime? AssignedDate { get; set; }
        public DateTime ExpirationDate { get; set; }
        public DateTime? ActivatedDate { get; set; }
        public bool CanRevokeLicense { get; set; }
        public int? AssignedLicenses { get; set; }
        public int? ActivatedLicenses { get; set; }
        public int? TotalLicenses { get; set; }
    }
}