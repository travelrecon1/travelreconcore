﻿using TravelRecon.AppServices.Modals.ViewDtos;
using TravelRecon.AppServices.SharedDtos;
using TravelRecon.Domain.Identity;
using TravelRecon.Domain.Localization;
using TravelRecon.Domain.Localization.QueryObjects;
using TravelRecon.Domain.RepositoryInterfaces;

namespace TravelRecon.AppServices.Modals
{
    public class ModalService: IAppService
    {
        private readonly IRepository<Label> _labelRepository;

        public ModalService(IRepository<Label> labelRepository)
        {
            _labelRepository = labelRepository;
        }

        public LocateModalViewModel GetLocateModalViewModel(LanguageType languageType)
        {
            var viewModel = new LocateModalViewModel();

            var labelsToRetrieve = new[]
            {
                LabelType.UnableGetCurrentLocation, LabelType.OpenLocateSettings
            };
            viewModel.Labels = _labelRepository.GetAll().FindLabelsLocalized(labelsToRetrieve, languageType);

            return viewModel;
        }

        public ConnectionModalViewModel GetConnectionModalViewModel(LanguageType languageType)
        {
            var viewModel = new ConnectionModalViewModel();

            var labelsToRetrieve = new[]
            {
                LabelType.WifiConnectionIsTurnedOff,
                LabelType.Close
            };
            viewModel.Labels = _labelRepository.GetAll().FindLabelsLocalized(labelsToRetrieve, languageType);

            return viewModel;
        }

        public TutorialModalViewModal GetTutorialModalViewModel(User currentUser)
        {
            var targetLang = currentUser.GetLanguageSetting();

            var viewModel = new TutorialModalViewModal();

            var labelsToRetrieve = new[]
            {
                LabelType.Back,
                LabelType.Next,
                LabelType.Done,
				LabelType.PerLocation,
				LabelType.ForAllLocations
            };
            viewModel.Labels = _labelRepository.GetAll().FindLabelsLocalizedDictionary(labelsToRetrieve, targetLang);

            return viewModel;
        }
    }
}
