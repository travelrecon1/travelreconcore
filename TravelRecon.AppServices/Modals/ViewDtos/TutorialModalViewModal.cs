﻿using System.Collections.Generic;

namespace TravelRecon.AppServices.Modals.ViewDtos
{
    public class TutorialModalViewModal
    {
        public Dictionary<int, string> Labels { get; set; }
    }
}
