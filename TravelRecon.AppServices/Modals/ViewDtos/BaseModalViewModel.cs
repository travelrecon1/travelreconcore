﻿using System.Collections.Generic;
using TravelRecon.Domain.Localization.QueryObjects;

namespace TravelRecon.AppServices.Modals.ViewDtos
{
    public class BaseModalViewModel
    {
        public IEnumerable<LabelLocalizedDto> Labels { get; set; }
    }
}
