﻿using System;
using System.ComponentModel;
using TravelRecon.Domain.Identity;
using TravelRecon.Domain.Missions;
using TravelRecon.Domain.Points;
using TravelRecon.Domain.RepositoryInterfaces;

namespace TravelRecon.AppServices.Points
{
    public class PointService : IAppService
    {
        private readonly IRepository<Point> _pointRepository;
        private readonly IRepository<Mission> _missionRepository;
 
        public PointService(IRepository<Point> pointRepository, IRepository<Mission> missionRepository)
        {
            if (pointRepository == null) throw new ArgumentNullException(nameof(pointRepository));
            _pointRepository = pointRepository;
            _missionRepository = missionRepository;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <param name="user"></param>
        /// <returns>Count of added points</returns>
        public short Create(PointType type, User user)
        {
            var point = new Point
            {
                Type = type,
                Value = GetValues(type),
                OwnerId = user.Id
            };

            _pointRepository.Add(point);
            _pointRepository.SaveChanges();

            return point.Value;
        }

        public short Create(PointType type, int id)
        {
            var point = new Point
            {
                Type = type,
                Value = GetValues(type),
                OwnerId = id
            };

            _pointRepository.Add(point);
            _pointRepository.SaveChanges();

            return point.Value;
        }

        private short GetValues(PointType type)
        {
            switch (type)
            {
                 case PointType.Referral:
                    return (short) (_missionRepository.Get((int) PointType.Referral)).Score;

                case PointType.InfoScoutReport:
                    return (short)(_missionRepository.Get((int)PointType.InfoScoutReport)).Score;

                case PointType.CriminalScoutReport:
                    return (short)(_missionRepository.Get((int)PointType.CriminalScoutReport)).Score;

                case PointType.Newcomer:
                    return 5;

                default:
                    throw new InvalidEnumArgumentException(type.ToString());
            }
        }
    }
}