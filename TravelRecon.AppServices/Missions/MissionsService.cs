﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TravelRecon.Domain.Localization;
using TravelRecon.Domain.Localization.QueryObjects;
using TravelRecon.Domain.Missions;
using TravelRecon.Domain.RepositoryInterfaces;

namespace TravelRecon.AppServices.Missions
{
    public class MissionsService: IAppService
    {
        private readonly IRepository<Mission> _missionRepository;
        private readonly IRepository<Label> _labelRepository;

        public MissionsService(IRepository<Mission> missionRepository, IRepository<Label> labelRepository)
        {
            _missionRepository = missionRepository;
            _labelRepository = labelRepository;
        }

        public Task<List<MissionDto>> GetAll(LanguageType type = LanguageType.English)
        {
            return _missionRepository.GetAll()
                .SelectDto()
                .ToListAsync();
        }
    }
}
