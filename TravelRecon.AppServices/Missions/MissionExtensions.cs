﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using TravelRecon.Domain.Missions;

namespace TravelRecon.AppServices.Missions
{
    public static class MissionExtensions
    {
        public static MissionDto ToMissionDto(this Mission mission)
        {
            return new MissionDto()
            {
                Id = mission.Id,
                LabelsFk = mission.LabelsFk,
                MissionDescription = mission.MissionDescription,
                Score = mission.Score
            };
        }

        public static IQueryable<MissionDto> SelectDto(this IQueryable<Mission> queryable)
        {
            return queryable.Select(x => new MissionDto
            {
                Id = x.Id,
                LabelsFk = x.LabelsFk,
                MissionDescription = x.MissionDescription,
                Score = x.Score
            });
        }
    }
}
