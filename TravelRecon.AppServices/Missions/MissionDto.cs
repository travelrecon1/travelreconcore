﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TravelRecon.AppServices.Missions
{
    public class MissionDto
    {
        public int Id { get; set; }
        public string MissionDescription { get; set; }
        public int Score { get; set; }
        public int LabelsFk { get; set; }
    }
}
