﻿using System.Collections.Generic;
using TravelRecon.AppServices.IntelReports.ViewDtos;
using TravelRecon.Domain.Ecommerce;
using TravelRecon.Domain.SubscriptionOffers;

namespace TravelRecon.AppServices.Ecommerce.Dtos
{
    public class StripePurchaseDto
    {
        public string StripeToken { get; set; }
        public PaymentTypes PaymentType { get; set; }
        public SubscriptionTypes SubscriptionType { get; set; }
        public List<int> DestinationIds { get; set; }
        public string CouponId { get; set; }
    }
}
