﻿using System.ComponentModel.DataAnnotations;

namespace TravelRecon.AppServices.Ecommerce.Dtos
{
    public class StripeEventObjectViewModel
    {
        [Required]
        public string Id { get; set; }

        [Required]
        public bool LiveMode { get; set; }
    }

    public class StripeEventDataViewModel<T> where T : StripeEventObjectViewModel
    {
        public T Object { get; set; }
    }

    public class StripeEventViewModel<T> where T : StripeEventObjectViewModel
    {
        [Required]
        public string Id { get; set; }
        [Required]
        public string Type { get; set; }

        [Required]
        public StripeEventDataViewModel<T> Data { get; set; }
    }
}
