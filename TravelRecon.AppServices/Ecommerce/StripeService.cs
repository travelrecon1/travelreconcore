﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using Stripe;
using TravelRecon.AppServices.Ecommerce.Dtos;
using TravelRecon.AppServices.IntelReports;
using TravelRecon.AppServices.Interfaces;
using TravelRecon.Data;
using TravelRecon.Domain;
using TravelRecon.Domain.Ecommerce;
using TravelRecon.Domain.Identity;
using TravelRecon.Domain.IntelReports;
using TravelRecon.Domain.RepositoryInterfaces;
using TravelRecon.Domain.SubscriptionOffers;

namespace TravelRecon.AppServices.Ecommerce
{
    public class StripeService : IAppService
    {
        private readonly TravelReconDbContext _dbContext;
        private readonly IRepository<Destination> _destinationsRepository;
        private readonly IRepository<UserDestinations> _userDestinationsRepository;
        private readonly IRepository<StripePayment> _paymentsRepository;
        private readonly IRepository<UserSubscription> _userSubscriptionsRepository;
        private readonly IRepository<SubscriptionOffer> _offerRepository;

        private readonly StripeChargeService _chargeService;

        public bool IsStripeLiveMode => ConfigSettings.StripeIsLiveMode;

        public string StripeKey => IsStripeLiveMode ? ConfigSettings.StripeApiKeyLive : ConfigSettings.StripeApiKeyTest;

        public StripeService(TravelReconDbContext dbContext,
            IRepository<Destination> destinationsRepository,
            IRepository<UserDestinations> userDestinationsRepository,
            IRepository<StripePayment> paymentsRepository,
            IRepository<UserSubscription> userSubscriptionsRepository,
            IRepository<SubscriptionOffer> offerRepository)
        {
            _dbContext = dbContext;
            _destinationsRepository = destinationsRepository;
            _userDestinationsRepository = userDestinationsRepository;
            _paymentsRepository = paymentsRepository;
            _userSubscriptionsRepository = userSubscriptionsRepository;
            _offerRepository = offerRepository;

            _chargeService = new StripeChargeService { ApiKey = StripeKey };
        }

        public void MakeCharge(User currentUser, StripePurchaseDto purchase)
        {
            var subscription = GetUserSubscription(currentUser, purchase.SubscriptionType);

            decimal totalPriceDecimal;
            int totalPrice;
            var paymentDescription = "Subscription purchase. Username: " + currentUser.UserName + "; Subscription type: ";

            if (purchase.SubscriptionType == SubscriptionTypes.Annual)
            {
                var annualOffer = _offerRepository.GetAll()
                    .FirstOrDefault(x => x.SubscriptionType.HasValue
                                         && x.SubscriptionType.Value == SubscriptionTypes.Annual
                                         && x.Price.HasValue);

                totalPriceDecimal = annualOffer?.Price ?? 129;

                totalPrice = CountPaymentAmount(totalPriceDecimal);
                paymentDescription += "Annual";
            }
            else
            {
                var destinations = _destinationsRepository.GetAll()
                    .Where(x => purchase.DestinationIds.Contains(x.Id))
                    .ToList();

                totalPriceDecimal = destinations.Select(x => x.Cost).ToList().Sum();
                totalPrice = CountPaymentAmount(totalPriceDecimal);
                paymentDescription += "Monthly";
            }

            var stripeCharge = new StripeChargeCreateOptions
            {
                Amount = totalPrice,
                Currency = "usd",
                Description = paymentDescription,
                SourceTokenOrExistingSourceId = purchase.StripeToken,
                Capture = true,
                ReceiptEmail = currentUser.Email
            };

            try
            {
                var charge = _chargeService.Create(stripeCharge);

                // TODO make stripe payments activated without webhooks
                // if (charge.Paid && !string.IsNullOrWhiteSpace(charge.FailureCode))

                var payment = new StripePayment
                {
                    UserFk = currentUser.Id,
                    AmountPaid = totalPriceDecimal,
                    Date = DateTime.UtcNow,
                    PaymentType = purchase.PaymentType,
                    IsPaid = false,
                    IsTestPayment = !IsStripeLiveMode,
                    PaymentId = charge.Id,
                    ReceiptEmail = stripeCharge.ReceiptEmail,
                    Status = "Charge request sent"
                };

                var newPayment = _paymentsRepository.Add(payment);

                subscription.PaymentFk = newPayment.Id;

                if (subscription.Id == 0)
                    _userSubscriptionsRepository.Add(subscription);
                else
                    _userSubscriptionsRepository.Modified(subscription);

                // if it was monthly subscription we need to update the ExpirationDate and PaymentFk for existing user destinations and
                // create new user destination if needed 
                if (purchase.SubscriptionType == SubscriptionTypes.Monthly)
                {
                    var existingDestinations = _userDestinationsRepository.GetAll()
                            .Where(x => x.UserFk == currentUser.Id
                                && x.ExpirationDate.HasValue
                                && x.DestinationId.HasValue
                                && purchase.DestinationIds.Contains(x.DestinationId.Value))
                            .ToList();

                    if (existingDestinations.Any())
                    {
                        foreach (var existingDestination in existingDestinations)
                        {
                            existingDestination.ExpirationDate = existingDestination.ExpirationDate.Value.AddMonths(1);
                            existingDestination.SubscriptionFk = subscription.Id;
                        }
                    }

                    var existingDestinationIds = existingDestinations.Select(x => x.DestinationId.Value).ToList();

                    // get destinations that are not in the UserDestinations table
                    var newDestinations = _destinationsRepository.GetAll()
                        .Where(x => purchase.DestinationIds.Contains(x.Id) && !existingDestinationIds.Contains(x.Id))
                        .SelectLocalized()
                        .ToList();

                    if (newDestinations.Any())
                    {
                        var userDestinations = newDestinations.Select(UserDestinations.ConvertFrom).Select(x =>
                        {
                            x.UserFk = currentUser.Id;
                            x.IsAvailable = true;
                            x.ReceiveNotifications = true;
                            x.SubscriptionFk = subscription.Id;
                            return x;
                        }).ToList();

                        _userDestinationsRepository.AddRange(userDestinations);
                    }
                }

                _dbContext.SaveChanges();
            }
            catch (StripeException ex)
            {
                // TODO process stripe exception
            }
        }

        public void HandleChargeSuccessEvent(StripeEventViewModel<StripeEventObjectViewModel> eventData)
        {
            var stripeCharge = GetStripeCharge(eventData.Data.Object.Id);

            if (stripeCharge == null)
                return;

            var payment = _paymentsRepository.GetAll().FirstOrDefault(x => x.PaymentId == stripeCharge.Id);

            if (payment == null)
                return;

            var subscription = _userSubscriptionsRepository.GetAll().FirstOrDefault(x => x.PaymentFk == payment.Id);

            if (subscription == null)
                return;

            payment.IsPaid = stripeCharge.Paid;
            payment.Status = stripeCharge.Status;

            subscription.IsActivated = payment.IsPaid;

            if (subscription.SubscriptionType == SubscriptionTypes.Monthly)
            {
                var userDestinations = _userDestinationsRepository.GetAll()
                    .Where(x => x.SubscriptionFk == subscription.Id)
                    .ToList();

                foreach (var userDestination in userDestinations)
                {
                    userDestination.ExpirationDate = userDestination.ExpirationDate.HasValue
                        ? userDestination.ExpirationDate.Value.AddMonths(1)
                        : DateTime.UtcNow.AddMonths(1);
                }
            }

            _dbContext.SaveChanges();
        }

        private UserSubscription GetUserSubscription(User currentUser, SubscriptionTypes subscriptionType)
        {
            if (subscriptionType == SubscriptionTypes.Annual)
            {
                var existingSubscriptions = _userSubscriptionsRepository.GetAll()
                    .Where(x => x.UserFk == currentUser.Id
                    && x.IsActivated
                    && x.SubscriptionType == SubscriptionTypes.Annual
                    && x.ExpirationDate.HasValue
                    && x.ExpirationDate >= DateTime.UtcNow)
                    .ToList();

                // if user has active annual subscription we should update it
                if (existingSubscriptions.Any())
                {
                    var latestSubscription = existingSubscriptions.OrderBy(x => x.ExpirationDate).Last();
                    latestSubscription.ExpirationDate = latestSubscription.ExpirationDate.Value.AddYears(1);
                    return latestSubscription;
                }

                // otherwise create a new subscription
                return new UserSubscription
                {
                    UserFk = currentUser.Id,
                    IsActivated = false,
                    SubscriptionType = SubscriptionTypes.Annual,
                    PurchaseDate = DateTime.UtcNow,
                    ExpirationDate = DateTime.UtcNow.AddYears(1)
                };
            }

            return new UserSubscription
            {
                UserFk = currentUser.Id,
                IsActivated = false,
                SubscriptionType = SubscriptionTypes.Monthly,
                PurchaseDate = DateTime.UtcNow
            };
        }

        private StripeCharge GetStripeCharge(string chargeId)
        {
            try
            {
                return _chargeService.Get(chargeId);
            }
            catch (StripeException ex)
            {
                if (ex.HttpStatusCode == HttpStatusCode.NotFound)
                    return null;
                throw;
            }
        }

        private int CountPaymentAmount(decimal priceUsd)
        {
            return decimal.ToInt32(priceUsd * 100);
        }
    }
}
