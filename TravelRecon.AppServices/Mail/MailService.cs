﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using SendGrid;
using SendGrid.Helpers.Mail;
using TravelRecon.AppServices.Extensions;
using TravelRecon.AppServices.Users;
using TravelRecon.Domain.Groups;
using TravelRecon.Domain.Identity;
using TravelRecon.Domain.Localization;
using TravelRecon.Domain.RepositoryInterfaces;


namespace TravelRecon.AppServices.Mail
{
    public class MailService : IAppService
    {
        private readonly AppSettingsService _appSettingsService;
        private readonly IRepository<UserEmailInvite> _userEmailInvite;

        public MailService(AppSettingsService appSettingsService, IRepository<UserEmailInvite> userEmailInvite)
        {
            if (appSettingsService == null) throw new ArgumentNullException(nameof(appSettingsService));
            _appSettingsService = appSettingsService;
            _userEmailInvite = userEmailInvite;
        }

        #region SendFriendInvite

        public bool SendFriendInvite(User user, string recipient, string message, LanguageType language, int? groupcode = null)
        {
            var isAlreadyInvited = GetIfUserIsAlreadyInvited(user.Id, recipient);

            SendFriendInvite(new List<string> { recipient }, message, language);

            if (!isAlreadyInvited)
                SaveUserEmailInvite(user, recipient, groupcode);

            return isAlreadyInvited;
        }

        private void SaveUserEmailInvite(User user, string recipient, int? userGroup = null)
        {
            _userEmailInvite.Add(new UserEmailInvite
            {
                Email = recipient,
                User = user,
                InvitedUserGroup = userGroup
            });
            _userEmailInvite.SaveChanges();
        }

        private bool GetIfUserIsAlreadyInvited(int userId, string email)
        {
            var getUserInvite = _userEmailInvite.GetAll().Where(x => x.User.Id == userId && x.Email.ToLower() == email.ToLower());
            if (getUserInvite.Any())
                return true;
            return false;
        }

        public UserInviteDto GetUserEmailInvite(string email)
        {
            var userInvite =
                _userEmailInvite.GetAll().Where(x => x.Email.ToLower() == email.ToLower()).ToList().FirstOrDefault();
            var inviteDto = userInvite?.ConvertToUserInviteDto();
            return inviteDto;
        }

        public void SendFriendInvite(List<string> recipients, string message, LanguageType language)
        {
            if (recipients == null) throw new ArgumentNullException(nameof(recipients));
            if (recipients.Count == 0)
                throw new ArgumentException("Value cannot be an empty collection.", nameof(recipients));

            var template = string.Empty;
            switch (language)
            {
                case LanguageType.English:
                    template = _appSettingsService.SendGridFriendInviteEmailTemplateEnglish;
                    break;

                case LanguageType.Spanish:
                    template = _appSettingsService.SendGridFriendInviteEmailTemplateSpanish;
                    break;

                case LanguageType.Portuguese:
                    template = _appSettingsService.SendGridFriendInviteEmailTemplatePortuguese;
                    break;
            }

            var mail = new SendGrid.Helpers.Mail.Mail
            {
                TemplateId = template,
                From = new Email(_appSettingsService.SendGridSendEmailFrom),
                Personalization = new List<Personalization>(recipients.Count)
            };

            mail.AddContent(new Content("text/html", ConvertNewlinesToHtml(message)));

            foreach (var rec in recipients)
            {
                mail.Personalization.Add(new Personalization()
                {
                    Tos = new List<Email>() { new Email(rec) }
                });
            }

            Send(mail);
        }

        public void SendUsernameRecovery(string recipient, string message, LanguageType language)
        {
            if (string.IsNullOrWhiteSpace(recipient)) throw new ArgumentNullException(nameof(recipient));

            var template = string.Empty;
            switch (language)
            {
                case LanguageType.English:
                    template = _appSettingsService.SendGridUsernameRecoveryEmailTemplate;
                    break;

                default:
                    template = _appSettingsService.SendGridUsernameRecoveryEmailTemplate;
                    break;
            }

            var mail = new SendGrid.Helpers.Mail.Mail
            {
                TemplateId = template,
                From = new Email(_appSettingsService.SendGridSendEmailFrom),
                Personalization = new List<Personalization>
                {
                    new Personalization()
                    {
                        Tos = new List<Email>() { new Email(recipient) }
                    }
                }
            };

            mail.AddContent(new Content("text/html", ConvertNewlinesToHtml(message)));

            Send(mail);
        }

        public void SendResetPassword(string recipient, string message, LanguageType language)
        {
            if (string.IsNullOrWhiteSpace(recipient)) throw new ArgumentNullException(nameof(recipient));

            var template = string.Empty;
            switch (language)
            {
                case LanguageType.English:
                    template = _appSettingsService.SendGridResetPasswordEmailTemplate;
                    break;

                default:
                    template = _appSettingsService.SendGridResetPasswordEmailTemplate;
                    break;
            }

            var mail = new SendGrid.Helpers.Mail.Mail
            {
                TemplateId = template,
                From = new Email(_appSettingsService.SendGridSendEmailFrom),
                Personalization = new List<Personalization>
                {
                    new Personalization()
                    {
                        Tos = new List<Email>() { new Email(recipient) }
                    }
                }
            };

            mail.AddContent(new Content("text/html", ConvertNewlinesToHtml(message)));

            Send(mail);
        }

        public void SendConfirmationEmail(string recipient, string message, LanguageType language)
        {
            if (string.IsNullOrWhiteSpace(recipient)) throw new ArgumentNullException(nameof(recipient));

            var template = string.Empty;
            switch (language)
            {
                case LanguageType.English:
                    template = _appSettingsService.SendGridEmailVerificationTemplate;
                    break;

                default:
                    template = _appSettingsService.SendGridEmailVerificationTemplate;
                    break;
            }

            var mail = new SendGrid.Helpers.Mail.Mail
            {
                TemplateId = template,
                From = new Email(_appSettingsService.SendGridSendEmailFrom),
                Personalization = new List<Personalization>
                {
                    new Personalization()
                    {
                        Tos = new List<Email>() { new Email(recipient) }
                    }
                }
            };

            mail.AddContent(new Content("text/html", ConvertNewlinesToHtml(message)));

            Send(mail);
        }

        public void SendContactUsEmail(string name, string email, string message)
        {
            var msg = $@"
                Request from Contact Us form.
                Name: {name},
                Email: {email},
                Message: {message}
            ";

            //todo use template
            var template = _appSettingsService.SendGridEmailContactUsTemplate;

            var mail = new SendGrid.Helpers.Mail.Mail
            {
                //TemplateId = template, //todo uncomment
                From = new Email(email),
                Personalization = new List<Personalization>
                {
                    new Personalization()
                    {
                        Tos = new List<Email>()
                        {
                            new Email(_appSettingsService.SupportEmailAddress)
                        }
                    }
                },
                Subject = "Request from Contact Us form." //todo remove it
            };

            mail.AddContent(new Content("text/html", ConvertNewlinesToHtml(msg)));

            Send(mail);
        }

        public void SendActivateUserLicenseEmail(string recipient, string firstName, string userName, string licenseKey)
        {
            var link = $"{_appSettingsService.WebSiteUrl}/activate-license/{Uri.EscapeDataString(licenseKey)}";

            var template = @"{0},

Welcome to Travel Recon and we appreciate your business.

Below you will find the login information for your newly created Account:

User Name:  {1}
License Number: {2}

To activate your account use the link below:
<a href=""{3}"">Link</a>

Once you have logged in your account will be activated.

Feel free to email us at support@travelrecon.com or call us at (855) 363-6362.

We look forward to working with and providing you the best Enterprise Risk Managment solution available today.
";

            var message = string.Format(template, firstName, userName, licenseKey, link);

            var mail = new SendGrid.Helpers.Mail.Mail
            {
                //TemplateId = template, //todo uncomment
                From = new Email(_appSettingsService.SendGridSendEmailFrom),
                Personalization = new List<Personalization>
                {
                    new Personalization()
                    {
                        Tos = new List<Email>()
                        {
                            new Email(recipient)
                        }
                    }
                },
                Subject = "Travel Recon Login Information"
            };

            mail.AddContent(new Content("text/html", ConvertNewlinesToHtml(message)));

            Send(mail);
        }

        public void SendActivateManagerLicenseEmail(string recipient, string firstName, string userName, string licenseKey)
        {
            var link = $"{_appSettingsService.WebSiteUrl}/activate-license/{Uri.EscapeDataString(licenseKey)}";

            var template = @"{0},

Welcome to Travel Recon and we appreciate your business.

Below you will find the login information for your newly created Manager Account:

User Name:  {1}
License Number: {2}

To activate your account use the link below:
<a href=""{3}"">Link</a>

Once you have logged in choose ""Dashboards"" and ""My Dashboard"" from the drop down menu.  From this screen you will have the ability to add Users and assign them licenses.  Below you will find the number of licenses that you have available to you.

You will must assign one(1) User License to each User that you add and will be subtracted from the amount of User licenses you have available to you.

Feel free to email us at support@travelrecon.com or call us at (855) 363 - 6362.

We look forward to working with and providing you the best Enterprise Risk Managment solution available today.
";

            var message = string.Format(template, firstName, userName, licenseKey, link);

            var mail = new SendGrid.Helpers.Mail.Mail
            {
                //TemplateId = template, //todo uncomment
                From = new Email(_appSettingsService.SendGridSendEmailFrom),
                Personalization = new List<Personalization>
                {
                    new Personalization()
                    {
                        Tos = new List<Email>()
                        {
                            new Email(recipient)
                        }
                    }
                },
                Subject = "Travel Recon Login Information"
            };

            mail.AddContent(new Content("text/html", ConvertNewlinesToHtml(message)));

            Send(mail);
        }

        public void SendActivateAdministratorEmail(string recipient, string firstName, string userName, string assignerName, string assignerEmail)
        {
            var link = $"{_appSettingsService.WebSiteUrl}/activate-admin/{Uri.EscapeDataString(userName)}";

            var template = @"{0},

Welcome to Travel Recon and we appreciate your business.

Below you will find the login information for your newly created Administrator Account:

User Name:  {1}

To activate your account use the link below:
<a href=""{2}"">Link</a>

Once you have logged in choose ""Dashboards"" and ""My Dashboard"" from the drop down menu.  From this screen you will have the ability to add Managers and assign them licenses. 

You will must assign one (1) Pro License to each Manager that you add.  You may add any number of User licenses to each Manager you choose from the amount of User licenses you have available to you.

Your assigning contact information is {3}, {4}.  This should be your first point of contact for information about your account.


Feel free to email us at support@travelrecon.com or call us at (855) 363 - 6362.

We look forward to working with and providing you the best Enterprise Risk Managment solution available today.
";

            var message = string.Format(template, firstName, userName, link, assignerName, assignerEmail);

            var mail = new SendGrid.Helpers.Mail.Mail
            {
                //TemplateId = template, //todo uncomment
                From = new Email(_appSettingsService.SendGridSendEmailFrom),
                Personalization = new List<Personalization>
                {
                    new Personalization()
                    {
                        Tos = new List<Email>()
                        {
                            new Email(recipient)
                        }
                    }
                },
                Subject = "Travel Recon Login Information"
            };

            mail.AddContent(new Content("text/html", ConvertNewlinesToHtml(message)));

            Send(mail);
        }

        public void SendActivateClientEmail(string recipient, string firstName, string userName, string assignerName, string assignerEmail)
        {
            var link = $"{_appSettingsService.WebSiteUrl}/activate-client/{Uri.EscapeDataString(userName)}";

            var template = @"{0},

Welcome to Travel Recon and we appreciate your business.

Below you will find the login information for your newly created Client Account:

User Name:  {1}

To activate your account use the link below:
<a href=""{2}"">Link</a>

Once you have logged in choose ""Dashboards"" and ""My Dashboard"" from the drop down menu.  From this screen you will have the ability to add Adminstrators and assign them licenses

If you have any other questions please feel free to contact your client manager {3} {4}.

Or feel free to email us at support@travelrecon.com or call us at (855) 363-6362.

We look forward to working with and providing you the best Enterprise Risk Managment solution available today.
";
            var message = string.Format(template, firstName, userName, link, assignerName, assignerEmail);

            var mail = new SendGrid.Helpers.Mail.Mail
            {
                //TemplateId = template, //todo uncomment
                From = new Email(_appSettingsService.SendGridSendEmailFrom),
                Personalization = new List<Personalization>
                {
                    new Personalization()
                    {
                        Tos = new List<Email>()
                        {
                            new Email(recipient)
                        }
                    }
                },
                Subject = "Travel Recon Login Information"
            };

            mail.AddContent(new Content("text/html", ConvertNewlinesToHtml(message)));

            Send(mail);
        }

        public void SendActivateLicenseEmail(string recipient, string message)
        {
            var mail = new SendGrid.Helpers.Mail.Mail
            {
                //TemplateId = template, //todo uncomment
                From = new Email(_appSettingsService.SendGridSendEmailFrom),
                Personalization = new List<Personalization>
                {
                    new Personalization()
                    {
                        Tos = new List<Email>()
                        {
                            new Email(_appSettingsService.SupportEmailAddress)
                        }
                    }
                },
                Subject = "Activate License" //todo remove it
            };

            mail.AddContent(new Content("text/html", ConvertNewlinesToHtml(message)));

            Send(mail);
        }

        #endregion

        #region

        public void SendNotificationAboutReportRequest(string userName, string reportType, string city, string country)
        {
            var from = new Email(_appSettingsService.SendGridSendEmailFrom);
            var to = new Email(_appSettingsService.SalesEmailAddress);
            var subject = "User has made a request for a report";
            var conent = new Content("text/plain",
                $"User {userName} has made the request for report type \"{reportType}\" for the location: {city}, {country}.");

            var mail = new SendGrid.Helpers.Mail.Mail(from, subject, to, conent);
            Send(mail);

        }

        #endregion

        private string ConvertNewlinesToHtml(string content)
        {
            return content.Replace("\r\n", "<br />").Replace("\n", "<br />");
        }

        private void Send(SendGrid.Helpers.Mail.Mail mail)
        {
            try
            {
                var client = new SendGridAPIClient(_appSettingsService.SendGridApiKey);
                var result = client.client.mail.send.post(requestBody: mail.Get());

                if (result.StatusCode != HttpStatusCode.OK)
                {
                    //System.Net.Http.StreamContent body = result.Body;
                    //var x = body.ReadAsStringAsync().Result;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
