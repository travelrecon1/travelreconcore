﻿using TravelRecon.AppServices.Interfaces;
using TravelRecon.AppServices.Intro.ViewDtos;
using TravelRecon.Domain.Identity;

namespace TravelRecon.AppServices.Intro
{
    public class IntroService: IAppService
    {
        public IntroPageViewModel GetIntroPage(User currentUser)
        {
            UserRoleTypeEnum? subscriptionLevel = null;

            if (currentUser != null)
                subscriptionLevel = currentUser.GetMaxReconRole();

            var vm = new IntroPageViewModel
            {
                UpgradePackageId = subscriptionLevel != null && subscriptionLevel < UserRoleTypeEnum.TravelRecon
                    ? UserRoleTypeEnum.TravelRecon
                    : (UserRoleTypeEnum?) null
            };

            return vm;
        }
    }
}
