﻿using TravelRecon.Domain.Identity;

namespace TravelRecon.AppServices.Intro.ViewDtos
{
    public class IntroPageViewModel
    {
        public UserRoleTypeEnum? UpgradePackageId { get; set; }
    }
}
