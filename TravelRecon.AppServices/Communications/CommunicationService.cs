﻿using System.Collections.Generic;
using System.Data.Entity.SqlServer.Utilities;
using TravelRecon.AppServices.Communications.ViewDtos;
using TravelRecon.AppServices.Interfaces;
using TravelRecon.AppServices.Tutorial;
using TravelRecon.Domain.Identity;
using TravelRecon.Domain.Localization;
using TravelRecon.Domain.Localization.QueryObjects;
using TravelRecon.Domain.RepositoryInterfaces;
using TravelRecon.Domain.Tutorial;

namespace TravelRecon.AppServices.Communications
{
    public class CommunicationService: IAppService
    {
        private readonly IRepository<Label> _labelRepository;
        private readonly TutorialService _tutorialService;

        public CommunicationService(IRepository<Label> labelRepository, TutorialService tutorialService)
        {
            _labelRepository = labelRepository;
            _tutorialService = tutorialService;
        }

        public CommunicationsViewModel GetCommunicationsPage(User user)
        {
            var targetLanguage = user.GetLanguageSetting();

            var vm = new CommunicationsViewModel();

            var labelsToRetrive = new[]
            {
                LabelType.Communications,
                LabelType.CheckIns,
                LabelType.Emergencies,
                LabelType.MenuContacts,
                LabelType.CheckIn,
                LabelType.Emergency,
                LabelType.ViewYourContactsLatestCheckInsAndEmergencies,
                LabelType.NoReportedCheckins,
                LabelType.NoReportedEmergencies,
                LabelType.NoContacts,
                LabelType.AddContacts,
				LabelType.Search,
				LabelType.SeeContacts,
				LabelType.SeeGroups,
				LabelType.SignaledAnEmergency,
				LabelType.HasntCheckedIn
			};

            var localizedLabels = _labelRepository.GetAll().FindLabelsLocalizedDictionary(labelsToRetrive, targetLanguage);

            vm.Labels = localizedLabels;
            vm.Communications = new List<CommunicationDto>();

            return vm;
        }
    }
}
