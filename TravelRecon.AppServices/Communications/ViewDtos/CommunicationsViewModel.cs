﻿using System.Collections.Generic;
using TravelRecon.AppServices.SharedDtos;
using TravelRecon.Domain.Tutorial;

namespace TravelRecon.AppServices.Communications.ViewDtos
{
    public class CommunicationsViewModel: BasePageViewModel
    {
        public Dictionary<int, string> Labels { get; set; }
        public IEnumerable<CommunicationDto> Communications { get; set; }
        public override PageTypeEnum PageType => PageTypeEnum.Communications;
    }
}
