﻿using System;
using TravelRecon.Domain.Map;
using TravelRecon.Domain.Statuses;

namespace TravelRecon.AppServices.Communications.ViewDtos
{
    public class CommunicationDto
    {
        public int Id { get; set; }
        public string UserFirstName { get; set; }
        public string UserLastName { get; set; }
        public string Username { get; set; }
        public DateTime ReportedOn { get; set; }
        public string AddressFormatted { get; set; }
        public string Description { get; set; }
        public Location Location { get; set; }
        public StatusType Type { get; set; }
        public string AvatarUrl { get; set; }
        public DateTime? ExpirationDate { get; set; }
    }
}
