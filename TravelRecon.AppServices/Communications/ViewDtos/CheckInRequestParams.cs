﻿using TravelRecon.AppServices.Alerts.ViewDtos;

namespace TravelRecon.AppServices.Communications.ViewDtos
{
    public class CheckInRequestParams
    {
        public int Id { get; set; }
        public MeansForLocationInputType MeansForLocationInput { get; set; }
        public string SelectedAddressFormatted { get; set; }
        public double? SelectedLatitude { get; set; }
        public double? SelectedLongitude { get; set; }
        public string Description { get; set; }
    }

}
