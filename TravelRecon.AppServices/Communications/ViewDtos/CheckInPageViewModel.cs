﻿using System.Collections.Generic;

namespace TravelRecon.AppServices.Communications.ViewDtos
{
    public class CheckInPageViewModel
    {
        public Dictionary<int, string> Labels { get; set; }
    }
}
