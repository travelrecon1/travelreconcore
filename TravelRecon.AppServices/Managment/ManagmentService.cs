﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TravelRecon.AppServices.IntelReports.ViewDtos;
using TravelRecon.AppServices.Interfaces;
using TravelRecon.AppServices.Users;
using TravelRecon.AppServices.Users.ViewDtos;
using TravelRecon.Common.Exceptions;
using TravelRecon.Domain.Identity;
using TravelRecon.Domain.IntelReports;
using TravelRecon.Domain.Localization;
using TravelRecon.Domain.RepositoryInterfaces;

namespace TravelRecon.AppServices.Managment
{
    public class ManagmentService: IAppService
    {
        private readonly IRepository<AnalystsUser> _assignedUsersRepository;
        private readonly IRepository<AnalystsCountry> _assignedCountriesRepository;
        private readonly IRepository<AnalystsDestination> _assignedDestinationsRepository;
        private readonly IRepository<User> _userRepository;
        private readonly IRepository<Destination> _destinationsRepository;
        private readonly IRepository<Country> _countriesRepository;
        private readonly IRepository<UserRoles> _userRolesRepository;

        private const int ADMIN = 4;

        public ManagmentService(
            IRepository<AnalystsUser> assignedUsersRepository,
            IRepository<AnalystsCountry> assignedCountriesRepository,
            IRepository<AnalystsDestination> assignedDestinations,
            IRepository<User> userRepository,
            IRepository<Destination> destinationsRepository,
            IRepository<Country> countriesRepository,
            IRepository<UserRoles>  userRolesRepository
            )
        {
            _assignedUsersRepository = assignedUsersRepository;
            _assignedCountriesRepository = assignedCountriesRepository;
            _assignedDestinationsRepository = assignedDestinations;
            _userRepository = userRepository;
            _destinationsRepository = destinationsRepository;
            _countriesRepository = countriesRepository;
            _userRolesRepository = userRolesRepository;
        }

        public async Task<List<PureUserDto>> GetTravelReconUsers()
        {
            return (await _userRepository.GetAll()
                .Where(x => x.UserRoles.Count(y => y.UserRole.Id == (int)UserRoleTypeEnum.TravelRecon || y.UserRole.Id == (int)UserRoleTypeEnum.TravelReconPro) > 0)
                .Select(x => new PureUserDto()
                {
                    Id = x.Id,
                    Name = x.UserName
                }).ToListAsync());
        }

        public async Task<List<PureUserDto>> GetAnalystUsers()
        {
            return (await _userRepository.GetAll()
                .Where(x => x.UserRoles.Count(y => y.UserRole.Id == (int) UserRoleTypeEnum.Analyst) > 0)
                .Select(x => new PureUserDto()
                {
                    Id = x.Id,
                    Name = x.UserName
                }).ToListAsync());
        }

        private async Task<List<PureUserDto>> GetUsersWithRole(UserRoleTypeEnum userRoleType)
        {
             return (await _userRepository.GetAll()
                .Where(x => x.UserRoles.Count(y => y.UserRole.Id == (int)userRoleType) > 0)
                .Select(x => new PureUserDto()
                {
                    Id = x.Id,
                    Name = x.UserName
                }).ToListAsync());
        }


        public async Task<List<UserRiskFactorDto>> GetUsersAssignedToAnalyst(User currentUser, int analystId)
        {
            if (currentUser.Id == analystId && currentUser.HasRole(UserRoleTypeEnum.Administrator))
            {
                return (await _userRepository.GetAll()
                    .ToListAsync())
                    .ConvertAll(x => new UserRiskFactorDto
                    {
                        Id = x.Id, RiskFactor = x.RiskFactor, UserName = x.UserName
                    });
            }

            return (await _assignedUsersRepository.GetAll()
                .Where(x => x.AnalystId == analystId)
                .Select(y => y.User)
                .ToListAsync())
                .ConvertAll(x => new UserRiskFactorDto() { Id = x.Id, RiskFactor = x.RiskFactor, UserName = x.UserName });
        }

        public async Task<List<CountryVm>> GetCountriesAssignedToAnalyst(User currentUser, int analystId)
        {
            if (currentUser.Id == analystId && currentUser.HasRole(UserRoleTypeEnum.Administrator))
            {
                return (await _countriesRepository.GetAll()
                    .Include(y => y.Translations)
                    .ToListAsync())
                    .ConvertAll(t => new CountryVm()
                    {
                        Id = t.Id,
                        RiskFactor = t.RiskFactor,
                        Translations = t.Translations?.Select(vm => new CountryTranslationVm
                        {
                            Language = vm.Language,
                            Name = vm.Name
                        }).ToList() ?? new List<CountryTranslationVm>()
                    });
            }

            return (await _assignedCountriesRepository.GetAll()
                .Where(x => x.AnalystId == analystId)
                .Select(y => y.Country)
                .Include(y => y.Translations)
                .ToListAsync())
                .ConvertAll(t => new CountryVm()
                {
                    Id = t.Id,
                    RiskFactor = t.RiskFactor,
                    Translations = t.Translations?.Select(vm => new CountryTranslationVm
                    {
                        Language = vm.Language,
                        Name = vm.Name
                    }).ToList() ?? new List<CountryTranslationVm>()
                });
        }

        public async Task AssignCountry(int analystId, int countryId)
        {    
           var assignedCountry = await
                    _assignedCountriesRepository
                        .GetAll()
                        .FirstOrDefaultAsync(x => x.AnalystId == analystId && x.CountryId == countryId);
           if (assignedCountry == null)
           {
                _assignedCountriesRepository.Add(new AnalystsCountry()
                {
                        AnalystId = analystId,
                        CountryId = countryId
                });
               _assignedCountriesRepository.SaveChanges();
           }
        }     

        public async Task AssignUser(int analystId, int userId)
        {
            var assignedUser = await
                _assignedUsersRepository
                    .GetAll()
                    .FirstOrDefaultAsync(x => x.AnalystId == analystId && x.UserId == userId);

            if (assignedUser == null)
            {
                _assignedUsersRepository.Add(new AnalystsUser()
                {
                    AnalystId = analystId,
                    UserId = userId
                });
                _assignedUsersRepository.SaveChanges();
            }
        }

        public async Task AssignDestination(int analystId, int destinationId)
        {
            var assignedDestination = await
                _assignedDestinationsRepository
                    .GetAll()
                    .FirstOrDefaultAsync(x => x.AnalystId == analystId && x.DestinationId == destinationId);

            var countryId = _destinationsRepository.Get(destinationId).CountryId;
            var isCountryAssigned = _assignedCountriesRepository.GetAll().Count(x => x.AnalystId == analystId && x.CountryId == countryId) > 0;

            if(!isCountryAssigned)
                throw new BusinessLogicException("Please assign country first!");

            if (assignedDestination == null && isCountryAssigned)
            {
                _assignedDestinationsRepository.Add(new AnalystsDestination()
                {
                    AnalystId = analystId,
                    DestinationId = destinationId
                });
                _assignedDestinationsRepository.SaveChanges();
            }



        }

        public async Task<List<DestinationVm>> GetDestinationsAssignedToAnalyst(User currentUser,int analystId)
        {
            if (currentUser.Id == analystId && currentUser.HasRole(UserRoleTypeEnum.Administrator))
            {
                return (await _destinationsRepository.GetAll()
                    .Include(z => z.Translations)
                    .ToListAsync())
                    .ConvertAll(x => new DestinationVm()
                    {
                        Id = x.Id,
                        RiskFactor = x.RiskFactor,
                        CountryId = x.CountryId,
                        City = x.Translations.FirstOrDefault(tr => tr.Language == LanguageType.English).City,
                        Translations = x.Translations?.Select(
                            t => new DestinationTranslationVm
                            {
                                City = t.City,
                                Description = t.Description,
                                Language = t.Language
                            }).ToList() ?? new List<DestinationTranslationVm>()
                    });
            }

            return (await _assignedDestinationsRepository.GetAll()
                    .Where(x => x.AnalystId == analystId)

                    .Select(y => y.Destination)
                    .Include(z => z.Translations)
                    .ToListAsync()).
                ConvertAll(x => new DestinationVm()
                {
                    Id = x.Id,
                    RiskFactor = x.RiskFactor,
                    CountryId = x.CountryId,
                    City = x.Translations.FirstOrDefault(tr => tr.Language == LanguageType.English).City,
                    Translations = x.Translations?.Select(
                        t => new DestinationTranslationVm
                        {
                            City = t.City,                           
                            Description = t.Description,
                            Language = t.Language
                        }).ToList() ?? new List<DestinationTranslationVm>()
                });
        }


        public async Task<List<PureUserDto>> GetAdmins()
        {
            return await this.GetUsersWithRole(UserRoleTypeEnum.Administrator);
        }

        public void AddToAdmins(int userId)
        {
            var user = _userRolesRepository.GetAll().FirstOrDefault(x => x.UserId == userId && x.RoleId == ADMIN);
            if (user == null)
            {
                _userRolesRepository.Add(new UserRoles() { UserId = userId, RoleId = ADMIN });
                _userRepository.SaveChanges();
            }
            else
            {
                throw new BusinessLogicException("User is already admin");
            }

        }

        public void RemoveFromAdmins(int userId)
        {
            var user = _userRolesRepository.GetAll( ).FirstOrDefault(x => x.UserId == userId && x.RoleId == ADMIN);
            _userRolesRepository.Delete(user);
            _userRepository.SaveChanges();
        }
    }
}
