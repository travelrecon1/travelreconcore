﻿namespace TravelRecon.AppServices.IntelReports.ViewDtos
{
    public class DestinationUnlockResultDto
    {
        public bool Unlocked => NeedForUnlock == 0;
        public int NeedForUnlock { get; set; }
    }
}
