﻿using System.ComponentModel.DataAnnotations;
using TravelRecon.Domain.Localization;

namespace TravelRecon.AppServices.IntelReports.ViewDtos
{
    public class CountryTranslationVm
    {
        [Required]
        public LanguageType Language { get; set; }
        [Required]
        public string Name { get; set; }
    }
}