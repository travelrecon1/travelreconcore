namespace TravelRecon.AppServices.IntelReports.ViewDtos
{
    public class IntelReportLabelsModel
    {
        public string ReportsListTitle { get; set; }
        public string Filters { get; set; }
        public string CurrentLocation { get; set; }
        public string ReportViewTitle { get; set; }
        public string UpgradeSubscription { get; set; }
        public string ReportNotFoundMessage { get; set; }
        public string TypeLabel { get; set; }
        public string DestinationLabel { get; set; }
        public string DateLabel { get; set; }
        public string DescriptionLabel { get; set; }
        public string ViewReportLabel { get; set; }
        public string IntelReportsGpsLocationSettingIsOff { get; set; }
        public string IntelReportsGpsLocationTurnOn { get; set; }
        public string PurchaseNewDestination { get; set; }
        public string IntelReportBusinessDescription { get; set; }
        public string Available { get; set; }
        public string NotAvailable { get; set; }
        public string ClickHereToPurchaseTravelRecon { get; set; }
        public string RequestThisLocation { get; set; }
        public string PurchaseLocation { get; set; }
        public string ThankYouForRequestingThisDestination { get; set; }
    }
}