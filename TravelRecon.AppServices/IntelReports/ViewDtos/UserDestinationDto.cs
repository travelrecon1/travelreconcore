﻿using TravelRecon.Domain.IntelReports;
using TravelRecon.Domain.Map;

namespace TravelRecon.AppServices.IntelReports.ViewDtos
{
	public class UserDestinationDto : DestinationDto
	{
		public bool IsSelected { get; set; }
            
        public int? DestinationId { get; set; }

        public static UserDestinationDto ConvertFrom(UserDestinations destination)
		{
			var location = new Location
			{
				Latitude = destination.Latitude,
				Longitude = destination.Longitude
			};

			var destinationName = string.Empty;

			if (destination.City != null)
			{
				destinationName += destination.City;
				if (destination.Country != null)
				{
					destinationName += ", ";
				}
			}
			destinationName += destination.Country;

			return new UserDestinationDto
			{
				Id = destination.Id,
				Country = destination.Country,
				City = destination.City,
				Description = destination.Description,
				DestinationName = destinationName,
				Location = location,
				IsSelected = destination.ReceiveNotifications,
				DestinationId = destination.DestinationId
			};
		}
	}
}
