using System;
using System.Collections.Generic;
using System.Linq;
using TravelRecon.Domain.Identity;
using TravelRecon.Domain.IntelReports;
using TravelRecon.Domain.Localization;

namespace TravelRecon.AppServices.IntelReports.ViewDtos
{
    public class IntelReportTypeDto
    {
        public IntelReportTypeDto()
        {
            IntelReports = new List<IntelReportDto>();
        }

        public string Description { get; set; }
        public string Thumbnail { get; set; }
        public string ReportAcronym { get; set; }
        public bool IsBusinessReport { get; set; }
        public bool IsEnabled { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsDestinationReportAvailable { get; set; }

        public string DestinationName { get; set; }

        public List<IntelReportDto> IntelReports { get; set; }

        public static List<IntelReportTypeDto> ConvertAll(
            List<IntelReportType> intelReportTypes,
            LocalizedDestination destination,
            List<IntelReport> availableIntelReports,
            List<IntelReportTypeEnum> userPermissionsForReport,
            Func<IntelReportTypeEnum, UserRoleTypeEnum> getSubscriptionLevelTypeByReportType,
            LanguageType language)
        {
            var intelReportTypeDtos = new List<IntelReportTypeDto>();
            foreach (var reportType in intelReportTypes)
            {
                var intelReportTypeEnum = (IntelReportTypeEnum)reportType.Id;
                var intelReportTypeTranslation = reportType.Translations.FirstOrDefault(x => x.Language == language);

                if (intelReportTypeTranslation == null)
                {
                    throw new ArgumentNullException(nameof(language),
                        "There is no translations available for this language. This needs to be corrected");
                }

                var isEnabled = userPermissionsForReport.Any(x => x == intelReportTypeEnum);
                var reports = availableIntelReports.Where(x => 
                        x.IntelReportType == reportType 
                        && x.DestinationId == destination.Id)
                    .ToList();

                var intelReportTypeDto = new IntelReportTypeDto
                {
                    Id = reportType.Id,
                    IsEnabled = isEnabled,
                    IsBusinessReport = reportType.IsBusinessReport,
                    ReportAcronym = reportType.ReportTypeAcronym,
                    Thumbnail = reportType.Thumbnail,
                    Description = intelReportTypeTranslation.Description,
                    Name = intelReportTypeTranslation.Name,
                    DestinationName = destination.DestinationName,
                    IsDestinationReportAvailable = reports.Any(x => x.IntelReportType == reportType && x.IsEnabled),
                    IntelReports = IntelReportDto.ConvertAll(reports, destination, userPermissionsForReport, language, getSubscriptionLevelTypeByReportType)
                };

                intelReportTypeDtos.Add(intelReportTypeDto);
            }

            return intelReportTypeDtos.OrderByDescending(x => x.IsEnabled).ToList();
        }
    }
}