using System.Collections.Generic;
using System.Linq;
using TravelRecon.Domain.Identity;

namespace TravelRecon.AppServices.IntelReports.ViewDtos
{
    public class IntelReportsViewModel
    {
        public IntelReportsViewModel()
        {
            IntelReportTypes = new List<IntelReportTypeDto>();
        }

        public List<IntelReportTypeDto> IntelReportTypes { get; set; }
        public IntelReportLabelsModel Labels { get; set; }
        public DestinationDto Destination { get; set; }
        public bool IsDestinationAccessibleForUser { get; set; }
        public bool IsDestinationRequested { get; set; }
        public UserRoleTypeEnum SubscriptionLevel { get; set; }

        public bool HasReports
        {
            get { return IntelReportTypes.Any(x => x.IntelReports.Any()); }
        }
    }
}