﻿using System.Collections.Generic;
using System.Linq;
using TravelRecon.Domain.Localization;

namespace TravelRecon.AppServices.IntelReports.ViewDtos
{
    public class CountryVm
    {
        private readonly LanguageType? _language;

        private LanguageType GetLanguage()
        {
            return _language ?? LanguageType.English;
        }

        public CountryVm()
        {
            Translations = new List<CountryTranslationVm>();
        }

        public CountryVm(LanguageType language): this()
        {
            _language = language;
        }

        public int Id { get; set; }

        public double RiskFactor { get; set; }

        public string Name
        {
            get
            {
                var trnaslation = Translations.FirstOrDefault(x => x.Language == GetLanguage()) ??
                                  Translations.FirstOrDefault(x => !string.IsNullOrWhiteSpace(x.Name));

                return trnaslation?.Name;
            }
        }

        public List<CountryTranslationVm> Translations { get; set; }
    }
}
