using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using TravelRecon.Domain.Extensions;
using TravelRecon.Domain.Identity;
using TravelRecon.Domain.IntelReports;
using TravelRecon.Domain.Localization;

namespace TravelRecon.AppServices.IntelReports.ViewDtos
{
    public class IntelReportDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ReportAcronym { get; set; }
        public string Thumbnail { get; set; }
        public string Uri { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsBusinessReport { get; set; }
        public int DestinationId { get; set; }
        public IntelReportTypeEnum Type { get; set; }
        public DateTime Date { get; set; }
        public string DestinationName { get; set; }
        public bool IsDestinationAccessibleToUser { set; get; }
        public UserRoleTypeEnum UserRole { get; set; }

        public static List<IntelReportDto> ConvertAll(List<IntelReport> intelReports, LocalizedDestination destination, List<IntelReportTypeEnum> userPermissionsForReport, LanguageType language, Func<IntelReportTypeEnum, UserRoleTypeEnum> getSubscriptionLevelTypeByReportType)
        {
            var intelReportDtos = new List<IntelReportDto>();
            foreach (var intelReport in intelReports)
            {
                var intelReportTypeEnum = (IntelReportTypeEnum)intelReport.IntelReportTypeFk;
                var intelReportTypeTranslation = intelReport.IntelReportType.Translations.FirstOrDefault(x => x.Language == language);

                if (intelReportTypeTranslation == null)
                {
                    throw new ArgumentNullException(nameof(language), "There is no translations available for this language. This needs to be corrected. Intel report: " + intelReportTypeEnum.ConvertToString());
                }

                var doesUserHaveAccessToReportType = userPermissionsForReport.Any(x => x == intelReportTypeEnum);

                var intelReportDto = new IntelReportDto();
                intelReportDto.Id = intelReport.Id;
                intelReportDto.Type = intelReportTypeEnum;
                intelReportDto.Uri = intelReport.ReportLocationUri;
                intelReportDto.Date = intelReport.CreatedDate;
                intelReportDto.IsEnabled = doesUserHaveAccessToReportType;
                intelReportDto.IsBusinessReport = intelReport.IntelReportType.IsBusinessReport;
                intelReportDto.ReportAcronym = intelReport.IntelReportType.ReportTypeAcronym;
                intelReportDto.Thumbnail = intelReport.IntelReportType.Thumbnail;
                intelReportDto.Description = intelReportTypeTranslation.Description;
                intelReportDto.Name = intelReportTypeTranslation.Name;
                intelReportDto.DestinationId = intelReport.DestinationId;
                intelReportDto.DestinationName = destination.DestinationName;
                
                intelReportDto.UserRole = getSubscriptionLevelTypeByReportType(intelReportDto.Type);
                intelReportDtos.Add(intelReportDto);
            }

            return intelReportDtos.OrderByDescending(x => x.IsEnabled).ToList();
        }
    }
}