﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TravelRecon.AppServices.IntelReports.ViewDtos
{
    public class EditCountryVm
    {
        [Required]
        public List<CountryTranslationVm> Translations { get; set; }
    }
}