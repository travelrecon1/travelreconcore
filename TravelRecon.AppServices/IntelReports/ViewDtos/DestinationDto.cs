using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using TravelRecon.Domain.IntelReports;
using TravelRecon.Domain.Map;

namespace TravelRecon.AppServices.IntelReports.ViewDtos
{
    public class DestinationDto
    {
        public int Id { get; set; }
        public int DrupalId { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string Description { get; set; }
        
        public string DestinationName { get; set; }
        public Location Location { get; set; }

        public int ZoomLevel { get; set; }
        public double? RiskFactor { get; private set; }

        public static DestinationDto ConvertFrom(LocalizedDestination destination)
        {
            return BuildDestinationDto(destination);
        }

		private static DestinationDto BuildDestinationDto(LocalizedDestination destination)
        {
            var location = new Location
            {
                Latitude = destination.Latitude,
                Longitude = destination.Longitude
            };

            var destinationName = string.Empty;

            if (destination.City != null)
            {
                destinationName += destination.City;
                if (destination.Country != null)
                {
                    destinationName += ", ";
                }
            }
            destinationName += destination.Country;
            
            return new DestinationDto
            {
                Id = destination.Id,
                Country = destination.Country,
                CountryId = destination.CountryId,
                City = destination.City,
                Description = destination.Description,
                DestinationName = destinationName,
                Location = location,
                ZoomLevel = destination.ZoomLevel,
                RiskFactor = destination.RiskFactor 
            };
        }

        public int CountryId { get; set; }
    }
}