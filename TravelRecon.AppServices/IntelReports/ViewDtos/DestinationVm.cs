﻿using System.Collections.Generic;
using System.Linq;
using TravelRecon.Domain.Localization;

namespace TravelRecon.AppServices.IntelReports.ViewDtos
{
    public class DestinationVm
    {
        public DestinationVm()
        {
            Translations = new List<DestinationTranslationVm>();
        }

        public int Id { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public int ZoomLevel { get; set; }
        public int CountryId { get; set; }
        public bool IsComingSoon { get; set; }
        public bool IsGoreconAccess { get; set; }
        public bool IsLocked { get; set; }
        public decimal Cost { get; set; }
        public bool IsPurchased { get; set; }
        public string City { get; set; }
        public string Description { get; set; }
        public string Country { get; set; }
        public double? RiskFactor { get; set; }

        public List<DestinationTranslationVm> Translations { get; set; }
    }
}
