﻿using System.ComponentModel.DataAnnotations;
using TravelRecon.Domain.Localization;

namespace TravelRecon.AppServices.IntelReports.ViewDtos
{
    public class DestinationTranslationVm
    {
        [Required(ErrorMessage = "Language required field.")]
        public LanguageType Language { get; set; }
        [Required(ErrorMessage = "City required field.")]
        public string City { get; set; }
        public string Description { get; set; }
    }
}