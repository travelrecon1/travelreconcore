﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TravelRecon.AppServices.IntelReports.ViewDtos
{
    public class EditDestinationVm
    {
        [Range(-90, 90, ErrorMessage = "Latitude may be only floating point value. In range (-90, 90).")]
        [Required(ErrorMessage = "Latitude required field.")]
        public double Latitude { get; set; }
        [Range(-180, 180, ErrorMessage = "Longitude may be only floating point value. In range (-180, 180).")]
        [Required(ErrorMessage = "Longitude required field.")]
        public double Longitude { get; set; }
        [Required(ErrorMessage = "Zoom Level required field.")]
        public int ZoomLevel { get; set; }
        [Required(ErrorMessage = "Contry required field.")]
        public int CountryId { get; set; }
        public bool IsComingSoon { get; set; }
        public bool IsGoreconAccess { get; set; }
        public bool IsLocked { get; set; }
        [Required(ErrorMessage = "Cost required field.")]
        public decimal Cost { get; set; }

        [Required(ErrorMessage = "Translations required.")]
        public List<DestinationTranslationVm> Translations { get; set; }
    }
}