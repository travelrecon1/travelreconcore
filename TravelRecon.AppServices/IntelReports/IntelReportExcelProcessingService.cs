using System;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using Excel;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using TravelRecon.AppServices.Interfaces;
using TravelRecon.Domain.IntelReports;
using TravelRecon.Domain.Localization;
using TravelRecon.Domain.RepositoryInterfaces;
using TravelRecon.Logger;

namespace TravelRecon.AppServices.IntelReports
{
    public class IntelReportExcelProcessingService : IAppService
    {
        private readonly IRepository<IntelReport> _intelReportRepository;
        private readonly IRepository<IntelReportType> _intelReportTypeRepository;
        private readonly DestinationAppService _destinationAppService;
        private readonly ILogger _logger;

        public IntelReportExcelProcessingService(IRepository<IntelReport> intelReportRepository, IRepository<IntelReportType> intelReportTypeRepository, DestinationAppService destinationAppService, ILogger logger)
        {
            _intelReportRepository = intelReportRepository;
            _intelReportTypeRepository = intelReportTypeRepository;
            _destinationAppService = destinationAppService;
            _logger = logger;
        }

        public IntelReportImportResult ProcessExcelFile(Stream fileUpload)
        {
            var dataReader = ExcelReaderFactory.CreateOpenXmlReader(fileUpload);
            dataReader.IsFirstRowAsColumnNames = true;
            var dataSet = dataReader.AsDataSet();

            var dataTable = dataSet.Tables[0];
            var dataRows = dataTable.AsEnumerable();
            var intelReportImportResult = new IntelReportImportResult();

            foreach (DataRow row in dataRows)
            {
                var intelReport = new IntelReport();

                var id = GetInteger(row[0]);

                if (id > 0)
                {
                    intelReport = _intelReportRepository.Get(id) ?? new IntelReport();
                }
                
                intelReport.IsBusinessReport = GetBoolean(row[1]);
                intelReport.ReportLocationUri = GetString(row[2]);
                intelReport.IsEnabled = GetBoolean(row[3]);
                intelReport.DestinationId = GetInteger(row[4]);
                intelReport.IntelReportTypeFk = GetInteger(row[5]);


                if (intelReport.Id == 0)
                {
                    _intelReportRepository.Add(intelReport);
                    intelReportImportResult.CreatedRows++;
                    intelReport.CreatedDate = GetDate(row[7]) ?? DateTime.Now;
                }
                else
                {
                    intelReport.ModifiedDate = DateTime.Now;
	                intelReport.CreatedDate = GetDate(row[7]) ?? intelReport.CreatedDate;
                    intelReportImportResult.ModifiedRows++;
                }
                
                _intelReportRepository.SaveChanges();
                intelReportImportResult.AffectedRows++;

            }

            return intelReportImportResult;

        }

	    private static DateTime? GetDate(object item)
	    {
			if (null == item || DBNull.Value == item)
			{
				return DateTime.UtcNow;
			}
		    try
		    {
				return Convert.ToDateTime(item);
			}
			catch (FormatException ex)
			{
				return null;
			}
		}

        private static string GetString(object item)
        {
            if (null == item || DBNull.Value == item)
            {
                return string.Empty;
            }
            return Convert.ToString(item);
        }

        private int GetInteger(object item)
        {
            if (null == item || DBNull.Value == item)
            {
                return 0;
            }
            double value = Convert.ToDouble(item);
            return Convert.ToInt32(value);
        }

        private bool GetBoolean(object item)
        {
            if (DBNull.Value == item)
            {
                return false;
            }
            //double result = (double)item;
            //return Convert.ToBoolean(result);
            return (bool) item;
        }

        

        public ExcelPackage GetIntelReportsExcelFile(string path = "")
        {
            if (string.IsNullOrEmpty(path))
            {
                path = GetExcelFilePath();
            }

            //CREATE THE EXCEL FILE NAME/PATH AND DELETE IT IF IT ALREADY EXISTS
            var destinationFile = new FileInfo(path);
            try
            {
                destinationFile.Delete();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return null;
            }

            //CREATE THE EXCEL SPREADSHEET FILE
            var newFile = new FileInfo(path);
            var excelPackage = new ExcelPackage(newFile);

            //ADD AND NAME A WORKHEET TO THE EXCEL SPREADSHEET
            ExcelWorksheet intelReports = excelPackage.Workbook.Worksheets.Add("IntelReports");

            BuildIntelReportsWorksheet(intelReports);

            ExcelWorksheet intelReportTypes = excelPackage.Workbook.Worksheets.Add("IntelReportTypes");

            BuildIntelReportTypesWorksheet(intelReportTypes);

            //SAVE AND CLOSE THE EXCEL SPREADSHEET
            excelPackage.Save();

            return excelPackage;

        }

        public static string GetExcelFilePath()
        {
            var defaultTemplate = @"C:\Users\Adam\code\recon\src\TravelRecon.Tests\Data\IntelReports\IntelReportTemplate.xlsx";
            return Path.GetFullPath(defaultTemplate);
        }

        private void BuildIntelReportTypesWorksheet(ExcelWorksheet worksheet)
        {
            //DEFINE THE DEFAULT FONT TYPE AND SIZE FOR THE WORKSHEET
            worksheet.Cells.Style.Font.Size = 12; //Default font size for whole sheet
            worksheet.Cells.Style.Font.Name = "Calibri"; //Default Font name for whole sheet

            //DEFINE THE WIDTHS OF COLUMNS IN THE WORKSHEET
            worksheet.Column(1).Width = 10;
            worksheet.Column(2).Width = 10;
            worksheet.Column(3).Width = 40;
            worksheet.Column(4).Width = 10;
            worksheet.Column(5).Width = 12;
            worksheet.Column(6).Width = 12;
            worksheet.Column(7).Width = 12;
            worksheet.Column(8).Width = 12;
            

            //DEFINE THE ALIGNMENT FOR COLUMNS IN THE WORKSHEET
            worksheet.Column(3).Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
            worksheet.Column(4).Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
            worksheet.Column(5).Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

            //ADD SOME LABELS TO CELLS IN THE WORKSHEET
            worksheet.Cells[1, 1].Value = "ID";
            worksheet.Cells[1, 2].Value = "Name";
            worksheet.Cells[1, 3].Value = "Acronym";
            worksheet.Cells[1, 4].Value = "IsEnabled";

            //CHANGE THE LABEL FONT TO BOLD
            worksheet.Cells[1, 1].Style.Font.Bold = true;
            worksheet.Cells[1, 2].Style.Font.Bold = true;
            worksheet.Cells[1, 3].Style.Font.Bold = true;
            worksheet.Cells[1, 4].Style.Font.Bold = true;
            worksheet.Cells[1, 5].Style.Font.Bold = true;
            worksheet.Cells[1, 6].Style.Font.Bold = true;
            worksheet.Cells[1, 7].Style.Font.Bold = true;
            worksheet.Cells[1, 8].Style.Font.Bold = true;

            var intelReportTypes = _intelReportTypeRepository.GetAll()
                .Include(i => i.Translations);

            var row = 2;
            foreach (var intelReportType in intelReportTypes)
            {
                var intelReportTypeTranslation = intelReportType.Translations.First(x=>x.Language == LanguageType.English);
                worksheet.Cells[row, 1].Value = intelReportType.Id;
                worksheet.Cells[row, 2].Value = intelReportTypeTranslation.Name;
                worksheet.Cells[row, 3].Value = intelReportType.ReportTypeAcronym;
                worksheet.Cells[row, 4].Value = intelReportType.IsEnabled;
                row++;
            }
        }

        private void BuildIntelReportsWorksheet(ExcelWorksheet worksheet)
        {
            //DEFINE THE DEFAULT FONT TYPE AND SIZE FOR THE WORKSHEET
            worksheet.Cells.Style.Font.Size = 12; //Default font size for whole sheet
            worksheet.Cells.Style.Font.Name = "Calibri"; //Default Font name for whole sheet

            //DEFINE THE WIDTHS OF COLUMNS IN THE WORKSHEET
            worksheet.Column(1).Width = 10;
            worksheet.Column(2).Width = 10;
            worksheet.Column(3).Width = 40;
            worksheet.Column(4).Width = 10;
            worksheet.Column(5).Width = 12;
            worksheet.Column(6).Width = 12;
            worksheet.Column(7).Width = 12;
            worksheet.Column(8).Width = 12;
            worksheet.Column(9).Width = 12;
            worksheet.Column(10).Width = 40;

            //DEFINE THE ALIGNMENT FOR COLUMNS IN THE WORKSHEET
            worksheet.Column(3).Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
            worksheet.Column(4).Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
            worksheet.Column(5).Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

            //ADD SOME LABELS TO CELLS IN THE WORKSHEET
            worksheet.Cells[1, 1].Value = "ID";
            worksheet.Cells[1, 2].Value = "BUSINESS REPORT";
            worksheet.Cells[1, 3].Value = "LOCATION OF FLIPPING BOOK";
            worksheet.Cells[1, 4].Value = "IS ENABLED";
            worksheet.Cells[1, 5].Value = "DUPAL DESTINATION ID";
            worksheet.Cells[1, 6].Value = "INTEL REPORT TYPE FK";
            worksheet.Cells[1, 7].Value = "LAST MODIFIED";
            worksheet.Cells[1, 8].Value = "CREATED";
            worksheet.Cells[1, 9].Value = "REPORT NAME - DO NOT EDIT";
            worksheet.Cells[1, 10].Value = "DESTINATION NAME - DO NOT EDIT";

        
            worksheet.Column(7).Style.Numberformat.Format = "yyyy-mm-dd";
            worksheet.Column(8).Style.Numberformat.Format = "yyyy-mm-dd";


            //CHANGE THE LABEL FONT TO BOLD
            worksheet.Cells[1, 1].Style.Font.Bold = true;
            worksheet.Cells[1, 2].Style.Font.Bold = true;
            worksheet.Cells[1, 3].Style.Font.Bold = true;
            worksheet.Cells[1, 4].Style.Font.Bold = true;
            worksheet.Cells[1, 5].Style.Font.Bold = true;
            worksheet.Cells[1, 6].Style.Font.Bold = true;
            worksheet.Cells[1, 7].Style.Font.Bold = true;
            worksheet.Cells[1, 8].Style.Font.Bold = true;
            worksheet.Cells[1, 9].Style.Font.Bold = true;
            worksheet.Cells[1, 10].Style.Font.Bold = true;

            var intelReports = _intelReportRepository.GetAll()
                .Include(x=>x.IntelReportType)
                .Include(y=>y.IntelReportType.Translations)
                .ToArray();

            var row = 2;
            foreach (var intelReport in intelReports)
            {
                var intelReportName = string.Empty;
                var intelReportTypeTranslation = intelReport.IntelReportType.Translations.FirstOrDefault();
                if (intelReportTypeTranslation != null)
                {
                    intelReportName = intelReportTypeTranslation.Name;
                }


                string destinationName = string.Empty;

                try
                {
                    var destination = _destinationAppService.GetDestination(intelReport.DestinationId, LanguageType.English);
                    if (destination != null)
                    {
                        if (destination.City != null)
                        {
                            destinationName += destination.City;

                            if (destination.Country != null)
                            {
                                destinationName += ", ";
                            }
                        }
                        destinationName += destination.Country;
                    }
                }
                catch (Exception)
                {
                    // ignored
                }


                worksheet.Cells[row, 1].Value = intelReport.Id;
                worksheet.Cells[row, 2].Value = intelReport.IsBusinessReport;
                worksheet.Cells[row, 3].Value = intelReport.ReportLocationUri;
                worksheet.Cells[row, 4].Value = intelReport.IsEnabled;
                worksheet.Cells[row, 5].Value = intelReport.DestinationId;
                worksheet.Cells[row, 6].Value = intelReport.IntelReportTypeFk;
                worksheet.Cells[row, 7].Value = intelReport.ModifiedDate;
                worksheet.Cells[row, 8].Value = intelReport.CreatedDate;
                worksheet.Cells[row, 9].Value = intelReportName;
                
                worksheet.Cells[row, 10].Value = destinationName;
                row++;
            }
        }
    }
}