using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using TravelRecon.AppServices.Interfaces;
using TravelRecon.Domain.IntelReports;
using TravelRecon.Domain.RepositoryInterfaces;

namespace TravelRecon.AppServices.IntelReports
{
    public class IntelReportTypeService : IAppService
    {
        private readonly IRepository<IntelReportType> _intelReportTypeRepository;

        public IntelReportTypeService(IRepository<IntelReportType> intelReportTypeRepository)
        {
            _intelReportTypeRepository = intelReportTypeRepository;
        }

        public IntelReportType GetReportType(IntelReportTypeEnum intelReportType)
        {
            var type = _intelReportTypeRepository.GetAll()
                .Where(x => x.IsEnabled && x.Id == (int)intelReportType)
                .Include(ur => ur.Translations)
                .FirstOrDefault();

            return type;

        }

        public List<IntelReportType> GetReportTypes()
        {
            var intelReportTypes = _intelReportTypeRepository.GetAll()
                .Where(x => x.IsEnabled)
                .Include(ur => ur.Translations)
                .ToList();

            return intelReportTypes;

        }
    }
}