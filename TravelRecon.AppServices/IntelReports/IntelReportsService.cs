using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using TravelRecon.AppServices.Exceptions;
using TravelRecon.AppServices.IntelReports.ViewDtos;
using TravelRecon.AppServices.Interfaces;
using TravelRecon.AppServices.Labels;
using TravelRecon.Domain.Factories;
using TravelRecon.Domain.Identity;
using TravelRecon.Domain.IntelReports;
using TravelRecon.Domain.Localization;
using TravelRecon.Domain.Localization.QueryObjects;
using TravelRecon.Domain.Map;
using TravelRecon.Domain.RepositoryInterfaces;

namespace TravelRecon.AppServices.IntelReports
{
    public class IntelReportsService : IAppService
    {
        private readonly IRepository<IntelReport> _intelReportRepository;
        private readonly DestinationAppService _destinationAppService;
        private readonly IntelReportAuthorizationService _intelReportAuthorizationService;
        private readonly IntelReportTypeService _intelReportTypeService;
        private readonly IRepository<Label> _labelRepository;
        private readonly IRepository<UserDestinationRequest> _userDestinationRequest;

        public IntelReportsService(IRepository<IntelReport> intelReportRepository, 
            DestinationAppService destinationAppService, 
            IntelReportAuthorizationService intelReportAuthorizationService, 
            IntelReportTypeService intelReportTypeService, 
            IRepository<Label> labelRepository,
            IRepository<UserDestinationRequest> userDestinationRequest)
        {
            _intelReportRepository = intelReportRepository;
            _destinationAppService = destinationAppService;
            _intelReportAuthorizationService = intelReportAuthorizationService;
            _intelReportTypeService = intelReportTypeService;
            _labelRepository = labelRepository;
            _userDestinationRequest = userDestinationRequest;
        }

        /// <summary>
        /// user is permitted to see:
        /// the intel reports for any destinations they've purchased 
        /// the intel freports where they are currnetly located

        /// </summary>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        public IntelReportsViewModel GetIntelReportViewModelForUser(User currentUser, Location currentMapLocation)
        {
            if (currentUser.HasMaxReconRole(UserRoleTypeEnum.GoRecon))
                return GetIntelReportViewModelForGorecon(currentUser);

            return GetIntelReportViewModelForNonGorecon(currentUser, currentMapLocation);
        }

        private IntelReportsViewModel GetIntelReportViewModelForNonGorecon(User currentUser, Location currentMapLocation)
        {
            var location = DbGeographyFactory.GenerateDbGeographyValue(currentMapLocation);

            var destination = _destinationAppService.GetDestinationFor(location, currentUser.GetLanguageSetting());
            var isDestinationAccessibleForUser = _destinationAppService.IsUserPermittedFor(destination.Id,
                currentUser);

            var isDestinationRequested = IsDestinationRequested(currentUser, destination);

            var authorizedIntelReportTypes = _intelReportAuthorizationService.GetReportTypeAuthorizations(currentUser);

            var languageType = currentUser.GetLanguageSetting();

            var intelReports = GetIntelReportsFor(destination);
            var intelReportTypes = _intelReportTypeService.GetReportTypes();

            var intelReportLabelsModel = GetIntelReportLabelsModel(languageType);

            var intelReportTypeDtos = IntelReportTypeDto.ConvertAll(intelReportTypes, destination, intelReports, authorizedIntelReportTypes, _intelReportAuthorizationService.GetSubscriptionLevelTypeByReportType, languageType);

            var intelReportTypesOrderedByDescending = intelReportTypeDtos.OrderByDescending(x => x.IsEnabled).ToList();
            var destinationDto = DestinationDto.ConvertFrom(destination);
            var intelReportsViewModel = new IntelReportsViewModel
            {
                IntelReportTypes = intelReportTypesOrderedByDescending,
                Labels = intelReportLabelsModel,
                Destination = destinationDto,
                IsDestinationAccessibleForUser = isDestinationAccessibleForUser,
                IsDestinationRequested = isDestinationRequested,
                SubscriptionLevel = currentUser.GetMaxReconRole()
            };

            return intelReportsViewModel;
        }

        private IntelReportsViewModel GetIntelReportViewModelForGorecon(User currentUser)
        {
            var languageType = currentUser.GetLanguageSetting();

            var intelReports = GetIntelReportsForGorecon();
            var intelReportType = _intelReportTypeService.GetReportType(IntelReportTypeEnum.SituationReport);

            var intelReportLabelsModel = GetIntelReportLabelsModel(languageType);
            var authorizedIntelReportTypes = _intelReportAuthorizationService.GetReportTypeAuthorizations(currentUser);

            var destinations = _destinationAppService.GetDestinationsFor(currentUser);

            var intelReportTypeDtos = destinations.SelectMany(x =>
                    IntelReportTypeDto.ConvertAll(
                        new List<IntelReportType> { intelReportType }, 
                        x,
                        intelReports, 
                        authorizedIntelReportTypes,
                        _intelReportAuthorizationService.GetSubscriptionLevelTypeByReportType,
                        languageType));

            var intelReportTypesOrderedByDescending = intelReportTypeDtos
                .OrderByDescending(x => x.IsEnabled)
                .ThenBy(x => x.DestinationName)
                .ToList();

            var intelReportsViewModel = new IntelReportsViewModel
            {
                IntelReportTypes = intelReportTypesOrderedByDescending,
                Labels = intelReportLabelsModel,
                Destination = null,
                IsDestinationAccessibleForUser = true,
                IsDestinationRequested = false,
                SubscriptionLevel = currentUser.GetMaxReconRole()
            };

            return intelReportsViewModel;
        }

        private bool IsDestinationAccessibleForUser(User currentUser, LocalizedDestination mapDestination)
        {
            var currentLocationDestination = _destinationAppService.GetDestinationFor(currentUser.CurrentMapLocation,
                currentUser.GetLanguageSetting());

            if (mapDestination.Id == currentLocationDestination.Id)
            {
                return true;
            }

            return _destinationAppService.IsUserPermittedFor(mapDestination.Id, currentUser);
        }

        private bool IsDestinationRequested(User currentUser, LocalizedDestination mapDestination)
        {
            return _userDestinationRequest.GetAll().Any(x => x.Destination.Id == mapDestination.Id && x.UserId == currentUser.Id);
        }


        public IntelReportLabelsModel GetIntelReportLabelsModel(LanguageType? targetLanguage = LanguageType.English)
        {
            LabelType[] contactListLabels =
            {
                LabelType.IntelReportsReportsListTitle,
                LabelType.IntelReportsFilters,
                LabelType.DestinationSettingCurrentLocation,
                LabelType.IntelReportsReportViewTitle,
                LabelType.IntelReportsUpgradeSubscription,
                LabelType.IntelReportsReportsNotFound,
                LabelType.IntelReportsType,
                LabelType.Destination,
                LabelType.Date,
                LabelType.Description,
                LabelType.IntelReportsViewReport,
                LabelType.IntelReportsGpsLocationSettingIsOff,
                LabelType.IntelReportsGpsLocationTurnOn,
                LabelType.DestinationPurchaseNew,
                LabelType.IntelReportBusinessDescription,
                LabelType.Available,
                LabelType.NotAvailable,
                LabelType.ClickHereToPurchaseTravelRecon,
                LabelType.RequestThisLocation,
                LabelType.PurchaseLocation,
                LabelType.ThankYouForRequestingThisDestination
            };
            var findLabelsLocalized = _labelRepository.GetAll().FindLabelsLocalized(contactListLabels, targetLanguage.Value).ToList();
            var intelReportLabelsModel = new IntelReportLabelsModel
            {
                ReportsListTitle = LabelService.GetContentFor(findLabelsLocalized, LabelType.IntelReportsReportsListTitle),
                Filters = LabelService.GetContentFor(findLabelsLocalized, LabelType.IntelReportsFilters),
                CurrentLocation = LabelService.GetContentFor(findLabelsLocalized, LabelType.DestinationSettingCurrentLocation),
                ReportViewTitle = LabelService.GetContentFor(findLabelsLocalized, LabelType.IntelReportsReportViewTitle),
                UpgradeSubscription = LabelService.GetContentFor(findLabelsLocalized, LabelType.IntelReportsUpgradeSubscription),
                ReportNotFoundMessage = LabelService.GetContentFor(findLabelsLocalized, LabelType.IntelReportsReportsNotFound),
                TypeLabel = LabelService.GetContentFor(findLabelsLocalized, LabelType.IntelReportsType),
                DestinationLabel = LabelService.GetContentFor(findLabelsLocalized, LabelType.Destination),
                DateLabel = LabelService.GetContentFor(findLabelsLocalized, LabelType.Date),
                DescriptionLabel = LabelService.GetContentFor(findLabelsLocalized, LabelType.Description),
                ViewReportLabel = LabelService.GetContentFor(findLabelsLocalized, LabelType.IntelReportsViewReport),
                IntelReportsGpsLocationSettingIsOff = LabelService.GetContentFor(findLabelsLocalized, LabelType.IntelReportsGpsLocationSettingIsOff),
                IntelReportsGpsLocationTurnOn = LabelService.GetContentFor(findLabelsLocalized, LabelType.IntelReportsGpsLocationTurnOn),
                PurchaseNewDestination = LabelService.GetContentFor(findLabelsLocalized, LabelType.DestinationPurchaseNew),
                IntelReportBusinessDescription = LabelService.GetContentFor(findLabelsLocalized, LabelType.IntelReportBusinessDescription),
                Available = LabelService.GetContentFor(findLabelsLocalized, LabelType.Available),
                NotAvailable = LabelService.GetContentFor(findLabelsLocalized, LabelType.NotAvailable),
                ClickHereToPurchaseTravelRecon = LabelService.GetContentFor(findLabelsLocalized, LabelType.ClickHereToPurchaseTravelRecon),
                RequestThisLocation = LabelService.GetContentFor(findLabelsLocalized, LabelType.RequestThisLocation),
                PurchaseLocation = LabelService.GetContentFor(findLabelsLocalized, LabelType.PurchaseLocation),
                ThankYouForRequestingThisDestination = LabelService.GetContentFor(findLabelsLocalized, LabelType.ThankYouForRequestingThisDestination)
            };

            return intelReportLabelsModel;
        }

        private List<IntelReport> GetIntelReportsForGorecon()
        {
            var intelReports = _intelReportRepository.GetAll()
                .Where(x => x.Destination.IsGoreconAccess && x.IntelReportTypeFk == (int)IntelReportTypeEnum.SituationReport && x.IsEnabled)
                .Include(ir => ir.IntelReportType)
                .Include(ur => ur.IntelReportType.Translations)
                .ToList();

            return intelReports;
        }

        private List<IntelReport> GetIntelReportsFor(LocalizedDestination destination)
        {
            var intelReports = _intelReportRepository.GetAll()
                .Where(x => x.DestinationId == destination.Id && x.IsEnabled)
                .Include(ir => ir.IntelReportType)
                .Include(ur => ur.IntelReportType.Translations)
                .ToList();

            return intelReports;
        }
    }
}
