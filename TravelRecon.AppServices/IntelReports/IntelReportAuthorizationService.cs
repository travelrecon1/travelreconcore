using System.Collections.Generic;
using TravelRecon.AppServices.Interfaces;
using TravelRecon.Domain.Identity;
using TravelRecon.Domain.IntelReports;

namespace TravelRecon.AppServices.IntelReports
{
    public class IntelReportAuthorizationService : IAppService
    {
        private readonly List<IntelReportTypeEnum> _authorizedReportsForTravelRecon;
        private readonly List<IntelReportTypeEnum> _authorizedReportsForTravelReconPro;

        public IntelReportAuthorizationService()
        {
            _authorizedReportsForTravelRecon = new List<IntelReportTypeEnum>
                    {
                        IntelReportTypeEnum.SituationReport,
                        IntelReportTypeEnum.EmergencyContactReport,
                        IntelReportTypeEnum.HotelAssessment,
                        IntelReportTypeEnum.HotelThreatVulnerabilityAssessment,
                        IntelReportTypeEnum.IntelligenceSummary,
                        IntelReportTypeEnum.TransportationAssessment,
                        IntelReportTypeEnum.MustKnowReport,
                        IntelReportTypeEnum.DoingBusinessIn,
                        IntelReportTypeEnum.HealthAndMedical,
                        IntelReportTypeEnum.HotelThreatAssessment,
                        IntelReportTypeEnum.ThreatAreaReport,
                        IntelReportTypeEnum.Telecommunications,
                        IntelReportTypeEnum.ThreatOverview,
                        IntelReportTypeEnum.TransportationAssessmentBusiness,

                    };

            _authorizedReportsForTravelReconPro = new List<IntelReportTypeEnum>
                    {
                        IntelReportTypeEnum.SituationReport,
                        IntelReportTypeEnum.EmergencyContactReport,
                        IntelReportTypeEnum.HotelAssessment,
                        IntelReportTypeEnum.HotelThreatVulnerabilityAssessment,
                        IntelReportTypeEnum.IntelligenceSummary,
                        IntelReportTypeEnum.TransportationAssessment,
                        IntelReportTypeEnum.MustKnowReport,
                        IntelReportTypeEnum.DoingBusinessIn,
                        IntelReportTypeEnum.HealthAndMedical,
                        IntelReportTypeEnum.HotelThreatAssessment,
                        IntelReportTypeEnum.ThreatAreaReport,
                        IntelReportTypeEnum.Telecommunications,
                        IntelReportTypeEnum.ThreatOverview,
                        IntelReportTypeEnum.TransportationAssessmentBusiness,
                    };
        }

        public UserRoleTypeEnum GetSubscriptionLevelTypeByReportType(IntelReportTypeEnum reportType)
        {
            if (IntelReportTypeEnum.SituationReport == reportType)
                return UserRoleTypeEnum.GoRecon;

            if (_authorizedReportsForTravelRecon.Contains(reportType))
                return UserRoleTypeEnum.TravelRecon;

            return UserRoleTypeEnum.TravelReconPro;
        }

        public List<IntelReportTypeEnum> GetReportTypeAuthorizations(User user)
        {

            if (user.HasMaxReconRole(UserRoleTypeEnum.TravelRecon))
                return _authorizedReportsForTravelRecon;
            if (user.HasMaxReconRole(UserRoleTypeEnum.TravelReconPro))
                return _authorizedReportsForTravelReconPro;
            
            return new List<IntelReportTypeEnum>
                    {
                        IntelReportTypeEnum.SituationReport
                    };
        }

    }
}