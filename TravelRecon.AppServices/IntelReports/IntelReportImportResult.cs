namespace TravelRecon.AppServices.IntelReports
{
    public class IntelReportImportResult
    {
        public int AffectedRows { get; set; }
        public int CreatedRows { get; set; }
        public int ModifiedRows { get; set; }
    }
}