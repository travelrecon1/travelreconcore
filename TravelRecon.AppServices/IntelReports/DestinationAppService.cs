using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Threading.Tasks;
using TravelRecon.AppServices.IntelReports.ViewDtos;
using TravelRecon.AppServices.Interfaces;
using TravelRecon.AppServices.Mail;
using TravelRecon.AppServices.Settings.ViewDtos;
using TravelRecon.Domain.Alerts;
using TravelRecon.Domain.Alerts.QueryObjects;
using TravelRecon.Domain.Ecommerce;
using TravelRecon.Domain.Factories;
using TravelRecon.Domain.Identity;
using TravelRecon.Domain.IntelReports;
using TravelRecon.Domain.Localization;
using TravelRecon.Domain.Map;
using TravelRecon.Domain.RepositoryInterfaces;
using TravelRecon.Domain.SubscriptionOffers;
using User = TravelRecon.Domain.Identity.User;

namespace TravelRecon.AppServices.IntelReports
{
    public class DestinationAppService : IAppService
    {
        public static readonly int ALERTS_FOR_UNLOCK = 10;
        public static readonly int DESTINATION_RADIUS = 20 * 1000; //20km

        private readonly IRepository<UserDestinationRequest> _userDestinationRequests;
        private readonly IRepository<Domain.IntelReports.Country> _countryRepository;
        private readonly IRepository<CountryTranslation> _countryTranslationRepository;
        private readonly IRepository<Destination> _destinationsRepository;
        private readonly IRepository<DestinationTranslation> _destinationTranslateRepository;
        private readonly IRepository<UserDestinations> _userDestinationsRepository;
        private readonly IRepository<UserSubscription> _userSubscriptionRepository;
        private readonly IRepository<Alert> _alertsRepository;
        private readonly MailService _mailService;

        public DestinationAppService(
            IRepository<Domain.IntelReports.Country> countryRepository,
            IRepository<CountryTranslation> countryTranslationRepository,
            IRepository<Destination> destinationsRepository,
            IRepository<DestinationTranslation> destinationTranslateRepository,
            IRepository<Alert> alertsRepository,
            IRepository<UserSubscription> userSubscriptionRepository,
            IRepository<UserDestinationRequest> userDestinationRequests,
            IRepository<UserDestinations> userDestinationsRepository,
            MailService mailService)
        {
            _countryRepository = countryRepository;
            _countryTranslationRepository = countryTranslationRepository;
            _destinationsRepository = destinationsRepository;
            _destinationTranslateRepository = destinationTranslateRepository;
            _alertsRepository = alertsRepository;
            _userSubscriptionRepository = userSubscriptionRepository;
            _userDestinationRequests = userDestinationRequests;
            _userDestinationsRepository = userDestinationsRepository;
            _mailService = mailService;
        }

        public LocalizedDestination GetDestinationFor(DbGeography coordinates, LanguageType languageType)
        {
            return GetClosestDestination(coordinates, languageType);
        }

        public List<LocalizedDestination> GetDestinationsFor(User user)
        {
            var isGorecon = user.HasMaxReconRole(UserRoleTypeEnum.GoRecon);

            IQueryable<int?> destinationIds = null;

            if (user.HasRole(UserRoleTypeEnum.TravelReconPro))
                return _destinationsRepository.GetAll()
                        .SelectLocalized(user.Language)
                        .ToList();

            if (isGorecon)
            {
                destinationIds = _destinationsRepository.GetAll()
                    .Where(x => x.IsGoreconAccess)
                    .Select(x => (int?)x.Id);
            }
            else
            {
                // get all activated and not expired subscriptions
                var userSubscriptions = _userSubscriptionRepository.GetAll()
                        .Where(x => x.UserFk == user.Id && x.IsActivated && x.ExpirationDate >= DateTime.UtcNow)
                        .ToList();

                // if a user has an active annual subscription return all destinations
                if (userSubscriptions.Any(x => x.SubscriptionType == SubscriptionTypes.Annual))
                {
                    return _destinationsRepository.GetAll()
                        .SelectLocalized(user.Language)
                        .ToList();
                }

                destinationIds = _userDestinationsRepository.GetAll()
                    .Where(x => x.UserFk == user.Id
                    && x.DestinationId != null
                    && x.ExpirationDate.HasValue
                    && x.ExpirationDate.Value >= DateTime.UtcNow)
                    .Select(x => x.DestinationId);
            }

            return _destinationsRepository.GetAll()
                .Where(x => destinationIds.Contains(x.Id))
                .SelectLocalized(user.Language)
                .ToList();
        }

        public LocalizedDestination GetDestination(int? destinationId, LanguageType languageType)
        {
            if (!destinationId.HasValue)
            {
                throw new ArgumentNullException("Null destination id provided");
            }

            return _destinationsRepository.GetAll()
                .SelectLocalized(languageType)
                .FirstOrDefault(x => x.Id == destinationId.Value);
        }

        public Task<LocalizedDestination> GetDestinationAsync(int destinationId, LanguageType languageType)
        {
            return _destinationsRepository.GetAll()
                .SelectLocalized(languageType)
                .FirstOrDefaultAsync(x => x.Id == destinationId);
        }

        public bool IsUserPermittedFor(int destinationId, User user)
        {
            var destinations = GetDestinationsFor(user);
            return destinations.Any(x => x.Id == destinationId);
        }


        public List<LocalizedDestination> GetAllDestinations()
        {
            return _destinationsRepository.GetAll()
                .SelectLocalized()
                .ToList();
        }

        public async Task<List<LocalizedDestination>> GetAllDestinationsAsync()
        {
            return await _destinationsRepository.GetAll()
                .SelectLocalized()
                .ToListAsync();
        }

        public async Task<PurchasedDestinationsVm> GetPurchasedDestinationsAsync(int userId, LanguageType language)
        {
            var userSubscriptions = await _userSubscriptionRepository.GetAll()
                .Where(x => x.UserFk == userId && x.IsActivated && x.ExpirationDate > DateTime.UtcNow)
                .ToListAsync();

            var maxSubscriptionType = userSubscriptions.Select(x => x.SubscriptionType).Union(new List<SubscriptionTypes> {SubscriptionTypes.None})
                .Max();

            PurchasedDestinationsVm result = new PurchasedDestinationsVm {SubscriptionType = maxSubscriptionType};

            switch (maxSubscriptionType)
            {
                case SubscriptionTypes.None:
                    result.Destinations = new List<PurchasedDestinationVm>();
                    break;
                case SubscriptionTypes.Monthly:
                    result.Destinations = await _userDestinationsRepository.GetAll()
                        .Where(x => x.UserFk == userId && x.ExpirationDate > DateTime.UtcNow)
                        .Select(x => new PurchasedDestinationVm
                        {
                             Country = x.Destination.Country.Translations
                                .FirstOrDefault(ct => ct.Language == language)
                                .Name,
                             City = x.Destination.Translations
                                .FirstOrDefault(dt => dt.Language == language)
                                .City,
                             ExpirationDate = x.ExpirationDate,
                             Id = x.Id
                        })
                        .OrderBy(x => x.Country)
                        .ThenBy(x => x.City)
                        .ToListAsync();
                    break;
                case SubscriptionTypes.Annual:
                    var annualSubscription = userSubscriptions.Where(x => x.SubscriptionType == maxSubscriptionType)
                        .OrderByDescending(x => x.ExpirationDate)
                        .First();

                    result.Destinations = await _destinationsRepository.GetAll()
                        .Select(x => new PurchasedDestinationVm
                        {
                            Country = x.Country.Translations
                                .FirstOrDefault(ct => ct.Language == language)
                                .Name,
                            City = x.Translations
                                .FirstOrDefault(dt => dt.Language == language)
                                .City,
                            ExpirationDate = annualSubscription.ExpirationDate,
                            Id = x.Id
                        })
                        .OrderBy(x => x.Country)
                        .ThenBy(x => x.City)
                        .ToListAsync();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return result;
        }

        public List<DestinationDto> GetLockedDestinations(Location location, double radiusInMeters)
        {
            var currentMapLocation = DbGeographyFactory.GenerateDbGeographyValue(location);

            var destinations = _destinationsRepository.GetAll()
                .FindByLocation(currentMapLocation, radiusInMeters)
                .Where(x => x.IsLocked)
                .SelectLocalized()
                .ToList();

            var dtos = destinations
                .Select(DestinationDto.ConvertFrom)
                .ToList();

            return dtos;
        }

        public LocalizedDestination GetDestination(DbGeography location)
        {
            var destination = _destinationsRepository.GetAll()
                .Include(x => x.Country)
                .FindByLocation(location, DESTINATION_RADIUS)
                .SelectLocalized()
                .FirstOrDefault();

            return destination;
        }

        public DestinationUnlockResultDto TryToUnlockDestination(LocalizedDestination destination)
        {
            if (destination == null)
                return new DestinationUnlockResultDto();

            var alertsWithDestination = _alertsRepository.GetAll()
                .FindActiveAlerts()
                .Count(x => x.DestinationId == destination.Id);

            var needForUnlock = ALERTS_FOR_UNLOCK - alertsWithDestination;
            needForUnlock = needForUnlock > 0 ? needForUnlock : 0;

            var result = new DestinationUnlockResultDto
            {
                NeedForUnlock = needForUnlock
            };

            if (!result.Unlocked)
                return result;

            var dest = _destinationsRepository.Get(destination.Id);
            dest.IsLocked = false;
            _destinationsRepository.Modified(dest);
            _destinationsRepository.SaveChanges();

            return result;
        }

        public RequestDestinationResult RequestDestination(User currentUser, int destinationId, string reportType)
        {
            var hasDestinationRequest = _userDestinationRequests.GetAll()
                .Any(x => x.UserId == currentUser.Id && x.Destination.Id == destinationId);

            if (hasDestinationRequest)
                return new RequestDestinationResult();

            var destination = _destinationsRepository.GetAll()
                .SelectLocalized(currentUser.Language)
                .FirstOrDefault(x => x.Id == destinationId);

            if (destination == null)
                return new RequestDestinationResult { Errors = new List<string> { "Destination not found." } };

            var destinationRequest = new UserDestinationRequest
            {
                UserId = currentUser.Id,
                DestinationId = destination.Id,
                RequestDate = DateTime.UtcNow
            };

            _userDestinationRequests.Add(destinationRequest);
            _userDestinationRequests.SaveChanges();

            _mailService.SendNotificationAboutReportRequest(
                currentUser.UserName,
                reportType,
                destination.City,
                destination.Country);

            return new RequestDestinationResult();
        }

        private LocalizedDestination GetClosestDestination(DbGeography coordinates, LanguageType languageType)
        {
            var destinations = _destinationsRepository.GetAll()
                .Where(x => !x.IsComingSoon)
                .SelectLocalized(languageType)
                .ToList();

            var orderedDestinations = destinations.OrderBy(destination => destination.Location.Distance(coordinates));

            return orderedDestinations.First();
        }

        public async Task<CountryVm> CreateCountryAsync(CreateCountryVm createModel)
        {
            var country = _countryRepository.Add(new Domain.IntelReports.Country());
            _countryTranslationRepository.AddRange(createModel.Translations.Select(x => new CountryTranslation
            {
                Country = country,
                Language = x.Language,
                Name = x.Name
            }).ToList());

            await _countryRepository.SaveChangesAsync();

            return await GetCountryAsync(country.Id);
        }

        public async Task<CountryVm> EditCountryAsync(int id, EditCountryVm editModel)
        {
            var translations = await _countryTranslationRepository.GetAll()
                .Where(x => x.CountryId == id)
                .ToListAsync();

            _countryTranslationRepository.DeleteRange(translations);

            translations = editModel.Translations.Select(x => new CountryTranslation
            {
                CountryId = id,
                Language = x.Language,
                Name = x.Name
            }).ToList();

            _countryTranslationRepository.AddRange(translations);

            await _countryRepository.SaveChangesAsync();

            return await GetCountryAsync(id);
        }

        public async Task<DestinationVm> CreateDestinationAsync(CreateDestinationVm createModel)
        {
            var destination = _destinationsRepository.Add(new Destination
            {
                CountryId = createModel.CountryId,
                ZoomLevel = createModel.ZoomLevel,
                IsComingSoon = createModel.IsComingSoon,
                Latitude = createModel.Latitude,
                Longitude = createModel.Longitude,
                Location = DbGeographyFactory.GenerateDbGeographyValue(createModel.Latitude, createModel.Longitude),
                IsLocked = createModel.IsLocked,
                IsGoreconAccess = createModel.IsGoreconAccess,
                Cost = createModel.Cost
            });

            _destinationTranslateRepository.AddRange(createModel.Translations.Select(x => new DestinationTranslation
            {
                Destination = destination,
                Language = x.Language,
                City = x.City,
                Description = x.Description
            }).ToList());

            await _countryRepository.SaveChangesAsync();

            return await GetDestinationAsync(destination.Id);
        }

        public async Task<DestinationVm> EditDestinationAsync(int id, EditDestinationVm editModel)
        {
            var translations = await _destinationTranslateRepository.GetAll()
               .Where(x => x.DestinationId == id)
               .ToListAsync();

            _destinationTranslateRepository.DeleteRange(translations);

            translations = editModel.Translations.Select(x => new DestinationTranslation
            {
                DestinationId = id,
                Language = x.Language,
                City = x.City,
                Description = x.Description
            }).ToList();

            _destinationTranslateRepository.AddRange(translations);

            var destination = _destinationsRepository.Get(id);
            destination.ZoomLevel = editModel.ZoomLevel;
            destination.IsComingSoon = editModel.IsComingSoon;
            destination.Latitude = editModel.Latitude;
            destination.Longitude = editModel.Longitude;
            destination.Location = DbGeographyFactory.GenerateDbGeographyValue(editModel.Latitude, editModel.Longitude);
            destination.IsLocked = editModel.IsLocked;
            destination.Cost = editModel.Cost;
            destination.IsGoreconAccess = editModel.IsGoreconAccess;

            await _countryRepository.SaveChangesAsync();

            return await GetDestinationAsync(destination.Id);
        }

        public async Task<List<CountryVm>> GetCountryListAsync(LanguageType language)
        {
            var countries = await _countryRepository.GetAll()
                .Include(x => x.Translations)
                .ToListAsync();

            return countries.Select(x => new CountryVm(language)
            {
                Id = x.Id,
                RiskFactor = x.RiskFactor,
                Translations = x.Translations.Select(t => new CountryTranslationVm
                {
                    Language = t.Language,
                    Name = t.Name
                }).ToList()
            }).ToList();
        }

        public Task<List<DestinationVm>> GetDestinationListAsync(User user)
        {
            if (user.HasMaxReconRole(UserRoleTypeEnum.GoRecon))
                return Task.FromResult(new List<DestinationVm>());

            return _destinationsRepository.GetAll()
                .SelectVm(user.Id, user.GetLanguageSetting())
                .ToListAsync();
        }

        public Task<List<DestinationVm>> GetDestinationListByCountryNameAsync(string name, User user)
        {
            return _destinationsRepository.GetAll()
                .Where(x => x.Country.Translations.Any(ct => ct.Name.ToLower() == name.ToLower()))
                .SelectVm(user.Id, user.GetLanguageSetting())
                .ToListAsync();
        }

        public async Task DeleteCountriesAsync(int[] ids)
        {
            var countries = await _countryRepository.GetAll()
                .Include(x => x.Translations)
                .Where(x => ids.Contains(x.Id))
                .ToListAsync();

            _countryTranslationRepository.DeleteRange(countries.SelectMany(x => x.Translations).ToList());
            _countryRepository.DeleteRange(countries);

            await _countryRepository.SaveChangesAsync();
        }

        public async Task DeleteCountryAsync(int id)
        {
            var country = await _countryRepository.GetAll()
                .Include(x => x.Translations)
                .FirstOrDefaultAsync(x => x.Id == id);

            _countryTranslationRepository.DeleteRange(country.Translations);
            _countryRepository.Delete(country);

            await _countryRepository.SaveChangesAsync();
        }

        public async Task DeleteDestinationsAsync(int [] ids)
        {
            var destinations = await _destinationsRepository.GetAll()
                .Where(x => ids.Contains(x.Id))
                .Include(x => x.Translations)
                .ToListAsync();

            _destinationTranslateRepository.DeleteRange(destinations.SelectMany(x => x.Translations).ToList());
            _destinationsRepository.DeleteRange(destinations);

            await _destinationsRepository.SaveChangesAsync();
        }

        public async Task DeleteDestinationAsync(int id)
        {
            var destination = await _destinationsRepository.GetAll()
                .Include(x => x.Translations)
                .FirstOrDefaultAsync(x => x.Id == id);

            _destinationTranslateRepository.DeleteRange(destination.Translations);
            _destinationsRepository.Delete(destination);

            await _destinationsRepository.SaveChangesAsync();
        }

        public async Task<CountryVm> GetCountryAsync(int id)
        {
            var country = await _countryRepository.GetAll()
                .Include(x => x.Translations)
                .FirstOrDefaultAsync(x => x.Id == id);

            return MapToVm(country);
        }

        public Task<DestinationVm> GetDestinationAsync(int id)
        {
            return _destinationsRepository.GetAll()
                .SelectVm()
                .FirstOrDefaultAsync(x => x.Id == id);
        }

        private CountryVm MapToVm(Domain.IntelReports.Country entity)
        {
            if (entity == null)
                return null;

            return new CountryVm
            {
                Id = entity.Id,
                Translations = entity.Translations?.Select(t => new CountryTranslationVm
                {
                    Language = t.Language,
                    Name = t.Name
                }).ToList() ?? new List<CountryTranslationVm>()
            };
        }
    }
}