﻿using System;
using System.Collections.Generic;
using System.Linq;
using TravelRecon.AppServices.IntelReports.ViewDtos;
using TravelRecon.Domain.IntelReports;
using TravelRecon.Domain.Localization;

namespace TravelRecon.AppServices.IntelReports
{
    public static class DestinationQueries
    {
        public static IQueryable<LocalizedDestination> SelectLocalized(this IQueryable<Destination> query, LanguageType? languageType = LanguageType.English)
        {
            return query.Select(x => new LocalizedDestination
            {
                City = x.Translations.FirstOrDefault(t => t.Language == languageType).City,
                Description = x.Translations.FirstOrDefault(t => t.Language == languageType).Description,
                Country = x.Country.Translations.FirstOrDefault(t => t.Language == languageType).Name,
                CountryId = x.CountryId,
                Id = x.Id,
                IsComingSoon = x.IsComingSoon,
                IsLocked = x.IsLocked,
                Latitude = x.Latitude,
                Location = x.Location,
                Longitude = x.Longitude,
                ZoomLevel = x.ZoomLevel,
                RiskFactor = x.RiskFactor
            });
        }

        public static IQueryable<DestinationVm> SelectVm(this IQueryable<Destination> query, int? userId = null, LanguageType? languageType = LanguageType.English)
        {
            return query.Select(x => new DestinationVm
            {
                City = x.Translations.FirstOrDefault(t => t.Language == languageType).City,
                Description = x.Translations.FirstOrDefault(t => t.Language == languageType).Description,
                Country = x.Country.Translations.FirstOrDefault(t => t.Language == languageType).Name,
                CountryId = x.CountryId,
                Id = x.Id,
                IsComingSoon = x.IsComingSoon,
                IsLocked = x.IsLocked,
                Latitude = x.Latitude,
                Longitude = x.Longitude,
                ZoomLevel = x.ZoomLevel,
                Cost = x.Cost,
                IsGoreconAccess = x.IsGoreconAccess,
                RiskFactor = x.RiskFactor,
                IsPurchased = x.UserDestinations.Any(ud => ud.UserFk == userId && ud.ExpirationDate > DateTime.UtcNow),
                Translations = x.Translations.Select(t => new DestinationTranslationVm
                {
                    City = t.City,
                    Description = t.Description,
                    Language = t.Language
                }).ToList()
            });
        }
    }
}
