﻿using TravelRecon.AppServices.ExternalWeb.ViewDtos;
using TravelRecon.AppServices.Interfaces;
using TravelRecon.Domain.Identity;
using TravelRecon.Domain.Localization;
using TravelRecon.Domain.Localization.QueryObjects;
using TravelRecon.Domain.RepositoryInterfaces;

namespace TravelRecon.AppServices.ExternalWeb
{
    public class ExternalWebService: IAppService
    {
        private readonly IRepository<Label> _labelRepository;

        public ExternalWebService(IRepository<Label> labelRepository)
        {
            _labelRepository = labelRepository;
        }

        public IframePageViewModel GetIframePageViewModel(User currentUser)
        {
            var targetLang = currentUser?.GetLanguageSetting() ?? LanguageType.English;
            var labelsToRetrive = new[] {LabelType.GoBack};
            var vm = new IframePageViewModel();

            var locLabels = _labelRepository.GetAll().FindLabelsLocalizedDictionary(labelsToRetrive, targetLang);
            vm.Labels = locLabels;

            return vm;
        }
    }
}
