﻿using System.Collections.Generic;
using TravelRecon.Domain.Localization;

namespace TravelRecon.AppServices.ExternalWeb.ViewDtos
{
    public class IframePageViewModel
    {
        public Dictionary<int, string> Labels { get; set; } 
    }
}
