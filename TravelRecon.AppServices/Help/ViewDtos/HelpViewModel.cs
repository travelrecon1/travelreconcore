using System.Collections.Generic;
using TravelRecon.Domain.Localization.QueryObjects;

namespace TravelRecon.AppServices.Help.ViewDtos
{
    public class HelpViewModel
    {
        public IEnumerable<LabelLocalizedDto> Labels { get; set; }
    }
}