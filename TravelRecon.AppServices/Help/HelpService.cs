﻿using TravelRecon.AppServices.Help.ViewDtos;
using TravelRecon.AppServices.Interfaces;
using TravelRecon.Domain.Identity;
using TravelRecon.Domain.Localization;
using TravelRecon.Domain.Localization.QueryObjects;
using TravelRecon.Domain.RepositoryInterfaces;

namespace TravelRecon.AppServices.Help
{
    public class HelpService: IAppService
    {
        private readonly IRepository<Label> _labelRepository;

        public HelpService(IRepository<Label> labelRepository)
        {
            _labelRepository = labelRepository;
        }

        public HelpViewModel GetHelpViewModel(User user)
        {
            var targetLanguage = user.GetLanguageSetting();

            var vm = new HelpViewModel();

            var labelsToRetrieve = new[] {LabelType.OfficialHelpPage, LabelType.MenuSupport, LabelType.EmailAddressForSupport,
                LabelType.WatchTutorial, LabelType.WatchTrTerminology, LabelType.WatchMapTutorial, LabelType.WatchCommunicationsTutorial,
                LabelType.WatchAlertsTutorial, LabelType.WatchContactsTutorial };
            vm.Labels = _labelRepository.GetAll().FindLabelsLocalized(labelsToRetrieve, targetLanguage);

            return vm;
        }
    }
}
