﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using TravelRecon.AppServices.SubscriptionOffers.Queries;
using TravelRecon.AppServices.SubscriptionOffers.ViewDtos;
using TravelRecon.Domain.Ecommerce;
using TravelRecon.Domain.Identity;
using TravelRecon.Domain.IntelReports;
using TravelRecon.Domain.Localization;
using TravelRecon.Domain.RepositoryInterfaces;
using TravelRecon.Domain.SubscriptionOffers;

namespace TravelRecon.AppServices.SubscriptionOffers
{
	public class SubscriptionOffersService : IAppService
	{
		private readonly IRepository<SubscriptionOffer> _offersRepository;
	    private readonly IRepository<UserDestinations> _userDestinationsRepository;
	    private readonly IRepository<UserSubscription> _subscriptionRepository;
	    private readonly IRepository<Destination> _destinationRepository;

	    public SubscriptionOffersService(IRepository<SubscriptionOffer> offersRepository, 
            IRepository<UserDestinations> userDestinationsRepository, 
            IRepository<UserSubscription> subscriptionRepository, 
            IRepository<Destination> destinationRepository)
	    {
	        _offersRepository = offersRepository;
	        _userDestinationsRepository = userDestinationsRepository;
	        _subscriptionRepository = subscriptionRepository;
	        _destinationRepository = destinationRepository;
	    }

		public async Task<SubscriptionOfferListVm> GetSubscriptionOffers(User currentUser)
		{
			var language = currentUser.GetLanguageSetting();
			var offers = await _offersRepository.GetAll()
                .Where(x => x.LanguageTypeFk == language)
                .SelectSubscriptionOfferVm()
                .ToListAsync();

			return new SubscriptionOfferListVm
			{
				SubscriptionOffers = offers
			};
		}

        public Task<List<SubscriptionOfferVm>> GetSubscriptionOffersInApp(LanguageType language)
        {
            return _offersRepository.GetAll()
                .Where(x => x.LanguageTypeFk == language && x.ShowInApp)
                .Select(offer => new SubscriptionOfferVm
                {
                    Id = offer.Id,
                    LanguageType = offer.LanguageTypeFk,
                    Title = offer.Title,
                    PrimaryText = offer.PrimaryText,
                    SecondaryText = offer.SecondaryText,
                    OfferPrice = offer.OfferPrice,
                    HasSpecialOffer = offer.HasSpecialOffer,
                    SpecialOfferPrice = offer.SpecialOfferPrice,
                    ButtonText = offer.ButtonText,
                    ButtonLink = offer.ButtonLink,
                    SubscriptionType = offer.SubscriptionType,
                    Price = offer.Price
                })
                .ToListAsync();
        }

        public Task<List<SubscriptionOfferVm>> GetManagedOffers(LanguageType language)
        {
            return _offersRepository.GetAll()
                .Where(x => x.ShowInApp)
                .Select(offer => new SubscriptionOfferVm
                {
                    Id = offer.Id,
                    LanguageType = offer.LanguageTypeFk,
                    Title = offer.Title,
                    PrimaryText = offer.PrimaryText,
                    SecondaryText = offer.SecondaryText,
                    OfferPrice = offer.OfferPrice,
                    HasSpecialOffer = offer.HasSpecialOffer,
                    SpecialOfferPrice = offer.SpecialOfferPrice,
                    ButtonText = offer.ButtonText,
                    ButtonLink = offer.ButtonLink,
                    SubscriptionType = offer.SubscriptionType,
                    Price = offer.Price
                })
                .ToListAsync();
        }

        public async Task<bool> HasSubscription(User user)
        {
            if (user.HasRole(UserRoleTypeEnum.TravelReconPro))
                return true;

	        var hasSubscription = await _subscriptionRepository.GetAll()
	            .AnyAsync(x => x.UserFk == user.Id
                               && x.IsActivated
	                           && x.ExpirationDate.HasValue
	                           && x.ExpirationDate.Value >= DateTime.UtcNow);

	        var hasDestinations = await _userDestinationsRepository.GetAll()
	            .AnyAsync(x => x.UserFk == user.Id
                               && x.ExpirationDate.HasValue
	                           && x.ExpirationDate.Value >= DateTime.UtcNow);

            return hasSubscription || hasDestinations;
	    }

	    public async Task SetAnnualPrice(decimal price)
	    {
	        var annualOffers = _offersRepository.GetAll()
	            .Where(x => x.SubscriptionType.HasValue && x.SubscriptionType.Value == SubscriptionTypes.Annual)
	            .ToList();

	        foreach (var offer in annualOffers)
	        {
	            offer.Price = price;
	            var pricePostfix = offer.OfferPrice.Split('/')[1].Trim();
	            offer.OfferPrice = $"${price} / {pricePostfix}";
	        }

            await _offersRepository.SaveChangesAsync();
        }

        public async Task SetAllDestinationsPrice(decimal price)
        {
            var destinations = _destinationRepository.GetAll().ToList();
            var monthlyOffers = _offersRepository.GetAll()
                .Where(x => x.SubscriptionType.HasValue && x.SubscriptionType.Value == SubscriptionTypes.Monthly)
                .ToList();

            foreach (var destination in destinations)
            {
                destination.Cost = price;
            }

            foreach (var offer in monthlyOffers)
            {
                offer.Price = price;
                var pricePostfix = offer.OfferPrice.Split('/')[1].Trim();
                offer.OfferPrice = $"${price} / {pricePostfix}";
            }

            await _destinationRepository.SaveChangesAsync();
            await _offersRepository.SaveChangesAsync();
        }

	    public async Task UpdateOfferTitle(int offerId, string newTitle)
	    {
	        var offer = _offersRepository.Get(offerId);
	        offer.Title = newTitle;
	        await _offersRepository.SaveChangesAsync();
	    }

        public async Task UpdateOfferPrimaryText(int offerId, string newText)
        {
            var offer = _offersRepository.Get(offerId);
            offer.PrimaryText = newText;
            await _offersRepository.SaveChangesAsync();
        }

	    public bool CanUpgradeSubscription(int userId)
	    {
            return !_subscriptionRepository.GetAll()
                .Any(x => x.UserFk == userId
                    && x.SubscriptionType == SubscriptionTypes.Annual
                    && x.IsActivated
                    && x.ExpirationDate.HasValue
                    && x.ExpirationDate.Value >= DateTime.UtcNow);
        }

	    public async Task<UserSubscriptionsVm> GetUserSubscriptionsHistoryAsync(int userId)
	    {
            var result = new UserSubscriptionsVm();

	        var subscriptions = await _subscriptionRepository.GetAll()
	            .Where(x => x.UserFk == userId && x.IsActivated)
                .SelectUserSubscriptionVm()
                .OrderByDescending(x => x.PurchaseDate)
                .ToListAsync();

	        result.Subscriptions = subscriptions;
	        result.CanUpgrade = subscriptions.All(x => x.SubscriptionType != SubscriptionTypes.Annual);

            return result;
	    }
    }
}
