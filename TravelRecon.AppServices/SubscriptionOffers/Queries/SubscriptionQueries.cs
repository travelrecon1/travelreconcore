﻿using System.Linq;
using TravelRecon.AppServices.SubscriptionOffers.ViewDtos;
using TravelRecon.Domain.Ecommerce;
using TravelRecon.Domain.SubscriptionOffers;
using TravelRecon.Web.Areas.SubscriptionsManagment.Models;

namespace TravelRecon.AppServices.SubscriptionOffers.Queries
{
    public static class SubscriptionQueries
    {
        public static IQueryable<UserSubscriptionVm> SelectUserSubscriptionVm(
            this IQueryable<UserSubscription> query
        )
        {
            return query.Select(x => new UserSubscriptionVm
            {
                Amount = x.Payment.AmountPaid,
                PurchaseDate = x.PurchaseDate,
                ExpirationDate = x.ExpirationDate,
                PaymentId = x.Payment.PaymentId,
                SubscriptionType = x.SubscriptionType,
                Id = x.Id
            });
        }

        public static IQueryable<ManageUserSubscriptionModel> SelectManageUserSubscriptionVm(
           this IQueryable<UserSubscription> query
       )
        {
            return query.Select(x => new ManageUserSubscriptionModel
            {
                Amount = x.Payment.AmountPaid,
                PurchaseDate = x.PurchaseDate,
                ExpirationDate = x.ExpirationDate,
                PaymentId = x.Payment.PaymentId,
                SubscriptionType = x.SubscriptionType,
                Id = x.Id
            });
        }

        public static IQueryable<SubscriptionOfferVm> SelectSubscriptionOfferVm(
            this IQueryable<SubscriptionOffer> query
        )
        {
			return query.Select(x => new SubscriptionOfferVm
            {
                Id = x.Id,
                LanguageType = x.LanguageTypeFk,
                Title = x.Title,
                PrimaryText = x.PrimaryText,
                SecondaryText = x.SecondaryText,
                OfferPrice = x.OfferPrice,
                HasSpecialOffer = x.HasSpecialOffer,
                SpecialOfferPrice = x.SpecialOfferPrice,
                ButtonText = x.ButtonText,
                ButtonLink = x.ButtonLink,
                SubscriptionType = x.SubscriptionType,
                Price = x.Price
            });
        }
    }
}

