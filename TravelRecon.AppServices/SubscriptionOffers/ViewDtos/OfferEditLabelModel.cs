﻿namespace TravelRecon.AppServices.SubscriptionOffers.ViewDtos
{
    public class OfferEditLabelModel
    {
        public int OfferId { get; set; }
        public string NewText { get; set; }
    }
}
