﻿using System;
using System.Collections.Generic;
using TravelRecon.AppServices.IntelReports.ViewDtos;
using TravelRecon.AppServices.SubscriptionOffers.ViewDtos;

namespace TravelRecon.Web.Areas.SubscriptionsManagment.Models
{
    public class ManageUserSubscriptionModel: UserSubscriptionVm
    {
        public ManageUserSubscriptionModel()
        {
            Destinations = new List<PureDestinationDto>();
        }
        public List<PureDestinationDto> Destinations { get; set; }
    }

    public class PureDestinationDto
    {
        public string City { get; internal set; }
        public DateTime? ExpirationDate { get; set; }
    }
}