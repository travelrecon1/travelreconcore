﻿using System;
using System.Collections.Generic;
using TravelRecon.Domain.SubscriptionOffers;

namespace TravelRecon.AppServices.SubscriptionOffers.ViewDtos
{
    public class UserSubscriptionVm
    {
        public DateTime PurchaseDate { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public decimal Amount { get; set; }
        public string PaymentId { get; set; }
        public int Id { get; set; }
        public SubscriptionTypes SubscriptionType { get; set; }
    }

    public class UserSubscriptionsVm
    {
        public UserSubscriptionsVm()
        {
            Subscriptions = new List<UserSubscriptionVm>();
        }

        public IList<UserSubscriptionVm> Subscriptions { get; set; }
        public bool CanUpgrade { get; set; }
    }
}
