﻿using System.Collections.Generic;

namespace TravelRecon.AppServices.SubscriptionOffers.ViewDtos
{
	public class SubscriptionOfferListVm
	{
		public List<SubscriptionOfferVm> SubscriptionOffers { get; set; }

		public SubscriptionOfferListVm()
		{
			SubscriptionOffers = new List<SubscriptionOfferVm>();
		}
	}
}
