﻿using TravelRecon.Domain.Localization;
using TravelRecon.Domain.SubscriptionOffers;

namespace TravelRecon.AppServices.SubscriptionOffers.ViewDtos
{
	public class SubscriptionOfferVm
	{
		public int Id { get; set; }
		public LanguageType LanguageType { get; set; }
		public string Title { get; set; }
		public string PrimaryText { get; set; }
		public string SecondaryText { get; set; }
		public string OfferPrice { get; set; }
		public bool HasSpecialOffer { get; set; }
		public string SpecialOfferPrice { get; set; }
		public string ButtonText { get; set; }
		public string ButtonLink { get; set; }
        public SubscriptionTypes? SubscriptionType { get; set; }
        public decimal? Price { get; set; }
	}
}
