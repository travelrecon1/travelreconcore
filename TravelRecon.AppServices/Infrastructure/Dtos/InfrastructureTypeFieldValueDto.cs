﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TravelRecon.AppServices.Infrastructure.Dtos
{
    public class InfrastructureTypeFieldValueDto
    {
        public int Id { get; set; }

        public int InfrastructureId { get; set; }

        public int InfrastructureTypeFieldId { get; set; }

        public int TypeId { get; set; }

        public string Value { get; set; }
    }
}
