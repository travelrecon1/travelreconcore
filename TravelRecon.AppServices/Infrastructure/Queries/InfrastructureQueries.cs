﻿using System;
using System.Data.SqlTypes;
using System.Linq;
using TravelRecon.AppServices.Infrastructure.Dtos;
using TravelRecon.AppServices.Infrastructure.ViewDtos;
using TravelRecon.AppServices.SharedDtos;
using TravelRecon.Domain.Infrastructure;

namespace TravelRecon.AppServices.Infrastructure.Queries
{
    public static class InfrastructureQueries
    {
        public static IQueryable<InfrastructureDto> SelectDto(this IQueryable<Domain.Infrastructure.Infrastructure> query)
        {
            return query.Select(x => new InfrastructureDto
            {
                Id = x.Id,
                TypeId = x.TypeId,
                Type = x.Type.Name,
                Name = x.Name,
                Country = x.Country,
                City = x.City,
                Lat = x.Lat,
                Long = x.Long,
                UserId = x.UserId,
                RiskRating = x.RiskRating,
                TypeIcon = x.Type.Icon ?? "fa-building"
            });
        }

        public static Domain.Infrastructure.Infrastructure ConvertFromInfrastructureDto(this InfrastructureDto infDto)
        {
            return new Domain.Infrastructure.Infrastructure()
            {
                Id = infDto.Id,
                TypeId = infDto.TypeId,
                City = infDto.City,
                Country = infDto.Country,
                Lat = infDto.Lat,
                Long = infDto.Long,
                UserId = infDto.UserId,
                Created = DateTimeOffset.Now,
                RiskRating = infDto.RiskRating,
                Name = infDto.Name,
                Description = infDto.Description,
                Address = infDto.Address
            };
        }

        public static void MapInfrastructureDto(this InfrastructureDto infDto, Domain.Infrastructure.Infrastructure infr)
        {
            infr.TypeId = infDto.TypeId;
            infr.City = infDto.City;
            infr.Country = infDto.Country;
            infr.Lat = infDto.Lat;
            infr.Long = infDto.Long;
            infr.UserId = infDto.UserId;
            infr.Created = DateTimeOffset.Now;
            infr.RiskRating = infDto.RiskRating;
            infr.Description = infDto.Description;
            infr.Address = infDto.Address;
        }

        public static IQueryable<InfrastructureTypeDto> SelectDto(
            this IQueryable<Domain.Infrastructure.InfrastructureType> queryable)
        {
            return queryable.Select(x => new InfrastructureTypeDto()
            {
                Id = x.Id,
                Name = x.Name,
                Icon = x.Icon ?? "fa-building"
            });
        }

        public static Domain.Infrastructure.InfrastructureType ConvertFromInfrastructureTypeDto(this InfrastructureTypeDto infDto)
        {
            return new Domain.Infrastructure.InfrastructureType()
            {
                Id = infDto.Id,
                Name = infDto.Name,
                Icon = infDto.Icon
            };
        }

        public static InfrastructureTypeField ConvertTo(this InfrastructureTypeFieldDto dto)
        {
            return new InfrastructureTypeField()
            {
                Id = dto.Id,
                Name = dto.Name,
                TypeId = dto.TypeId
            };
        }

        public static IQueryable<InfrastructureTypeFieldDto> ConvertFrom(this IQueryable<InfrastructureTypeField> domain)
        {
            return domain.Select(x => new InfrastructureTypeFieldDto()
            {
                Id = x.Id,
                Name = x.Name,
                TypeId = x.TypeId
            });
        }

        public static InfrastructureFieldValue ConvertTo(this InfrastructureTypeFieldValueDto dto)
        {
            return new InfrastructureFieldValue()
            {
                Id = dto.Id,
                Value = dto.Value,
                TypeId = dto.TypeId,
                InfrastructureId = dto.InfrastructureId,
                InfrastructureTypeFieldId = dto.InfrastructureTypeFieldId
            };
        }

        public static IQueryable<InfrastructureTypeFieldValueDto> ConvertFrom(this IQueryable<InfrastructureFieldValue> domain)
        {
            return domain.Select(x => new InfrastructureTypeFieldValueDto()
            {
                Id = x.Id,
                Value = x.Value,
                TypeId = x.TypeId,
                InfrastructureId = x.InfrastructureId,
                InfrastructureTypeFieldId = x.InfrastructureTypeFieldId
            });
        }

        public static IQueryable<Domain.Infrastructure.Infrastructure> Filter(this IQueryable<Domain.Infrastructure.Infrastructure> query, 
            InfrastructureFilterDto filter)
        {
            return query
                .FilterByTypes(filter.Types.ToArray())
                .FilterByCity(filter.City)
                .FilterByCountry(filter.Country);
        }

        public static IQueryable<Domain.Infrastructure.Infrastructure> FilterByTypes(this IQueryable<Domain.Infrastructure.Infrastructure> query, 
            int[] types)
        {
            return query.Where(x => types.Contains(x.TypeId));
        }

        public static IQueryable<Domain.Infrastructure.Infrastructure> FilterByCity(this IQueryable<Domain.Infrastructure.Infrastructure> query,
            string city)
        {
            if (string.IsNullOrWhiteSpace(city))
                return query;

            return query.Where(x => x.City == city);
        }

        public static IQueryable<Domain.Infrastructure.Infrastructure> FilterByCountry(this IQueryable<Domain.Infrastructure.Infrastructure> query,
            string country)
        {
            if (string.IsNullOrWhiteSpace(country))
                return query;

            return query.Where(x => x.Country == country);
        }
    }
}