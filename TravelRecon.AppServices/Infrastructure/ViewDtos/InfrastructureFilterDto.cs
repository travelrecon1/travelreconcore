﻿using System.Collections.Generic;

namespace TravelRecon.AppServices.Infrastructure.ViewDtos
{
    public class InfrastructureFilterDto
    {
        public List<int> Types { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
    }
}
