﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using TravelRecon.AppServices.Infrastructure.Dtos;
using TravelRecon.AppServices.Infrastructure.Queries;
using TravelRecon.AppServices.Infrastructure.ViewDtos;
using TravelRecon.AppServices.Interfaces;
using TravelRecon.AppServices.SharedDtos;
using TravelRecon.Common.DynamicLinq;
using TravelRecon.Domain.Infrastructure;
using TravelRecon.Domain.RepositoryInterfaces;

namespace TravelRecon.AppServices.Infrastructure
{
    public class InfrastructureService : IAppService
    {
        private readonly IRepository<Domain.Infrastructure.Infrastructure> _infrostructureRepository;
        private readonly IRepository<InfrastructureType> _infrostructureTypeRepository;
        private readonly IRepository<InfrastructureFieldValue> _infrastructureFieldValuesRepository;

        public InfrastructureService(
            IRepository<Domain.Infrastructure.Infrastructure> infrostructureRepository,
            IRepository<InfrastructureType> infrostructureTypeRepository,
            IRepository<InfrastructureFieldValue> infrastructureFieldValuesRepository,
            IRepository<InfrastructureTypeField> infrastructureTypeFieldRepository)
        {
            _infrostructureRepository = infrostructureRepository;
            _infrostructureTypeRepository = infrostructureTypeRepository;
            _infrastructureFieldValuesRepository = infrastructureFieldValuesRepository;
        }

        public Task<List<InfrastructureDto>> GetAll()
        {
            return _infrostructureRepository.GetAll()
                .SelectDto()
                .ToListAsync();
        }

        public Task<List<InfrastructureDto>> GetFilteredAsync(InfrastructureFilterDto filterModel)
        {
            return _infrostructureRepository.GetAll()
                .Filter(filterModel)
                .SelectDto()
                .ToListAsync();
        }

        public async Task<InfrastructureDto> Get(int infrastructureId)
        {
            var infrastructure = await _infrostructureRepository.GetAll()
                .SelectDto()
                .FirstOrDefaultAsync(x => x.Id == infrastructureId);

            infrastructure.CustomFieldsValuesCollection = await this.GetCombinedInfrastructureFieldAndValues(infrastructure.Id);
            return infrastructure;
        }

        public bool Save(InfrastructureDto dto)
        {
            Domain.Infrastructure.Infrastructure infr = null;
            if (dto.Id == 0)
            {
                infr = _infrostructureRepository.Add(dto.ConvertFromInfrastructureDto());
            }
            else
            {
                infr = _infrostructureRepository.GetAll()
                    .FirstOrDefault(x => x.Id == dto.Id);

                int typeId = infr.TypeId;
                if (dto.Id != typeId)
                {
                    var valuesForDelete =
                        _infrastructureFieldValuesRepository.GetAll().Where(x => x.InfrastructureId == infr.Id).ToList();
                    _infrastructureFieldValuesRepository.DeleteRange(valuesForDelete);
                }
                dto.MapInfrastructureDto(infr);
                _infrostructureRepository.Modified(infr);
            }
          
            _infrostructureRepository.SaveChanges();
            //here after save infrastructure we save custom fields dto;
            var valuesCollection = dto.CustomFieldsValuesCollection.ConvertAll(
                (x) =>
                    new InfrastructureTypeFieldValueDto()
                    {
                        Id = x.Id,
                        InfrastructureId = infr.Id,
                        TypeId = infr.TypeId,
                        InfrastructureTypeFieldId = x.FieldId,
                        Value = x.FieldValue
                    });
            //after save
            if(valuesCollection.Count > 0)
                this.SaveInfrastructureFieldValues(infr.Id, valuesCollection.ToArray());
            return true;
        }

        public Task<List<InfrastructureTypeDto>> GetAllInfrastructureTypesAsync()
        {
            return _infrostructureTypeRepository
                .GetAll()
                .SelectDto()
                .ToListAsync();
        }

        public Task<InfrastructureTypeDto> GetInfrastructureTypeAsync(int infrastructureTypeId)
        {
            return _infrostructureTypeRepository.GetAll()
                .SelectDto()
                .FirstOrDefaultAsync(x => x.Id == infrastructureTypeId);
        }

        public bool SaveInfrastructureType(InfrastructureTypeDto dto)
        {
            if (dto.Id == 0)
            {
                var type = _infrostructureTypeRepository.Add(dto.ConvertFromInfrastructureTypeDto());
                _infrostructureTypeRepository.SaveChanges();
                var customFields = new List<InfrastructureTypeFieldDto>();
                foreach (var cutomField in dto.CustomFields)
                {
                    customFields.Add(new InfrastructureTypeFieldDto()
                    {
                        TypeId = type.Id,
                        Name = cutomField
                    });
                }
                this.SaveInfrastructureFields(type.Id, customFields.ToArray());
                return true;
            }
            else
            {
                _infrostructureTypeRepository.Modified(dto.ConvertFromInfrastructureTypeDto());
            }
            _infrostructureTypeRepository.SaveChanges();
            return true;
        }

        public Task<DataSourceResult<InfrastructureDto>> GetInfrastructureDataSourceAsync(DataSourceRequest request)
        {
            return _infrostructureRepository.GetAll()
                .SelectDto()
                .OrderBy(x => x.Name)
                .ToDataSourceResultAsync(request);
        }

        public void SaveInfrastructureFields(int typeId, params InfrastructureTypeFieldDto[] infrastructureTypeFields)
        {
            var infrastructureType = _infrostructureTypeRepository.Get(typeId);
            var listTypeFields = infrastructureTypeFields.ToList();
            var domainList = listTypeFields.ConvertAll(x => x.ConvertTo());

            infrastructureType.FieldsCollection = domainList;

            _infrostructureTypeRepository.SaveChanges();
        }

        public void SaveInfrastructureFieldValues(int infrastructureId, params InfrastructureTypeFieldValueDto[] values)
        {
            var infrastructure =_infrostructureRepository.GetAll().Include(x => x.ValuesCollection).FirstOrDefault(x => x.Id == infrastructureId);
            var listFieldValues = values.ToList().ConvertAll(x => x.ConvertTo());
           
            if (listFieldValues.FirstOrDefault().Id != 0 && infrastructure.ValuesCollection != null && infrastructure.ValuesCollection.Count != 0)
            {
                foreach (var item in listFieldValues)
                {
                    var customField = infrastructure.ValuesCollection.FirstOrDefault(x => x.InfrastructureTypeFieldId == item.InfrastructureTypeFieldId);                       
                     customField.Value = item.Value;
                    _infrastructureFieldValuesRepository.Modified(customField);
                }
            }
            else
            {
                infrastructure.ValuesCollection = listFieldValues;
            }
                      
            _infrostructureRepository.SaveChanges();
        }

        public async Task<List<CustomFieldsValuesDto>> GetnfrastructureFields(int typeId)
        {
            var infrastructureType =
                await _infrostructureTypeRepository.GetAll()
                    .Include(x => x.FieldsCollection)
                    .Where(x => x.Id == typeId)
                    .FirstOrDefaultAsync();

            return ConvertToCombinedInfrastructureFieldAndValues(new List<InfrastructureFieldValue>(), infrastructureType.FieldsCollection.ToList());
        }

        public async Task<List<CustomFieldsValuesDto>> GetCombinedInfrastructureFieldAndValues(int infrastructureId)
        {
            var infrastructure = 
                await _infrostructureRepository.GetAll()
                     .Include(x => x.ValuesCollection).Where( x => x.Id == infrastructureId).FirstOrDefaultAsync();
            var infrastructureType =
                await _infrostructureTypeRepository.GetAll()
                    .Include(x => x.FieldsCollection)
                    .Where(x => x.Id == infrastructure.TypeId)
                    .FirstOrDefaultAsync();

            return ConvertToCombinedInfrastructureFieldAndValues(infrastructure.ValuesCollection.ToList(), infrastructureType.FieldsCollection.ToList());
        }

        private List<CustomFieldsValuesDto> ConvertToCombinedInfrastructureFieldAndValues(List<InfrastructureFieldValue> values, List<InfrastructureTypeField> fields)
        {
            var listWithValues = new List<CustomFieldsValuesDto>();
            foreach (var field in fields)
            {
                var value = values.FirstOrDefault(x => x.InfrastructureTypeFieldId == field.Id);
                if (value != null)
                    listWithValues.Add(new CustomFieldsValuesDto() { FieldId = field.Id, FieldName = field.Name, FieldValue = value.Value, Id = value.Id });
                else
                {
                    listWithValues.Add(new CustomFieldsValuesDto() { FieldId = field.Id, FieldName = field.Name, FieldValue = string.Empty});
                }
            }

            return listWithValues;
        }
    }
}
