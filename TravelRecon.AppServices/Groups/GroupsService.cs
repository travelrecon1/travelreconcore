﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using TravelRecon.AppServices.Groups.Queries;
using TravelRecon.AppServices.Groups.ViewDtos;
using TravelRecon.AppServices.Interfaces;
using TravelRecon.AppServices.SharedDtos;
using TravelRecon.AppServices.Users;
using TravelRecon.AppServices.Users.ViewDtos;
using TravelRecon.Common.DynamicLinq;
using TravelRecon.Common.Exceptions;
using TravelRecon.Data;
using TravelRecon.Domain.Groups;
using TravelRecon.Domain.Identity;
using TravelRecon.Domain.License;
using TravelRecon.Domain.Localization;
using TravelRecon.Domain.Localization.QueryObjects;
using TravelRecon.Domain.RepositoryInterfaces;
using LeaveFromGroupDto = TravelRecon.AppServices.Groups.ViewDtos.LeaveFromGroupDto;

namespace TravelRecon.AppServices.Groups
{
    public class GroupsService : IAppService
    {
        private readonly TravelReconDbContext _dbContext;
        private readonly IRepository<Group> _groupRepository;
        private readonly IRepository<User> _userRepository;
        private readonly IRepository<UserLicense> _userLicenseRepository;
        private readonly IRepository<UserGroupRelation> _userGroupRelationRepository;
        private readonly IRepository<Label> _labelRepository;
        private readonly UserRelationshipService _userRelationshipService;

        public GroupsService(TravelReconDbContext dbContext, 
            IRepository<Group> groupRepository, 
            IRepository<UserGroupRelation> userGroupRelationRepository,
            IRepository<Label> labelRepository,
            UserRelationshipService userRelationshipService,
			IRepository<User> userRepository,
            IRepository<UserLicense> userLicenseRepository)
        {
            _dbContext = dbContext;
            _groupRepository = groupRepository;
            _userGroupRelationRepository = userGroupRelationRepository;
            _labelRepository = labelRepository;
            _userRelationshipService = userRelationshipService;
	        _userRepository = userRepository;
            _userLicenseRepository = userLicenseRepository;
        }

        public Tuple<bool, string> AddToGroupAfterRegisterIfInvited(int inviterId, InivteToGroupRequestDto dto)
        {
            _userGroupRelationRepository.Add(new UserGroupRelation
            {
                GroupId = dto.GroupId,
                UserId = dto.UserId,
                UserGroupRelationType = UserGroupRelationType.Accepted,
                UserGroupRole = UserGroupRole.User,
                UpdatedById = inviterId
            });

            _dbContext.SaveChanges();

            return new Tuple<bool, string>(true, "Successfully added in group");

        }

        public async Task<UserGroupRelationDto> InviteToGroup(InivteToGroupRequestDto dto, User currentUser)
        {
            var isAdmin = _groupRepository.GetAll().IsGroupAdmin(dto.GroupId, currentUser.Id);

            if (!isAdmin)
                throw new Exception("You not administrator in this group");

            var userGroupRelation = await _userGroupRelationRepository.GetAll()
                .GetUserGroupRelationAsync(dto.UserId, dto.GroupId);

            if (userGroupRelation != null)
            {
                if (userGroupRelation.UserGroupRelationType == UserGroupRelationType.Accepted)
                    return GetDto(userGroupRelation);

                userGroupRelation.UserGroupRelationType = UserGroupRelationType.InvitedByAdministrator;
                userGroupRelation.UpdatedById = currentUser.Id;
                _userGroupRelationRepository.Modified(userGroupRelation);

                _dbContext.SaveChanges();

                return GetDto(userGroupRelation);
            }

            userGroupRelation = _userGroupRelationRepository.Add(new UserGroupRelation
            {
                GroupId = dto.GroupId,
                UserId = dto.UserId,
                UserGroupRelationType = UserGroupRelationType.InvitedByAdministrator,
                UserGroupRole = UserGroupRole.User,
                UpdatedById = currentUser.Id
            });

            _dbContext.SaveChanges();

            return GetDto(userGroupRelation);
        }

        public async Task<UserGroupRelationDto> AcceptInvite(int groupId, User currentUser)
        {
            var userGroupRelation = await _userGroupRelationRepository.GetAll()
                .GetUserGroupRelationAsync(currentUser.Id, groupId);

            if (userGroupRelation == null)
                throw new Exception("Relation not found");

            if (userGroupRelation.UserGroupRelationType != UserGroupRelationType.InvitedByAdministrator)
                throw new Exception("This relation is not invite by administrator");

            userGroupRelation.UserGroupRelationType = UserGroupRelationType.Accepted;
            userGroupRelation.UpdatedById = currentUser.Id;
            _userGroupRelationRepository.Modified(userGroupRelation);

            await _dbContext.SaveChangesAsync();

            return GetDto(userGroupRelation);
        }

        public Task<List<GroupDto>> GetUserGroups(int userId)
        {
            return _userGroupRelationRepository
                .GetAll()
                .GetGroupsByUserId(userId)
                .SelectGroupDto()
                .ToListAsync();
        }

        public async Task<List<GroupDto>> GetUserAllowedLeaveGroups(int userId)
        {
            var managerId =
                await _userLicenseRepository.GetAll()
                    .Where(x => x.OwnerId == userId)
                    .Select(x => x.ManagerId)
                    .FirstOrDefaultAsync();

            var managerGroupsQuery = _groupRepository.GetAll()
                .FindOwnedGroups(managerId.Value);

            var groups = await _userGroupRelationRepository
                .GetAll()
                .GetGroupsByUserId(userId)
                .Where(x => !managerGroupsQuery.Any(mg => mg.Id == x.GroupId))
                .SelectGroupDto()
                .ToListAsync();

            return groups;
        }

        public IEnumerable<UserViewModel> GetUsersInGroup(int groupId, User currentUser)
        {
            var users = _userGroupRelationRepository
                .GetAll()
                .GetUsersByGroupId(groupId)
                .ToList();

            return users.Select(GetVm).ToList();
        }

        public Task<List<UserGroupRelationMaxDto>> GetRelationsInsideGroupAsync(User currentUser, int groupId)
        {
            return _userGroupRelationRepository.GetAll()
                .GetRelationsInsideGroup(groupId, currentUser.Id)
                .SelectUserGroupRelationMaxDto()
                .ToListAsync();
        }

        public bool HasAvailableGroups(User currentUser, int contactId)
        {
            var available = _groupRepository.GetAll()
                .Any(x => x.UserGroupRelations.Any(ug => ug.UserId == currentUser.Id && ug.UserGroupRole == UserGroupRole.Administrator)
                    && (x.UserGroupRelations.All(ug => ug.UserId != contactId)
                        || x.UserGroupRelations.Any(ug => ug.UserId == contactId &&
                                    (ug.UserGroupRelationType == UserGroupRelationType.None ||
                                     ug.UserGroupRelationType == UserGroupRelationType.RejectedByAdministrator))));

            return available;
        }

        public IEnumerable<UserMinDto> GetIndividuals(User currentUser)
        {
            var contacts = _userRelationshipService.GetContacts(currentUser.Id).ToList();
            var contactIds = contacts.Select(x => x.Id).ToList();

            var availableInvites = _groupRepository.GetAll()
                .Where(x => x.UserGroupRelations.Any(ug => ug.UserId == currentUser.Id && ug.UserGroupRole == UserGroupRole.Administrator))
                .Select(x => contactIds.Select(userId => new
                {
                    UserId = userId,
                    CanInvite = x.UserGroupRelations.All(ug => ug.UserId != userId)
                            || x.UserGroupRelations.Any(ug => ug.UserId == userId &&
                                    (ug.UserGroupRelationType == UserGroupRelationType.None ||
                                     ug.UserGroupRelationType == UserGroupRelationType.RejectedByAdministrator))
                }))
                .SelectMany(x => x.ToList())
                .GroupBy(x => x.UserId)
                .Select(x => new
                {
                    UserId = x.Key,
                    CanInvite = x.Any(i => i.CanInvite)
                })
                .ToDictionary(x => x.UserId, x => x.CanInvite);

            foreach (var c in contacts)
                c.CanInviteToGroup = availableInvites.ContainsKey(c.Id) && availableInvites[c.Id];

            return contacts;
        }

        public Task<List<GroupDto>> GetAvalibleGroups(User currentUser, int userId)
        {
            return _groupRepository.GetAll()
                .Where(x => x.UserGroupRelations.Any(ug => ug.UserId == currentUser.Id && ug.UserGroupRole == UserGroupRole.Administrator)
                        && (x.UserGroupRelations.All(ug => ug.UserId != userId)
                            || x.UserGroupRelations.Any(ug => ug.UserId == userId &&
                                    (ug.UserGroupRelationType == UserGroupRelationType.None ||
                                     ug.UserGroupRelationType == UserGroupRelationType.RejectedByAdministrator))))
                .OrderBy(x => x.Name)
                .SelectGroupDto()
                .ToListAsync();
        }

        public async Task<GroupInviteViewModel> GetGroupInvitePage(User currentUser, int userId)
        {
            var vm = new GroupInviteViewModel();

            var targetLang = currentUser.GetLanguageSetting();
            var labelsToRetrive = new[] {LabelType.GroupName, LabelType.InvitationToGroup, LabelType.UserName,
                LabelType.GroupsNotFound, LabelType.Submit, LabelType.Group, LabelType.GroupInvite,
				LabelType.InviteToGroup, LabelType.WhenTheUserHasNoGroupsToListHere,
				LabelType.RemoveUser, LabelType.RemoveFromGroup};
            var localizedLabels = _labelRepository.GetAll()
                    .FindLabelsLocalized(labelsToRetrive, targetLang)
                    .ToDictionary(x => (int) x.LabelType, x => x.Content);

            vm.Groups = await GetAvalibleGroups(currentUser, userId);
            vm.ConsistsGroups = await GetUserConsistsGroups(currentUser, userId);
            vm.Labels = localizedLabels;

            return vm;
        }

	    public Task<List<GroupDto>> GetUserConsistsGroups(User currentUser, int userId)
	    {
		    var groups = _groupRepository.GetAll()
				.Where(group => group.UserGroupRelations
					.Any(relation => relation.UserId == currentUser.Id && relation.UserGroupRole == UserGroupRole.Administrator))
				.Where(group => group.UserGroupRelations
					.Any(relation => relation.UserId == userId && relation.UserGroupRelationType == UserGroupRelationType.Accepted));

		    return groups.SelectGroupDto()
			    .ToListAsync();
		}

	    public async Task RemoveUserFromAllGroups(User currentUser, int userId)
	    {
		    var currentUserGroups = await GetUserConsistsGroups(currentUser, userId);
		    var currentUserGroupsIds = currentUserGroups.Select(x => x.Id).ToList();

	        var relations = await _userGroupRelationRepository.GetAll()
	            .Where(x => currentUserGroupsIds.Contains(x.GroupId) && x.UserId == userId)
	            .ToListAsync();

		    foreach (var r in relations)
			    await RemoveFromGroupAsync(r.Id, currentUser.Id);

			var removedUserGroups = await GetUserConsistsGroups(new User {Id = userId}, currentUser.Id);
			var removedUserGroupsIds = removedUserGroups.Select(x => x.Id).ToList();

            relations = await _userGroupRelationRepository.GetAll()
                .Where(x => removedUserGroupsIds.Contains(x.GroupId) && x.UserId == currentUser.Id)
                .ToListAsync();

            foreach (var r in relations)
                await RemoveFromGroupAsync(r.Id, userId);

		    var groups = _userGroupRelationRepository.GetAll()
			    .Where(x => x.UserId == currentUser.Id && x.UserGroupRole == UserGroupRole.Administrator);

		    var invites = _userGroupRelationRepository.GetAll()
			    .Where(
				    x =>
					    groups.Any(g => g.GroupId == x.GroupId) && x.UserId == userId &&
					    x.UserGroupRelationType == UserGroupRelationType.InvitedByAdministrator)
			    .ToList();

		    foreach (var invite in invites)
		    {
			    invite.UserGroupRelationType = UserGroupRelationType.RejectedByUser;
				_userGroupRelationRepository.Modified(invite);
		    }

			_userGroupRelationRepository.SaveChanges();
	    }

	    public IEnumerable<GroupContactDto> GetContactsToInviteToGroup(User currentUser, int groupId)
	    {
			var contacts = _userRelationshipService.GetContacts(currentUser.Id).ToList();

			var group = _groupRepository.Get(groupId);
		    var relations = _userGroupRelationRepository.GetAll().Where(gr => gr.GroupId == group.Id).ToList();

		    var groupContacts = new List<GroupContactDto>();

		    foreach (var contact in contacts)
		    {
			    var groupRelation = relations.FirstOrDefault(gr => gr.UserId == contact.Id);

				if (groupRelation == null)
			    {
				    groupContacts.Add(new GroupContactDto
				    {
					    UserId = contact.Id,
					    UserName = contact.UserName,
						DrupalId = contact.DrupalId,
					    GroupRelationType = UserGroupRelationType.None
				    });
			    }
			    else
			    {
				    if (groupRelation.UserGroupRelationType != UserGroupRelationType.Accepted 
						&& groupRelation.UserGroupRole != UserGroupRole.Administrator)
				    {
					    groupContacts.Add(new GroupContactDto
					    {
						    UserId = contact.Id,
							UserName = contact.UserName,
							DrupalId = contact.DrupalId,
							GroupRelationType = groupRelation.UserGroupRelationType
					    });
				    }
			    }
		    }

		    return groupContacts;
	    }

	    public IEnumerable<UserViewModel> GetContactsToRemoveFromGroup(User currentUser, int groupId)
	    {
			return GetUsersInGroup(groupId, currentUser).Where(x => x.Id != currentUser.Id);
		}

        private UserGroupRelationDto GetDto(UserGroupRelation userGroupRelation)
        {
            return new UserGroupRelationDto
            {
                GroupId = userGroupRelation.GroupId,
                UserGroupRelationType = userGroupRelation.UserGroupRelationType,
                UserGroupRole = userGroupRelation.UserGroupRole,
                UserId = userGroupRelation.UserId,
                Id = userGroupRelation.Id
            };
        }

        private UserViewModel GetVm(User user)
        {
            return new UserViewModel
            {
                Id = user.Id,
                UserName = user.UserName,
                Email = user.Email,
                FirstName = user.FirstName,
                LastName = user.LastName
            };
        }

        public Task<List<GroupDto>> GetGroupsAsync(int userId)
        {
            return _userGroupRelationRepository.GetAll()
                .Where(x => x.UserId == userId 
                    && x.UserGroupRelationType == UserGroupRelationType.Accepted 
                    && x.UserGroupRole == UserGroupRole.User)
                .Select(x => x.Group)
                .SelectGroupDto()
                .ToListAsync();
        }

        public Task<List<GroupDto>> GetManageGroupsAsync(int userId)
        {
            return _userGroupRelationRepository.GetAll()
                .Where(x => x.UserId == userId 
                    && x.UserGroupRelationType == UserGroupRelationType.Accepted 
                    && x.UserGroupRole == UserGroupRole.Administrator)
                .Select(x => x.Group)
                .SelectGroupDto()
                .ToListAsync();
        }

        public Task<DataSourceResult<GroupDto>> GetGroupsAsync(DataSourceRequest request)
        {
            return _groupRepository.GetAll()
                .SelectGroupDto()
                .ToDataSourceResultAsync(request);
        }


        public async Task LeaveFromGroups(LeaveFromGroupDto groupIds, User currentUser)
        {
            foreach (var groupId in groupIds.groupids)
            {
                var userGroupRelation = await _userGroupRelationRepository.GetAll()
                    .GetUserGroupRelationAsync(currentUser.Id, groupId);

                if (userGroupRelation == null)
                    throw new Exception("Relation not found");

                if (userGroupRelation.UserGroupRelationType == UserGroupRelationType.RejectedByUser ||
                    userGroupRelation.UserGroupRelationType == UserGroupRelationType.RejectedByAdministrator)
                    return;

                userGroupRelation.UserGroupRelationType = UserGroupRelationType.RejectedByUser;
                userGroupRelation.UpdatedById = currentUser.Id;
                _userGroupRelationRepository.Modified(userGroupRelation);
            }
            await _dbContext.SaveChangesAsync();
        }

        public async Task CreateGroupAsync(EditGroupVm model, int userId)
        {
            using (_groupRepository.BeginTransaction())
            {
                var group = new Group
                {
                    CreatedDate = DateTime.UtcNow,
                    Icon = Group.DefaultColor,
                    Name = model.Name
                };

                _groupRepository.Add(group);
                await _groupRepository.SaveChangesAsync();

                var userGroup = new UserGroupRelation
                {
                    UserId = userId,
                    GroupId = group.Id,
                    UserGroupRelationType = UserGroupRelationType.Accepted,
                    UserGroupRole = UserGroupRole.Administrator,
                    UpdatedById = userId
                };

                _userGroupRelationRepository.Add(userGroup);
                await _userGroupRelationRepository.SaveChangesAsync();

                _groupRepository.CommitTransaction();
            }
        }

        public async Task EditGroupAsync(int groupId, EditGroupVm model, int userId)
        {
            var group = await _groupRepository.GetAll()
                .FindOwnedGroup(groupId, userId)
                .FirstOrDefaultAsync();

            group.Name = model.Name;
            group.Description = model.Description;

            _groupRepository.Modified(group);
            await _groupRepository.SaveChangesAsync();
        }

        public async Task DeleteGroupAsync(int groupId, int userId)
        {
            var group = await _groupRepository.GetAll()
                .FindOwnedGroup(groupId, userId)
                .FirstOrDefaultAsync();

            var userGroupRelations = await _userGroupRelationRepository.GetAll()
                .Where(x => x.GroupId == groupId)
                .ToListAsync();

            _userGroupRelationRepository.DeleteRange(userGroupRelations);
            _groupRepository.Delete(group);
            await _groupRepository.SaveChangesAsync();
        }

        public async Task AddUsersToGroupAsync(AddUsersToGroupVm model, int userId)
        {
            if (!model.UsersIds.Any())
                throw new ValidationException("Users required");

            var oldRelations = await _userGroupRelationRepository.GetAll()
                .Where(x => model.UsersIds.Contains(x.UserId) && x.GroupId == model.GroupId)
                .ToListAsync();

            _userGroupRelationRepository.DeleteRange(oldRelations);

            var newRelations = model.UsersIds.Select(x => new UserGroupRelation
            {
                GroupId = model.GroupId,
                UserId = x,
                UserGroupRelationType = UserGroupRelationType.Accepted,
                UserGroupRole = UserGroupRole.User,
                UpdatedById = userId
            }).ToList();

            _userGroupRelationRepository.AddRange(newRelations);

            await _userGroupRelationRepository.SaveChangesAsync();
        }

        public Task<GroupDto> GetGroupAsync(int groupId)
        {
            return _groupRepository.GetAll()
                .Where(x => x.Id == groupId)
                .SelectGroupDto()
                .FirstOrDefaultAsync();
        }

        public Task<List<UserGroupRelationMaxDto>> GetParticipantsAsync(int groupId)
        {
            return _userGroupRelationRepository.GetAll()
                .GetParticipants(groupId)
                .SelectUserGroupRelationMaxDto()
                .ToListAsync();
        }

        public Task<List<UserGroupRelationMaxDto>> GetRequestsToGroupAsync(int groupId, int userId)
        {
            var isAdmin = _groupRepository.GetAll()
                .IsGroupAdmin(groupId, userId);

            if (!isAdmin)
                throw new Exception("You not administrator in this group");

            return _userGroupRelationRepository.GetAll()
                .GetRequestsToGroup(groupId)
                .SelectUserGroupRelationMaxDto()
                .ToListAsync();
        }

        public async Task<GroupAvailableActionsDto> GetGroupAvailableActionsAsync(int groupId, int userId)
        {
            var managerId = await _userLicenseRepository.GetAll()
                .Where(x => x.OwnerId == userId)
                .Select(x => x.ManagerId)
                .FirstOrDefaultAsync();

            var actions = await _groupRepository.GetAll()
                .Where(x => x.Id == groupId)
                .SelectAvailableActionsDto(userId, managerId)
                .FirstOrDefaultAsync();

            return actions;
        }

        public async Task AcceptToGroupAsync(int relationtId, int userId)
        {
            var userGroupRelation = await _userGroupRelationRepository.GetAll()
                .Where(x => x.Id == relationtId)
                .FirstOrDefaultAsync();

            if (userGroupRelation == null)
                throw new Exception("Relation not found");

            var isAdmin = _groupRepository.GetAll()
                .IsGroupAdmin(userGroupRelation.GroupId, userId);

            if (!isAdmin)
                throw new Exception("You not administrator in this group");

            if (userGroupRelation.UserGroupRelationType != UserGroupRelationType.RequestedByUser)
                throw new Exception("This relation is not request by user");

            userGroupRelation.UserGroupRelationType = UserGroupRelationType.Accepted;
            userGroupRelation.UpdatedById = userId;
            _userGroupRelationRepository.Modified(userGroupRelation);

            await _dbContext.SaveChangesAsync();
        }

        public async Task SendRequestAsync(int groupId, int userId)
        {
            var userGroupRelation = await _userGroupRelationRepository.GetAll()
                .GetUserGroupRelationAsync(userId, groupId);

            if (userGroupRelation != null)
            {
                if (userGroupRelation.UserGroupRelationType == UserGroupRelationType.RejectedByAdministrator ||
                    userGroupRelation.UserGroupRelationType == UserGroupRelationType.Accepted)
                    return;

                userGroupRelation.UserGroupRelationType = UserGroupRelationType.RequestedByUser;
                userGroupRelation.UpdatedById = userId;
                _userGroupRelationRepository.Modified(userGroupRelation);

                await _dbContext.SaveChangesAsync();
            }
            else
            {
                _userGroupRelationRepository.Add(new UserGroupRelation
                {
                    GroupId = groupId,
                    UserId = userId,
                    UserGroupRelationType = UserGroupRelationType.RequestedByUser,
                    UserGroupRole = UserGroupRole.User,
                    UpdatedById = userId
                });

                await _dbContext.SaveChangesAsync();
            }
        }

        public async Task RemoveFromGroupAsync(int relationId, int userId)
        {
            var userGroupRelation = await _userGroupRelationRepository.GetAll()
                .GetUserGroupRelationAsync(relationId);

            if (userGroupRelation == null)
                throw new Exception("Relation not found");

            var isAdmin = _groupRepository.GetAll().IsGroupAdmin(userGroupRelation.GroupId, userId);

            if (!isAdmin)
                throw new Exception("You not administrator in this group");

            if (userGroupRelation.UserGroupRelationType == UserGroupRelationType.RejectedByUser ||
                userGroupRelation.UserGroupRelationType == UserGroupRelationType.RejectedByAdministrator)
                return;

            userGroupRelation.UserGroupRelationType = UserGroupRelationType.RejectedByAdministrator;
            userGroupRelation.UpdatedById = userId;
            _userGroupRelationRepository.Modified(userGroupRelation);

            await _dbContext.SaveChangesAsync();
        }

        public async Task LeaveFromGroupAsync(int userId, int groupId)
        {
            var userGroupRelation = await _userGroupRelationRepository.GetAll()
                .GetUserGroupRelationAsync(userId, groupId);

            if (userGroupRelation == null)
                throw new Exception("Relation not found");

            if (userGroupRelation.UserGroupRelationType == UserGroupRelationType.RejectedByUser ||
                userGroupRelation.UserGroupRelationType == UserGroupRelationType.RejectedByAdministrator)
                return;

            userGroupRelation.UserGroupRelationType = UserGroupRelationType.RejectedByUser;
            userGroupRelation.UpdatedById = userId;
            _userGroupRelationRepository.Modified(userGroupRelation);

            await _dbContext.SaveChangesAsync();
        }

        public Task<List<GroupDto>> GetAvailableGroupsAsync(int currentUserId)
        {
            var userGroupIdsQuery = _userGroupRelationRepository.GetAll()
                .Where(x => x.UserId == currentUserId)
                .Select(x => x.GroupId);

            return _groupRepository.GetAll()
                .Where(x => !userGroupIdsQuery.Contains(x.Id)
                    && !x.UserGroupRelations.Any(ugr => ugr.UserGroupRelationType == UserGroupRelationType.Accepted
                        && ugr.UserGroupRole == UserGroupRole.Administrator
                        && ugr.User.UserRoles.Any(ur => ur.UserRole.Id == (int)UserRoleTypeEnum.TravelReconProManager)))
                .SelectGroupDto()
                .ToListAsync();
        }
    }
}
