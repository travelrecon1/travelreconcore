﻿namespace TravelRecon.AppServices.Groups.ViewDtos
{
    public class ChangeGroupNameRequestDto
    {
        public int GroupId { get; set; }
        public string Name { get; set; }
    }
}
