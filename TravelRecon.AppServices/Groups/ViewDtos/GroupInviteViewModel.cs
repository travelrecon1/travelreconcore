﻿using System.Collections.Generic;

namespace TravelRecon.AppServices.Groups.ViewDtos
{
    public class GroupInviteViewModel
    {
        public IEnumerable<GroupDto> Groups { get; set; }
        public IEnumerable<GroupDto> ConsistsGroups { get; set; }
        public Dictionary<int, string> Labels { get; set; }  
    }
}
