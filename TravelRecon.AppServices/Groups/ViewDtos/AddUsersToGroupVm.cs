﻿using System.ComponentModel.DataAnnotations;

namespace TravelRecon.AppServices.Groups.ViewDtos
{
    public class AddUsersToGroupVm
    {
        [Required]
        public int GroupId { get; set; }
        [Required]
        public int[] UsersIds { get; set; }
    }
}
