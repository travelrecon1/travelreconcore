﻿using System;
using System.Linq;
using TravelRecon.Domain.Groups;

namespace TravelRecon.AppServices.Groups.ViewDtos
{
    public class GroupAvailableActionsDto
    {
        public int GroupId { get; set; }
        public bool CanEdit { get; set; }
        public bool CanRequest { get; set; }
        public bool CanLeave { get; set; }
    }
}
