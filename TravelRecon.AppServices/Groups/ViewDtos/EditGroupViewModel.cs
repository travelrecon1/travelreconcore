﻿using System.Collections.Generic;
using TravelRecon.AppServices.Users.ViewDtos;
using TravelRecon.Domain.Localization.QueryObjects;

namespace TravelRecon.AppServices.Groups.ViewDtos
{
    public class EditGroupViewModel
    {
        public bool IsOwner { get; set; }
        public GroupDto Group { get; set; }
        public IEnumerable<LabelLocalizedDto> Labels { get; set; }
	    public bool HasUsers { get; set; } 
    }
}
