﻿namespace TravelRecon.AppServices.Groups.ViewDtos
{
	public class MultipleGroupInviteRequestDto
	{
		public int GroupId { get; set; }
		public int[] UserIds { get; set; }
	}
}
