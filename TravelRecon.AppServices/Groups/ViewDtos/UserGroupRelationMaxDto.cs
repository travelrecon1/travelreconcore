﻿using System;
using TravelRecon.Domain.Groups;

namespace TravelRecon.AppServices.Groups.ViewDtos
{
    public class UserGroupRelationMaxDto
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int GroupId { get; set; }
        public string UserName { get; set; }
        public string UserFullName { get; set; }
        public string UserAddress { get; set; }
        public string UserAvatarUrl { get; set; }
        public DateTime? UserLastTrack { get; set; }
        public string GroupName { get; set; }
        public string UpdaterName { get; set; }

        public bool IsAdmin => UserGroupRole == UserGroupRole.Administrator && UserGroupRelationType == UserGroupRelationType.Accepted;

        public UserGroupRelationType UserGroupRelationType { get; set; }
        public UserGroupRole UserGroupRole { get; set; }
    }
}
