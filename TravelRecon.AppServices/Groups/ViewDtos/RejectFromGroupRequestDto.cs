﻿namespace TravelRecon.AppServices.Groups.ViewDtos
{
    public class RejectFromGroupRequestDto
    {
        public int UserId { get; set; }
        public int GroupId { get; set; }
    }
}
