﻿using TravelRecon.Domain.Identity;

namespace TravelRecon.AppServices.Groups.ViewDtos
{
	public class GroupInviteDto
	{
		public GroupDto Group { get; set; }
		public User InvitedBy { get; set; }
	}
}
