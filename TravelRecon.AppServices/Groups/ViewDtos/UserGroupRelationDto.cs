﻿using TravelRecon.Domain.Groups;

namespace TravelRecon.AppServices.Groups.ViewDtos
{
    public class UserGroupRelationDto
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int GroupId { get; set; }
        public UserGroupRelationType UserGroupRelationType { get; set; }
        public UserGroupRole UserGroupRole { get; set; }
    }
}
