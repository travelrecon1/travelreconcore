﻿using System.ComponentModel.DataAnnotations;

namespace TravelRecon.AppServices.Groups.ViewDtos
{
    public class EditGroupVm
    {
        [Required]
        public string Name { get; set; }

        public string Description { get; set; }
    }
}
