﻿using System;
using System.Linq;
using TravelRecon.Domain.Groups;

namespace TravelRecon.AppServices.Groups.ViewDtos
{
    public class GroupDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Icon { get; set; }
        public DateTime CreatedDate { get; set; }
        public int MembersCount { get; set; }
    }
}
