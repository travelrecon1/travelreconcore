﻿using System.Linq;
using TravelRecon.AppServices.Groups.ViewDtos;
using TravelRecon.Domain.Groups;

namespace TravelRecon.AppServices.Groups.Queries
{
    public static class GroupQueries
    {
        public static IQueryable<GroupDto> SelectGroupDto(this IQueryable<Group> query)
        {
            return query.Select(x => new GroupDto
            {
                Id = x.Id,
                CreatedDate = x.CreatedDate,
                Icon = x.Icon,
                Name = x.Name,
                Description = x.Description,
                MembersCount = x.UserGroupRelations.Count(ur => ur.UserGroupRelationType == UserGroupRelationType.Accepted)
            });
        }

        public static IQueryable<GroupDto> SelectGroupDto(this IQueryable<UserGroupRelation> query)
        {
            return query.Select(x => new GroupDto
            {
                Id = x.Group.Id,
                CreatedDate = x.Group.CreatedDate,
                Icon = x.Group.Icon,
                Name = x.Group.Name,
                Description = x.Group.Description,
                MembersCount = x.Group.UserGroupRelations.Count(ur => ur.UserGroupRelationType == UserGroupRelationType.Accepted)
            });
        }

        public static IQueryable<Group> FindOwnedGroup(this IQueryable<Group> query, int groupId, int userId)
        {
            return query.Where(x => x.Id == groupId
                    && x.UserGroupRelations.Any(ug => ug.UserId == userId && ug.UserGroupRole == UserGroupRole.Administrator));
        }

        public static bool IsGroupAdmin(this IQueryable<Group> groups, int groupId, int userId)
        {
            return groups.Any(x => x.Id == groupId &&
                                   x.UserGroupRelations.Any(
                                       ugr => ugr.UserGroupRole == UserGroupRole.Administrator && ugr.UserId == userId));
        }

        public static IQueryable<Group> FindOwnedGroups(this IQueryable<Group> query, int userId)
        {
            return query.Where(x => x.UserGroupRelations.Any(ug => ug.UserId == userId && ug.UserGroupRole == UserGroupRole.Administrator));
        }

        public static IQueryable<GroupAvailableActionsDto> SelectAvailableActionsDto(this IQueryable<Group> groupQuery,
            int userId, int? managerId)
        {
            return groupQuery.Select(x => new GroupAvailableActionsDto
            {
                CanEdit = x.UserGroupRelations.Any(ugr => ugr.UserId == userId 
                    &&  ugr.UserGroupRelationType == UserGroupRelationType.Accepted 
                    && ugr.UserGroupRole == UserGroupRole.Administrator),
                CanLeave = x.UserGroupRelations.Any(ugr =>  ugr.UserId == userId
                    && ugr.UserGroupRelationType == UserGroupRelationType.Accepted
                    && ugr.UserGroupRole != UserGroupRole.Administrator)
                    && !x.UserGroupRelations.Any(ugr => ugr.UserId == managerId
                    && ugr.UserGroupRelationType == UserGroupRelationType.Accepted 
                    && ugr.UserGroupRole == UserGroupRole.Administrator),
                CanRequest = !x.UserGroupRelations.Any(ugr => ugr.UserId == userId
                    && (ugr.UserGroupRelationType == UserGroupRelationType.Accepted 
                    || ugr.UserGroupRelationType == UserGroupRelationType.RequestedByUser
                    || ugr.UserGroupRelationType == UserGroupRelationType.InvitedByAdministrator))
            });
        }
    }
}
