﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using LinqKit;
using TravelRecon.AppServices.Groups.ViewDtos;
using TravelRecon.AppServices.Users.Queries;
using TravelRecon.Domain.Groups;
using TravelRecon.Domain.Identity;

namespace TravelRecon.AppServices.Groups.Queries
{
    public static class UserGroupRelationsExtensions
    {
        public static IQueryable<UserGroupRelation> GetGroupsByUserId(this IQueryable<UserGroupRelation> userGroupRelations, int userId)
        {
            return userGroupRelations.Where(x => x.UserId == userId && x.UserGroupRelationType == UserGroupRelationType.Accepted);
        }

        public static IQueryable<UserGroupRelation> FindOwnedGroup(this IQueryable<UserGroupRelation> query, int userId)
        {
            return query.Where(x => x.UserId == userId && x.UserGroupRole == UserGroupRole.Administrator);
        }

        public static IEnumerable<User> GetUsersByGroupId(this IQueryable<UserGroupRelation> userGroupRelations,
            int groupId)
        {
            return userGroupRelations.Where(
                    x => x.GroupId == groupId && x.UserGroupRelationType == UserGroupRelationType.Accepted)
                    .Select(x => x.User).ToList();
        }

        public static UserGroupRelationType GetUserGroupRelationType(
            this IQueryable<UserGroupRelation> userGroupRelations, int userId, int groupId)
        {
            return userGroupRelations.Where(x => x.GroupId == groupId && x.UserId == userId)
                .Select(x => x.UserGroupRelationType).FirstOrDefault();
        }

        public static Task<UserGroupRelation> GetUserGroupRelationAsync(
            this IQueryable<UserGroupRelation> userGroupRelations, int id)
        {
            return userGroupRelations.FirstOrDefaultAsync(x => x.Id == id);
        }

        public static Task<UserGroupRelation> GetUserGroupRelationAsync(
            this IQueryable<UserGroupRelation> userGroupRelations, int userId, int groupId)
        {
            return userGroupRelations.FirstOrDefaultAsync(x => x.GroupId == groupId && x.UserId == userId);
        }

        public static IQueryable<UserGroupRelation> GetParticipants(this IQueryable<UserGroupRelation> userGroupRelations, int groupId)
        {
            return userGroupRelations.Where(x => x.GroupId == groupId && x.UserGroupRelationType == UserGroupRelationType.Accepted);
        }

        public static IQueryable<UserGroupRelation> GetRequestsToGroup(this IQueryable<UserGroupRelation> userGroupRelations, int groupId)
        {
            return userGroupRelations.Where(x => (x.UserGroupRelationType == UserGroupRelationType.RequestedByUser)
                        && x.GroupId == groupId);
        }

        public static IQueryable<UserGroupRelation> GetRequests(this IQueryable<UserGroupRelation> userGroupRelations, int userId)
        {
            return
                userGroupRelations.Where(
                    x =>
                        (x.UserGroupRelationType == UserGroupRelationType.RequestedByUser ||
                        x.UserGroupRelationType == UserGroupRelationType.InvitedByAdministrator) && x.UserId == userId);
        }

        public static IQueryable<UserGroupRelation> GetRelationsInsideGroup(this IQueryable<UserGroupRelation> userGroupRelations, int groupId, int currentUserId)
        {
            var isAdmin = userGroupRelations.Any(x => x.GroupId == groupId && x.UserId == currentUserId && x.UserGroupRole == UserGroupRole.Administrator);

            var query = userGroupRelations.Where(x => x.GroupId == groupId && x.UserId != currentUserId);

            if (isAdmin)
                return query.Where(x => x.UserGroupRelationType == UserGroupRelationType.Accepted ||
                     x.UserGroupRelationType == UserGroupRelationType.InvitedByAdministrator ||
                     x.UserGroupRelationType == UserGroupRelationType.RequestedByUser);

            return query.Where(x => x.UserGroupRelationType == UserGroupRelationType.Accepted);
        }

        public static IQueryable<UserGroupRelationMaxDto> SelectUserGroupRelationMaxDto(
            this IQueryable<UserGroupRelation> query)
        {
            var q = query.AsExpandable();
            var fullNameExpr = UserQueries.GetFullNameExpr();

            return q.Select(x => new UserGroupRelationMaxDto
            {
                GroupId = x.GroupId,
                UserGroupRelationType = x.UserGroupRelationType,
                UserGroupRole = x.UserGroupRole,
                UserId = x.UserId,
                Id = x.Id,
                UserName = x.User.UserName,
                UserFullName = fullNameExpr.Invoke(x.User),
                UserAddress = x.User.CurrentMapAddressFormatted,
                UserLastTrack = x.User.CurrentLocationDateTime,
                GroupName = x.Group.Name,
                UpdaterName = x.UpdatedBy.UserName,
                UserAvatarUrl = "http://www.snappertech.com/images/prashant-surana.jpg"
            });
        }
    }
}
