﻿using System.Data.Entity.ModelConfiguration;
using TravelRecon.Domain.Identity;

namespace TravelRecon.Data.MapOverrides.Identity
{
    public class UserMap : EntityTypeConfiguration<User>
    {
        public UserMap() {
            Property(c => c.Language)
                .HasColumnName("LanguageTypeFk");
        }
    }
}
