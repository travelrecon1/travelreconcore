﻿using System.Data.Entity.ModelConfiguration;
using TravelRecon.Domain.Groups;

namespace TravelRecon.Data.MapOverrides.Groups
{
    class UserGroupRelationMap : EntityTypeConfiguration<UserGroupRelation>
    {
        public UserGroupRelationMap()
        {
            Property(c => c.GroupId).HasColumnName("GroupFk");
            Property(c => c.UserId).HasColumnName("UserFk");
            Property(c => c.UserGroupRelationType).HasColumnName("UserGroupRelationTypeFk");
            Property(c => c.UserGroupRole).HasColumnName("UserGroupRoleFk");
            Property(c => c.UpdatedById).HasColumnName("UpdatedByFk");

            HasRequired(x => x.UpdatedBy)
                .WithMany()
                .HasForeignKey(x => x.UpdatedById);
        }
    }
}
