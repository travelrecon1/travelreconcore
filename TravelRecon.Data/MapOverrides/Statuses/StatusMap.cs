﻿using System.Data.Entity.ModelConfiguration;
using TravelRecon.Domain.Statuses;

namespace TravelRecon.Data.MapOverrides.Statuses
{
    public class StatusMap : EntityTypeConfiguration<Status>
    {
        public StatusMap() {
            //todo: investigate why it is necessary here
            ToTable("Statuses");
            Property(c => c.Type)
                .HasColumnName("StatusTypeFk");
        }
    }
}
