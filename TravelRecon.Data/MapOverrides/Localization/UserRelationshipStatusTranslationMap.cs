﻿using System.Data.Entity.ModelConfiguration;
using TravelRecon.Domain.Localization;

namespace TravelRecon.Data.MapOverrides.Localization
{
    public class UserRelationshipStatusTranslationMap : EntityTypeConfiguration<UserRelationshipStatusTranslation>
    {
        public UserRelationshipStatusTranslationMap()
        {
            Property(c => c.Language)
                .HasColumnName("LanguageTypeFk");
        }
    }
}