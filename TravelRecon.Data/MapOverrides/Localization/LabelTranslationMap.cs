﻿using System.Data.Entity.ModelConfiguration;
using TravelRecon.Domain.Localization;

namespace TravelRecon.Data.MapOverrides.Localization
{
    public class LabelTranslationMap : EntityTypeConfiguration<LabelTranslation>
    {
        public LabelTranslationMap() {
            Property(c => c.Language)
                .HasColumnName("LanguageTypeFk");
        }
    }
}
