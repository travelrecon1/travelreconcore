using System.Data.Entity.ModelConfiguration;
using TravelRecon.Domain.Permissions;

namespace TravelRecon.Data.MapOverrides.Permissions
{
    public class SubscriptionLevelTypesPermissionMap : EntityTypeConfiguration<SubscriptionLevelTypesPermission>
    {
        public SubscriptionLevelTypesPermissionMap()
        {
            Property(x => x.SubscriptionlevelTypeId)
                .HasColumnName("SubscriptionlevelTypeIdFk");
        }
    }
}