﻿using System.Data.Entity.ModelConfiguration;
using TravelRecon.Domain.Alerts;

namespace TravelRecon.Data.MapOverrides.Alerts
{
    public class AlertMap : EntityTypeConfiguration<Alert>
    {
        public AlertMap() {
            Property(c => c.Source)
                .HasColumnName("AlertSourceTypeFk");
        }
    }
}
