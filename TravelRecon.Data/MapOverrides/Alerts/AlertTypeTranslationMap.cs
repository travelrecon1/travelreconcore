﻿using System.Data.Entity.ModelConfiguration;
using TravelRecon.Domain.Alerts;

namespace TravelRecon.Data.MapOverrides.Alerts
{
    public class AlertTypeTranslationMap : EntityTypeConfiguration<AlertTypeTranslation>
    {
        public AlertTypeTranslationMap() {
            Property(c => c.Language)
                .HasColumnName("LanguageTypeFk");
        }
    }
}
