﻿using System.Data.Entity.ModelConfiguration;
using TravelRecon.Domain.Alerts;

namespace TravelRecon.Data.MapOverrides.Alerts
{
    public class AlertTypeCategoryTranslationMap : EntityTypeConfiguration<AlertTypeCategoryTranslation>
    {
        public AlertTypeCategoryTranslationMap() {
            Property(c => c.Language)
                .HasColumnName("LanguageTypeFk");
        }
    }
}
