using System.Data.Entity.ModelConfiguration;
using TravelRecon.Domain.IntelReports;
using TravelRecon.Domain.Localization;

namespace TravelRecon.Data.MapOverrides.IntelReports
{
    public class IntelReportTypeTranslationMap  : EntityTypeConfiguration<IntelReportTypeTranslation>
    {
        public IntelReportTypeTranslationMap()
        {
            Property(c => c.Language)
                .HasColumnName("LanguageTypeFk");
        }
    }
}