﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TravelRecon.Domain;
using TravelRecon.Domain.RepositoryInterfaces;

namespace TravelRecon.Data.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class, IEntity
    {
        private DbContextTransaction dbContextTransaction;

        public Repository(TravelReconDbContext dbContext) {
            DbContext = dbContext;
        }

        public TEntity Get(int id) {
            return DbContext.Set<TEntity>().Find(id);
        }

        public IQueryable<TEntity> GetAll() {
            if (typeof(IDeletableEntity).IsAssignableFrom(typeof(TEntity)))
            {
                return ((IQueryable<IDeletableEntity>)DbContext.Set<TEntity>())
                    .Where(x => !x.IsDeleted)
                    .Cast<TEntity>();
            }

            return DbContext.Set<TEntity>();
        }

        public void AddRange(IList<TEntity> t)
        {
            DbContext.Set<TEntity>().AddRange(t);
        }

        public TEntity Add(TEntity t) {
            DbContext.Set<TEntity>().Add(t);
            return t;
        }

        public void DeleteRange(IList<TEntity> items)
        {
            if (typeof(IDeletableEntity).IsAssignableFrom(typeof(TEntity)))
            {
                foreach (var i in items)
                    Delete(i);
                return;
            }

            DbContext.Set<TEntity>().RemoveRange(items);
        }

        public void Delete(TEntity t) {
            var deletableEntity = t as IDeletableEntity;

            if (deletableEntity != null)
            {
                deletableEntity.IsDeleted = true;
                Modified(t);
                return;
            }

            DbContext.Set<TEntity>().Remove(t);
        }

        public void SaveChanges() {
            DbContext.SaveChanges();
        }

        public async Task SaveChangesAsync()
        {
            try
            {
                await DbContext.SaveChangesAsync();
            }
            catch (Exception)
            {
                dbContextTransaction?.Rollback();
                throw;
            }
        }


        public void Reload(TEntity t)
        {
            DbContext.Entry(t).Reload();
        }

        public void Modified(TEntity t)
        {
            DbContext.Entry(t).State = EntityState.Modified;

        }

        public void AddRange(IEnumerable<TEntity> t)
        {
            DbContext.Set<TEntity>().AddRange(t);
        }

        public IDisposable BeginTransaction()
        {
            dbContextTransaction = DbContext.Database.BeginTransaction();
            return dbContextTransaction;
        }

        public void CommitTransaction()
        {
            dbContextTransaction.Commit();
        }

        protected readonly TravelReconDbContext DbContext;
    }
}
