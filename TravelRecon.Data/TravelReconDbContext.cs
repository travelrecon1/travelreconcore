﻿using System;
//using System.Data.Entity;
//using System.Data.Entity.ModelConfiguration.Conventions;
//using System.Data.Entity.Validation;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using TravelRecon.Data.MapOverrides.Alerts;
using TravelRecon.Data.MapOverrides.Groups;
using TravelRecon.Data.MapOverrides.Identity;
using TravelRecon.Data.MapOverrides.IntelReports;
using TravelRecon.Data.MapOverrides.Localization;
using TravelRecon.Data.MapOverrides.Permissions;
using TravelRecon.Data.MapOverrides.Statuses;
using TravelRecon.Domain.Alerts;
using TravelRecon.Domain.ClientCompany;
using TravelRecon.Domain.Ecommerce;
using TravelRecon.Domain.Groups;
using TravelRecon.Domain.Identity;
using TravelRecon.Domain.Identity.UserRelationships;
using TravelRecon.Domain.Infrastructure;
using TravelRecon.Domain.IntelReports;
using TravelRecon.Domain.Itinerary;
using TravelRecon.Domain.License;
using TravelRecon.Domain.Locale;
using TravelRecon.Domain.Localization;
using TravelRecon.Domain.Missions;
using TravelRecon.Domain.Permissions;
using TravelRecon.Domain.Points;
using TravelRecon.Domain.Polls;
using TravelRecon.Domain.Risk;
using TravelRecon.Domain.Statuses;
using TravelRecon.Domain.SubscriptionOffers;
using TravelRecon.Domain.Tutorial;

namespace TravelRecon.Data
{
    public class TravelReconDbContext : DbContext
    {
        [Obsolete("For migrations")]
        public TravelReconDbContext(string connectionString)
            : base(connectionString)
        {
        }

        public TravelReconDbContext()
            : base("DefaultConnection") {
            Database.SetInitializer<TravelReconDbContext>(null);
        }

        // Add all domain objects here
        public DbSet<Address> Addresses { get; set; }
        public DbSet<Alert> Alerts { get; set; }
        public DbSet<AlertType> AlertTypes { get; set; }
        public DbSet<AnalystPushedGroup> AnalystPushedGroups { get; set; }
        public DbSet<AnalystPushedAlert> AnalystPushedAlerts { get; set; }
        public DbSet<AlertTypeTranslation> AlertTypeTranslations { get; set; }
        public DbSet<AlertTypeCategory> AlertCategoryTypes { get; set; }

        public DbSet<Label> Labels { get; set; }
        public DbSet<User> Users{ get; set; }
        public DbSet<UserRoles> UserRoles { get; set; }
        public DbSet<UserRoleType> UserRoleTypes { get; set; }
        public DbSet<UserRelationship> UserRelationship{ get; set; }
        public DbSet<UserRelationshipStatus> UserRelationshipStatus { get; set; }
        public DbSet<SubscriptionLevelTypesPermission> SubscriptionLevelTypesPermissions { get; set; }
        public DbSet<Destination> Destinations { get; set; }
        public DbSet<DestinationTranslation> DestinationTranslations { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<CountryTranslation> CountryTranslations { get; set; }
        public DbSet<IntelReport> IntelReports { get; set; }
        public DbSet<IntelReportType> IntelReportTypes { get; set; }
        public DbSet<IntelReportTypeTranslation> IntelReportTypeTranslations { get; set; }
        public DbSet<PushedAlert> PushedAlerts { get; set; }
        public DbSet<EmailAlert> EmailAlerts { get; set; }
        public DbSet<Status> Statuses { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<Point> Points { get; set; }
        public DbSet<UserGroupRelation> UserGroupRelations { get; set; }
        public DbSet<ViewedPage> ViewedPages { get; set; }
		public DbSet<UserDestinations> UserDestinations { get; set; }
		public DbSet<UserDestinationRequest> UserDestinationRequest { get; set; }
	    public DbSet<SubscriptionOffer> SubscriptionOffers { get; set; }
	    public DbSet<Rank> Ranks { get; set; }
        public DbSet<UserEmailInvite> UserEmailInvite { get; set; }
        public DbSet<StripePayment> StripePayments { get; set; }
        public DbSet<UserSubscription> UserSubscriptions { get; set; }
        public DbSet<AnalystAlertCategory> AnalystAlertCategories { get; set; }
        public DbSet<AnalystAlertType> AnalystAlertTypes { get; set; }
        public DbSet<AlertsThank> AlertsThanks { get; set; }
        public DbSet<AlertsValidation> AlertsValidations { get; set; }
        public DbSet<ReadCommunication> ReadCommunications { get; set; }

        public DbSet<Mission> Missions { get; set; }
        public DbSet<AlertSource> AlertSources { get; set; }
        public DbSet<Source> Sources { get; set; }

        public DbSet<RiskRatingHistory> RiskRatingHistory { get; set; }
        public DbSet<AnalystAlertPriority> AnalystAlertPriorities { get; set; }
        public DbSet<AnalystsUser> AnalystsUser { get; set; }
        public DbSet<AnalystsCountry> AnalystsCountry { get; set; }
        public DbSet<AnalystsDestination> AnalystsDestination { get; set; }
        public DbSet<Infrastructure> Infrastructures { get; set; }
        public DbSet<InfrastructureType> InfrastructureTypes { get; set; }
        public DbSet<License> Licenses { get; set; }
        public DbSet<UserLicense> UserLicenses { get; set; }

        public DbSet<ClientCompany> ClientCompanies { get; set; }
        public DbSet<UserLocationHistory> UserLocationHistories { get; set; }
        public DbSet<Itinerary> Itineraries { get; set; }
        public DbSet<UserDetails> UserDetailses { get; set; }
        public DbSet<InfrastructureTypeField> InfrastructureTypeFields { get; set; }
        public DbSet<InfrastructureFieldValue> InfrastructureTypeFieldvalues { get; set; }

        public DbSet<Poll> Polls { get; set; }
        public DbSet<PollAnswer> PollAnswers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder) {
            AddConventionsTo(modelBuilder);
            AddMapOverridesTo(modelBuilder);
        }

        private void AddConventionsTo(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

            modelBuilder.Entity<Destination>().Property(e => e.Cost).HasPrecision(19, 5);
            modelBuilder.Entity<StripePayment>().Property(e => e.AmountPaid).HasPrecision(19, 5);
            modelBuilder.Entity<SubscriptionOffer>().Property(e => e.Price).HasPrecision(19, 5);

            modelBuilder.Entity<IntelReport>().Property(e => e.ModifiedDate).HasColumnType("datetime2");
            modelBuilder.Entity<IntelReport>().Property(e => e.CreatedDate).HasColumnType("datetime2");
            modelBuilder.Entity<UserDestinations>().Property(e => e.CreatedDate).HasColumnType("datetime2");
            modelBuilder.Entity<UserDestinations>().Property(e => e.ModifiedDate).HasColumnType("datetime2");
        }

        private void AddMapOverridesTo(DbModelBuilder modelBuilder) {
            modelBuilder.Configurations.Add(new AlertMap());
            modelBuilder.Configurations.Add(new AlertTypeCategoryTranslationMap());
            modelBuilder.Configurations.Add(new AlertTypeTranslationMap());
            modelBuilder.Configurations.Add(new LabelTranslationMap());
            modelBuilder.Configurations.Add(new UserMap());
            modelBuilder.Configurations.Add(new UserRelationshipStatusTranslationMap());
            modelBuilder.Configurations.Add(new SubscriptionLevelTypesPermissionMap());
            modelBuilder.Configurations.Add(new IntelReportTypeTranslationMap());
            modelBuilder.Configurations.Add(new StatusMap());
            modelBuilder.Configurations.Add(new UserGroupRelationMap());
        }

        public static TravelReconDbContext Create() {
            return new TravelReconDbContext();
        }
        public override int SaveChanges()
        {
            try
            {
                return base.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                string seperator = "; " + Environment.NewLine;
                var fullErrorMessage = string.Join(seperator, errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
        }
    }
}
