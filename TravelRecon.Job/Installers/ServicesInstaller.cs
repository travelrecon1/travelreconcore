﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using TravelRecon.Common.DI;
using TravelRecon.Job.Jobs;
using TravelRecon.Logger;

namespace TravelRecon.Job.Installers
{
    public class ServicesInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Component.For<TravelReconDbContext>()
                    // When used by Hangfire, needs to be available as transient dependency
                    .LifestyleTransient());

            container.Register(
                Component.For<ILanguageTranslator>()
                    .ImplementedBy<GoogleTranslateService>()
                    // When used by Hangfire, needs to be available as transient dependency
                    .LifestyleTransient());

            container.Register(
               Component.For<IPushService>()
                   .ImplementedBy<PushService>()
                   // When used by Hangfire, needs to be available as transient dependency
                   .LifestyleTransient());

            container.Register(
               Component.For<IAndroidPushService>()
                   .ImplementedBy<GooglePushService>()
                   // When used by Hangfire, needs to be available as transient dependency
                   .LifestyleTransient());

            container.Register(
               Component.For<IIosPushService>()
                   .ImplementedBy<IosPushService>()
                   // When used by Hangfire, needs to be available as transient dependency
                   .LifestyleTransient());

            container.Register(
               Component.For<RiskService>()
                   .ImplementedBy<RiskService>()
                   // When used by Hangfire, needs to be available as transient dependency
                   .LifestyleTransient());

            container.Register(
                Component.For<DestinationAppService>()
                    .ImplementedBy<DestinationAppService>()
                    // When used by Hangfire, needs to be available as transient dependency
                    .LifestyleTransient());

            container.Register(
                Component.For<MailService>()
                    .ImplementedBy<MailService>()
                    // When used by Hangfire, needs to be available as transient dependency
                    .LifestyleTransient());

            container.Register(
                Component.For<AppSettingsService>()
                    .ImplementedBy<AppSettingsService>()
                    // When used by Hangfire, needs to be available as transient dependency
                    .LifestyleTransient());

            container.Register(
                Component.For(typeof (IRepository<>))
                    .ImplementedBy(typeof (Repository<>))
                    // When used by Hangfire, needs to be available as transient dependency
                    .LifestyleTransient());

            container.Register(Component.For<ILogger>()
                .UsingFactoryMethod(x => new Logger.Logger())
                .LifestyleTransient());

            container.Register(
             Component.For<IComponentResolver>()
                 .ImplementedBy<WindsorComponentResolver>()
                 // When used by Hangfire, needs to be available as transient dependency
                 .LifestyleTransient());

            container.Register(Classes.FromAssemblyContaining<EmailJob>()
              .BasedOn<BaseJob>()
              .WithService.Self()
              .LifestyleTransient());
            container.Register(Classes.FromAssemblyContaining<NotificationJob>()
               .BasedOn<BaseJob>()
               .WithService.Self()
               .LifestyleTransient());
            container.Register(Classes.FromAssemblyContaining<InvalidateEmergenciesJob>()
                .BasedOn<BaseJob>()
                .WithService.Self()
                .LifestyleTransient());
        }
    }
}


