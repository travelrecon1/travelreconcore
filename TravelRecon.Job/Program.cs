﻿using System;
using Topshelf;

namespace TravelRecon.Job
{
    class Program
    {
        static void Main(string[] args)
        {
            HostFactory.Run(x =>
            {
                x.Service<Application>(s =>
                {
                    s.ConstructUsing(name => new Application());
                    s.WhenStarted(tc => tc.Start());
                    s.WhenStopped(tc => tc.Stop());
                });
                x.RunAsLocalSystem();

                x.SetDescription("TravelRecon Hangfire Windows Service");
                x.SetServiceName("TravelRecon-hangfire-jobs");
            });
        }

        private class Application
        {
            private IDisposable _host;

            public void Start()
            {
                string endPoint = ConfigurationManager.AppSettings["baseAddress"];
                _host = WebApp.Start<Startup>(endPoint);

                Console.WriteLine("Hangfire Server started.");
                Console.WriteLine("Dashboard is available at {0}/hangfire", endPoint);
            }

            public void Stop()
            {
                _host.Dispose();
            }
        }
    }

}
