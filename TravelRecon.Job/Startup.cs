﻿using System;
using System.Configuration;
//using System.Web.Http;
using Castle.Windsor;
using Castle.Windsor.Installer;
using Hangfire;
using Hangfire.SqlServer;
using Hangfire.Windsor;
using Microsoft.Owin;
using Owin;
using TravelRecon.Common.DI;
using TravelRecon.Job.Jobs;

[assembly: OwinStartup(typeof(TravelRecon.Job.Startup))]

namespace TravelRecon.Job
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {

            HttpConfiguration config = new HttpConfiguration();
            app.UseWebApi(config);

            string connectionString = ConfigurationManager.ConnectionStrings["HangfireConnection"].ConnectionString;

            GlobalConfiguration.Configuration
                .UseSqlServerStorage(connectionString,
                    new SqlServerStorageOptions { QueuePollInterval = TimeSpan.FromSeconds(1) });

            var container = new WindsorContainer().Install(FromAssembly.This());
            ServiceLocator.SetResolver(new WindsorComponentResolver(container));

            JobActivator.Current = new WindsorJobActivator(container.Kernel);

            app.UseHangfireDashboard();
            app.UseHangfireServer();

            RecurringJob.AddOrUpdate<InvalidateEmergenciesJob>(job =>job.StartAsync(JobCancellationToken.Null), "0 * * * *");
            RecurringJob.AddOrUpdate<EmailJob>(job => job.StartAsync(JobCancellationToken.Null), "*/1 * * * *");

            BackgroundJob.Enqueue<ComputeRiskFactorJob>(job => job.StartAsync(JobCancellationToken.Null));
            BackgroundJob.Enqueue<NotificationJob>(job => job.Start(JobCancellationToken.Null));
        }
    }
}
