﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Hangfire;
using TravelRecon.Data;
using TravelRecon.Domain.Alerts;
using TravelRecon.Logger;
using Z.EntityFramework.Plus;

namespace TravelRecon.Job.Jobs
{
    public class InvalidateEmergenciesJob : BaseJob
    {
        private readonly TravelReconDbContext _travelReconDbContext;
        private const int EmergencyLiveTimeInMinutes = 30;

        public InvalidateEmergenciesJob(ILogger logger, TravelReconDbContext travelReconDbContext) : base(logger)
        {
            _travelReconDbContext = travelReconDbContext;
        }

        public override async Task Start(IJobCancellationToken cancellationToken)
        {
            Logger.Debug("Invalidate emergencies job start invoked");

            try
            {
                await ProcessEmergencies(_travelReconDbContext);
                Console.WriteLine("Invalidate emergencies job start invoked");
            }
            catch (Exception e)
            {
                Logger.Error(e);
            }
        }
        
        private async Task ProcessEmergencies(TravelReconDbContext context)
        {
            var now = DateTime.UtcNow;
            await context.Alerts
                .Where(m => m.IsEmergency && DbFunctions.DiffMinutes(m.ReportedOn, now) >= EmergencyLiveTimeInMinutes)
                .UpdateAsync(m => new Alert { IsEmergency = false });

            await context.SaveChangesAsync();
        }
    }
}
