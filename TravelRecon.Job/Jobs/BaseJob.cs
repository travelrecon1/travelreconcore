﻿using System.Threading.Tasks;
using Hangfire;
using TravelRecon.Logger;

namespace TravelRecon.Job.Jobs
{
    public abstract class BaseJob : IJob
    {
        protected ILogger Logger;

        protected BaseJob(ILogger logger)
        {
            Logger = logger;
        }

        public abstract Task Start(IJobCancellationToken cancellationToken);

        public void StartAsync(IJobCancellationToken cancellationToken)
        {
            Start(cancellationToken).Wait();
        }
    }
}
