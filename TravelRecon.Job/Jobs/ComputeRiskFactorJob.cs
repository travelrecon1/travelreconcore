﻿using System;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using Hangfire;
using LinqKit;
using TravelRecon.AppServices.Risks;
using TravelRecon.Common.DI;
using TravelRecon.Data;
using TravelRecon.Domain.Alerts;
using TravelRecon.Domain.Identity;
using TravelRecon.Domain.IntelReports;
using TravelRecon.Logger;

namespace TravelRecon.Job.Jobs
{
    public class ComputeRiskFactorJob : BaseJob
    {
        private readonly RiskService _riskService;
        private const int BATCH_SIZE = 100;
        private IJobCancellationToken _cancellationTokenSource;
        private Task _job;

        public ComputeRiskFactorJob(ILogger logger, RiskService riskService) : base(logger)
        {
            _riskService = riskService;
        }

        [DisableConcurrentExecution(1000)]
        public override Task Start(IJobCancellationToken cancellationToken)
        {
            _cancellationTokenSource = cancellationToken;

            Logger.Debug("ComputeRiskFactor job start invoked");

            return _job = Task.Run(() =>
            {
                Logger.Debug("ComputeRiskFactor job started");

                while (true)
                {
                    try
                    {
                        _cancellationTokenSource.ThrowIfCancellationRequested();

                        TravelReconDbContext dbContext = null;

                        try
                        {
                            dbContext = ServiceLocator.Resolve<TravelReconDbContext>();
                            UpdateUserComputedRiskFactor(dbContext);
                            dbContext.SaveChanges();
                        }
                        catch (Exception e)
                        {
                            Logger.Error(e);
                        }
                        finally
                        {
                            ServiceLocator.Release(dbContext);
                        }

                        Thread.Sleep(5000);
                    }
                    catch (OperationCanceledException)
                    {
                        Logger.Debug("ComputeRiskFactor job deleted");
                        return;
                    }

                }
            }, _cancellationTokenSource.ShutdownToken);
        }

        public Task Stop()
        {
            return _job;
        }

        private async void UpdateUserComputedRiskFactor(TravelReconDbContext dbContext)
        {
            var now = DateTime.UtcNow;

            Expression<Func<User, Alert>> nearByAlertExpr = (u) =>
                dbContext.Alerts
                    .AsExpandable()
                    .Where(a =>
                        a.ShouldShowOnMap &&
                        !a.IsRejected &&
                        !a.Workflow.HasValue &&
                        ((a.ExpiresOn.HasValue && a.ExpiresOn.Value > now)
                            || (!a.AlertType.ExpirationTime.HasValue || DbFunctions.AddHours(a.ReportedOn, a.AlertType.ExpirationTime) > now))
                    )
                    .OrderBy(a => u.CurrentLocation.Distance(a.Location))
                    .FirstOrDefault();

            Expression<Func<Destination, double?>> destinationFunctionExpr = (d) =>
                    d == null ? null : 
                    d.RiskFactor ?? (double?)d.Country.RiskFactor;

            Expression<Func<User, Alert, double?>> functionExpr = (u, a) =>
                a == null
                    ? null
                    : (
                        a.AlertType.AffectProximity == null 
                        || a.AlertType.AffectProximity == 0 
                        || a.AlertType.Severity == null 
                        || a.AlertType.Severity == 0 
                        || a.ImpactRating == null
                        || a.ImpactRating == 0
                        ? destinationFunctionExpr.Invoke(a.Destination)
                        : u.CurrentLocation.Distance(a.Location)
                          /a.AlertType.AffectProximity
                          * a.AlertType.Severity
                          *a.ImpactRating
                          + destinationFunctionExpr.Invoke(a.Destination));

            var previousCompute = DateTime.UtcNow.AddDays(-1);

            var users = dbContext.Users
                .Where(x => x.ComputedRiskFactorDate == null || x.ComputedRiskFactorDate < previousCompute)
                .Take(BATCH_SIZE)
                .ToList();

            var userIds = users.Select(x => x.Id);

            var computedValues = dbContext.Users
                .Where(x => userIds.Contains(x.Id))
                .AsExpandable()
                .Select(x => new
                {
                    Id = x.Id,
                    ComputedRiskFactor = functionExpr.Invoke(x, nearByAlertExpr.Invoke(x))
                }).ToDictionary(x => x.Id, x => x.ComputedRiskFactor);

            foreach (var user in users)
            {
                //user.ComputedRiskFactor = computedValues[user.Id];
                user.ComputedRiskFactor = await _riskService.GetUserRisk(user.Id);
                user.ComputedRiskFactorDate = DateTime.UtcNow;
            }
        }
    }
}
