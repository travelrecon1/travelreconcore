﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using Hangfire;
using TravelRecon.AppServices.Alerts;
using TravelRecon.Data;
using TravelRecon.Domain.Alerts;
using TravelRecon.Domain.Identity;
using TravelRecon.Domain.Localization;
using TravelRecon.Logger;

namespace TravelRecon.Job.Jobs
{
    public class EmailJob : BaseJob
    {
        private readonly TravelReconDbContext _travelReconDbContext;
        private readonly ILanguageTranslator _languageTranslator;

        private const int MaxUsers = 100;
        private const int MaxAlerts = 10;

        public EmailJob(ILogger logger,TravelReconDbContext travelReconDbContext, ILanguageTranslator languageTranslator):base(logger)
        {
            _travelReconDbContext = travelReconDbContext;
            _languageTranslator = languageTranslator;
        }

        public override async Task Start(IJobCancellationToken cancellationToken)
        {
            Logger.Debug("Email job start invoked");

            try
            {
                await GetEmergencyAlertsNotificatons(_travelReconDbContext, _languageTranslator);
                Console.WriteLine("Email job start invoked");
            }
            catch (Exception e)
            {
                Logger.Error(e);
            }

        }

        private async Task GetEmergencyAlertsNotificatons(TravelReconDbContext dbContext, ILanguageTranslator languageTranslator)
        {
            try
            {
                var now = DateTime.UtcNow;
                var localizedLabelsDictionary = await GetLocalizedLabels(dbContext);

                var query = dbContext.Users.Where(x =>
                    x.EmergencyEmail != null &&
                    x.UserRoles.Any(role => role.RoleId == (int) UserRoleTypeEnum.TravelRecon ||
                                            role.RoleId == (int)UserRoleTypeEnum.TravelReconPro));


                //get users with alerts to push
                var usersWithAlertsToPush =await query
                    .Select(x => new
                    {
                        x.Id,
                        x.EmergencyEmail,
                        x.FirstName,
                        Language = x.Language ?? LanguageType.English,
                        Location = x.CurrentLocation,
                        AlertsToPush = dbContext.Alerts.Where(a => a.ShouldPushToUsers
                                                                   &&
                                                                   ((a.ExpiresOn.HasValue && a.ExpiresOn.Value >= now) ||
                                                                   (!a.AlertType.ExpirationTime.HasValue ||
                                                                    DbFunctions.AddHours(a.ReportedOn,
                                                                        a.AlertType.ExpirationTime) >= now))
                                                                   &&
                                                                   a.Location.Distance(x.CurrentLocation) <=
                                                                   ManageAlertsService
                                                                       .RadiusFromCurrentLocationForPullingAlertUpdates
                                                                   && x.EmailAlerts.All(pa => pa.AlertFk != a.Id)
                                                                   && a.IsEmergency)
                            .Select(a => new
                            {
                                a.Id,
                                a.AddressFormatted,
                                AlertTypeName =
                                    a.AlertType.Translations.Where(
                                        t => t.Language == (x.Language ?? LanguageType.English))
                                        .Select(t => t.Name)
                                        .FirstOrDefault(),
                                AlertTypeCategoryName =
                                    a.AlertType.Category.Translations.Where(
                                        t => t.Language == (x.Language ?? LanguageType.English))
                                        .Select(t => t.Name)
                                        .FirstOrDefault(),
                                ReporterName = a.ReportedBy.UserName,
                                a.Description,
                                a.ReportedOn
                            })
                            .Take(MaxAlerts)
                            .ToList()
                    })
                    .Take(MaxUsers)
                    .ToListAsync();

                foreach (var user in usersWithAlertsToPush)
                {
                    foreach (var alert in user.AlertsToPush)
                    {
                        var title =
                            string.Format(
                                localizedLabelsDictionary[LabelType.TravelReconNewEmergencyAlert][user.Language],
                                alert.ReporterName);
                        var flag = TryToSendEmail(
                            user.EmergencyEmail,
                            user.FirstName,
                            title,
                            alert.Description, //todo should be translated
                            alert.AddressFormatted,
                            alert.AlertTypeName,
                            alert.ReportedOn,
                            localizedLabelsDictionary[LabelType.Location][user.Language],
                            localizedLabelsDictionary[LabelType.AlertType][user.Language],
                            localizedLabelsDictionary[LabelType.Date][user.Language],
                            localizedLabelsDictionary[LabelType.Time][user.Language],
                            localizedLabelsDictionary[LabelType.Description][user.Language]);

                        if (flag)
                        {
                            dbContext.EmailAlerts.Add(new EmailAlert
                            {
                                UserFk = user.Id,
                                AlertFk = alert.Id
                            });
                        }
                    }
                }

                await dbContext.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private async Task<Dictionary<LabelType, Dictionary<LanguageType, string>>> GetLocalizedLabels(TravelReconDbContext dbContext)
        {
            var labelsToRetrive = new[] { (int)LabelType.TravelReconNewEmergencyAlert, (int)LabelType.Location, (int)LabelType.Date, (int)LabelType.Time, (int)LabelType.Description, (int)LabelType.AlertType };

            var localizedLabels =await dbContext.Labels.Where(
                x => labelsToRetrive.Contains(x.Id))
                .Include(x => x.Translations)
                .Select(x => new
                {
                    Id = (LabelType)x.Id,
                    Translations = x.Translations.Select(t => new { t.Language, t.Content }).ToList()
                }).ToListAsync();

            return localizedLabels.Select(x => new
            {
                x.Id,
                Translations = x.Translations.ToDictionary(t => t.Language, t => t.Content)
            }).ToDictionary(x => x.Id, x => x.Translations);
        }

        private bool TryToSendEmail(
            string to,
            string userName,
            string title,
            string description,
            string location,
            string alertType,
            DateTime reportedOn,
            string locationLabel,
            string alertTypeLabel,
            string dateLabel,
            string timeLabel,
            string descriptionLabel)
        {
            var message = new MailMessage();
            var client = new SmtpClient();

            message.To.Add(new MailAddress(to));
            message.Subject = title;
            message.Body = $"Hi, {userName} <br/><br/>" +
                           $"{locationLabel}: {location} <br/>" +
                           $"{alertTypeLabel}: {alertType} <br/>" +
                           $"{dateLabel}: {reportedOn:d MMM yyyy} <br/>" +
                           $"{timeLabel}: {reportedOn:H: mm} GMT <br/>" +
                           $"{descriptionLabel}: {description}";

            message.IsBodyHtml = true;

            try
            {
                client.Send(message);
            }
            catch (Exception exception)
            {
                return false;
            }

            return true;

        }
    }
}
