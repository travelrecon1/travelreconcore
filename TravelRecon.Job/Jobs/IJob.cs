﻿using System.Threading.Tasks;
using Hangfire;

namespace TravelRecon.Job.Jobs
{
    public interface IJob
    {
        Task Start(IJobCancellationToken cancellationToken);
    }
}
