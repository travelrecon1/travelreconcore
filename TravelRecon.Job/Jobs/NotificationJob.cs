﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Hangfire;
using TravelRecon.AppServices.Alerts;
using TravelRecon.AppServices.Push;
using TravelRecon.AppServices.Push.Dtos;
using TravelRecon.Common.DI;
using TravelRecon.Data;
using TravelRecon.Domain.Alerts;
using TravelRecon.Domain.Identity;
using TravelRecon.Domain.Localization;
using TravelRecon.Logger;

namespace TravelRecon.Job.Jobs
{
    public class NotificationJob : BaseJob
    {
        private const int MAX_USERS = 100;
        private const int MAX_ALERTS = 10;

        private IJobCancellationToken _cancellationTokenSource;
        private Task _job;

        public NotificationJob(ILogger logger) : base(logger)
        {
        }

        [DisableConcurrentExecution(1000)]
        public override Task Start(IJobCancellationToken cancellationToken)
        {
            _cancellationTokenSource = cancellationToken;

            Logger.Debug("Notification job start invoked");

            return _job = Task.Run(() =>
            {
                Logger.Debug("Notification job started");

                while (true)
                {
                    try
                    {
                        _cancellationTokenSource.ThrowIfCancellationRequested();

                        IPushService pushService = null;
                        ILanguageTranslator languageTranslator = null;
                        TravelReconDbContext dbContext = null;

                        try
                        {
                            dbContext = ServiceLocator.Resolve<TravelReconDbContext>();
                            pushService = ServiceLocator.Resolve<IPushService>();
                            languageTranslator = ServiceLocator.Resolve<ILanguageTranslator>();

                            var pushMessages = GetPushAlertsNotificatons(dbContext, languageTranslator).ToList();

                            pushService.Push(pushMessages);

                            Logger.Debug("Pushed " + pushMessages.Count + " messages");
                            Console.WriteLine("Notification job start invoked");
                            Console.WriteLine($"Pushed {pushMessages.Count} messages");
                           
                            dbContext.SaveChanges();
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e);
                            Logger.Error(e);
                        }
                        finally
                        {
                            ServiceLocator.Release(dbContext);
                            ServiceLocator.Release(pushService);
                            ServiceLocator.Release(languageTranslator);
                        }

                        Thread.Sleep(50000);
                    }
                    catch (OperationCanceledException)
                    {
                        Logger.Debug("Notification job deleted");
                        return;
                    }

                }
            }, _cancellationTokenSource.ShutdownToken);
        }

        public Task Stop()
        {
            return _job;
        }

        private IEnumerable<PushMessageDto> GetPushAlertsNotificatons(TravelReconDbContext dbContext, ILanguageTranslator languageTranslator)
        {
            var now = DateTime.UtcNow;
            var resultList = new List<PushMessageDto>();

            var availableDestinations = dbContext.UserDestinations.Where(x => x.IsAvailable && x.ReceiveNotifications);
            var labelsToRetrive = new[] { (int)LabelType.TravelReconNewAlert, (int)LabelType.TravelReconNewEmergencyAlert };
            var localizedLabels = dbContext.Labels.Where(x => labelsToRetrive.Contains(x.Id))
                .Include(x => x.Translations).Select(x => new
                {
                    Id = (LabelType)x.Id,
                    Translations = x.Translations
                        .Select(t => new
                        {
                            t.Language,
                            t.Content
                        })
                        .ToList()
                }).ToList();

            var localizedLabelsDictionary = localizedLabels.Select(x => new
            {
                x.Id,
                Translations = x.Translations.ToDictionary(t => t.Language, t => t.Content)
            }).ToDictionary(x => x.Id, x => x.Translations);

            var query = dbContext.Users.Where(x => x.ShowAlertPopups &&
            x.UserRoles.Any(role => role.RoleId == (int)UserRoleTypeEnum.TravelRecon ||
                role.RoleId == (int)UserRoleTypeEnum.TravelReconPro ||
                role.RoleId == (int)UserRoleTypeEnum.Administrator));


            //get users with alerts to push
            var usersWithAlertsToPush = query
                .Select(x => new
                {
                    x.Id,
                    Language = x.Language ?? LanguageType.English,
                    x.AlertPopupsShowIntervalMinutes,
                    x.MaxAlertPopups,
                    x.Devices,
                    x.UserRoles,
                    Location = x.CurrentLocation,
                    AlertsToPush = dbContext.Alerts.Where(a => a.ShouldPushToUsers
                        && ((a.ExpiresOn.HasValue && a.ExpiresOn >= now) 
                            || (!a.AlertType.ExpirationTime.HasValue || DbFunctions.AddHours(a.ReportedOn, a.AlertType.ExpirationTime) >= now))
                        &&
                        ((a.Location.Distance(x.CurrentLocation) <= ManageAlertsService.RadiusFromCurrentLocationForPullingAlertUpdates )
                        ||
                        (availableDestinations.Any(m => m.UserFk == x.Id && m.DestinationId == a.DestinationId) ))

                        && x.PushedAlerts.All(pa => pa.AlertFk != a.Id))
                        .Select(a => new
                        {
                            a.Id,
                            a.AddressFormatted,
                            AlertTypeName = a.AlertType.Translations.Where(t => t.Language == (x.Language ?? LanguageType.English)).Select(t => t.Name).FirstOrDefault(),
                            AlertTypeCategoryName = a.AlertType.Category.Translations.Where(t => t.Language == (x.Language ?? LanguageType.English)).Select(t => t.Name).FirstOrDefault(),
                            ReporterUserName = a.ReportedBy.UserName,
                            ReporterName = a.ReportedBy.FirstName + " " + a.ReportedBy.LastName,
                            IsEmergency = a.IsEmergency,
                            a.Description,
                            a.ReportedOn,
                            a.DestinationId,
                            a.Title
                        })
                        .Take(MAX_ALERTS)
                        .ToList(),
                    CountPushed = x.PushedAlerts.Count(a => a.WhenPushed > DbFunctions.AddMinutes(now, -x.AlertPopupsShowIntervalMinutes))
                })
                .Where(x => x.AlertsToPush.Any() && x.CountPushed < x.MaxAlertPopups)
                .Take(MAX_USERS)
                .ToList();

            foreach (var user in usersWithAlertsToPush)
            {
                var devices = PushService.GetDevices(user.Devices).ToList();
                if (!devices.Any())
                    continue;

                var pushedAlerts = new List<PushedAlert>();

                foreach (var alert in user.AlertsToPush.Take(user.MaxAlertPopups))
                {
                    string title;
                    string message;

                    if (alert.IsEmergency)
                    {
                        title = string.IsNullOrWhiteSpace(alert.ReporterName) ? alert.ReporterUserName : alert.ReporterName;
                        message = $"{localizedLabelsDictionary[LabelType.Emergency][user.Language]}: {alert.AddressFormatted}";
                    }
                    else
                    {
                        title = string.IsNullOrWhiteSpace(alert.Title) ? alert.AlertTypeCategoryName : alert.Title;
                        message = $"{alert.AlertTypeName}: {alert.AddressFormatted}";
                    }

                    foreach (var dev in devices)
                    {
                        resultList.Add(new PushMessageDto
                        {
                            Device = dev,
                            Data = new AlertNotificationDto
                            {
                                Title = title,
                                Body = "",
                                Alert = message,
                                AlertId = alert.Id,
                                Description = alert.Description /* languageTranslator.Translate(alert.Description, usersByLang.Key) */, //TODO CHANGE LATER!!!
                                AddressFormatted = alert.AddressFormatted,
                                AlertTypeCategoryName = alert.AlertTypeCategoryName,
                                AlertTypeName = alert.AlertTypeName,
                                ReportedOnDateFormatted = $"{alert.ReportedOn:d MMM yyyy}",
                                ReportedOnTimeFormatted = $"{alert.ReportedOn:H:mm}" + " GMT"
                            }
                        });
                    }

                    pushedAlerts.Add(new PushedAlert
                    {
                        UserFk = user.Id,
                        AlertFk = alert.Id
                    });

                }

                if (pushedAlerts.Any())
                    dbContext.PushedAlerts.AddRange(pushedAlerts);
            }

            return resultList;
        }
    }
}
